# Contributors

 - **Etienne Boespflug**: etienne.boespflug@gmail.com
 - **Marie-Laure Potet**: marie-laure.potet@univ-grenoble-alpes.fr
 - **Laurent Mounier**: laurent.mounier@univ-grenoble-alpes.fr

# Previous Contributors

 - **Guilhem Lacombe**: guilhem.lacombe.mpsi@gmail.com
 - **Cristian Ene**: cristian.ene@univ-grenoble-alpes.fr
 - **Abderrahmane Bouguern**.

# Older versions contributors

- **Sahar Bello**.
- **Guillaume Petiot**.
- **Maxime Puys**.
- **Valentin Touzeau**.
- **Louis Dureuil**.
