# Lazart


![](https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/uploads/133cfd5d596bd2a8af8437780a243811/lazart-logo-red.png){width="386" height="193"}

**Lazart** is an [**LLVM**](https://llvm.org/)-level robustness evaluation framework targeting multi-faults injection. It relies on **Dynamic-Symbolic Execution (DSE)** using [KLEE](https://klee.github.io/), to explore the attack paths depending on user-provided [**attack model**](Attack-Model) and [**attack objective**](Core/Attack-objective).

**Lazart** aims to assist developers and evaluators to analyze LLVM programs and countermeasures in context of physical attacks.

## Installation

**Lazart** can be used with docker, avoiding to install all dependencies on the host machine. Please consults the [dedicated guide](https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/Environment/Install-Lazart). 

## Resources and guides

**Lazart** is distributed with documentation and tutorials. The documentation index can be found [here](doc/index.md).

The following is a non exhaustive list of useful links:

 - [Wiki home page](https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/home).
 - [Install Lazart:](https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/Environement/Install-Lazart) how to install Lazart using docker.
 - [Tutorial 1:](https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/Overview/Tutorial-1:-Get-started-with-verify_pin-analysis) Get started with `verify_pin` analysis.
 - [Tutorial 2:](https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/Overview/Tutorial-2:-Symbolic-Inputs,-Equivalence-and-Redundancy) Symbolic Inputs, equivalence and redundancy.
 - [Tutorial 3:](https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/Overview/Tutorial-3:-Data-Load-and-model-combination-on-memcmps) Data Load and modelcombination on memcmps.
 - [Analysis page:](https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/Analysis) Description of analysis parameters and steps.
 - [Attack Model:](https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/Attack-Model) Attack model and fault models presentation. 
 - [Frequently Asked Questions](https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/Overview/Frequently-Asked-Questions-(FAQ))

## Contributors

Current contributors:
 - **Etienne Boespflug**: etienne.boespflug@univ-grenoble-alpes.fr
 - **Marie-Laure Potet**: marie-laure.potet@univ-grenoble-alpes.fr
 - **Laurent Mounier**: laurent.mounier@univ-grenoble-alpes.fr

Please find the complete contribution list [here](CONTRIBUTORS.md).

## Licence

**Lazart** is a tool of VERIMAG laboratory. The licence is available [here](LICENCE.md). 

The tool is [registered](https://lazart.gricad-pages.univ-grenoble-alpes.fr/home/APP/S293_Lazart_IDDN_Certificate.pdf) at Agence pour la Protection des Programmes (APP).


## Publication about Lazart

**2023**

* E. Boespflug, L. Mounier, M-L. Potet, A. Bouguern, "A compositional methodology to harden programs against multi-fault attacks" - _2023 Workshop on Fault Detection and Tolerance in Cryptography (FDTC)_, \[[PDF](https://hal.science/hal-04231496/document)\] \[[Slides](https://fdtc.deib.polimi.it/FDTC23/slides/FDTC2023-slides-1-3.pdf)\]
* E.Boespflug, "Tools for code and countermeasures analysis against multiple faults attacks" - _2023 PhD thesis (French),_ \[[PDF (FR)](https://github.com/EBoespflug/phd-thesis-manuscript-fr/blob/main/phd-thesis-manuscript-fr.pdf)\]
* G. Lacombe, D. Feliot, E. Boespflug and  M-L. Potet, "Combining Static Analysis and Dynamic Symbolic Execution in a Toolchain to detect Fault Injection Vulnerabilities" - _2023_ _Journal of Cryptographic Engineering (JCE)_, pp. 1-18, \[[PDF](https://arxiv.org/pdf/2303.03999.pdf)\]

**2021**

* G. Lacombe, D. Feliot, E. Boespflug and  M-L. Potet, "Combining Static Analysis and Dynamic Symbolic Execution in a Toolchain to detect Fault Injection Vulnerabilities" - _2021 PROOF Workshop_, \[[PDF](https://www.proofs-workshop.org/2021/papers/paper2.pdf)\]

**2020**

* E. Boespflug, C. Ene, L. Mounier and  M-L. Potet, "Countermeasures Optimization in Multiple Fault-Injection Context" - _2020 Workshop on Fault Detection and Tolerance in Cryptography (FDTC),_ pp. 26-34, doi: 10.1109/FDTC51366.2020.00011. \[[Slides](https://hal.science/hal-02951150/document)\]

**2014**

* M-L. Potet, L. Mounier, M. Puys, "Lazart: A Symbolic Approach for Evaluation the Robustness of Secured Codes against Control Flow Injections" - _22014 IEEE Seventh International Conference on Software Testing, Verification and Validation_ , doi: 10.1109/ICST.2014.34. \[[PDF](https://hal.science/hal-02951150/document)\]

## Disclaimer

THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

