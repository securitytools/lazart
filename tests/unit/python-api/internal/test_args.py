""" Unit tests for lazart._internal_.args.py. 

TODO verify
:since: 4.0
:author: Etienne Boespflug 
"""

import unittest
import types
from unittest.mock import MagicMock
from lazart._internal_.args import ListType, DictType, is_type, get_or_throw, expect_type, warning_unknown_fields, unknown_fields, get_satisfies_function, get_quiet, all_same_lengths, forward
import lazart.constants as constants

class TestArgsModule(unittest.TestCase):

    def test_list_type_str(self):
        list_type = ListType(int)
        result = str(list_type)
        self.assertEqual(result, "list(<class 'int'>)")

    def test_dict_type_str(self):
        dict_type = DictType(str, int)
        result = str(dict_type)
        self.assertEqual(result, "dict(<class 'str'>, <class 'int'>)")

    def test_is_type_callable(self):
        self.assertTrue(is_type(lambda x: x, types.FunctionType))

    def test_is_type_list_type(self):
        list_type = ListType(int)
        self.assertTrue(is_type([1, 2, 3], list_type))

    def test_is_type_dict_type(self):
        dict_type = DictType(str, int)
        self.assertTrue(is_type({"key": 1}, dict_type))

    def test_expect_type(self):
        obj = [1, 2, 3]
        list_type = ListType(int)
        expect_type(obj, list_type, "my_list")

    def test_get_or_throw(self):
        kwargs = {"my_key": "my_value"}
        result = get_or_throw(kwargs, "my_key", str, "default_value")
        self.assertEqual(result, "my_value")

    def test_forward_present(self):
        kwargs = {"my_key": "my_value"}
        result = forward(kwargs, "my_key")
        self.assertEqual(result, "my_value")

    def test_forward_absent(self):
        kwargs = {"other_key": "other_value"}
        result = forward(kwargs, "my_key", "default_value")
        self.assertEqual(result, "default_value")

    def test_unknown_fields(self):
        kwargs = {"key1": "value1", "key2": "value2"}
        keys = ["key1"]
        result = unknown_fields(kwargs, keys)
        self.assertEqual(result, ["key2"])

    def test_warning_unknown_fields(self):
        kwargs = {"key1": "value1", "key2": "value2"}
        keys = ["key1"]
        msg = "Test message"
        with unittest.mock.patch("lazart.logs.warning") as mock_warning:
            result = warning_unknown_fields(kwargs, keys, msg)
            self.assertFalse(result)
            mock_warning.assert_called_once_with("unexpected field [key2] in Test message.")

    def test_warning_unknown_fields_no_warning(self):
        kwargs = {"key1": "value1"}
        keys = ["key1"]
        msg = "Test message"
        with unittest.mock.patch("lazart.logs.warning") as mock_warning:
            result = warning_unknown_fields(kwargs, keys, msg)
            self.assertTrue(result)
            mock_warning.assert_not_called()

    def test_get_satisfies_function_present(self):
        kwargs = {constants.arg_satisfies_function: lambda x: x > 0}
        result = get_satisfies_function(kwargs, None)
        self.assertEqual(result, kwargs[constants.arg_satisfies_function])

    def test_get_satisfies_function_absent(self):
        kwargs = {}
        result = get_satisfies_function(kwargs, None)
        self.assertEqual(result, None)

    def test_get_quiet_present(self):
        kwargs = {"quiet": True}
        result = get_quiet(kwargs, False)
        self.assertTrue(result)

    def test_get_quiet_absent(self):
        kwargs = {}
        result = get_quiet(kwargs, False)
        self.assertFalse(result)

    def test_all_same_lengths_equal(self):
        lists = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
        result = all_same_lengths(lists)
        self.assertTrue(result)

    def test_all_same_lengths_unequal(self):
        lists = [[1, 2, 3], [4, 5, 6], [7, 8]]
        with unittest.mock.patch("lazart.logs.warning") as mock_warning:
            result = all_same_lengths(lists, "Mismatched lengths")
            self.assertFalse(result)
            mock_warning.assert_called_once_with("Mismatched lengths")

if __name__ == '__main__':
    unittest.main()
