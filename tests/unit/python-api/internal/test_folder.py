import unittest
from unittest.mock import patch
import os
import shutil
from lazart._internal_.folder import Dir, check_dir, make_folder, lazart_dir, lazart_file, RDir, make_lazart_dir, make_analysis_folder, remove_file, remove_folder, remove_item
from lazart import logs
from lazart.core.analysis import Analysis
from lazart.core.attack_model import ti_model, functions_list

def get_analysis():
    return Analysis(["file.c"], functions_list("foo", ti_model()))

class TestFolderModule(unittest.TestCase):

    def test_Dir(self):
        with patch.object(os, 'chdir') as mock_chdir:
            old_dir = 'old_dir'
            d = Dir(old_dir)
            d.revert()
            mock_chdir.assert_called_once_with(old_dir)

    def test_RDir(self):
        with patch.object(os, 'chdir') as mock_chdir:
            old_dir = 'old_dir'
            r = RDir(old_dir)
            with r:
                pass
            mock_chdir.assert_has_calls([unittest.mock.call(old_dir), unittest.mock.call(r.saved_dir)])

    def test_check_dir(self):
        with patch.object(os, 'getcwd', return_value='/current_dir'):
            with patch.object(os, 'chdir') as mock_chdir:
                old_dir = '/tmp/old_dir'
                d = check_dir(old_dir)
                mock_chdir.assert_has_calls([unittest.mock.call(old_dir)])
            self.assertEqual(d.old_dir, '/current_dir')

    def test_make_folder(self):
        with patch.object(os, 'makedirs') as mock_makedirs:
            make_folder('/new_dir')
            mock_makedirs.assert_called_once_with('/new_dir')

    def test_lazart_dir(self):
        result = lazart_dir('/root_folder')
        self.assertEqual(result, '/root_folder/.lazart/')

    def test_lazart_file(self):
        result = lazart_file('/root_folder', 'filename')
        self.assertEqual(result, '/root_folder/.lazart/filename')

    def test_make_lazart_dir(self):
        with patch.object(os, 'makedirs') as mock_makedirs:
            make_lazart_dir('/root_folder')
            mock_makedirs.assert_called_once_with('/root_folder/.lazart/')

    def test_make_analysis_folder(self):
        analysis = get_analysis()
        with patch.object(os, 'makedirs') as mock_makedirs:
            with patch.object(os.path, 'join') as mock_join:
                with patch('builtins.open', unittest.mock.mock_open()):
                    make_analysis_folder(analysis)
            mock_makedirs.assert_called_once_with(analysis.path())
            mock_join.assert_called_once_with(analysis.path(), '.gitignore')
            
    def test_remove_file(self):
        path = "file"
        with patch.object(os, 'remove') as mock_remove:
            remove_file(path)
            mock_remove.assert_not_called()

    def test_remove_folder(self):
        path = "folder_path"
        with patch.object(shutil, 'rmtree') as mock_rmtree:
            remove_folder(path)
            mock_rmtree.assert_called_once_with(path)

    def test_remove_item_file(self):
        path = "path"
        with patch.object(os.path, 'isfile', return_value=True):
            with patch.object(os, 'remove') as mock_remove:
                remove_item(path)
                mock_remove.assert_not_called()

    def test_remove_item_folder(self):
        path = "path"
        with patch.object(os.path, 'isfile', return_value=False):
            with patch.object(os.path, 'isdir', return_value=True):
                with patch.object(shutil, 'rmtree') as mock_rmtree:
                    remove_item(path)
                    mock_rmtree.assert_called_once_with(path)

if __name__ == '__main__':
    unittest.main()
