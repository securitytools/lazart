
import unittest
from lazart._internal_.util import parse_klee_duration, str_time

class TestParseKleeDuration(unittest.TestCase):

    def test_valid_duration(self):
        self.assertEqual(parse_klee_duration("00:00:00"), 0.0)
        self.assertEqual(parse_klee_duration("00:01:30"), 90.0)
        self.assertEqual(parse_klee_duration("01:30:45"), 5445.0)
        self.assertEqual(parse_klee_duration("02:00:00"), 7200.0)
        self.assertEqual(parse_klee_duration("23:59:59"), 86399.0)

    def test_invalid_format(self):
        with self.assertRaises(ValueError):
            parse_klee_duration("12:34")
        with self.assertRaises(ValueError):
            parse_klee_duration("12:34:56:78")
        with self.assertRaises(ValueError):
            parse_klee_duration("12:34:56:78:90")
        with self.assertRaises(ValueError):
            parse_klee_duration("12::56")
        with self.assertRaises(ValueError):
            parse_klee_duration(":34:56")

    def test_invalid_values(self):
        with self.assertRaises(ValueError):
            parse_klee_duration("12:34:56.7890")
        with self.assertRaises(ValueError):
            parse_klee_duration("12:34:56.-1")
        with self.assertRaises(ValueError):
            parse_klee_duration("12:34:-56.7890")

class TestStrTime(unittest.TestCase):

    def test_seconds(self):
        self.assertEqual(str_time(0.5), "500ms")
        self.assertEqual(str_time(30), "30s")

    def test_minutes(self):
        self.assertEqual(str_time(90), "1min and 30s")
        self.assertEqual(str_time(1500), "25min and 0s")

    def test_hours(self):
        self.assertEqual(str_time(3600), "1h 0min 0s")
        self.assertEqual(str_time(7200), "2h 0min 0s")
        self.assertEqual(str_time(3720), "1h 2min 0s")

    def test_numeric(self):
        self.assertEqual(str_time(90, numeric=True), "1:30")
        self.assertEqual(str_time(3720, numeric=True), "1:2:0")

    def test_nan(self):
        self.assertEqual(str_time(None), "nan")

    def test_microseconds(self):
        self.assertEqual(str_time(0.000001), "1us")
        self.assertEqual(str_time(0.0001), "100us")


if __name__ == '__main__':
    unittest.main()
