import unittest
import os 

#from lazart.lazart import *
from lazart.core.analysis import Analysis, AnalysisFlag
from lazart.core.attack_model import data_model, ti_model, functions_list

def get_args():
    return (["main.c", "program.h", "foo.c"],
                functions_list("foo", ti_model()))

class TestAnalysis(unittest.TestCase):

    def test_init(self):
        input_files, attack_model = get_args()
        analysis = Analysis(input_files, attack_model)
        
        self.assertEqual(analysis.input_files(), [os.path.abspath(path) for path in input_files])
        self.assertEqual(analysis.attack_model(), attack_model)
        self.assertEqual(analysis.flags(), AnalysisFlag.AttackAnalysis)
        self.assertEqual(analysis.max_order(), 2)
        self.assertEqual(analysis.compiler_args(), '')
        self.assertEqual(analysis.linker_args(), '')
        self.assertEqual(analysis.dis_args(), '')
        self.assertEqual(analysis.wolverine_args(), '')
        self.assertEqual(analysis.klee_args(), '')
        self.assertEqual(analysis.tasks(), {"rename_bb": ["__mut__"], "add_trace": ["__mut__"]})
        self.assertEqual(analysis.countermeasures(), [])
        self.assertEqual(analysis.name(), '')
        self.assertFalse(analysis._init)

    def test_init_with_kwargs(self):
        input_files, attack_model = get_args()
        kwargs = {
            "path": "/some/path",
            "name": "Analysis1",
            "flags": AnalysisFlag.HSAnalysisOnly,
            "max_order": 3,
            "compiler_args": "-Wall",
            "linker_args": "-L/usr/lib",
            "dis_args": "-O3",
            "wolverine_args": "--optimize",
            "klee_args": "--timeout 1000",
            "tasks": {"task1": ["func1"], "task2": ["func2"]},
            "countermeasures": [{"type" : "load-multiplication"}]
        }
        analysis = Analysis(input_files, attack_model, **kwargs)

        self.assertEqual(analysis.input_files(), [os.path.abspath(path) for path in input_files])
        self.assertEqual(analysis.attack_model(), attack_model)
        self.assertEqual(analysis.flags(), AnalysisFlag.HSAnalysisOnly)
        self.assertEqual(analysis.compiler_args(), "-Wall")
        self.assertEqual(analysis.linker_args(), "-L/usr/lib")
        self.assertEqual(analysis.dis_args(), "-O3")
        self.assertEqual(analysis.wolverine_args(), "--optimize")
        self.assertEqual(analysis.klee_args(), "--timeout 1000")
        self.assertEqual(analysis.tasks(), {"task1": ["func1"], "task2": ["func2"]})
        self.assertEqual(analysis.countermeasures(), [{"type" : "load-multiplication"}])
        self.assertEqual(analysis.path(), "/some/path")
        self.assertEqual(analysis.name(), "Analysis1")
        self.assertFalse(analysis._init)

    def test_init_invalid_max_order(self):
        input_files, attack_model = get_args()
        analysis = Analysis(input_files, attack_model, max_order=-1)
        self.assertEqual(analysis.max_order(), 2)

    def test_init_invalid_countermeasures(self):
        with self.assertRaises(Exception):
            input_files, attack_model = get_args()
            Analysis(input_files, attack_model, countermeasures=[{"invalid_key": "invalid_value"}])

    def test_init_with_string_attack_model(self):
        input_files = ['file1.c', 'file2.c']
        attack_model = "invalid_attack_model.yaml"
        with self.assertRaises(Exception):
            Analysis(input_files, attack_model)

if __name__ == '__main__':
    unittest.main()