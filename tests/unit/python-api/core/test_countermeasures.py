import unittest

from lazart.lazart import *
from lazart.core.countermeasures import cm_sec_swift, cm_tm, cm_lm, verify_countermeasures
import lazart.logs as log

class TestCountermeasures(unittest.TestCase):
    
    def test_countermeasures_helpers(self):
        self.assertDictEqual(cm_lm(), {"type" : "load-multiplication"})
        self.assertDictEqual(cm_tm(), {"type" : "test-multiplication"})
        self.assertDictEqual(cm_sec_swift(), {"type" : "sec_swift"})

    def test_verify_countermeasure(self):
        self.assertTrue(verify_countermeasures([cm_tm({"on": ["fname"]})]))

    def test_verify_countermeasures(self):
        with self.assertRaises(Exception):
            verify_countermeasures([0, 2])
        self.assertTrue(verify_countermeasures([cm_tm({"on": ["fname"]})]))

if __name__ == '__main__':
    unittest.main()