import unittest

from lazart.core.attack_model import _verify_data_value, data_model, ti_model,functions_list, verify_model
import lazart.logs as log

class TestAttackModel(unittest.TestCase):
    
    def test_ti_model(self):
        # Empty
        test = {"type": "test-inversion"}
        self.assertDictEqual(ti_model(), test)
        self.assertDictEqual(ti_model({}), test)
        # Named
        test["name"] =  "test"        
        self.assertDictEqual(ti_model({"name": "test"}), test)

    def test_data_model(self):
        # Empty
        test = {"type": "data"}
        self.assertDictEqual(data_model(), test)
        self.assertDictEqual(data_model({}), test)
        # Named
        test["name"] =  "test"        
        self.assertDictEqual(data_model({"name": "test"}), test)
        # Complete
        vars = {"vars" : {"a" : 2, "b": "__sym__", "c": "__sym__:predicate"}}
        test["vars"] = vars
        self.assertDictEqual(data_model({"name": "test", "vars": vars}), test)
        # Changes
        vars["test"] = 0
        self.assertDictEqual(data_model({"name": "test", "vars": vars}), test)

    def test_functions_list(self):
        models = [ti_model(), data_model()]
        fs = {"functions": {"test": {"models": models}, "test2": {"models": models}}}

        self.assertDictEqual(functions_list(["test", "test2"], models), fs)

    def test_merge_am(self):
        # TODO
        pass

    def test__verify_data_value(self):
        self.assertTrue(_verify_data_value(22, ""))
        self.assertTrue(_verify_data_value("22", ""))
        self.assertTrue(_verify_data_value("__sym__", ""))


        v = log.current_level()
        log.set_verbosity(log.VerbosityLevel.Error)
        self.assertFalse(_verify_data_value(["test"], ""))
        log.set_verbosity(v)
    
    def test_verify_model(self):
        self.assertTrue(verify_model(ti_model({"protected_detectors": ["det1", "det2"]})))
        self.assertTrue(verify_model(data_model({"inline": True, "vars" : {"a" : "__sym__"}})))


        v = log.current_level()
        log.set_verbosity(log.VerbosityLevel.Error)
        self.assertFalse(verify_model({"type": "none"}))
        #self.assertFalse(verify_model(ti_model({"unknown": 0})))
        self.assertFalse(verify_model(data_model({"all": ["True"]})))
        log.set_verbosity(v)

    def test_verify_attack_model(self):
        # TODO
        pass

if __name__ == '__main__':
    unittest.main()