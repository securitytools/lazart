import unittest
from unittest.mock import MagicMock
from lazart.core.traces import UserEvent, FaultModel, BasicBlock, Detection, Fault

class TestTracesEvents(unittest.TestCase):
    def test_user_event_str(self):
        user_event = UserEvent("some_data")
        result = user_event.str()
        self.assertEqual(result, "EVENT(some_data)")

    def test_user_event_str_method(self):
        user_event = UserEvent("some_data")
        result = str(user_event)
        self.assertEqual(result, "EVENT(some_data)")

    def test_basic_block_str(self):
        basic_block = BasicBlock("BB1")
        result = basic_block.str()
        self.assertEqual(result, "BB(BB1)")

    def test_basic_block_str_method(self):
        basic_block = BasicBlock("BB1")
        result = str(basic_block)
        self.assertEqual(result, "BB(BB1)")

    def test_detection_str(self):
        ccp = "ccp1"
        det_ptr = {"key": "value"}
        detection = Detection(ccp, det_ptr)
        result = detection.str()
        self.assertEqual(result, "CM(ccp1)")

    def test_detection_str_method(self):
        ccp = "ccp1"
        det_ptr = {"id": "ccp1"}
        detection = Detection(ccp, det_ptr)
        result = str(detection)
        self.assertEqual(result, "CM(ccp1)")

    def test_fault_init(self):
        ip = "injection_point"
        model = FaultModel.SwitchCall
        ip_ptr = {"origin_fct": "foo", "dest_fct": "bar"}
        fault = Fault(ip, model, ip_ptr)
        self.assertEqual(fault.injection_point, "injection_point")
        self.assertEqual(fault.model, FaultModel.SwitchCall)
        self.assertEqual(fault._ip_ptr, ip_ptr)

if __name__ == '__main__':
    unittest.main()
