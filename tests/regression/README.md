This folder contains scripts for regression test in **Lazart**.

Each *.lzt* file correspond to a test.

Run the script *clean.py* to clean the build folder and *regression.py* to run all regression tests.

Example folders contain scripts which generate the corresponding test file.