#!/usr/bin/python3

from lazart.lazart import *
import glob

params = install_script()
params.order = 3 # force

value = 0
model = data_model({"inline": False,
"vars": {
    "tmp": value,
    "Cp": value, "Cq": value,
    "g_q": value, "g_p": value, "g_dq": value, "g_dp": value, "g_iq": value, "g_M": value
}})



model_simple = data_model({"inline": False, "vars": {"Cq": value, "Cp": value, "tmp": value}})

a = read_or_make(glob.glob("src/*.c") + ["../commons/oracle.c", "../commons/init.c"], 
    functions_list(["rsa_sign"], model),  
    path="results", 
    flags=AnalysisFlag.EqRedAnalysis,
    compiler_args="-Wall -I /opt/lazart/tests/regression/rsa/commons",
    wolverine_args="-I",
    params=params
) 

res = results = execute(a)


from lazart.tests.regression import generate_regression_test

generate_regression_test(a, res, "../../test-rsa0-3f-dl0.lzt", "rsa0-3f-dl0", full_trace=True, full_arr=True, execute={"no_aar":False})


