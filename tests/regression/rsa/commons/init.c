#include "klee/klee.h"
#include "rsa.h"

data_t g_M;
data_t g_N;
data_t g_e;
data_t g_p;
data_t g_q;
data_t g_dp;
data_t g_dq;
data_t g_iq;
data_t g_sign;

#ifdef FULL_SYM
// (eq & !auth ?)
#error not implemented
#elif defined(SYM)
void init()
{
    klee_make_symbolic(&g_M, sizeof(g_M), "g_M");
    klee_make_symbolic(&g_N, sizeof(g_N), "g_N");
    klee_make_symbolic(&g_e, sizeof(g_e), "g_e");
    klee_make_symbolic(&g_p, sizeof(g_p), "g_p");
    klee_make_symbolic(&g_q, sizeof(g_q), "g_q");
    klee_make_symbolic(&g_dp, sizeof(g_dp), "g_dp");
    klee_make_symbolic(&g_dq, sizeof(g_dq), "g_dq");
    klee_make_symbolic(&g_iq, sizeof(g_iq), "g_iq");
    klee_make_symbolic(&g_sign, sizeof(g_sign), "g_sign");
    g_sign = -1;
}
#else
void init()
{
    g_N = 11413;
    g_e = 3533;
    g_p = 101;
    g_q = 113;
    g_iq = 59; /* iq = (1/q) mod p */
    g_dp = 97; /* dp = d mod (p-1) */
    g_dq = 101; /* dq = d mod (q-1) */
    g_sign = -1;
    g_M = 23;
}

#endif