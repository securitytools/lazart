#pragma once

#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>

#ifndef data_t
#define data_t int
#endif

extern data_t g_M;
extern data_t g_N;
extern data_t g_e;
extern data_t g_p;
extern data_t g_q;
extern data_t g_dp;
extern data_t g_dq;
extern data_t g_iq;
extern data_t g_sign;

bool oracle();
void init();

data_t rsa_sign(void);