#!/usr/bin/python3

import lazart.tests.test_factory as tf
import argparse

parser = argparse.ArgumentParser(description="Lazart regression tests script")

parser.add_argument("-F", "--fast", action="store_true", help="Run only test that are not flagged as time consuming (fast: false).")
parser.add_argument("-o", "--only", type=str, action="append", help="List the .")

args = parser.parse_args()

if args.only is not None:
    tests = []
    for path in args.only:
        tests.append(tf.parse_test(path))
    tests.sort()
    tf.run_tests(tests)
else:
    tf.lazart_tests("/opt/lazart/tests/regression", args.fast)