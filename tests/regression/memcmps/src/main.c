#include "lazart.h"
#include "memcmps.h"
#include <stdio.h>

#define SIZE 4

BOOL oracle(int resultat)
{
    return resultat == TRUE_VAL;
}

BOOL oracle2(int resultat)
{
    return resultat != FALSE_VAL;
}

void initialize_sym(char s1[SIZE], char s2[SIZE])
{
    klee_make_symbolic(s1, SIZE * sizeof(char), "s1");
    klee_make_symbolic(s2, SIZE * sizeof(char), "s2");
}

void initialize(char s1[SIZE], char s2[SIZE])
{
    for (unsigned int i = 0; i < SIZE; i++) {
        s1[i] = '0';
    }
    for (unsigned int i = 0; i < SIZE; i++) {
        s2[i] = '1';
    }
}

void check_equals(char s1[SIZE], char s2[SIZE])
{
    int equal = 0;
    for (unsigned int i = 0; i < SIZE; i++) {
        equal += s1[i] == s2[i];
    }
    klee_assume(equal != SIZE);
}

int foo(int size)
{
    if (klee_is_replay()) {
        printf("todo");
    }
    return (0 == 0);
}

int main()
{
    char s1[SIZE], s2[SIZE];
#ifdef SYM
    initialize_sym(s1, s2);
#else
    initialize(s1, s2);
#endif

    int res;
    //res = foo(SIZE);
    res = memcmps(s1, s2, SIZE);

    //printf("res=%x\n", res);
    _LZ__ORACLE(oracle(res));
    check_equals(s1, s2);
    // printf("%x",res);
    // _LZ__ORACLE(res == TRUE_VAL);
    return 0;
}