#ifndef MEMCMPS_0_H
#define MEMCMPS_0_H
#include <stdio.h>
#include <string.h>
typedef unsigned char UBYTE;
typedef unsigned char BOOL;

#define TRUE_VAL 0x1234
#define FALSE_VAL 0x5678
#define MASK 0xABCD
#define MASK2 0xF4F4

int memcmp_stub(const char* str1, const char* str2, size_t count);
int memcmps(char* a, char* b, int len);
int memcmps2(char* a, char* b, int len);

#endif