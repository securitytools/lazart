#include "memcmps.h"
#include <stdio.h>
#include <stdlib.h>
//#include "lazart.h"

int memcmp_stub(const char* str1, const char* str2, size_t count)
{
    register const unsigned char* s1 = (const unsigned char*)str1;
    register const unsigned char* s2 = (const unsigned char*)str2;
    while (count-- > 0) {
        if (*s1++ != *s2++)
            return s1[-1] < s2[-1] ? -1 : 1;
    }
    return 0;
}

#if defined(V2)
int memcmps(char* a, char* b, int len)
{
    int result = FALSE_VAL;

    if (!memcmp_stub(a, b, len)) {
        // inj FALSE ^ MASK => FALSE
        result ^= MASK; // result = FALSE ^ MASK
        if (!memcmp_stub(a, b, len)) {
            //  FALSE ^ FALSE ^TRUE
            result ^= FALSE_VAL ^ TRUE_VAL; // result = MASK ^ TRUE
            if (!memcmp_stub(a, b, len)) {
                result ^= MASK; // result = TRUE
            }
        }
    }

    return result;
}
#elif defined(V3)
int memcmps(char* a, char* b, int len)
{
    int result = FALSE_VAL;

    if (!memcmp_stub(a, b, len)) {
        result ^= FALSE_VAL ^ MASK; // result = MASK
        if (!memcmp_stub(a, b, len)) {
            result ^= MASK2; // result = MASK ^ MASK2
            if (!memcmp_stub(a, b, len)) {
                result ^= TRUE_VAL ^ MASK; // result = MASK2 ^ TRUE_VAL
                if (!memcmp_stub(a, b, len))
                    result ^= MASK2; // result = TRUE_VAL
            }
        }
    }

    return result;
}

#else

int memcmps(char* a, char* b, int len)
{
    int result = MASK;
    int tmp = len;
    if (!memcmp_stub(a, b, tmp))
        result ^= MASK ^ TRUE_VAL;
    else
        result ^= MASK ^ FALSE_VAL;
    return result;
}

#endif