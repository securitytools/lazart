#!/usr/bin/python3

from lazart.lazart import *
import glob

params = install_script()
params.order = 4

data_sym = data_model({"vars": {
                    "len": "__sym__",
                    "result": "__sym__"
                }})

am = functions_list(["memcmps"], [ti_model(), data_sym])

a = read_or_make(["src/memcmps.c", "src/main.c"], 
    am,  
    path="results",
    flags=AnalysisFlag.EqRedAnalysis,
    compiler_args="-Wall -DV2 -DSYM -D_LZ__NO_STD",
    params=params,
    #klee_args="--search=bfs"
) 

res = execute(a)

verify.attack_analysis(a)
verify.traces_parsing(a)

from lazart.tests.regression import generate_regression_test

generate_regression_test(a, res, "../test-mcmps2-4f-sym-ti_dl.lzt", "mcmps2-4f-sym-ti_dl", full_trace=True, full_arr=True, execute={"no_aar":False})
