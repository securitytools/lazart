#!/usr/bin/python3

from lazart.lazart import *
import os 
import glob

params = install_script()
params.order = 1

data_fix_i = data_model({
                "vars": {"i": 0},
                "name": "data-fix-i"
            })
ti = ti_model({
        "name": "ti"
    })

am_foo = functions_list(["foo"], [ti])
am_bar = functions_list(["bar"], [data_fix_i])
am = merge_am(am_foo, am_bar)

a = read_or_make(glob.glob("src/instr_directives.c"), 
    am,
    path="results-test-instr", 
    compiler_args="-Wall",
    params=params,
    wolverine_args="-t mut -t instr")

res = execute(a)

verify.traces_parsing(a)
verify.attack_analysis(a)

from lazart.tests.regression import generate_regression_test

generate_regression_test(a, res, "../test-instr-directives.lzt", "instr-directives", full_trace=False, full_arr=False)
