# Lazart report

Using Lazart version 4.0.0.
Reported at *2024-02-21 15:07:07.253320*.

## Analysis

Path: */opt/lazart/tests/regression/basic/results-test-instr*.
Flags: *AnalysisFlag.AttackAnalysis*.
Order (max faults): *1*.

Attack-model: `{
    "functions": {
        "bar": {
            "models": [
                {
                    "name": "data-fix-i",
                    "type": "data",
                    "vars": {
                        "i": 0
                    }
                }
            ]
        },
        "foo": {
            "models": [
                {
                    "name": "ti",
                    "type": "test-inversion"
                }
            ]
        }
    }
}`.

Tasks:
 - name: `{task}`.
 - name: `{task}`.
 - name: `{task}`.
 - auto-countermeasures: `[]`.

## Compilation Information

Compiled at: `2024-02-21 15:07:06.974910`.
Duration: `4ms`.
Input files: `instr_directives.c`
Compiler: `clang`.
 - args: `-Wall`.
Linker: `llvm-link`.
 - args: ``.
Disassembler: `llvm-dis`.
 - args: ``.

## DSE Information

Run at 2024-02-21 15:07:07.228683
Wolverine args: -t mut -t instr.
DSE Engine: Klee (args: )

Execution metrics:
 +-----------+----------------+------------+-------+-------------+----------+----------------+-----------------+----------+------------------+-----------------+-------------------------+---------+-----------+-----------+--------------------+----------------+-------------------+
| Program   |   Instructions |   Branches |   IPs |   Detectors |   Traces |   PartialPaths |   IgnoredTraces |   Ktests |   CompletedPaths |   ExploredPaths |   Executed Instructions | State   |   ICov(%) |   BCov(%) |   IncompleteTraces |   FullBranches |   PartialBranches |
|-----------+----------------+------------+-------+-------------+----------+----------------+-----------------+----------+------------------+-----------------+-------------------------+---------+-----------+-----------+--------------------+----------------+-------------------|
| unnamed   |            322 |         15 |     4 |           0 |        4 |              0 |               0 |        4 |                4 |               4 |                     409 | ok      |     85.09 |        60 |                  4 |              6 |                 6 |
+-----------+----------------+------------+-------+-------------+----------+----------------+-----------------+----------+------------------+-----------------+-------------------------+---------+-----------+-----------+--------------------+----------------+-------------------+


***Warnings:*** *KLEE: WARNING: undefined reference to function: printf
KLEE: WARNING ONCE: calling external: printf(139756175564800) at src/instr_directives.c:10 0
*

### Metrics

Fault limit                 0-order    1-order    Total
--------------------------  ---------  ---------  -------
Explored paths              N/A        N/A        4.0
Total instructions          N/A        N/A        409.0
Generated tests             N/A        N/A        4.0
Correct traces              1          3          4
DSE duration                N/A        N/A        0us
Trace parsing duration      N/A        N/A        7ms
Attacks Analysis duration   N/A        N/A        74us
Hotspots Analysis duration  N/A        N/A        66us

## Attack Analysis

 - 0-faults attacks: 1
    - t3: TerminationType.Correct

### Attacks

+---------+-----------+-----------+---------+
| Order   |   0-order |   1-order |   Total |
|---------+-----------+-----------+---------|
| attacks |         0 |         3 |       3 |
+---------+-----------+-----------+---------+

### Hotspots Analysis

  Order  Info                   0-order  1-order
     IP
-------  -------------------  ---------  ---------
      0  data [l:7, bb:bb0]           0  1 (mc:1)
      1  ti [l:15, bb:bb1]            0  1 (mc:1)
      2  data [l:20, bb:bb3]          0  0
      3  ti [l:23, bb:bb5]            0  1 (mc:1)

