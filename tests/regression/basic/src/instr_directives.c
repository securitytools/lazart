#include <lazart.h>
#include <stdio.h>
#include <wolverine.h>

int bar(int i)
{
    return i * 2; // IP0 (data)
}

int foo(int i)
{
    _LZ__DISABLE_ALL(); // all disabled.
    int j = 2 * i + 2;
    _LZ__RESET(); // all reset.

    if (i == 0) { // IP1 (ti)
        _LZ__DISABLE_MODEL("ti"); // ti explicitly disabled.
        if (i != 0) { // not mutated !
            _LZ__ENABLE_MODEL("data-fix-i"); // data-fix-i explicitly enabled.
            _LZ__RESET_DISABLED(); // reset ti, data-fix-i is still enabled.
            i = i * j; // IP2 (data)
            _LZ__RESET_ENABLED(); // reset data-fix-i;
        }
    } else if (i == 1) { // IP3 (data)
        _LZ__DISABLE_BB();
        _LZ__ENABLE_MODEL("data-fix-i");
        i = i * 2; // not mutated !
        _LZ__DISABLE_ALL();
    }
    return bar(i); // no mutation on data
}

int main(void)
{
    foo(12);
}