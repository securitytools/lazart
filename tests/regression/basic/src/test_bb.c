#include <lazart.h>
#include <stdio.h>
#include <wolverine.h>

int foo(int i)
{
    int j = 0;

    _LZ__RENAME_BB("start");
    j = i * (i / 2);

    if (j > 0) { // IP 0 (DL) & IP 1 (TI)
        _LZ__RENAME_BB("in");
        j += i; // IP 2 (DL)
    }

    _LZ__RENAME_BB("end");

    return j; // IP 3 (DL)
}

int main(void)
{
    int res = foo(12);

    _LZ__ORACLE(res != 84);
}