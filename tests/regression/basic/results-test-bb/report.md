# Lazart report

Using Lazart version 4.0.0.
Reported at *2024-02-22 10:58:38.512820*.

## Analysis

Path: */opt/lazart/tests/regression/basic/results-test-bb*.
Flags: *AnalysisFlag.AttackAnalysis*.
Order (max faults): *4*.

Attack-model: `{
    "basic-blocks": {
        "end": {
            "models": [
                {
                    "type": "data",
                    "vars": {
                        "j": "__sym__"
                    }
                }
            ]
        },
        "in": {
            "models": [
                {
                    "type": "data",
                    "vars": {
                        "j": "__sym__"
                    }
                }
            ]
        },
        "start": {
            "models": [
                {
                    "type": "test-inversion"
                },
                {
                    "type": "data",
                    "vars": {
                        "j": "__sym__"
                    }
                }
            ]
        }
    }
}`.

Tasks:
 - name: `{task}`.
 - name: `{task}`.
 - name: `{task}`.
 - auto-countermeasures: `[]`.

## Compilation Information

Compiled at: `2024-02-22 10:58:38.184558`.
Duration: `6ms`.
Input files: `test_bb.c`
Compiler: `clang`.
 - args: `-Wall`.
Linker: `llvm-link`.
 - args: ``.
Disassembler: `llvm-dis`.
 - args: ``.

## DSE Information

Run at 2024-02-22 10:58:38.441289
Wolverine args: -t pre -t mut.
DSE Engine: Klee (args: )

Execution metrics:
 +-----------+----------------+------------+-------+-------------+----------+----------------+-----------------+----------+------------------+-----------------+-------------------------+---------+-----------+-----------+--------------------+----------------+-------------------+
| Program   |   Instructions |   Branches |   IPs |   Detectors |   Traces |   PartialPaths |   IgnoredTraces |   Ktests |   CompletedPaths |   ExploredPaths |   Executed Instructions | State   |   ICov(%) |   BCov(%) |   IncompleteTraces |   FullBranches |   PartialBranches |
|-----------+----------------+------------+-------+-------------+----------+----------------+-----------------+----------+------------------+-----------------+-------------------------+---------+-----------+-----------+--------------------+----------------+-------------------|
| unnamed   |            346 |         17 |     4 |           0 |       15 |              3 |               1 |       16 |               15 |              18 |                    1164 | ok      |     82.66 |     61.76 |                 15 |              5 |                11 |
+-----------+----------------+------------+-------+-------------+----------+----------------+-----------------+----------+------------------+-----------------+-------------------------+---------+-----------+-----------+--------------------+----------------+-------------------+


***Warnings:*** *KLEE: WARNING: undefined reference to function: printf
KLEE: WARNING ONCE: calling external: printf(140426914533376) at src/test_bb.c:6 0
*

### Metrics

Fault limit                 0-order    1-order    2-order    3-order    4-order    Total
--------------------------  ---------  ---------  ---------  ---------  ---------  -------
Explored paths              N/A        N/A        N/A        N/A        N/A        18.0
Total instructions          N/A        N/A        N/A        N/A        N/A        1164.0
Generated tests             N/A        N/A        N/A        N/A        N/A        16.0
Correct traces              0          4          6          4          1          15
DSE duration                N/A        N/A        N/A        N/A        N/A        0us
Trace parsing duration      N/A        N/A        N/A        N/A        N/A        30ms
Attacks Analysis duration   N/A        N/A        N/A        N/A        N/A        193us
Hotspots Analysis duration  N/A        N/A        N/A        N/A        N/A        228us

## Attack Analysis

 - 0-faults attacks: 0

### Attacks

+---------+-----------+-----------+-----------+-----------+-----------+---------+
| Order   |   0-order |   1-order |   2-order |   3-order |   4-order |   Total |
|---------+-----------+-----------+-----------+-----------+-----------+---------|
| attacks |         0 |         4 |         6 |         4 |         1 |      15 |
+---------+-----------+-----------+-----------+-----------+-----------+---------+

### Hotspots Analysis

  Order  Info                     0-order  1-order    2-order    3-order    4-order
     IP
-------  ---------------------  ---------  ---------  ---------  ---------  ---------
      0  data [l:12, bb:start]          0  1 (mc:1)   4 (mc:1)   4 (mc:1)   1 (mc:1)
      1  ti [l:12, bb:start]            0  1 (mc:1)   2 (mc:1)   3 (mc:1)   1 (mc:1)
      2  data [l:14, bb:in]             0  1 (mc:1)   2 (mc:1)   2 (mc:1)   1 (mc:1)
      3  data [l:19, bb:end]            0  1 (mc:1)   4 (mc:1)   3 (mc:1)   1 (mc:1)

