#!/usr/bin/python3

from lazart.lazart import *
import os 
import glob

params = install_script()
params.order = 4

data_sym = data_model({
    "vars": {"j": "__sym__"}
})

amA = bb_list(["start"], [ti_model(), data_sym])
amB = bb_list(["in", "end"], [data_sym])

am = merge_am(amA, amB)

a = read_or_make(glob.glob("src/test_bb.c"), 
    am,
    path="results-test-bb", 
    compiler_args="-Wall",
    klee_args="",
    wolverine_args="-t pre -t mut",
    tasks={"rename_bb":["foo"], "add_trace": ["foo"]},
    params=params)

res = execute(a)

verify.traces_parsing(a)
verify.attack_analysis(a)


from lazart.tests.regression import generate_regression_test

generate_regression_test(a, res, "../test-fault-space-bbs.lzt", "fault-space-bbs", full_trace=False, full_arr=False)