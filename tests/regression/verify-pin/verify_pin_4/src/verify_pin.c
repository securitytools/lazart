#include "verify_pin.h"
#include "lazart.h"

BOOL verify_pin(uint8_t* user_pin)
{
    int i;
    BOOL status;
    BOOL diff;
    int step_counter;
    uint8_t tc_copy = try_counter;

    if (try_counter > 0) {
        if (tc_copy != try_counter)
            _LZ__CM("0");
        try_counter--;
        if (try_counter != tc_copy - 1)
            _LZ__CM("1");
        tc_copy--;

        //- ByteArrayCompare : Start
        status = FALSE;
        diff = FALSE;
        step_counter = 0;
        for (i = 0; i < PIN_SIZE; i++) {
            if (user_pin[i] != card_pin[i])
                diff = TRUE;
            step_counter++;
        }
        if (step_counter != PIN_SIZE)
            _LZ__CM("2");
        if (i != PIN_SIZE)
            _LZ__CM("3");
        if (diff == FALSE)
            status = TRUE;
        else
            status = FALSE;

        if (status == TRUE) {
            if (tc_copy != try_counter)
                _LZ__CM("4");
            try_counter = 3;
            return TRUE;
        } else if (status == FALSE) {
            try_counter--;
            return FALSE;
        } else
            _LZ__CM("5");
    }

    return FALSE;
}