#!/usr/bin/python3

from lazart.lazart import *
import glob

# Parse CLI parameters
params = install_script()
params.order = 3

ti = ti_model()
data_br_simple = data_model({"vars": {"size": 0, "result": 0, "i" : 0}})
data_br = data_model({"vars": {"size": 0, "result": 0, "i" : 0, "diff" : 0, "status": 0, "try_counter": 0, "step_counter": 0, "tc_copy": 0}})
data_sym = data_model({"vars": {"diff": "__sym__", "status": "__sym__", "result": "__sym__", "step_counter": "__sym__", "tc_copy": "__sym__", "try_counter": "__sym__"}})

# Create analysis or load from path if available
a = read_or_make(glob.glob("src/*.c") + glob.glob("../commons/*.c"), # Source files
    functions_list(["verify_pin"], [data_br_simple]), # Attack model: test inversion for the two specified function 
    path="results", # Pass in which analysis file will be stored
    flags=AnalysisFlag.EqRedAnalysis, # Attack equivalence and redundancy analysis
    compiler_args="-Wall -DSYM -DHARDENNED_BOOLEAN -I /opt/lazart/tests/regression/verify-pin/commons/ -DPHI_PTC", # Compiler arguments
    params=params, # Pass CLI params to the analysis
    klee_args="",
    countermeasures=[cm_tm(dict(depth=2, on=["__mut__"], trigger_function="_LZ__trigger_detector"))]
    #klee_args=Analysis.default_klee_args + " libc=klee") or: libc=uclibc
) 

res = execute(a, old_parse_mode=False) # Execute analysis, print results and generate report

from lazart.tests.regression import generate_regression_test

generate_regression_test(a, res, "../../test-vp4-3f-sym-ti_dl-tm2-nsd-ptc.lzt", "vp4-3f-sym-ti_dl-tm2-nsd-ptc", full_trace=True, full_arr=True, execute={"no_aar":False})

