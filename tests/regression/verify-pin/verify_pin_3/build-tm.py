#!/usr/bin/python3

from lazart.lazart import *
import glob

# Parse CLI parameters
params = install_script()
params.order = 4 # forced

breset = data_model({"vars": {"size": 0, "result": 0, "i" : 0}})
models = [ti_model(), breset]
# Create analysis or load from path if available
a = read_or_make(glob.glob("src/*.c") + glob.glob("../commons/*.c"), # Source files
    functions_list(["verify_pin"], models), # Attack model: test inversion for the two specified function 
    path="results", # Pass in which analysis file will be stored
    flags=AnalysisFlag.EqRedAnalysis, # Attack equivalence and redundancy analysis
    compiler_args="-Wall -DHARDENNED_BOOLEAN -I /opt/lazart/tests/regression/verify-pin/commons/ -DPHI_AUTH_PTC_OR", # Compiler arguments
    params=params, # Pass CLI params to the analysis
    klee_args="",
    countermeasures=[cm_tm(dict(depth=1, on=["__mut__"]))]
    #klee_args=Analysis.default_klee_args + " libc=klee") or: libc=uclibc
) 

res = execute(a) # Execute analysis, print results and generate report

from lazart.tests.regression import generate_regression_test

generate_regression_test(a, res, "../../test-vp3-4f-ti-tm1-ptc_or_auth.lzt", "vp3-4f-ti-tm1-ptc_or_auth", full_trace=True, full_arr=True, execute={"no_aar":False})