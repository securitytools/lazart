#include "lazart.h"
#include "verify_pin.h"
#include <stdio.h>

int main()
{
    uint8_t user_pin[PIN_SIZE];

    init(user_pin, card_pin);

    BOOL ret = verify_pin(user_pin);

#ifdef UEVENT_ORACLE
    _LZ__ORACLE(!_LZ__triggered());
    oracle_uevent(ret);
#else
    _LZ__ORACLE(oracle(ret));
#endif
    return 0;
}
