#include "verify_pin.h"
#include "lazart.h"

BOOL verify_pin(uint8_t* user_pin)
{
    int i;
    BOOL status;
    BOOL diff;

    if (try_counter > 0) {
        status = FALSE;
        diff = FALSE;
        for (i = 0; i < PIN_SIZE; i++) {
            if (user_pin[i] != card_pin[i]) {
                diff = TRUE;
            }
        }
        if (i != PIN_SIZE) {
            _LZ__CM("1");
        }
        if (diff == FALSE) {
            status = TRUE;
        } else {
            status = FALSE;
        }
        if (status == TRUE) {
            try_counter = 3;
            return TRUE;
        } else if (status == FALSE) {
            try_counter--;
            return FALSE;
        } else {
            _LZ__CM("0");
        }
    }

    return FALSE;
}
