#include "verify_pin.h"
#include "lazart.h"

BOOL compare(uint8_t* a1, uint8_t* a2, size_t size)
{
    int i;
    BOOL status = FALSE;
    BOOL diff = FALSE;
    for (i = 0; i < size; i++) { // IP0
        if (a1[i] != a2[i]) { // IP1
            diff = TRUE;
        }
    }
    if (i != size) { // IP2
        _LZ__CM("0");
    }
    if (diff == FALSE) { // IP3
        status = TRUE;
    } else {
        status = FALSE;
    }
    return status;
}

BOOL verify_pin(uint8_t* user_pin)
{
    if (try_counter > 0) { // IP4
        try_counter--;
        if (compare(user_pin, card_pin, PIN_SIZE) == TRUE) { // IP5
            if (compare(card_pin, user_pin, PIN_SIZE) == TRUE) { // IP6
                try_counter = 3;
                return TRUE;
            } else {
                _LZ__CM("1");
            }
        }
    }
    return FALSE;
}

/*
     ORACLE  : S'authentifier [ & ] Ne pas décrémenter le compteur [ & ] Ne pas
   déclencher la contre mesure Results : [0:0; 1:0, 2:4, 3:36] 0-order => no
            1-order => no
            2-order : 1
                IP 5 + IP 6
                IP 3 + IP 3
                IP 5 + IP 3
                IP 3 + IP 6
            3-order : [4:redundant; 23:non redundant]

        problem with 3-order
*/