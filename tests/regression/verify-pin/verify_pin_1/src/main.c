#include "lazart.h"
#include "verify_pin.h"
#include <stdio.h>

int main()
{
    uint8_t user_pin[PIN_SIZE];

    init(user_pin, card_pin);

    bool ret = verify_pin(user_pin);

    _LZ__ORACLE(oracle(ret));

    return 0;
}
