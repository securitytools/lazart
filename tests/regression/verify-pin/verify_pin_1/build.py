#!/usr/bin/python3

from lazart.lazart import *
import glob

# Parse CLI parameters
params = install_script()
params.order = 5 # forced 

ti = ti_model()
breset = data_model({"vars": {"size": 0, "result": 0, "i" : 0}})
data_sym = data_model({"vars": {"size": "__sym__", "result": "__sym__", "i" : "__sym__"}})

# Create analysis or load from path if available
a = read_or_make(glob.glob("src/*.c") + glob.glob("../commons/*.c"), # Source files
    functions_list(["verify_pin", "compare"], ti_model()), # Attack model: test inversion for the two specified function 
    path="results", # Pass in which analysis file will be stored
    flags=AnalysisFlag.EqRedAnalysis, # Attack equivalence and redundancy analysis.
    compiler_args="-Wall -DSYM -DHARDENNED_BOOLEAN -I /opt/lazart/tests/regression/verify-pin/commons/ -DPHI_AUTH", # Compiler arguments
    params=params # Pass CLI params to the analysis
) 

res = execute(a) # Execute analysis, print results and generate report

verify.attack_analysis(a)
verify.traces_parsing(a)


from lazart.tests.regression import generate_regression_test

generate_regression_test(a, res, "../../test-vp1-5f-sym-ti-auth.lzt", "vp1-5f-sym-ti-auth", full_trace=True, full_arr=True, execute={"no_aar":False})