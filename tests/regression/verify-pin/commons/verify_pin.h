// verifyPIN.h
#ifndef VERIFY_PIN_H
#define VERIFY_PIN_H

#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>

typedef int sec_bool_;
typedef enum { sec_true = 0xAA,
    sec_false = 0x55 } sec_bool_t;

#ifndef PIN_SIZE
#define PIN_SIZE 4
#endif
#ifndef TRY_COUNT
#define TRY_COUNT 3
#endif

#ifdef HARDENED_BOOLEAN
#define BOOL sec_bool_t
#define TRUE sec_true
#define FALSE sec_false
#else
#define BOOL bool
#define TRUE true
#define FALSE false
#endif

extern uint8_t try_counter;
extern uint8_t card_pin[PIN_SIZE];

bool oracle(BOOL ret);
void oracle_uevent(BOOL ret);
void init(uint8_t* user_pin, uint8_t* card_pin);

BOOL verify_pin(uint8_t* user_pin);
BOOL compare(uint8_t* a1, uint8_t* a2, size_t size);

#endif