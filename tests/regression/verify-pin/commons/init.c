#include "klee/klee.h"
#include "verify_pin.h"

uint8_t try_counter = TRY_COUNT;
uint8_t card_pin[PIN_SIZE];

#ifdef FULL_SYM
// (eq & !auth ?)
#error not implemented
#elif defined(SYM)
void init(uint8_t* user_pin, uint8_t* card_pin)
{
    try_counter = TRY_COUNT;
    klee_make_symbolic(user_pin, PIN_SIZE, "user_pin");
    klee_make_symbolic(card_pin, PIN_SIZE, "card_pin");

    int equal = (card_pin[0] == user_pin[0]) & (card_pin[1] == user_pin[1]) & (card_pin[2] == user_pin[2]) & (card_pin[3] == user_pin[3]);

    klee_assume(!equal);
}
#else
void init(uint8_t* user_pin, uint8_t* card_pin)
{
    try_counter = TRY_COUNT;
    for (int i = 0; i < PIN_SIZE; ++i) {
        user_pin[i] = 0; // 0000
        card_pin[i] = i + 1; // 1234
    }
}
#endif