echo "Building documentation."
cd python
sphinx-apidoc --implicit-namespaces -M -P --separate -f -l -o source/ ../../src/python-api/src/lazart/ -d 20
ls -a
make clean
make html
cd ../cpp
doxygen Doxyfile
cd ../cpp-api
doxygen Doxyfile