var searchData=
[
  ['raw_777',['raw',['../de/dd6/classlazart_1_1util_1_1_log.html#ac0833c1955fe6dcdc9b65d281e8f6b00',1,'lazart::util::Log']]],
  ['red_778',['red',['../de/dd6/classlazart_1_1util_1_1_log.html#ab154dbc9506f312f3acdcf8375f2b935',1,'lazart::util::Log::red()'],['../d4/db8/namespacelazart_1_1util_1_1term__modifiers_1_1colors.html#a269654767ac0dc4296767b3ecec298ee',1,'lazart::util::term_modifiers::red()'],['../d4/db8/namespacelazart_1_1util_1_1term__modifiers_1_1colors.html#a269654767ac0dc4296767b3ecec298ee',1,'lazart::util::term_modifiers::colors::red()']]],
  ['remove_5funused_5ffunctions_779',['remove_unused_functions',['../dc/d56/namespacelazart_1_1tasks.html#a145df1b4562473d42aba814fe78e3371',1,'lazart::tasks']]],
  ['rename_5fbasic_5fblock_780',['rename_basic_block',['../dc/d56/namespacelazart_1_1tasks.html#a6bdebff9831e5606534d62bbbe4a53e7',1,'lazart::tasks']]],
  ['reset_781',['reset',['../dd/d51/structerror__counter.html#ad20897c5c8bd47f5d4005989bead0e55',1,'error_counter::reset()'],['../de/dd6/classlazart_1_1util_1_1_log.html#aa34729fc1647f413a8acbcd5d52badee',1,'lazart::util::Log::reset()'],['../d8/d70/classlazart_1_1util_1_1optional.html#ad20897c5c8bd47f5d4005989bead0e55',1,'lazart::util::optional::reset()'],['../db/d53/namespacelazart_1_1util_1_1term__modifiers_1_1controls.html#a8098ee0df0afcdf0d0f20ce49da34788',1,'lazart::util::term_modifiers::reset()'],['../db/d53/namespacelazart_1_1util_1_1term__modifiers_1_1controls.html#a8098ee0df0afcdf0d0f20ce49da34788',1,'lazart::util::term_modifiers::controls::reset()']]],
  ['rose_782',['rose',['../d4/db8/namespacelazart_1_1util_1_1term__modifiers_1_1colors.html#aeae0e65bf4dfcf8c38457d59ffd8cf84',1,'lazart::util::term_modifiers::rose()'],['../d4/db8/namespacelazart_1_1util_1_1term__modifiers_1_1colors.html#aeae0e65bf4dfcf8c38457d59ffd8cf84',1,'lazart::util::term_modifiers::colors::rose()']]],
  ['run_783',['run',['../d6/d16/classlazart_1_1mutation_1_1mutation__pass.html#ae65ee1484b8c75f59aae621e4d9c3edb',1,'lazart::mutation::mutation_pass']]],
  ['runonbb_784',['runOnBB',['../d3/d33/classlazart_1_1tasks_1_1sec__swift.html#ad7e364bef1872ce61ae4e1cf77357d6c',1,'lazart::tasks::sec_swift']]]
];
