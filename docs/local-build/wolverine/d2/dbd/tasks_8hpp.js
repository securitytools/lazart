var tasks_8hpp =
[
    [ "TaskType", "d2/dbd/tasks_8hpp.html#a026844c14ab62f42a2e19b54d622609b", [
      [ "AddTrace", "d2/dbd/tasks_8hpp.html#a026844c14ab62f42a2e19b54d622609bad4025420a7e7a03b21baebb7bcd48845", null ],
      [ "RenameBB", "d2/dbd/tasks_8hpp.html#a026844c14ab62f42a2e19b54d622609bab363898880d7e28aa0885fd602b54418", null ]
    ] ],
    [ "add_trace", "d2/dbd/tasks_8hpp.html#a2d01d6f026292586b7d90172e81873e8", null ],
    [ "inline_mutations", "d2/dbd/tasks_8hpp.html#a94224e86257792c4cb35f7accdd4b5d0", null ],
    [ "preprocess", "d2/dbd/tasks_8hpp.html#aa47dabfdba8630121356ec14a023a463", null ],
    [ "preprocess_bb", "d2/dbd/tasks_8hpp.html#a58f7003c3ef631df3f6d99aefa96a26b", null ],
    [ "remove_unused_functions", "d2/dbd/tasks_8hpp.html#a145df1b4562473d42aba814fe78e3371", null ],
    [ "rename_basic_block", "d2/dbd/tasks_8hpp.html#a6bdebff9831e5606534d62bbbe4a53e7", null ]
];