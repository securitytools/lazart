#ifndef MEMCMPS_H
#define MEMCMPS_H

#define SIZE 4

#define TRUE_VAL 0x1234
#define FALSE_VAL 0x5678
#define MASK 0xABCD
#define MASK2 0xF4F4

#include <string.h>

// Program to verify.
int memcmps(char* a, char* b, int len);

#endif // MEMCMPS_H