#include "lazart.h"
#include "memcmps.h"

int oracle(int result)
{
    return result == TRUE_VAL;
}

void initialize_sym(char s1[SIZE], char s2[SIZE])
{
    klee_make_symbolic(s1, SIZE * sizeof(char), "s1");
    klee_make_symbolic(s2, SIZE * sizeof(char), "s2");
}

void check_equals(char s1[SIZE], char s2[SIZE])
{
    int equal = 0;
    for (unsigned int i = 0; i < SIZE; i++) {
        equal += s1[i] == s2[i];
    }
    klee_assume(equal != SIZE);
}

int flip_pred(int value)
{
    return ~value;
}

int magic_pred(int original, int value)
{
    return value == TRUE_VAL;
}

int magic_fct(int original)
{
    return TRUE_VAL;
}
int main()
{
    char s1[SIZE], s2[SIZE];
    initialize_sym(s1, s2);

    int res = memcmps(s1, s2, SIZE);

    _LZ__ORACLE(oracle(res));
    check_equals(s1, s2);

    return 0;
}