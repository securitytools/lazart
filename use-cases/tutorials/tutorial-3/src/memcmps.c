// memcmps.c
#include "memcmps.h"
#include "lazart.h"

int bitflip_pred(int original, int value)
{
    return (original ^ value) == 1;
}

int memcmps(char* a, char* b, int len)
{
    int result = FALSE_VAL;

    if (!memcmp(a, b, len)) // IP0 (DL:len), IP1 (TI)
    {
        result ^= FALSE_VAL ^ MASK; // IP2 (DL:result)
        if (!memcmp(a, b, len)) // IP3 (DL:len), IP4 (TI)
        {
            result ^= MASK2; // IP5 (DL:result)
            if (!memcmp(a, b, len)) // IP6 (DL:len), IP7 (TI)
            {
                result ^= TRUE_VAL ^ MASK; // IP8 (DL:result)
                if (!memcmp(a, b, len)) // IP9 (DL:len), IP10 (TI)
                    result ^= MASK2; // IP 11 (DL:result)
            }
        }
    }

    return result; // IP 12 (DL:result)
}