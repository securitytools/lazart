#!/usr/bin/python3

from lazart.lazart import *

# Parse CLI parameters
params = install_script()


# Data Load symbolic mutation on several programs variable.
data_sym = data_model({"vars": {
                    "len": "__sym__", # len is faulted using symbolic value.
                    "result": "__sym__" # results is faulted using symbolic value.
                }})


# AM
ti = ti_model()
def gen_dl(val): return data_model({"vars": {
    "len": val, 
    "result": val
}})
data_br = gen_dl(0) # Bit reset.
data_fix = gen_dl(4660) # Inject BOOL_TRUE (on result).
data_flip = gen_dl("flip_pred") # Flip.
data_bitflip = gen_dl("__sym__:bitflip_pred") # Bit flip (all, symbolic).
data_magic = gen_dl("__sym__:magic_pred") # TODO
data_magic2 = gen_dl("magic_fct") # TODO
data_sym = gen_dl("__sym__") # Symbolic, unconstrained.

# Attack model: combine DL (`data_sym`) and TI models on `memcmps` function.
am = functions_list(["memcmps"], [ti_model(), data_sym]) 

a = read_or_make(["src/memcmps.c", "src/main.c"], # Source files
    am, # Attack model
    path="results", # Path in which analysis file will be stored
    flags=AnalysisFlag.EqRedAnalysis, # Attack analysis with equivalence & redundancy.
    compiler_args="-Wall", # Compiler arguments
    params=params, # Pass CLI params to the analysis
    #klee_args="--write-kqueries --write-smt2s" # Generate SMT formula for each KTest
) 

#execute(a
execute(a, red_rule=sub_word_rule) # Execute analysis, print results and generate reports. Use sub-word rule (default is prefix rule) for equivalence/redundancy analysis.

# Optional verification of results by Lazart checking for commons errors.
verify.attack_analysis(a) 
verify.traces_parsing(a)

print("--------------")
# Print all minimals traces 
for trace in minimals(a, lambda t: t.order <= 2):
    print(trace.str())

print("--------------")
trace_name = "t68"
t = get_trace_by_name(a, trace_name) # Get specific trace.
# Please note that in case of non-determinism of the DSE (for instance because of rng-based exploration technique), the name of the trace could change between executions. In this example, please be sure that 4-fault analysis is used.
if t != None:
    eq_class = attacks_redundancy_results(a).graph().node(t.name).equivalents # Get list of equivalent node in the graph.

    for node in eq_class: # Print each trace in the equivalence group.
        print(node.get_trace(a).str())
else:
    print(f"cannot find trace {trace_name}")