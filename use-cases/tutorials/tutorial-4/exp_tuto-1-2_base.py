#!/usr/bin/python3

import glob

import lazart.exps.experimentations as exp
import lazart.exps.variants as vrt

from lazart.core.analysis import AnalysisFlag
import lazart.core.attack_model as am


# Define variants parameters.
vl = vrt.VariantsList()

v_sym = vl.add_variant(
    vrt.Variant(["", "-DSYM "], # List of variant's values
                ["fix", "sym"]), # List of values's names
)

MAX_ORDER = 2

campaign = exp.Experimentation(
    vl, # List of variants
    glob.glob("../tutorial-1-2/src/*.c"), # Source files
    am.functions_list(["verify_pin", "compare"], am.ti_model()), # Attack model
    path="campaign/", # Prefix for analysis folders
    flags=AnalysisFlag.AttackAnalysis,
    compiler_args = "-Wall " + v_sym, # Usage of the variant v_sym, value will be concatenated
    max_order=MAX_ORDER, # Faults limit.   
)
print(f"{len(campaign.exps)} analysis created.")

results = campaign.run(exp.run_callback) # Default run callback

exp.print_and_save(campaign, MAX_ORDER, results)