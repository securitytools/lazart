#!/usr/bin/python3

import glob

import lazart.exps.experimentations as exp
import lazart.exps.variants as vrt

from lazart.core.analysis import AnalysisFlag
import lazart.core.attack_model as am
import lazart.analysis.attack_redundancy as ar

import lazart.logs as log

# Define variants parameters.
vl = vrt.VariantsList()

v_sym = vl.add_variant_and_option(
    vrt.Variant(["", "-DSYM "], # List of variant's values
                ["fix", "sym"]), # List of values's names
    # Parser information
    "entries", # Variant name
    "determines if concrete or symbolic entries are used.", # Variant description
    ["Using fixed entries (user_pin=[0000], card_pin=[1234]).", "Using symbolic entries for PINs."], # Description for each value
    '-S', # CLI argument shortcut
    '__all__', # Default value (use '__all__' to use all values)
)

# file version 
v_files = vl.add_variant_and_option(
    vrt.Variant([glob.glob("../../verify-pin/verify_pin_0/src/*.c"), glob.glob("../../verify-pin/verify_pin_1/src/*.c"), glob.glob("../../verify-pin/verify_pin_2/src/*.c"),
    glob.glob("../../verify-pin/verify_pin_3/src/*.c"), glob.glob("../../verify-pin/verify_pin_4/src/*.c"), glob.glob("../../verify-pin/verify_pin_5/src/*.c"), glob.glob("../../verify-pin/verify_pin_6/src/*.c"), glob.glob("../../verify-pin/verify_pin_7/src/*.c")],
            ["vp0", "vp1", "vp2", "vp3", "vp4", "vp5", "vp6", "vp7"]),
    # Parser...
    "version", 
    "the version of the program to be used."
)

# Attack Model
ti = am.ti_model()
data_br = am.data_model({"vars": {"size": 0, "result": 0, "i" : 0, "diff" : 0, "status": 0, "try_counter": 0, "step_counter": 0, "tc_copy": 0}})
data_sym = am.data_model({"vars": {"diff": "__sym__", "status": "__sym__", "result": "__sym__", "step_counter": "__sym__", "tc_copy": "__sym__", "try_counter": "__sym__"}})
v_model = vl.add_variant_and_option(
    vrt.Variant([ti, data_br, [ti, data_br],
        data_sym, [ti, data_sym]],
    ["ti", "set0", "ti_set0", "sym", "ti_sym"]),
    "model", 
    "determine the fault models of the analysis.",
    None, None, # descr & shortcut
    "ti"
)

# Redundancy definition
v_red_rule = vl.add_variant_and_option(
    vrt.Variant([ar.prefix_rule, ar.sub_word_rule],
    ["pre", "sw"]),
    "red_rule",
    "determines the redundancy rule to be used",
    None, None,
    "pre")



params = vl.install_exp_script(vl) # Install Lazart script with variant specific options
log.info(vl)

MAX_ORDER = params.order if hasattr(params, "order") else 2 # Set fault limit if it is not specified by CLI

v_files.add_key_value("functions", lambda v, indexes, exps: ["verify_pin", "compare"] if v.name(indexes, exps) in ["vp0", "vp1", "vp2", "vp5"] else ["verify_pin"])
v_files.add_key_value("hb", lambda v, indexes, exps: "" if v.value(indexes, exps) == "vp0" else "-DHARDENED_BOOLEAN ")

log.info(f"variant combination: {', '.join([str(i) for i in vl.factors()])} => {vl.combinations()}")
campaign = exp.Experimentation(
    vl, # List of variants
    v_files + ["../../verify-pin/commons/oracle.c", "../../verify-pin/commons/init.c"], # Input files
    vrt.VariantLambda(lambda idx, ex: am.functions_list(v_files.relvalue("functions", idx, ex), v_model.value(idx, ex))), # Attack model
    path="campaign/", # Prefix for analysis folders
    flags=AnalysisFlag.EqRedAnalysis,
    compiler_args = "-Wall  -I ../../verify-pin/commons -DPHI_AUTH " + v_sym + v_files.get("hb"), # Usage of the variant v_sym, value will be concatenated
    max_order=MAX_ORDER, # Faults limit.
    execute_params={
        "red_rule": v_red_rule
    },
    params=params,
)
log.info(f"{len(campaign.exps)} analysis created.")

def at_start(a, e, r): 
    print(f"running {a.name()}")
    #log.set_verbosity(log.VerbosityLevel.Debug)
results = campaign.run(exp.run_callback, # Default run callback
                       at_start, # At_start callback
                       None, # At end callback
                       # keyword arguments are be forwarded to each analysis's constructor
                       no_save=True, no_report=True, no_display=True)


exp.print_and_save(campaign, MAX_ORDER, results)