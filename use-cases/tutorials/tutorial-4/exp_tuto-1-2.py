#!/usr/bin/python3

import glob

import lazart.exps.experimentations as exp
import lazart.exps.variants as vrt

from lazart.core.analysis import AnalysisFlag
import lazart.core.attack_model as am

import lazart.logs as log


# Define variants parameters.
vl = vrt.VariantsList()

v_sym = vl.add_variant_and_option(
    vrt.Variant(["", "-DSYM "], # List of variant's values
                ["fix", "sym"]), # List of values's names
    # Parser information
    "entries", # Variant name
    "determines if concrete or symbolic entries are used.", # Variant description
    ["Using fixed entries (user_pin=[0000], card_pin=[1234]).", "Using symbolic entries for PINs."], # Description for each value
    '-S', # CLI argument shortcut
    '__all__', # Default value (use '__all__' to use all values)
)

params = vl.install_exp_script(vl) # Install Lazart script with variant specific options
log.info(vl)

MAX_ORDER = params.order if hasattr(params, "order") else 2 # Set fault limit if it is not specified by CLI

log.info(f"variant combination: {', '.join([str(i) for i in vl.factors()])} => {vl.combinations()}")
campaign = exp.Experimentation(
    vl, # List of variants
    glob.glob("../tutorial-1-2/src/*.c"), # Source files
    am.functions_list(["verify_pin", "compare"], am.ti_model()), # Attack model
    path="campaign/", # Prefix for analysis folders
    flags=AnalysisFlag.AttackAnalysis,
    compiler_args = "-Wall " + v_sym, # Usage of the variant v_sym, value will be concatenated
    max_order=MAX_ORDER, # Faults limit.    
    params=params,
)
log.info(f"{len(campaign.exps)} analysis created.")

results = campaign.run(exp.run_callback, # Default run callback
                       None, # At_start callback
                       None, # At end callback
                       # keyword arguments are be forwarded to each analysis's constructor
                       no_save=True, no_report=True, no_display=True)


exp.print_and_save(campaign, MAX_ORDER, results)