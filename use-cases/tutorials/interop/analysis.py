#!/usr/bin/python3

from lazart.lazart import *
import glob
from lazart.core.compile import external_compilation

params = install_script()

a = read_or_make(glob.glob("src/*.c"), 
    functions_list(["func"], ti_model()),  
    path="results", 
    flags=AnalysisFlag.EqRedAnalysis,
    params=params, 
    wolverine_args="-t mut -t pre"
)
# TMP
from lazart.core.compile import CompileStep, CompileResults, external_compilation
import lazart._internal_.extensions as _ext
import lazart.constants as constants
import datetime, os, shutil
import lazart.logs as log

os.mkdir("results")
shutil.move('main.bc', 'results/main.bc')
external_compilation(a, 'results/main.bc')

execute(a)