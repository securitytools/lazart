#include "lazart.h"
#include <klee/klee.h>
//#include "klee.h"

int func(int i)
{
    if (i % 2 == 0)
        return 2 * i + 1;
    return 2 * i;
}

int main()
{
    int i;
    klee_make_symbolic(&i, sizeof(int), "i");

    int ret = func(i);
    _LZ__ORACLE(ret == (2 * i) | ret == (2 * i + 1));
    return 0;
}