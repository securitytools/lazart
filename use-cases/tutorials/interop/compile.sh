clang -c -g src/main.c /opt/lazart/usr/src/lazart.c -emit-llvm -DLAZART -D_LZ__VERSION=4.0.0 -D_LZ__FAULT_LIMIT=2 -I /home/klee/include -I /opt/lazart/usr/includes -Wall -D_LZ__ATTACKS
mkdir -p results
mv main.bc main_base.bc
llvm-link main_base.bc lazart.bc -o main.bc