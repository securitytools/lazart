// verify_pin.h
#pragma once

#ifndef VERIFY_PIN_H
#define VERIFY_PIN_H

#include <lazart.h>

typedef int sec_bool_;
typedef enum { TRUE = 0xAA,
    FALSE = 0x55 } sec_bool_t;

#define PIN_SIZE 4
#define TRY_COUNT 3

extern int authenticated;
extern int try_counter;

bool verify_pin(uint8_t* user_pin, uint8_t* card_pin);

#endif