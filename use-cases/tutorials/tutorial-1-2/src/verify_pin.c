// verify_pin.c
#include "verify_pin.h"
#include "lazart.h"

sec_bool_t compare(uint8_t* a1, uint8_t* a2, uint8_t size)
{
    int result = TRUE;

    for (int i = 0; i < size; i++) {
        if (a1[i] != a2[i]) {
            result = FALSE;
        }
    }

    return result;
}

bool verify_pin(uint8_t* user_pin, uint8_t* card_pin)
{
    if (try_counter > 0) {
        _LZ__RENAME_BB("precomare");
        if (compare(user_pin, card_pin, PIN_SIZE) == TRUE) {
            _LZ__RENAME_BB("then");
            // Authentication
            try_counter = TRY_COUNT;
            return true;
        } else {
            _LZ__RENAME_BB("else");
            try_counter--;
            return false;
        }
    }

    return false;
}
