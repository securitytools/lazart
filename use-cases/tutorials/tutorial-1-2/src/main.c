#include "lazart.h"
#include "verify_pin.h"

int try_counter;

#ifdef SYM
void initialize(uint8_t* user_pin, uint8_t* card_pin)
{
    try_counter = TRY_COUNT;
    klee_make_symbolic(user_pin, PIN_SIZE, "user_pin");
    klee_make_symbolic(card_pin, PIN_SIZE, "card_pin");

    int equal = 0;
    for (unsigned int i = 0; i < PIN_SIZE; i++) {
        equal += (user_pin[i] == card_pin[i]);
    }
    klee_assume(equal != PIN_SIZE);
}
#else
void initialize(uint8_t* user_pin, uint8_t* card_pin)
{
    try_counter = TRY_COUNT;
    // card PIN = 1 2 3 4...
    for (int i = 0; i < PIN_SIZE; ++i)
        card_pin[i] = i + 1;
    // user PIN = 0 0 0 0...
    for (int i = 0; i < PIN_SIZE; ++i)
        user_pin[i] = 0;
}
#endif

bool oracle(bool ret)
{
    return ret | (try_counter >= TRY_COUNT);
}

int main()
{
    // Initialize entries.
    uint8_t user_pin[PIN_SIZE];
    uint8_t card_pin[PIN_SIZE];
    initialize(user_pin, card_pin);

    bool ret = verify_pin(user_pin, card_pin); // Run studied program.
    _LZ__ORACLE(oracle(ret)); // Attack success predicate.

    return 0;
}
