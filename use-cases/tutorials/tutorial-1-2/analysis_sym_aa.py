#!/usr/bin/python3

from lazart.lazart import *
import glob

# Parse CLI parameters
params = install_script()

# Create analysis or load from path if available
a = read_or_make(["src/verify_pin.c", "src/main.c"], # Source files
    functions_list(["verify_pin", "compare"], ti_model()), # Attack model: test inversion for the two specified functions
    path="results_sym_aa", # Pass in which analysis file will be stored
    flags=AnalysisFlag.AttackAnalysis, # Attack analysis.
    compiler_args="-Wall -DSYM", # Compiler arguments
    params=params, # Pass CLI params to the analysis
)


execute(a) # Execute analysis, print results and generate report

verify.attack_analysis(a)
verify.traces_parsing(a)

