#!/usr/bin/python3

from lazart.lazart import *
import glob

# Parse CLI parameters
params = install_script()

# Create analysis or load from path if available
a = read_or_make(["src/verify_pin.c", "src/main.c"], # Source files
    functions_list(["verify_pin", "compare"], ti_model()), # Attack model: test inversion for the two specified function 
    path="results", # Path in which analysis file will be stored
    flags=AnalysisFlag.AttackAnalysis, # Attack analysis (default)
    compiler_args="-Wall", # Compiler arguments
    params=params # Pass CLI params to the analysis
)

execute(a) # Execute analysis, print results and generate reports

# Optional verification of results by Lazart checking for commons errors.
verify.attack_analysis(a) 
verify.traces_parsing(a)

# Print all traces 
"""for order in range(a.max_order() + 1):
    for trace in traces_list(a, order):
        print(trace.str()) """

