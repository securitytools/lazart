#!/usr/bin/python3

from lazart.lazart import *
import glob

params = install_script()

# Model templates
value = 0 # 1 or __sym__
model_simple = data_model({"vars": {"Cq": value, "Cp": value, "tmp": value}})

model_mid = data_model({
"vars": {
    "tmp": value,
    "Cp": value, "Cq": value,
    "g_q": value, "g_p": value, "g_dq": value, "g_iq": value, "g_M": value
}})

model = data_model({
"vars": {
    "tmp": value, "tmp2": value, "r": value,
    "dp2": value, "dq2": value,
    "Cp": value, "Cq": value, "Cp2": value, "Cq2": value,
    "g_q": value, "g_p": value, "g_dp": value, "g_dq": value,  "g_iq": value, "g_M": value,
    "Cpt": value, "CQt": value, "dpt": value, "dqt": value
}})

a = read_or_make(glob.glob("src/*.c") + ["../commons/oracle.c", "../commons/init.c"], 
    functions_list(["rsa_sign"], model),  
    path="results", 
    flags=AnalysisFlag.AttackAnalysis,
    compiler_args="-Wall -I ../commons",# -D_LZ__MUT_VALUE",
    params=params,
    #klee_args="--search=dfs",#--emit-all-errors"
    #countermeasures=[cm_tm(dict(depth=1, on=["__mut__"]))]

) 

results = execute(a, no_aar=False)

verify.attack_analysis(a)
verify.traces_parsing(a)


