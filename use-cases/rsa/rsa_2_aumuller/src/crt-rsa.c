#include "lazart.h"
#include "rsa.h"

/* Returns: 1 if a != b [n], 0 else. */
#ifdef INLINE
__attribute__((always_inline)) inline data_t
IsNotCongruentTo(data_t a, data_t b, data_t n)
#else
data_t IsNotCongruentTo(data_t a, data_t b, data_t n)
#endif
{
    return (!((((a - b) / n) * n) == (a - b)));
}

/* Returns: a + b. */
data_t Add(data_t a, data_t b) { return a + b; }

/* Returns: a - b. */
data_t Sub(data_t a, data_t b) { return a - b; }

/* Returns: a * b. */
data_t Mul(data_t a, data_t b) { return a * b; }

/* Returns: a mod n. */
#ifdef INLINE
__attribute__((always_inline)) inline data_t Mod(data_t a, data_t n)
#else
data_t Mod(data_t a, data_t n)
#endif
{
    int r = a % n;
    while (r < 0) {
        r = Add(r, n);
    }
    return r;
}

/* Returns: (a * b) mod n. */
#ifdef INLINE
__attribute__((always_inline)) inline data_t MulMod(data_t a, data_t b,
    data_t n)
#else
data_t MulMod(data_t a, data_t b, data_t n)
#endif
{
    int r = 0;
    while (b--) {
        r = Add(r, a);
        r = Mod(r, n);
    }
    return r;
}

/* Right-to-left algorithm, returns: (a ^ b) mod n. */
#ifdef INLINE
__attribute__((always_inline)) inline data_t PowerMod(data_t a, data_t b,
    data_t n)
#else
data_t PowerMod(data_t a, data_t b, data_t n)
#endif
{
    int r = 1;
    a = Mod(a, n);
    while (b > 0) {
        if (b % 2 == 1) {
            r = MulMod(r, a, n);
        }
        if (b >>= 1) {
            a = MulMod(a, a, n);
        }
    }
    return r;
}

#ifdef INLINE_Rsa
__attribute__((always_inline)) inline data_t rsa_sign()
#else
data_t rsa_sign()
#endif
{
    data_t p2, q2, dp2, dq2, Cp2, Cq2, dpt, dqt, Cpt, Cqt, Cp, Cq, S, tmp, tmp2,
        t = 373, rand1 = 401, rand2 = 421; // FIXME Should be random.
    p2 = Mul(g_p, t);

    tmp = Sub(g_p, 1);
    tmp = Mul(rand1, tmp);
    dp2 = Add(g_dp, tmp);

    Cp2 = PowerMod(g_M, dp2, p2);

    tmp2 = Sub(g_p, 1);
    if ((Mod(p2, g_p) != 0) || IsNotCongruentTo(dp2, g_dp, tmp2)) {
        _LZ__CM("0");
    }

    q2 = Mul(g_q, t);

    tmp = Sub(g_q, 1);
    tmp = Mul(rand2, tmp);
    dq2 = Add(g_dq, tmp);

    Cq2 = PowerMod(g_M, dq2, q2);

    tmp2 = Sub(g_q, 1);
    if ((Mod(q2, g_q) != 0) || IsNotCongruentTo(dq2, g_dq, tmp2)) {
        _LZ__CM("1");
    }

    Cp = Mod(Cp2, g_p);
    Cq = Mod(Cq2, g_q);

    tmp = Sub(Cp, Cq);
    tmp = MulMod(tmp, g_iq, g_p);
    tmp = Mul(tmp, g_q);

    S = Add(tmp, Cq);
    if (IsNotCongruentTo(S, Cp2, g_p) || IsNotCongruentTo(S, Cq2, g_q)) {
        _LZ__CM("2");
    }

    Cpt = Mod(Cp2, t);
    Cqt = Mod(Cq2, t);

    tmp = Sub(t, 1);
    dpt = Mod(dp2, tmp);

    tmp = Sub(t, 1);
    dqt = Mod(dq2, tmp);

    tmp = PowerMod(Cpt, dqt, t);
    tmp2 = PowerMod(Cqt, dpt, t);

    if (IsNotCongruentTo(tmp, tmp2, t)) {
        _LZ__CM("3");
        return 0;
    } else {
        return g_sign = S;
    }
}
