#!/usr/bin/python3
import glob
import time 

# from lazart.lazart import *
import lazart.script as script
import lazart.exps.experimentations as exp
import lazart.exps.variants as vrt
import lazart.exps.std_variants as sv
from lazart.core.attack_model import functions_list, ti_model, data_model
from lazart.results.formats import Table
from lazart._internal_.util import str_time
import lazart.core.countermeasures as cm
import lazart.logs as log
from lazart.util.exec import execute
import lazart.results.results as _res
from lazart.analysis.attack_redundancy import feq_rule, eq_rule, prefix_rule, sub_word_rule
from lazart.results.formats import column

# Define variants parameters.
vl = vrt.VariantsList()

# file version 
v_files = vl.add_variant_and_option(
    vrt.Variant([glob.glob("rsa_0/src/*.c"), glob.glob("rsa_1_shamir/src/*.c"), glob.glob("rsa_2_aumuller/src/*.c")],
            ["rsa0", "rsa1", "rsa2"]),
    # Parser...
    "version", 
    "the version of the program used."
)

# Inlining
v_inline = vl.add_variant_and_option(vrt.Variant(["", "-DINLINE"],
    ["ip", "inl"]),
    "inline", "determine if functions are analyzed using inline version..",
    None, None, # descr & shortcut
    "ip") # default

# Attack model
def full_model(value):
    return data_model({"vars": {
        "tmp": value, "tmp2": value, "r": value,
        "dp2": value, "dq2": value,
        "Cp": value, "Cq": value, "Cp2": value, "Cq2": value,
        "g_q": value, "g_p": value, "g_dp": value, "g_dq": value,  "g_iq": value, "g_M": value,
        "Cpt": value, "CQt": value, "dpt": value, "dqt": value
    }})
 
def simple_model(value):
    return data_model({"vars": {"Cq": value, "Cp": value, "tmp": value}})

ti = ti_model()
data_br = simple_model(0)
data_set = simple_model(1)
data_sym = simple_model("__sym__")
full_data_br = full_model(0)
full_data_bs = full_model(1)
full_data_sym = full_model("__sym__")

v_model = vl.add_variant_and_option(
    vrt.Variant([ti, data_br, full_data_br, data_sym, full_data_sym, [ti, full_data_br]],
    ["ti", "rst", "frst", "sym", "full_sym", "ti_frst"]),
    "model", 
    "determine the fault models of the analysis.",
    None, None, # descr & shortcut
    "frst"
)

# Standard variants.
v_sym = sv.entries(vl) # Symbolic inputs (default 'sym').
v_mut_value = sv.mut_value(vl) # Keep injected value (default 'yes').
v_cm = sv.countermeasures(vl) # Automated countermeasures (default 'none').
v_det_mode = sv.det_mode(vl) # Detection mode.
v_flag = sv.aa_type(vl) # Analysis flag.
v_red_rule = sv.red_rule(vl) # Redundancy definition (generate useless variation without EqRedAnalysis).
v_eq_rule = sv.eq_rule(vl) # Equivalence definition (generate useless variation without EqRedAnalysis).
v_eae = sv.eae(vl) # KLEE's error emitting mode.
v_timeout = sv.timeout(vl) # KLEE's timeout (default 'no').
v_strategy = sv.strategy(vl) # KLEE's exploration strategy (default 'std' = random-path interleaved with nurs:covnew)

params = vl.install_exp_script(vl) # Install Lazart script with variant specific options.
log.info(vl)

MAX_ORDER = params.order if hasattr(params, "order") else 2

start = time.process_time()
log.info("variant combination: {0} => {1}".format(", ".join([str(i) for i in vl.factors()]), vl.combinations()))
campaign = exp.Experimentation(
    vl,
    v_files + ["commons/oracle.c", "commons/init.c"],#,
    vrt.VariantLambda(lambda idx, ex: functions_list(["rsa_sign"], v_model.value(idx, ex))),
    path="campaign/", # Prefix for analysis folders.
    flags=v_flag,
    compiler_args = "-Wall -I commons " + v_sym + v_det_mode + v_inline,
    #wolverine_args="-I",
    klee_args=v_eae + v_timeout + v_strategy,
    max_order=MAX_ORDER,
    countermeasures=v_cm,
    params=params,
    execute_params={
        "eq_rule": v_eq_rule,
        "red_rule": v_red_rule
    },
    remove_fix_variants_names=True # don't use name of variant that are fixed.
)
log.debug("{0} analysis created.".format(len(campaign.exps)))

# RUN 
def at_start(a, e, r): print(f"running {a.name()}")
try:
    results = campaign.run(exp.run_callback, at_start, None, no_save=True, no_report=True, no_display=True)
except InterruptedError as e:
    print("interrupted.")
    exit(0)

# DISPLAY
print("\n\n")
dse_time = sum(r[_res.Metrics.TDSE] for r in results) # Compute before stringification.
exp.print_and_save(campaign, MAX_ORDER, results)
total_time =  dse_time + (time.process_time() - start)
print(f"campaign ({len(campaign.exps)}) completed in {str_time(total_time)}.")