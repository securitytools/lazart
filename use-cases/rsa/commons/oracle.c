#include "lazart.h"
#include "rsa.h"

/* Returns: a + b. */
data_t Add2(data_t a, data_t b) { return a + b; }

/* Returns: a - b. */
data_t Sub2(data_t a, data_t b) { return a - b; }

/* Returns: a * b. */
data_t Mul2(data_t a, data_t b) { return a * b; }

/* Returns: a mod n. */
data_t Mod2(data_t a, data_t n)
{
    int r = a % n;
    while (r < 0) {
        r = Add2(r, n);
    }
    return r;
}

/* Returns: (a * b) mod n. */
data_t MulMod2(data_t a, data_t b, data_t n)
{
    int r = 0;
    while (b--) {
        r = Add2(r, a);
        r = Mod2(r, n);
    }
    return r;
}

/* Right-to-left algorithm, returns: (a ^ b) mod n. */
data_t PowerMod2(data_t a, data_t b, data_t n)
{
    int r = 1;
    a = Mod2(a, n);
    while (b > 0) {
        if (b % 2 == 1) {
            r = MulMod2(r, a, n);
        }
        if (b >>= 1) {
            a = MulMod2(a, a, n);
        }
    }
    return r;
}

data_t RsaSign2()
{
    data_t Cp, Cq, tmp;
    Cp = PowerMod2(g_M, g_dp, g_p);
    Cq = PowerMod2(g_M, g_dq, g_q);
    tmp = Sub2(Cp, Cq);
    tmp = MulMod2(tmp, g_iq, g_p);
    tmp = Mul2(tmp, g_q);
    tmp = Add2(tmp, Cq);
    g_sign = tmp;
    return g_sign;
}

bool oracle()
{
    data_t sign = g_sign;
    data_t S = RsaSign2();

    _LZ__EVENT("%d == %d\n", sign, S);
    _LZ__EVENT("p == %d & q == %d\n", g_p, g_q);

    return !_LZ__triggered() & (S != sign) & ((Mod2(S, g_p) == Mod2(sign, g_p)) | ((Mod2(S, g_q) == Mod2(sign, g_q))));
}
