#include "lazart.h"
#include "rsa.h"

/* Returns: 1 if a != b [n], 0 else. */
#ifdef INLINE
__attribute__((always_inline)) inline data_t
IsNotCongruentTo(data_t a, data_t b, data_t n)
#else
data_t IsNotCongruentTo(data_t a, data_t b, data_t n)
#endif
{
    return (!((((a - b) / n) * n) == (a - b)));
}

/* Returns: a + b. */
data_t Add(data_t a, data_t b) { return a + b; }

/* Returns: a - b. */
data_t Sub(data_t a, data_t b) { return a - b; }

/* Returns: a * b. */
data_t Mul(data_t a, data_t b) { return a * b; }

/* Returns: a mod n. */
#ifdef INLINE
__attribute__((always_inline)) inline data_t Mod(data_t a, data_t n)
#else
data_t Mod(data_t a, data_t n)
#endif
{
    int r = a % n;
    while (r < 0) {
        r = Add(r, n);
    }
    return r;
}

/* Returns: (a * b) mod n. */
#ifdef INLINE
__attribute__((always_inline)) inline data_t MulMod(data_t a, data_t b,
    data_t n)
#else
data_t MulMod(data_t a, data_t b, data_t n)
#endif
{
    int r = 0;
    while (b--) {
        r = Add(r, a);
        r = Mod(r, n);
    }
    return r;
}

/* Right-to-left algorithm, returns: (a ^ b) mod n. */
#ifdef INLINE
__attribute__((always_inline)) inline data_t PowerMod(data_t a, data_t b,
    data_t n)
#else
data_t PowerMod(data_t a, data_t b, data_t n)
#endif
{
    int r = 1;
    a = Mod(a, n);
    while (b > 0) {
        if (b % 2 == 1) {
            r = MulMod(r, a, n);
        }
        if (b >>= 1) {
            a = MulMod(a, a, n);
        }
    }
    return r;
}

#ifdef INLINE_Rsa
__attribute__((always_inline)) inline data_t rsa_sign()
#else
data_t rsa_sign()
#endif
{
    data_t p2, q2, Cp, Cq, Cp2, Cq2, tmp, tmp2, S,
        r = 373; // FIXME Should be random
    p2 = Mul(g_p, r);

    tmp = Sub(g_p, 1);
    tmp2 = Sub(r, 1);
    tmp = Mul(tmp, tmp2);
    g_dp = Mod(g_dp, tmp);

    Cp2 = PowerMod(g_M, g_dp, p2);

    q2 = Mul(g_q, r);

    tmp = Sub(g_p, 1);
    tmp2 = Sub(r, 1);
    tmp = Mul(tmp, tmp2);
    g_dq = Mod(g_dq, tmp);

    Cq2 = PowerMod(g_M, g_dq, q2);
    Cp = Mod(Cp2, g_p);
    Cq = Mod(Cq2, g_q);

    tmp = Sub(Cp, Cq);
    tmp = MulMod(tmp, g_iq, g_p);
    tmp = Mul(tmp, g_q);

    S = Add(tmp, Cq);

    if (IsNotCongruentTo(Cp2, Cq2, r)) {
        _LZ__CM("0");
        return 0;
    } else {
        return g_sign = S;
    }
}
