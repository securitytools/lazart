# RSA programs

 - [Wiki](https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/Experimentation-and-use-cases)

The RSA programs implements the RSA cipher (with CRT). Three versions of the program are available:
 - `rsa-0`: basic version.
 - `rsa_1_shamir`: Shamir's Algorithm.
 - `rsa_2_aumuller`: Aumuller's algorithm.

## Folder structure

The `experimentation.py` script allows to run the `RSA` collection using [experimentation module](https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/Experimentation-and-use-cases/#experimentation-module).

Each version of the RSA program is structured as:
 - `src/`: 
   - `main.c`: main analysis file.
   - `crt-rsa.c`: studied program.
 - `results/`: results folder (after analysis execution).
 - `analysis.py`: analysis script (use `-h` for more information).

Additionally, the directory `commons` contains standard headers and source for all versions:
 - `rsa.h`: common header.
 - `init.c`: implements initialization of input (fixed or symbolic version).
 - `oracle.c`: implements attack objectives verification.

## Attack objective

The RSA collection attack objective represents the Bellcore attack corresponding to get a different signature than the expected one, but congruent modulo `p` or `q`.

## Input 

RSA collection use fixed input or symbolic inputs.

In case of symbolic inputs, you should use timeout and test different exploration strategies for KLEE to obtain results.

## Attack model

The RSA collection uses *data load mutation* fault model, and can combine it with *test inversion*.

Symbolic values may require to used timeout and change exploration strategy. 