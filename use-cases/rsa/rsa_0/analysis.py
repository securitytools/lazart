#!/usr/bin/python3

from lazart.lazart import *
import glob

params = install_script()

# Model templates
value = 0 # 0, 1 or __sym__
model_simple = data_model({"inline": False, "vars": {"Cq": value, "Cp": value, "tmp": value}})

model = data_model({"inline": False,
"vars": {
    "tmp": value,
    "Cp": value, "Cq": value,
    "g_q": value, "g_p": value, "g_dq": value, "g_dp": value, "g_iq": value, "g_M": value
}})


a = read_or_make(glob.glob("src/*.c") + ["../commons/oracle.c", "../commons/init.c"], 
    functions_list(["rsa_sign"], model),  
    path="results", 
    flags=AnalysisFlag.AttackAnalysis,
    compiler_args="-Wall -I ../commons",
    params=params,
    add_trace=["__mut__"], #  "PowerMod", "Sub", "Mul", "MulMod", "Add", 
    wolverine_args="-t mut -t pre" #"-I"
    #klee_args="--max-time=100 --watchdog"# --search=bfs"# --search=dfs"#"--search=dfs --max-time=1800",#--emit-all-errors"
    
) 

results = execute(a)

verify.attack_analysis(a)
verify.traces_parsing(a)

print("traces en 1 faute:")
for trace in traces_list(a, 1):
    if trace.termination_is(TerminationType.Correct):
        print(trace.str())

for trace in all_fault_traces(a, a.max_order()):
    pass # print(trace.str())

