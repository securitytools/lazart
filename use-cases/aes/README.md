# AES programs

AES programs collection is split in two folders:
 - `aes`: full AES cipher.
 - `ark`: AES round key.

See the README.md file for each folder.

The `commons` folder contains header and sources used by both.