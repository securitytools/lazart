/**************************************************************************/
/*                                                                        */
/*  This file is part of FISSC.                                           */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 3.0.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 3.0                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

#include "code.h"
#include "code_CFI.h"
#include "commons.h"
#include "interface.h"
#include "types.h"

extern UBYTE g_buf[SIZE];
extern UBYTE g_key[SIZE];
extern UBYTE g_cpk[SIZE];

#ifdef INLINE
inline void aes_addRoundKey_cpy(unsigned short* CNT_0___aes_addRoundKey_cpy) __attribute__((always_inline))
#else
void aes_addRoundKey_cpy(unsigned short* CNT_0___aes_addRoundKey_cpy)
#endif
{
    INCR(*CNT_0___aes_addRoundKey_cpy)
    short i = 16;

    INCR(*CNT_0___aes_addRoundKey_cpy)
    DECL_INIT(CNT_1___aes_addRoundKey_cpywhile, 0)
    INCR(*CNT_0___aes_addRoundKey_cpy)
    DECL_INIT(CNT_1___aes_addRoundKey_cpy_b, 1)
    INCR(*CNT_0___aes_addRoundKey_cpy)
beginCNT_1___aes_addRoundKey_cpywhile0:
    CNT_1___aes_addRoundKey_cpywhile = !(CNT_1___aes_addRoundKey_cpywhile == 0 || CNT_1___aes_addRoundKey_cpywhile == CNT_1___aes_addRoundKey_cpyNBEND0) ? killcard() : 0;
    if (!INCR_COND(CNT_1___aes_addRoundKey_cpy_b, CNT_1___aes_addRoundKey_cpywhile, i--))
        goto endCNT_1___aes_addRoundKey_cpywhile0;
    CHECK_LOOP_INCR(CNT_1___aes_addRoundKey_cpywhile, 1, CNT_1___aes_addRoundKey_cpy_b)
    {
        g_buf[i] ^= g_key[i];
        INCR(CNT_1___aes_addRoundKey_cpywhile)
        g_cpk[i] = g_key[i];
        INCR(CNT_1___aes_addRoundKey_cpywhile)
        g_cpk[16 + i] = g_key[16 + i];
        INCR(CNT_1___aes_addRoundKey_cpywhile)
    }
    goto beginCNT_1___aes_addRoundKey_cpywhile0;
endCNT_1___aes_addRoundKey_cpywhile0:;
    INCR(*CNT_0___aes_addRoundKey_cpy)
    CHECK_END_LOOP(CNT_1___aes_addRoundKey_cpywhile, CNT_1___aes_addRoundKey_cpy_b, 1, "1")
    INCR(*CNT_0___aes_addRoundKey_cpy)
    INCR(*CNT_0___aes_addRoundKey_cpy)
}
