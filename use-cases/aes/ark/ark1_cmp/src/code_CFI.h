#include "lazart.h"
#include <stdlib.h>

// Lalande CM

// Initial counter values
#define CNT_INIT_VP 0 // 4444
#define CNT_INIT_BAC 0 // 12904

#define CNT_1_byteArrayCompareForCount 8
#define CNT_0_byteArrayCompareNBENDFUNC CNT_INIT_BAC + 0
#define CNT_0_verifyPIN_1NBENDFUNC CNT_INIT_VP + 9

// SEQUENTIAL STATEMENTS
//changed from ESORICS
//define DECL_INIT(cnt,val) unsigned short cnt; if ((cnt = val) != val)killcard();
#define DECL_INIT(cnt, val) unsigned short cnt = val;
//changed from ESORICS
//#define DECL_INIT_MAIN(cnt,val) unsigned short * cnt = (unsigned short *)malloc(sizeof(unsigned short)); if ((*cnt = val) != val)killcard();
#define DECL_INIT_MAIN(cnt, val)                                           \
    unsigned short* cnt = (unsigned short*)malloc(sizeof(unsigned short)); \
    *cnt = val;
//changed from ESORICS
//#define INIT(cnt,val)  if ((cnt = val) != val)killcard();
#define INIT(cnt, val) cnt = val;
//unchanged from ESORICS (but never used in light version)
#define CHECK_INCR(cnt, val, cm_id) \
    if (cnt != val)                 \
        _LZ__CM(cm_id);             \
    cnt = cnt + 1;
#define CHECK_INCR_OLD(cnt, val, cm_id) cnt = (cnt == val ? cnt + 1 : _LZ__CM(cm_id));
// IF : unchanged from ESORICS
#define CHECK_END_IF_ELSE(cnt_then, cnt_else, b, x, y)                                      \
    if (!((cnt_then == x && cnt_else == 0 && b) || (cnt_else == y && cnt_then == 0 && !b))) \
        killcard();
#define CHECK_END_IF(cnt_then, b, x, cm_id)               \
    if (!((cnt_then == x && b) || (cnt_then == 0 && !b))) \
        _LZ__CM(cm_id);
// FOR/WHILE STATEMENTS
//changed from ESORICS : no need to check in the if condition
//#define CHECK_INCR_COND(b, cnt, val, cond)  (b = (((cnt)++ != val) ? killcard() : cond))
// #define CHECK_INCR_COND(b, cnt, val, cond)  (b = (((cnt)++ != val) ? _LZ__CM(0) : cond)) LM
// #define CHECK_INCR_COND(b, cnt, val, cond)  ((cnt)++ && (b=(cond))) LM
#define CHECK_INCR_COND(b, cnt, val, cond) ((cnt)++, (b = (cond)))
//to use at end of loops: unchanged from ESORICS
#define CHECK_END_LOOP(cnt_loop, b, val, cm_id) \
    if (!(cnt_loop == val && !b))               \
        _LZ__CM(cm_id);
//changed from ESORICS and to use at the first statement of loops
//#define CHECK_LOOP_INCR(cnt, val, b) cnt = (b && cnt == val ? cnt + 1 : killcard());
#define CHECK_LOOP_INCR(cnt, val, b) cnt = (b ? (cnt + 1) : 0);
// AFTER A FUNCTION CALL : unchanged from ESORICS
#define CHECK_INCR_FUNC(cnt1, x1, cnt2, x2, cm_id) cnt1 = ((cnt1 == x1) && (cnt2 == x2) ? cnt1 + 1 : _LZ__CM(cm_id));
//unchanged from ESORICS
#define INCR(cnt) cnt = cnt + 1;
#define INCR_COND(b, cnt, cond) (++(cnt) && (b = (cond)))
#define RESET_CNT(cnt_while, val, cm_id)     \
    cnt_while = 0;                           \
    if (!cnt_while == 0 || cnt_while == val) \
        _LZ__CM(cm_id);
#define RESET_CNT_OLD(cnt_while, val, cm_id) cnt_while = !(cnt_while == 0 || cnt_while == val) ? _LZ__CM(cm_id) : 0;

#define CNT_1___aes_addRoundKey_cpyNBEND0 5
#define CNT_0___aes_addRoundKey_cpyNBENDFUNC 55
#define CNT_0___mainNBENDFUNC 55
