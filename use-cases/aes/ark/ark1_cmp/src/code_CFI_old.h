#include <stdlib.h>
// SEQUENTIAL STATEMENTS
//changed from ESORICS
//define DECL_INIT(cnt,val) unsigned short cnt; if ((cnt = val) != val)killcard();
#define DECL_INIT(cnt, val) unsigned short cnt = val;
//changed from ESORICS
//#define DECL_INIT_MAIN(cnt,val) unsigned short * cnt = (unsigned short *)malloc(sizeof(unsigned short)); if ((*cnt = val) != val)killcard();
//#define DECL_INIT_MAIN(cnt,val) unsigned short * cnt = (unsigned short *)malloc(sizeof(unsigned short)); *cnt = val;
#define DECL_INIT_MAIN(cnt, val)       \
    unsigned short cnt_STATIC;         \
    unsigned short* cnt = &cnt_STATIC; \
    *cnt = val;
//changed from ESORICS
//#define INIT(cnt,val)  if ((cnt = val) != val)killcard();
#define INIT(cnt, val) cnt = val;
//unchanged from ESORICS (but never used in light version)
#define CHECK_INCR(cnt, val) cnt = (cnt == val ? cnt + 1 : killcard());
// IF : unchanged from ESORICS
#define CHECK_END_IF_ELSE(cnt_then, cnt_else, b, x, y)                                      \
    if (!((cnt_then == x && cnt_else == 0 && b) || (cnt_else == y && cnt_then == 0 && !b))) \
        killcard();
#define CHECK_END_IF(cnt_then, b, x)                      \
    if (!((cnt_then == x && b) || (cnt_then == 0 && !b))) \
        killcard();
// FOR/WHILE STATEMENTS
//changed from ESORICS : no need to check in the if condition
//#define CHECK_INCR_COND(b, cnt, val, cond)  (b = (((cnt)++ != val) ? killcard() : cond))
#define CHECK_INCR_COND(b, cnt, val, cond) (cnt++ && (b = (cond)))
//to use at end of loops: unchanged from ESORICS
#define CHECK_END_LOOP(cnt_loop, b, val) \
    if (!(cnt_loop == val && !b))        \
        killcard();
//changed from ESORICS and to use at the first statement of loops
//#define CHECK_LOOP_INCR(cnt, val, b) cnt = (b && cnt == val ? cnt + 1 : killcard());
#define CHECK_LOOP_INCR(cnt, val, b) cnt = (b ? (cnt + 1) : 0);
// AFTER A FUNCTION CALL : unchanged from ESORICS
#define CHECK_INCR_FUNC(cnt1, x1, cnt2, x2) cnt1 = ((cnt1 == x1) && (cnt2 == x2) ? cnt1 + 1 : killcard());
//unchanged from ESORICS
#define INCR(cnt) cnt = cnt + 1;
#define INCR_COND(b, cnt, cond) (++cnt && (b = (cond)))

#define CNT_1___aes_addRoundKey_cpyNBEND0 5
#define CNT_0___aes_addRoundKey_cpyNBENDFUNC 55
#define CNT_0___mainNBENDFUNC 55
