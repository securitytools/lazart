/**************************************************************************/
/*                                                                        */
/*  This file is part of FISSC.                                           */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 3.0.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 3.0                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

/*$
  @name = aes_addRoundKey_cpy+CFI
  @feature = AES Key Copy
  @fault-model = test-inversion
  @attack-scenario = oracle
  @countermeasure = Control Flow Integrity
  @maintainers = Etienne Boespflug, VERIMAG
  @authors = Ilya O. Levin, Hal Finney, Jean-François Lalande, Pascal Berthomé,
             SERTIF Consortium
  @version 2.2
 */

#include "code.h"
#include "code_CFI.h"
#include "commons.h"
#include "interface.h"
#include "lazart.h"
#include "types.h"

extern UBYTE g_countermeasure;
extern UBYTE g_buf[SIZE];
extern UBYTE g_key[SIZE];
extern UBYTE g_cpk[SIZE];

int main()
{
    initialize();

    DECL_INIT_MAIN(CNT_0___main, 49)
    CHECK_INCR(*CNT_0___main, 49, "2")
    INCR(*CNT_0___main)
    DECL_INIT(CNT_0___aes_addRoundKey_cpy_CALLNB_1, 48)
    INCR(*CNT_0___main)
    aes_addRoundKey_cpy(&CNT_0___aes_addRoundKey_cpy_CALLNB_1);
    CHECK_INCR_FUNC(*CNT_0___main, 52, CNT_0___aes_addRoundKey_cpy_CALLNB_1, CNT_0___aes_addRoundKey_cpyNBENDFUNC, "3")
    INCR(*CNT_0___main)
    CHECK_INCR(*CNT_0___main, 54, "4")

    _LZ__ORACLE(oracle());

    return oracle();
}
