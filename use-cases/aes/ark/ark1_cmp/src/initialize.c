/**************************************************************************/
/*                                                                        */
/*  This file is part of FISSC.                                           */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 3.0.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 3.0                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

#include "commons.h"
#include "interface.h"
#include "types.h"

// global variables definition
UBYTE g_countermeasure;
UBYTE g_cpk[SIZE];
UBYTE g_buf[SIZE];
UBYTE g_key[SIZE];

void initialize()
{
    // local variables
    int i;
    // global variables initialization
    g_countermeasure = 0;
    for (i = 0; i < SIZE; ++i) {
        g_buf[i] = 0;
    }
    for (i = 0; i < SIZE; ++i) {
        g_key[i] = i;
    }
}
