/**************************************************************************/
/*                                                                        */
/*  This file is part of FISSC.                                           */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 3.0.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 3.0                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

#include "commons.h"
#include "interface.h"
#include "lazart.h"
#include "types.h"

extern UBYTE g_countermeasure;
extern UBYTE g_key[SIZE];
extern UBYTE g_cpk[SIZE];

BOOL oracle()
{
    int eq = 1;

    for (int j = 0; j < SIZE; j++)
        if (g_cpk[j] != g_key[j]) {
            eq = 0;
            break;
        }

    return (_LZ__triggered()) & (eq == 0);
}
