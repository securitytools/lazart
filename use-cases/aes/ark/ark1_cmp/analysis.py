#!/usr/bin/python3

from lazart.lazart import *
import glob

params = install_script()

a = read_or_make(glob.glob("src/*.c") , #+ ["../commons/oracle.c", "../commons/init.c"], 
    functions_list(["aes_addRoundKey_cpy"], ti_model()),  
    path="results", 
    flags=AnalysisFlag.AttackAnalysis,
    compiler_args="-Wall -I ../../commons",
    params=params
) 

execute(a)#, no_aar=False, lazy=True)
