#include "commons.h"
#include "interface.h"
#include "lazart.h"
#include "types.h"

void aes_addRoundKey_cpy(void);

extern UBYTE g_countermeasure;
extern UBYTE g_buf[SIZE];
extern UBYTE g_key[SIZE];
extern UBYTE g_cpk[SIZE];

int main()
{
    initialize();
    aes_addRoundKey_cpy();
    _LZ__ORACLE(oracle());
}
