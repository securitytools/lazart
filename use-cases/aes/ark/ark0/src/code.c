/**************************************************************************/
/*                                                                        */
/*  This file is part of FISSC.                                           */
/*                                                                        */
/*  you can redistribute it and/or modify it under the terms of the GNU   */
/*  Lesser General Public License as published by the Free Software       */
/*  Foundation, version 3.0.                                              */
/*                                                                        */
/*  It is distributed in the hope that it will be useful,                 */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU Lesser General Public License for more details.                   */
/*                                                                        */
/*  See the GNU Lesser General Public License version 3.0                 */
/*  for more details (enclosed in the file LICENSE).                      */
/*                                                                        */
/**************************************************************************/

#include "commons.h"
#include "interface.h"
#include "types.h"

extern UBYTE g_buf[SIZE];
extern UBYTE g_key[SIZE];
extern UBYTE g_cpk[SIZE];

void aes_addRoundKey_cpy()
{
    short i = 16;

    while (i--) {
        g_buf[i] ^= g_key[i];
        g_cpk[i] = g_key[i];
        g_cpk[16 + i] = g_key[16 + i];
    }
}
