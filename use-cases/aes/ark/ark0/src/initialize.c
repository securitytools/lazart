#include "commons.h"
#include "interface.h"
#include "types.h"

// global variables definition
UBYTE g_countermeasure;
UBYTE g_cpk[SIZE];
UBYTE g_buf[SIZE];
UBYTE g_key[SIZE];

void initialize()
{
    g_countermeasure = 0;
    for (int i = 0; i < SIZE; ++i) {
        g_buf[i] = 0;
    }
    for (int i = 0; i < SIZE; ++i) {
        g_key[i] = i;
    }
}
