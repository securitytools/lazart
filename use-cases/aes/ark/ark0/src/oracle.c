#include "commons.h"
#include "interface.h"
#include "lazart.h"
#include "types.h"

extern UBYTE g_key[SIZE];
extern UBYTE g_cpk[SIZE];

BOOL oracle()
{
    int eq = 1;
    for (int j = 0; j < SIZE; j++)
        if (g_cpk[j] != g_key[j]) {
            eq = 0;
            break;
        }

    return (!_LZ__triggered()) & (eq == 0);
}
