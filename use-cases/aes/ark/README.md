# ARK programs

 - [Wiki](https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/Experimentation-and-use-cases)

The ARK (AES Round Key) programs implements the add round key step of AES cipher and provides two versions:
 - `ark0`: basic implementation.
 - `ark1_cmp`: protected implementation using [Lalande and al](https://inria.hal.science/hal-01059201/file/llncs.pdf) countermeasure.

## Folder structure

The `experimentation.py` script allows to run the ARK collection using [experimentation module](https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/Experimentation-and-use-cases/#experimentation-module).

Each version of the ARK program is contained in a sub-folder structured as:
 - `src/`: 
   - `main.c`: main analysis file.
   - `code.c`: studied program.
   - `oracle.c`: attack objective verification.
   - `initialize.c`: initialization of the inputs.
   - *additional files for some implementations.*
 - `results/`: results folder (after analysis execution).
 - `analysis.py`: analysis script (use `-h` for more information).

Additionally, the directory `../commons` contains standard headers and definitions for all AES programs.

## Attack objectives

The attack objective is to have an invalid results (`g_key` and `g_cpk` arrays unequal).

## Inputs

ARK programs use fixed input defined in `initialize.c` source file for each version. `g_buf` is filled by '0' and `g_key` is filled by '1'.
Modify definitions in `../commons/` headers to changes parameters.

## Attack models

The ARK programs uses *test inversion* fault model. 