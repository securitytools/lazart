#include "commons.h"
#include "interface.h"
#include "lazart.h"
#include "types.h"

void aes_cipher_result_verified(void);

int main()
{
    initialize();
    aes_cipher_result_verified();
    _LZ__ORACLE(oracle());

    return oracle();
}
