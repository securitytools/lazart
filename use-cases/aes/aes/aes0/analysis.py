#!/usr/bin/python3

from lazart.lazart import *
import glob

params = install_script()

a = read_or_make(glob.glob("src/*.c") , #+ ["../commons/oracle.c", "../commons/init.c"], 
    functions_list(["aes_cipher"], ti_model()),  
    path="results", 
    flags=AnalysisFlag.AttackAnalysis,
    compiler_args="-Wall -DINLINE -I ../../commons",
    params=params
    ,wolverine_args="-t mut"
) 

execute(a, lazy=True)
