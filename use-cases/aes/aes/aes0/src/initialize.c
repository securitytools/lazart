#include "commons.h"
#include "interface.h"
#include "types.h"

UBYTE rk[176]; // Round keys
UBYTE* pt_rk; // pointer on rk
UBYTE aes_state[16];
UBYTE ciphertext[16];

extern void aes_key_expansion(void);

void initialize(void)
{
    //Test vector from FIPS 197
    UBYTE test_key[16] = { 0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c };
    UBYTE test_pln[16] = { 0x32, 0x43, 0xf6, 0xa8, 0x88, 0x5a, 0x30, 0x8d, 0x31, 0x31, 0x98, 0xa2, 0xe0, 0x37, 0x07, 0x34 };

    // global variables initialization
    for (ULONG i = 0; i < 16; i++) {
        rk[i] = test_key[i];
    }

    for (ULONG i = 0; i < 16; i++) {
        aes_state[i] = test_pln[i];
    }
    aes_key_expansion();
    pt_rk = rk;
}
