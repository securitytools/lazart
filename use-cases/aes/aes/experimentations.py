#!/usr/bin/python3
import glob
import time 

# from lazart.lazart import *
import lazart.script as script
import lazart.exps.experimentations as exp
import lazart.exps.variants as vrt
import lazart.exps.std_variants as sv
from lazart.core.attack_model import functions_list, ti_model, data_model
from lazart._internal_.util import str_time
import lazart.logs as log
from lazart.util.exec import execute
import lazart.results.results as _res
from lazart.analysis.attack_redundancy import feq_rule, eq_rule, prefix_rule, sub_word_rule
from lazart.results.formats import column

# Define variants parameters.
vl = vrt.VariantsList()

# file version 
v_files = vl.add_variant_and_option(
    vrt.Variant([glob.glob("aes0/src/*.c"), glob.glob("aes1_cmp/src/*.c")],
            ["aes0", "aes1"]),
    # Parser...
    "version", 
    "the version of the program used."
)

v_files.add_key_value("functions", lambda v, indexes, exps: ["aes_cipher"] if v.name(indexes, exps) in ["aes0"] else ["aes_cipher_result_verified"])


# Inlining
v_inline = vl.add_variant_and_option(vrt.Variant(["", "-DINLINE"],
    ["ip", "inl"]),
    "inline", "determine if functions are analyzed using inline version..",
    None, None, # descr & shortcut
    "inl") # default

# Attack model
# TODO: data 
ti = ti_model()

v_model = vl.add_variant_and_option(
    vrt.Variant([ti],
    ["ti"]),
    "model", 
    "determine the fault models of the analysis.",
    None, None, # descr & shortcut
    "ti"
)

# Standard variants.
#v_mut_value = sv.mut_value(vl) # Keep injected value (default 'yes').
v_cm = sv.countermeasures(vl) # Automated countermeasures (default 'none').
v_det_mode = sv.det_mode(vl) # Detection mode.
v_flag = sv.aa_type(vl) # Analysis flag.
v_red_rule = sv.red_rule(vl) # Redundancy definition (generate useless variation without EqRedAnalysis).
v_eq_rule = sv.eq_rule(vl) # Equivalence definition (generate useless variation without EqRedAnalysis).
v_eae = sv.eae(vl) # KLEE's error emitting mode.
v_timeout = sv.timeout(vl) # KLEE's timeout (default 'no').
v_strategy = sv.strategy(vl) # KLEE's exploration strategy (default 'std' = random-path interleaved with nurs:covnew)

params = vl.install_exp_script(vl) # Install Lazart script with variant specific options.
log.info(vl)

MAX_ORDER = params.order if hasattr(params, "order") else 2

start = time.process_time()
log.info("variant combination: {0} => {1}".format(", ".join([str(i) for i in vl.factors()]), vl.combinations()))
campaign = exp.Experimentation(
    vl,
    v_files,#,
    vrt.VariantLambda(lambda idx, ex: functions_list(v_files.relvalue("functions", idx, ex), v_model.value(idx, ex))),
    #functions_list(["aes_cipher"], ti_model()),
    #vrt.VariantLambda(lambda idx, ex: functions_list(["rsa_sign"], v_model.value(idx, ex))),
    path="campaign/", # Prefix for analysis folders.
    flags=v_flag,
    compiler_args = "-Wall -I ../commons " + v_det_mode + v_inline,
    #wolverine_args="-I",
    klee_args=v_eae + v_timeout + v_strategy,
    max_order=MAX_ORDER,
    countermeasures=v_cm,
    params=params,
    execute_params={
        "eq_rule": v_eq_rule,
        "red_rule": v_red_rule
    },
    remove_fix_variants_names=True # don't use name of variant that are fixed.
)
log.debug("{0} analysis created.".format(len(campaign.exps)))

# RUN 
def at_start(a, e, r): print(f"running {a.name()}")
try:
    results = campaign.run(exp.run_callback, at_start, None, no_save=True, no_report=True, no_display=True)
except InterruptedError as e:
    print("interrupted.")
    exit(0)

# DISPLAY
print("\n\n")
dse_time = sum(r[_res.Metrics.TDSE] for r in results) # Compute before stringification.
exp.print_and_save(campaign, MAX_ORDER, results)
total_time =  dse_time + (time.process_time() - start)
print(f"campaign ({len(campaign.exps)}) completed in {str_time(total_time)}.")