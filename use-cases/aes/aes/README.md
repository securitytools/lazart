# AES programs

 - [Wiki](https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/Experimentation-and-use-cases)

The AES programs implements AES cipher with two versions:
 - `aes0`: basic implementation.
 - `aes1_cmp`: protected implementation using a second calculus and comparison.

## Folder structure

The `experimentation.py` script allows to run the AES collection using [experimentation module](https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/Experimentation-and-use-cases/#experimentation-module).

Each version of the AES program is contained in a sub-folder structured as:
 - `src/`: 
   - `main.c`: main analysis file.
   - `code.c`: studied program.
   - `oracle.c`: attack objective verification.
   - `initialize.c`: initialization of the inputs.
 - `results/`: results folder (after analysis execution).
 - `analysis.py`: analysis script (use `-h` for more information).

Additionally, the directory `../commons` contains standard headers and definitions for all AES programs.

## Attack objectives

The attack objective is to have a result (`aes_state`) that is different than the expected one (hardcoded `test_out` in `commons/oracle.c`)

## Inputs

AES programs use fixed input defined in `initialize.c` source file for each version.
Modify definitions in `../commons/` headers to changes parameters.

## Attack models

The AES programs uses *test inversion* fault model. 