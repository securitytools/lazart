#!/usr/bin/python3

from lazart.lazart import *
import glob

# Parse CLI parameters
params = install_script()

ti = ti_model()
breset = data_model({"vars": {"size": 0, "result": 0, "i" : 0}})
data_sym = data_model({"vars": {"0": "__sym__", "result": "__sym__", "i" : "0"}})
data_sym_full = data_model({"vars": {"size": "__sym__", "result": "__sym__", "i" : "__sym__"}})

# Create analysis or load from path if available
a = read_or_make(glob.glob("src/*.c") + glob.glob("../commons/*.c"), # Source files
    functions_list(["verify_pin","compare"], [ti]), # Attack model: test inversion for the two specified functions 
    path="results", # Pass in which analysis file will be stored
    flags=AnalysisFlag.EqRedAnalysis, # Attack analysis with equivalence & redundancy.
    compiler_args="-Wall -DSYM -DHARDENED_BOOLEAN -I ../commons -DPHI_AUTH", # Compiler arguments
    params=params, # Pass CLI params to the analysis
    wolverine_args="-t mut" # Wolverine's parameters (trace mutation).
) 

execute(a) # Execute analysis, print results and generate report

# Optional verification of results by Lazart checking for commons errors.
verify.attack_analysis(a)
verify.traces_parsing(a)

# Print all traces.
for t in all_traces(a, a.max_order()):
    if t.order == 0 and t.termination_is(TerminationType.Correct):
        print(t.str())
