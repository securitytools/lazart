// verifyPIN.c
#include "verify_pin.h"
#include "lazart.h"

BOOL verify_pin(uint8_t* user_pin)
{
    int i;
    BOOL status;
    BOOL diff;

    if (try_counter > 0) { // IP
        try_counter--;

        // ByteArrayCompare -> START
        status = FALSE;
        diff = FALSE;
        for (i = 0; i < PIN_SIZE; i++) { // IP1
            if (user_pin[i] != card_pin[i]) { // IP2
                diff = TRUE;
            }
        }
        if (i != PIN_SIZE) { // IP3
            _LZ__CM("0");
        }
        // Double test
        if (diff == FALSE) { // IP4
            if (FALSE == diff) { // IP5
                status = TRUE;
            } else {
                _LZ__CM("1");
            }
        } else {
            status = FALSE;
        }
        // ByteArrayCompare -> END

        // Double test
        if (status == TRUE) { // IP6
            if (TRUE == status) { // IP7
                try_counter = 3;
                return TRUE;
            } else {
                _LZ__CM("2");
            }
        }
    }

    return FALSE;
}