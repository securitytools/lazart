#!/usr/bin/python3

from lazart.lazart import *
import glob

# Parse CLI parameters
params = install_script()

ti = ti_model()
breset = data_model({"vars": {"size": 0, "result": 0, "i" : 0}})
data_sym = data_model({"vars": {"size": "__sym__", "result": "__sym__", "i" : "__sym__"}})


# Create analysis or load from path if available
a = read_or_make(glob.glob("src/*.c") + glob.glob("../commons/*.c"), # Source files
    functions_list(["verify_pin"], ti), # Attack model: test inversion for the specified function 
    path="results", # Pass in which analysis file will be stored
    flags=AnalysisFlag.EqRedAnalysis, # Attack analysis with equivalence & redundancy.
    compiler_args="-Wall -DHARDENED_BOOLEAN -I ../commons -DPHI_AUTH", # Compiler arguments
    params=params, # Pass CLI params to the analysis
    wolverine_args="-t mut" # Wolverine's parameters (trace mutation).
) 

execute(a) # Execute analysis, print results and generate report

# Optional verification of results by Lazart checking for commons errors.
verify.attack_analysis(a)
verify.traces_parsing(a)

# Print all traces.
for t in all_traces(a, a.max_order()):
    pass #print(t.str())
