# verify_pin programs

 - [Wiki](https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/Experimentation-and-use-cases)

The verify_pin programs implements the PIN verification process of a smart-card. A try counter try_counter is verified and the two PINs user_pin and card_pin are compared (in the function compare). Several variation of the verify_pin program are available, implementing different countermeasures:

* _HB_: hardened boolean.
* _FTL_: fixed time loop.
* _INL_: inlining of the compare function.
* _DTC_: anticipated `try_counter` decrement.
* _TCBK_: duplication of the `try_counter`.
* _CD_: double call to `compare` function.
* _TD_: test duplication.
* _SC_: step counter.

The following table shows the variation of the `verify_pin` program and the implemented countermeasures.

| Version | HB | FTL | INL | DTC | TCBK | CD | TD | SC |
|---------|----|-----|-----|-----|------|----|----|----|
| vp0     | \- | \- | \- | \- | \- | \- | \- | \- |
| vp1     | ✓  | \- | \- | \- | \- | \- | \- | \- |
| vp2     | ✓  | ✓  | \- | \- | \- | \- | \- | \- |
| vp3     | ✓  | ✓  | ✓  | \- | \- | \- | \- | \- |
| vp4     | ✓  | ✓  | \- | ✓  | ✓  | \- | \- | \- |
| vp5     | ✓  | ✓  | \- | ✓  | \- | ✓  | \- | \- |
| vp6     | ✓  | ✓  | ✓  | ✓  | \- | \- | ✓  | \- |
| vp7     | ✓  | ✓  | ✓  | ✓  | \- | \- | ✓  | ✓  |

## Folder structure

The `experimentation.py` script allows to run the `verify_pin` collection using [experimentation module](https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/Experimentation-and-use-cases/#experimentation-module).

Each version of the `verify_pin` program is contained in a `verify_pin_N` sub-folder structured as:
 - `src/`: 
   - `main.c`: main analysis file.
   - `verify_pin.c`: studied program.
 - `results/`: results folder (after analysis execution).
 - `analysis.py`: analysis script (use `-h` for more information).

Additionally, the directory `commons` contains standard headers and source for all `verify_pin` version:
 - `verify_pin.h`: common header.
 - `init.c`: implements initialization of input (fixed or symbolic version).
 - `oracle.c`: implements attack objectives verification.

## Attack objectives

The `verify_pin` programs supports several attacks objectives, that can be specified using the define macros. The following predicate are studied:
 - *Detection of the attack* (`det`).
 - *Being authenticated* (`auth`).
 - *Being authenticated or return a value that is not `FALSE`* (`nf`).
 - *Do not decrement the `try_counter`* (`ptc`).

By default `det` is always checked. If `PHI_NO_DET` is defined, blocked attacks are considered as attacks.
Predicates `nf`, `auth` and `ptc` can be combined using the following defines (see `commons/oracle.c`):
 - `PHI_AUTH`: being authenticated (`phi = auth`).
 - `PHI_NF`: return a value that is not `FALSE` (`phi = nf`).
 - `PHI_PTC`: do not decrement the `try_counter` (`phi = ptc`).
 - `PHI_AUTH_PTC`: being authenticated **and** do not decrement the `try_counter` (`phi = auth & ptc`).
 - `PHI_AUTH_PTC_OR`: being authenticated **or** do not decrement the `try_counter`  (`phi = auth | ptc`).
 - `PHI_NF_PTC`: return a value that is not `FALSE` **and** do not decrement the `try_counter` (`phi = nf & ptc`).
 - `PHI_NF_PTC_OR`: return a value that is not `FALSE` **or** do not decrement the `try_counter`  (`phi = nf | ptc`).

## Inputs

The two entry arrays `user_pin` and `card_pin` can be initialized (see `commons/init.c`) using *fixed* or *symbolic* entry.

Default mode is *fixed entry* where `user_pin = "0000"` and `card_pin = "1234"`.

Symbolic mode is defined with the macro `SYM` and use symbolic array constrained to be unequal on at least one byte.

The macro `PIN_SIZE` allows to specify the size of input arrays (by default `4`) and the macro `TRY_COUNT` specifies the number of failed authentication allowed (by default `3`).

## Attack models

The `verify_pin` collection uses a combination of *test inversion* and *data load mutation* fault models. 

*Test inversion* can be applied on all functions of the program, i.e. `verify_pin` and `compare` (for some versions).

*Data load mutation* fault model focuses on variables `result`, and `try_counter`, and for some version additionally `diff`, `status`, `step_counter` and `tc_copy`.
The variables `size` and `i` are can also be attacked but are involved in loop and can thus lead to path explosion when using high or symbolic values.
