#include "verify_pin.h"
#include "lazart.h"

BOOL compare(uint8_t* a1, uint8_t* a2, size_t size)
{
    int i;
    for (i = 0; i < size; i++) {
        if (a1[i] != a2[i]) {
            return FALSE;
        }
    }
    return TRUE;
}

BOOL verify_pin(uint8_t* user_pin)
{
    if (try_counter > 0) {
        BOOL comp = compare(user_pin, card_pin, PIN_SIZE);
        if (comp == TRUE) {
            try_counter = TRY_COUNT;
            return TRUE;
        } else if (comp == FALSE) {
            try_counter--;
            return FALSE;
        } else {
            _LZ__CM("1");
        }
    }
    return FALSE;
}
