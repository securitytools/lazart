#include "verify_pin.h"
#include "lazart.h"

BOOL compare(uint8_t* a1, uint8_t* a2, size_t size)
{
    int i;
    for (i = 0; i < size; i++) {
        if (a1[i] != a2[i]) {
            return FALSE;
        }
    }
    return TRUE;
}

BOOL verify_pin(uint8_t* user_pin)
{
    if (try_counter > 0) {
        if (compare(user_pin, card_pin, PIN_SIZE)) {
            try_counter = TRY_COUNT;
            return TRUE;
        } else {
            try_counter--;
            return FALSE;
        }
    }
    return FALSE;
}