#!/usr/bin/python3

from lazart.lazart import *
import glob

# Parse CLI parameters
params = install_script()

ti = ti_model()
data_br_simple = data_model({"vars": {"size": 0, "result": 0, "i" : 0}})
data_br = data_model({"vars": {"size": 0, "result": 0, "i" : 0, "diff" : 0, "status": 0, "try_counter": 0, "step_counter": 0, "tc_copy": 0}})
data_sym = data_model({"vars": {"diff": "__sym__", "status": "__sym__", "result": "__sym__", "step_counter": "__sym__", "tc_copy": "__sym__", "try_counter": "__sym__"}})

# Create analysis or load from path if available
a = read_or_make(glob.glob("src/*.c") + glob.glob("../commons/*.c"), # Source files
    functions_list(["verify_pin"], [ti]), # Attack model: test inversion for the two specified function 
    path="results", # Pass in which analysis file will be stored
    flags=AnalysisFlag.EqRedAnalysis, # Attack analysis with equivalence & redundancy.
    compiler_args="-Wall -DSYM -DHARDENED_BOOLEAN -I ../commons -DPHI_AUTH", # Compiler arguments
    params=params, # Pass CLI params to the analysis
    wolverine_args="-t mut" # Wolverine's parameters (trace mutation).
) 

execute(a) # Execute analysis, print results and generate report

# Optional verification of results by Lazart checking for commons errors.
verify.attack_analysis(a)
verify.traces_parsing(a)

# Print all traces.
for t in all_traces(a, a.max_order()):
    pass #print(t.str())
