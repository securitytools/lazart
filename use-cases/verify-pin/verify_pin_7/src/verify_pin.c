#include "verify_pin.h"
#include "lazart.h"

BOOL verify_pin(uint8_t* user_pin)
{
    int step_counter = 0;
    int i;
    BOOL status;
    BOOL diff;

    if (try_counter > 0) { // IP0
        step_counter++;
        if (step_counter != 1) { // IP1 Countermeasure if stepCounter haven't been incremented.
            _LZ__CM("0");
        }
        try_counter--;
        step_counter++;
        if (step_counter != 2) { // IP2 Countermeasure if stepCounter haven't been incremented twice
            _LZ__CM("1");
        }

        status = FALSE;
        diff = FALSE;

        step_counter++;
        if (step_counter != 3) { // IP3 Countermeasure if stepCounter haven't been incremented third
            _LZ__CM("2");
        }

        for (i = 0; i < PIN_SIZE; i++) { // IP4
            if (user_pin[i] != card_pin[i]) { // IP5
                diff = TRUE;
            }
            step_counter++;
            if (step_counter != i + 4) { // IP6
                _LZ__CM("3");
            }
        }
        step_counter++;
        if (step_counter != 4 + PIN_SIZE) { // IP7
            _LZ__CM("12");
        }
        if (i != PIN_SIZE) { // IP8
            _LZ__CM("4");
        }
        if (diff == FALSE) { // IP9
            if (FALSE == diff) { // IP10
                status = TRUE;
            } else {
                _LZ__CM("5");
            }
        } else {
            status = FALSE;
        }
        step_counter++;
        if (step_counter != 5 + PIN_SIZE) { // IP11
            _LZ__CM("6");
        }

        if (status == TRUE) { // IP12
            step_counter++;
            if (step_counter != 6 + PIN_SIZE) { // IP13
                _LZ__CM("7");
            }
            if (TRUE == status) { // IP14
                step_counter++;
                if (step_counter != 7 + PIN_SIZE) { // IP14
                    _LZ__CM("8");
                }
                try_counter = 3;
                step_counter++;
                if (step_counter != 8 + PIN_SIZE) { // IP15
                    _LZ__CM("9");
                }
                return TRUE;
            } else {
                _LZ__CM("10");
            }
        } else {
            _LZ__CM("11");
        }
    }

    return FALSE;
}
