#!/usr/bin/python3

import glob
import time 

# from lazart.lazart import *
import lazart.script as script
import lazart.exps.experimentations as exp
import lazart.exps.variants as vrt
import lazart.exps.std_variants as sv
from lazart.core.attack_model import functions_list, ti_model, data_model
from lazart._internal_.util import str_time
import lazart.logs as log
import lazart.results.results as _res
from lazart.analysis.attack_redundancy import feq_rule, eq_rule, prefix_rule, sub_word_rule

# Define variants parameters.
vl = vrt.VariantsList()

# Oracle
v_oracle = vl.add_variant_and_option(
    # Values
    vrt.Variant(["-DPHI_AUTH_PTC ", "-DPHI_AUTH ", "-DPHI_PTC ", "-DPHI_AUTH_PTC_OR "],
    ["and", "auth", "ptc", "or"]),
    # Parser...
    "oracle", 
    "the attack objective define used for compiler arg.",
    None, None, # descr & shortcut
    "auth"
    )

# file version 
v_files = vl.add_variant_and_option(
    vrt.Variant([glob.glob("verify_pin_0/src/*.c"), glob.glob("verify_pin_1/src/*.c"), glob.glob("verify_pin_2/src/*.c"),
    glob.glob("verify_pin_3/src/*.c"), glob.glob("verify_pin_4/src/*.c"), glob.glob("verify_pin_5/src/*.c"), glob.glob("verify_pin_6/src/*.c"), glob.glob("verify_pin_7/src/*.c")],
            ["vp0", "vp1", "vp2", "vp3", "vp4", "vp5", "vp6", "vp7"]),
    # Parser...
    "version", 
    "the version of the program to be used."
)

v_files.add_key_value("functions", lambda v, indexes, exps: ["verify_pin", "compare"] if v.name(indexes, exps) in ["vp0", "vp1", "vp2", "vp5"] else ["verify_pin"])
v_files.add_key_value("hb", lambda v, indexes, exps: "" if v.value(indexes, exps) == "vp0" else "-DHARDENED_BOOLEAN ")

# Attack model
ti = ti_model()
data_br = data_model({"vars": {"size": 0, "result": 0, "i" : 0, "diff" : 0, "status": 0, "try_counter": 0, "step_counter": 0, "tc_copy": 0}})
data_sym = data_model({"vars": {"diff": "__sym__", "status": "__sym__", "result": "__sym__", "step_counter": "__sym__", "tc_copy": "__sym__", "try_counter": "__sym__"}})
v_model = vl.add_variant_and_option(
    vrt.Variant([ti, data_br, [ti, data_br],
        data_sym, [ti, data_sym]],
    ["ti", "set0", "ti_set0", "sym", "ti_sym"]),
    "model", 
    "determine the fault models of the analysis.",
    None, None, # descr & shortcut
    "ti"
)

# Standard variants.
v_sym = sv.entries(vl) # Symbolic inputs (default 'sym').
v_size = sv.int_range(vl, [4, 2, 3, 5, 6, 10], "pin_size", "Size of the PIN code.", value_fmt="-DPIN_SIZE={} ") # PIN size (default '4').
v_mut_value = sv.mut_value(vl) # Keep injected value (default 'yes').
v_cm = sv.countermeasures_tm(vl) # Automated countermeasures (default 'none').
v_det_mode = sv.det_mode(vl) # Detection mode.
v_flag = sv.aa_type(vl) # Analysis flag.
v_red_rule = sv.red_rule(vl) # Redundancy definition (generate useless variation without EqRedAnalysis).
v_eq_rule = sv.eq_rule(vl) # Equivalence definition (generate useless variation without EqRedAnalysis).
v_eae = sv.eae(vl) # KLEE's error emitting mode.

params = vl.install_exp_script(vl) # Install Lazart script with variant specific options.
log.info(vl)

MAX_ORDER = params.order if hasattr(params, "order") else 2

start = time.process_time()
log.info("variant combination: {0} => {1}".format(", ".join([str(i) for i in vl.factors()]), vl.combinations()))
campaign = exp.Experimentation(
    vl,
    v_files + ["commons/oracle.c", "commons/init.c"], # Input files.
    vrt.VariantLambda(lambda idx, ex: functions_list(v_files.relvalue("functions", idx, ex), v_model.value(idx, ex))),
    path="campaign/", # Prefix for analysis folders.
    flags=v_flag,
    compiler_args = "-Wall -I commons " + v_oracle + v_files.get("hb") + v_sym + v_det_mode + v_size,
    #wolverine_args="-I",
    klee_args=v_eae,
    max_order=MAX_ORDER,
    countermeasures=v_cm,
    params=params,
    execute_params={
        "eq_rule": v_eq_rule,
        "red_rule": v_red_rule
    },
    remove_fix_variants_names=True # don't use name of variant that are fixed.
)
log.debug("{0} analysis created.".format(len(campaign.exps)))

# RUN 
def at_start(a, e, r): print(f"running {a.name()}")
try:
    results = campaign.run(exp.run_callback, at_start, None, no_save=True, no_report=True, no_display=True)
except InterruptedError as e:
    print("interrupted.")
    exit(0)

# DISPLAY
print("\n\n")
dse_time = sum(r[_res.Metrics.TDSE] for r in results) # Compute before stringification.
exp.print_and_save(campaign, MAX_ORDER, results)
total_time =  dse_time + (time.process_time() - start)
print(f"campaign ({len(campaign.exps)}) completed in {str_time(total_time)}.")