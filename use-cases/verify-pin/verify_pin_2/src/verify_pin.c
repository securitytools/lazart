#include "verify_pin.h"
#include "lazart.h"

BOOL compare(uint8_t* a1, uint8_t* a2, size_t size)
{
    int i;
    BOOL status = FALSE;
    BOOL diff = FALSE;
    for (i = 0; i < size; i++) { // IP : 0
        if (a1[i] != a2[i]) { // IP : 1
            diff = TRUE;
        }
    }
    if (i != size) { // IP : 2
        _LZ__CM("1");
    }
    if (diff == FALSE) { // IP : 3
        status = TRUE;
    } else {
        status = FALSE;
    }
    return status;
}

BOOL verify_pin(uint8_t* user_pin)
{
    int comp;
    if (try_counter > 0) { // IP : 4
        comp = compare(user_pin, card_pin, PIN_SIZE);
        if (comp == TRUE) { // IP : 5
            try_counter = 3;
            return TRUE;
        } else if (comp == FALSE) {
            try_counter--;
            return FALSE;
        } else {
            _LZ__CM("0");
        }
    }
    return FALSE;
}