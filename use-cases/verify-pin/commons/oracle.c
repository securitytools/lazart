#include "lazart.h"
#include "verify_pin.h"
#include <stdio.h>

bool oracle_auth(BOOL ret) { return ret == TRUE; }

bool oracle_not_false(BOOL ret) { return ret != FALSE; }

bool oracle_ptc() { return try_counter >= TRY_COUNT; }

void oracle_uevent(BOOL ret)
{
    if (oracle_auth(ret)) {
        _LZ__EVENT("AUTH");
    }
    if (oracle_ptc()) {
        _LZ__EVENT("PTC");
    }
    if (oracle_not_false(ret)) {
        _LZ__EVENT("NF");
    }
}

bool oracle_cm()
{
#ifdef PHI_NO_DET
    return true;
#else
    return !_LZ__triggered();
#endif
}

#ifdef PHI_AUTH
bool oracle(BOOL ret)
{
    return oracle_auth(ret) & (oracle_cm());
}
#elif defined(PHI_NF)
bool oracle(BOOL ret)
{
    return (oracle_not_false(ret)) & (oracle_cm());
}
#elif defined(PHI_PTC)
bool oracle(BOOL ret)
{
    return (oracle_ptc()) & (oracle_cm());
}
// auth and ptc
#elif defined(PHI_AUTH_PTC)
bool oracle(BOOL ret)
{
    return (oracle_auth(ret) & oracle_ptc()) & (oracle_cm());
}
#elif defined(PHI_AUTH_PTC_OR)
bool oracle(BOOL ret)
{
    return (oracle_auth(ret) | oracle_ptc()) & (oracle_cm());
}
#elif defined(PHI_NF_PTC)
bool oracle(BOOL ret)
{
    return (oracle_not_false(ret) & oracle_ptc()) & (oracle_cm());
}
#elif defined(PHI_NF_PTC_OR)
bool oracle(BOOL ret)
{
    return (oracle_not_false(ret) | oracle_ptc()) & (oracle_cm());
}

#else
#error "No oracle specified"
#endif