#include "firmware_updater.h"
#include <stdlib.h>

void dump_mem()
{
    for (int i = 0; i < (NW_SIZE() * 2); ++i) {
        printf("%2x ", memory[i]);
        if (i % 3 == 2)
            printf("\n");
    }
    printf("\n");
}

uint8_t compute_checksum(page_t* page_buffer)
{
    // Fixed size.
    return page_buffer->t[0] ^ page_buffer->t[1] ^ page_buffer->t[2];
}

byte_t memory[NW_SIZE() * 2] = { 0 };
byte_t network[NW_SIZE()] = {
    0xDE, 0xAD, 0xAA,
#ifndef NO_CHECKSUM
    (0xDE ^ 0xAD ^ 0xAA),
#endif
    0xAD, 0xDA, 0xAD,
#ifndef NO_CHECKSUM
    (0xAD ^ 0xDA ^ 0xAD),
#endif
    0xDE, 0xAD, 0xAA,
#ifndef NO_CHECKSUM
    (0xDE ^ 0xAD ^ 0xAA),
#endif
    0xAD, 0xDA, 0xAD,
#ifndef NO_CHECKSUM
    (0xAD ^ 0xDA ^ 0xAD),
#endif
#ifdef GLOBAL_CHECKSUM
#error "Not implemented"
#endif
    0xF5, 0xF5, 0xF5
};

void dump_nw()
{
    for (int i = 0; i < (NW_SIZE()); ++i) {
        printf("%2x ", network[i]);
        if (i % 4 == 3)
            printf("\n");
    }
    printf("\n");
}

byte_t receive_data()
{
    static unsigned nwstate = 0;

    if (nwstate >= (NW_SIZE()))
        exit(1);
    //klee_assume(nwstate < (NW_SIZE()));
    //nwstate++;
    return network[nwstate++];
}

bool save_firmware(uint8_t* memory, firmware_t* firmware, unsigned address)
{
    for (unsigned i = 0; i < PAGE_NUMBER; ++i) {
        page_t* page = &firmware->pages[i];
#ifdef CM_FINAL_CS_CHECK
        uint8_t cs = compute_checksum(page);
        if (cs != page->checksum) {
            _LZ__CM("fcs");
        }
#endif
        unsigned offset = address + (i * SIZEP);
        for (int k = 0; k < SIZEP; ++k) {
            memory[offset + k] = page->t[k];
        }
    }

    return true;
}

void init_firmware(firmware_t* firmware)
{
    for (int i = 0; i < PAGE_NUMBER; ++i) {
        firmware->pages[i].checksum = 0;
        for (int k = 0; k < SIZEP; ++k)
            firmware->pages[i].t[k] = 0xFE;
    }
}

uint8_t fault0 = 0;
uint8_t fault1 = 0;
uint8_t fault2 = 0;
uint8_t fault3 = 0;

void firmware_updater()
{
    firmware_t firmware;
    unsigned page_counter = 0;
    init_firmware(&firmware);

    // Read
    int byte_counter = 0;
    while (true) {
        byte_t received = receive_data() ^ fault0;
        page_t* page_buffer = &firmware.pages[page_counter];
        page_buffer->t[byte_counter] = received;
        byte_counter = byte_counter + 1;
#ifdef DEBUG
        printf("%2x ", received);
#endif

        if (byte_counter >= SIZEP) { // End of page
#ifndef NO_CHECKSUM
            byte_t cs = receive_data() ^ fault1; // return expected checksum
            // Compute checksum
            page_buffer->checksum = compute_checksum(page_buffer) ^ fault2;
            if (cs != page_buffer->checksum) {
                _LZ__CM("ics");
            }
#ifdef DEBUG
            printf("%2x", cs);
#endif
#endif
#ifdef DEBUG
            printf("\n");
#endif
            page_counter++;
            byte_counter = 0;

            if (page_counter >= PAGE_NUMBER) {
                break;
            }
            continue; // next page
        }
    }

    // check that all pages have been (properly) transferred)
#ifndef NO_PC_CM
    if (page_counter != PAGE_NUMBER) {
        _LZ__CM("pc");
    }
#endif
    save_firmware(memory, &firmware, LOAD_ADDRESS ^ fault3);
}