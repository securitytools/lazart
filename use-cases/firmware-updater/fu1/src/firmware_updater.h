#pragma once
#ifndef FIRMWARE_UPDATER_H
#define FIRMWARE_UPDATER_H

//#include <stdio.h>
#include "lazart.h"
#include <stdint.h>

//#define DEBUG // Enable tracing.

#define PAGE_NUMBER 4 // number of pages in a firmware
#define SIZEP 3 // number of Bytes in a page
#define LOAD_ADDRESS 0x0F // default load address value for the new firmware
#define BAD_ADDRESS 0x0 // represents any corrupted load address ...

#define NW_CANARY() 3
#define NW_SIZE_BASE() (PAGE_NUMBER * SIZEP)
#ifndef GLOBAL_CHECKSUM
#define NW_SIZE_GCS() 0
#else
#define NW_SIZE_GCS() 1
#endif
#ifndef NO_CHECKSUM
#define NW_SIZE_CS() (PAGE_NUMBER)
#else
#define NW_SIZE_CS() 0
#endif
#define NW_SIZE() (NW_SIZE_BASE() + NW_SIZE_CS() + NW_SIZE_GCS() + NW_CANARY())

typedef uint8_t byte_t;

typedef struct {
    byte_t t[SIZEP];
    byte_t checksum;
} page_t;

typedef struct {
    page_t pages[PAGE_NUMBER];
} firmware_t;

extern byte_t memory[NW_SIZE() * 2];

void dump_mem();

uint8_t compute_checksum(page_t* page_buffer);

byte_t network[NW_SIZE()];

void dump_nw();

byte_t receive_data();

bool save_firmware(uint8_t* memory, firmware_t* firmware, unsigned address);

void init_firmware(firmware_t* firmware);

// XOR Fault Model
extern uint8_t fault0;
extern uint8_t fault1;
extern uint8_t fault2;
extern uint8_t fault3;

void firmware_updater();

#endif