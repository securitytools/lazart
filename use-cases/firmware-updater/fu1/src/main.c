#include "firmware_updater.h"
#include "lazart.h"

bool valid()
{
    for (unsigned i = 0; i < PAGE_NUMBER; ++i) {
        unsigned i_fw = LOAD_ADDRESS + (i * SIZEP); // [B B B B B B]
#ifndef NO_CHECKSUM
        unsigned i_nw = (i * (SIZEP + 1)); // [B B B CS B B B CS]
#else
        unsigned i_nw = i * SIZEP;
#endif
        for (int k = 0; k < SIZEP; ++k) {
            if (memory[i_fw + k] != network[i_nw + k]) {
                return false;
            }
        }
    }
    return true;
}

bool oob()
{
    for (unsigned i = 0; i < LOAD_ADDRESS; ++i) {
        if (memory[i] != 0) {
            return true;
        }
    }
    for (unsigned i = LOAD_ADDRESS + (SIZEP * PAGE_NUMBER); i < (NW_SIZE() * 2); ++i) {
        if (memory[i] != 0)
            return true;
    }
    return false;
}

#ifdef PHI_INVALID
bool oracle()
{
    return !valid();
}
#endif

#ifdef PHI_OOB
bool oracle()
{
    return oob();
}
#endif

#ifdef PHI_INVALID_OOB
bool oracle()
{
    return !valid() & oob();
}
#endif

#ifdef PHI_INVALID_OOB_OR
bool oracle()
{
    return !valid() | oob();
}
#endif

int main()
{
#ifdef DEBUG
    if (klee_is_replay()) {
        dump_mem();
        printf("nw:\n");
        dump_nw();
        printf("start\n");
    }
#endif
    firmware_updater();
#ifdef DEBUG
    if (klee_is_replay()) {
        printf("mem:\n");
        dump_mem();
        printf("%s.\n", valid() ? "valid" : "invalid");
        printf("%s.\n", oob() ? "oob" : "no oob");
    }
#endif

#ifdef LAZART
    _LZ__ORACLE(oracle());
#endif
}
