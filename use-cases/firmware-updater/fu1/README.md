# Firmware updater v1

 - [Wiki](https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/Experimentation-and-use-cases)

`fu1` program simulates the process of loading a new firmware from a source to replace the existing one.

The firmware corresponds to a list of *pages* composed by Bytes, and the program load those values inside a local memory array at the specified `LOAD_ADDRESS`.

## Folder structure

The `fu1` program's folder is structured as:
 - `src/`: 
   - `main.c`: main analysis file.
   - `firmware_updater.c`: studied program.
   - `firmware_updater.h`: studied program's header.
 - `campaign/`: results folders for analysis.
 - `experimentation.py`: experimentation script (use `-h` for more information).

## Attack objectives

The `fu1` program studies two properties:
 - `oob`: does the firmware has been written outside of bounds (for instance bad `LOAD_ADDRESS`).
 - `invalid`: an invalid firmware code has been written in the memory (at least one bit is invalid).

The `fu1` program is studied using a combination of those properties depending on program's defines:
 - `PHI_INVALID` (`invalid`): attack objective is `invalid`.
 - `PHI_OOB` (`oob`): attack objective is `oob`.
 - `PHI_INVALID_OOB` (`iv_and_oob`): attack objective is `invalid & oob`.
 - `PHI_INVALID_OOB_OR` (`iv_or_oob`): attack objective is `invalid | oob`.

## Inputs

`fu1` program uses fixed inputs, corresponding to a memory filled with `0` and a firmware to load (variable `network`) with fixed values.

The program can be modified using several define macros:
 - `PAGE_NUMBER`: number of pages in a firmware.
 - `SIZEP`: number of Bytes in a page.
 - `NO_CHECKSUM`: if defined, no checksum is used at a end of page.
 - `GLOBAL_CHECKSUM`: if defined, a checksum is used for each page.
 - `NO_PC_CM`: if defined, the final verification of `page_counter` (loop verification) is not included.
 - `CM_FINAL_CS_CHECK`: if defined, an additional checksum verification is performed at save.

Additionally, `LOAD_ADDRESS` and `BAD_ADDRESS` can be changed.

## Attack models

`fu1` example uses combination of *Test Inversion* and *Data load mutation* fault models.

Test inversion is applied on functions `firmware_updater` and `save_firmware`.

Data load fault model uses the [XOR model](https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/Attack-Model/Data-Load-(DL)-Fault-Model#xor-fault-model) to inject symbolic values on specified `load` locations (see `firmware_updater.c`).