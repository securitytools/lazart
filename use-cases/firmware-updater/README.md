# Firmware updater programs

Firmware updater programs simulate the process of loading a new firmware from a source to replace the existing one. Two versions are provided, that used different attacks objectives and fault models:
 - `fu0`: version 0.
 - `fu1`: more complex version with symbolic memory chunks.

See the README.md file for each version.