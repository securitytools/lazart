#!/usr/bin/python3

import time

import lazart.core.countermeasures as cm
import lazart.exps.experimentations as exp
import lazart.exps.variants as vrt
import lazart.exps.std_variants as sv
import lazart.logs as log
import lazart.results.results as _res

from lazart._internal_.util import str_time
from lazart.core.attack_model import data_model, functions_list, ti_model, merge_am

# Define variants parameters.
vl = vrt.VariantsList()

# Attack objective
v_oracle = vl.add_variant_and_option(
    # Values
    vrt.Variant(["-DP1 ", "-DP2 ", "-DP3 ", "-DP4 ", "-DP5 "],
    ["p1", "p2", "p3", "p4", "p5"]),
    # Parser...
    "oracle", 
    "specify the attack objective to be used (see README.md for more information.)",
    None, None, # descr & shortcut
    "p1"
    )

# Countermeasures
v_cm = vl.add_variant_and_option(
    vrt.Variant([[], [cm.cm_tm(dict(depth=1, on=["__mut__"]))], [cm.cm_tm(dict(depth=2, on=["__mut__"]))],
        [cm.cm_sec_swift(dict(on=["__mut__"],trigger_function="_LZ__trigger_detector_stop"))]],
    ["none", "td", "tt", "st"]),
    "countermeasure", 
    "the countermeasure applied on the program.",
    None, None, # descr & shortcut
    "none" # default
)

# Attack model
data = data_model({"vars": { "loadAddress": "__sym__"} })

am_ti = functions_list(["firmware_updater", "receiveData", "triggerUpdate", "loadFirmware", "writePage"], ti_model())
am_dl = {
            "functions": {
                "loadFirmware": {
                    "models": [data]
                },
                "__all__": {
                    "models": [cm.secswift_data_model()] # For SecSwift countermeasures (no effect for other variants)
                }
            }
}
am_ti_dl = merge_am(am_ti, am_dl)

v_model = vl.add_variant_and_option(
    vrt.Variant([am_ti, am_dl, am_ti_dl],
    ["ti", "dl", "ti_dl"]),
    "model", 
    "determine the fault models of the analysis.",
    None, None, # descr & shortcut
    "ti"
)

# Standard variants.
v_mut_value = sv.mut_value(vl) # Keep injected value (default 'yes').
v_det_mode = sv.det_mode(vl) # Detection mode.
v_flag = sv.aa_type(vl) # Analysis flag.
v_red_rule = sv.red_rule(vl) # Redundancy definition (generate useless variation without EqRedAnalysis).
v_eq_rule = sv.eq_rule(vl) # Equivalence definition (generate useless variation without EqRedAnalysis).
v_eae = sv.eae(vl) # KLEE's error emitting mode.

params = vl.install_exp_script(vl) # Install Lazart script with variant specific options.
log.info(vl)

MAX_ORDER = params.order if hasattr(params, "order") else 2

start = time.process_time()
log.info("variant combination: {0} => {1}".format(", ".join([str(i) for i in vl.factors()]), vl.combinations()))
campaign = exp.Experimentation(
    vl,
    ["src/firmware_updater.c", "src/main.c"], # Source files
    v_model,
    path="campaign/", # Prefix for analysis folders.
    flags=v_flag,
    compiler_args = "-Wall " + v_oracle + v_det_mode,
    wolverine_args="-I",
    klee_args=v_eae,
    max_order=MAX_ORDER,    
    countermeasures=v_cm,
    params=params,
    execute_params={
        "eq_rule": v_eq_rule,
        "red_rule": v_red_rule
    },
    remove_fix_variants_names=True # don't use name of variant that are fixed.
)

log.debug("{0} analysis created.".format(len(campaign.exps)))

# RUN 
def at_start(a, e, r): print(f"running {a.name()}")
try:
    results = campaign.run(exp.run_callback, at_start, None, no_save=True, no_report=True, no_display=True)
except InterruptedError as e:
    print("interrupted.")
    exit(0)

# DISPLAY
print("\n\n")
dse_time = sum(r[_res.Metrics.TDSE] for r in results) # Compute before stringification.
exp.print_and_save(campaign, MAX_ORDER, results)
total_time =  dse_time + (time.process_time() - start)
print(f"campaign ({len(campaign.exps)}) completed in {str_time(total_time)}.")