
#ifndef FIRMWARE_UPDATER_H
#define FIRMWARE_UPDATER_H

#define PAGE_NUMBER 5 // number of pages in a firmware
#define SIZEP 3 // number of Bytes in a page
#define LMAX 7 // maximal number of page  -- useless for now ...
#define LOAD_ADDRESS 0xFFFF // default load address value for the new firmware
#define BAD_ADDRESS 0x0 // represents any corrupted load address ...

#include <stdio.h>

typedef char Byte;
typedef struct {
    Byte t[LMAX][SIZEP];
    int nbPage;
} tpage;

// ghost global variables for the oracle
extern int triggered; // indicates if the update has been triggered or not
extern int iscorrupted; // indicates if the firmare is corrupted or not
extern int updated; // indicates if the firmare is updated or not
extern int loadAddress;

void firmware_updater();

#endif
