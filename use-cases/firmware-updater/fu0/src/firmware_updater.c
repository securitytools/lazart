#include "firmware_updater.h"
#include "lazart.h"

// ghost global variables for the oracle
int triggered; // indicates if the update has been triggered or not
int iscorrupted = 0; // indicates if the firmare is corrupted or not
int updated = 0; // indicates if the firmare is updated or not
int loadAddress = 0; // address where the firmware will be actually loaded

// local variables
int aborted = 0; // indicates if the update is aborted or not

int dataReceived()
{
    return 1; // no network failure
}

int triggerUpdate()
{
    int requiresUpdate = 1; // indicates if a firware update is required or not ...
    if (requiresUpdate) { // can be faulted
        triggered = 1; // update is required
    } else {
        triggered = 0; // no update required
    }
    return triggered;
}

void bootFirmware(int bootAddress)
{
    // empty, supposed to run the boot process of the new firmware ...
    loadAddress = bootAddress; // a modifier, il faudrait un bootaddress dans l'oracle ??
}

void loadFirmware(tpage* copyFirmware, int address)
{
    // complete the update by preparing the "reboot" step with the new firmware
    // here simply assigns the memory address where the reboot should take place
    // loadAddress = LOAD_ADDRESS;

    int loadOk = 1; // to simulate a TI fault injection in the load process
    if (loadOk)
        loadAddress = LOAD_ADDRESS;
    else
        loadAddress = BAD_ADDRESS;
    bootFirmware(loadAddress);
}

void deleteFirmware(tpage* copyFirmware)
{
    // simulate the firmware deletion
    // left empty at this stage ...
}

tpage* decryptPage(tpage* page)
{
    return page; // no encryption yet, not clear what it would add at this stage ?
}

void initFirmware(tpage* dest)
{
    // initializes a correct firmware content, each byte equals 1
    int i, j;
    int payload = 0;
#ifndef P5
    payload = 1;
#endif
    for (j = 0; j < PAGE_NUMBER; j++) {
        for (i = 0; i < SIZEP; i++)
            dest->t[j][i] = payload;
    }
}

void receiveData(char* dest, char src)
{
    // not used in this version ; we assume that the attacker cannot control the
    // reception channel or it is already taken into account by the network layer
    // ... (could be added if required, but need to see how it would interact with
    // network errors)
    int receiveOk = 1; // to simulate a fault injection when receiving a whole page
    if (receiveOk)
        *dest = src;
    else
        *dest = 0;
}

// FU:

int checkPageIntegrity(tpage buf, int pagenum)
{
    // checksum is the sum of each page content
    int i, checksum = 0;
    for (i = 0; i < SIZEP; i++)
        checksum += buf.t[pagenum][i];
    return checksum == SIZEP;
}

// Remove if ?
void writePage(tpage* dest, tpage* src, int page)
{
    int writePageOk = 1; // to simulate a fault injection when writing a page
    int i;
    for (i = 0; i < SIZEP; i++)
        if (writePageOk)
            dest->t[page][i] = src->t[page][i];
        else
            dest->t[page][i] = 0;
}

void firmware_updater()
{
    tpage network; // data received from the network
    tpage copyFirmware;
    tpage bufferFirmware;
    int curPage = 0;
    int curByte;

    initFirmware(&network);
    copyFirmware.nbPage = 0;

    if (triggerUpdate()) { // check if an update is required or not
        curByte = 0;
        while (1) {
            // wait for data ...
            receiveData(&(bufferFirmware.t[curPage][curByte]),
                network.t[curPage][curByte]);
            curByte++;
            if (curByte == SIZEP) {
                //  a whole page has been received
                // now check page integrity
                if (checkPageIntegrity(bufferFirmware, curPage)) {
                    // page is correct
                    writePage(&copyFirmware, decryptPage(&bufferFirmware), curPage);
                } else {
                    // page is incorrect
                    iscorrupted = 1;
                    break;
                };
                curPage++;
                curByte = 0;
            }

            if (curPage == PAGE_NUMBER) { // check if end of transfer
                copyFirmware.nbPage = PAGE_NUMBER;
                break;
            }
        }

        // check that all pages have been (properly) transfered)
        if (copyFirmware.nbPage != PAGE_NUMBER) {
            iscorrupted = 1;
            aborted = 1;
        }

        if (!aborted) {
            loadFirmware(&copyFirmware, LOAD_ADDRESS);
            updated = 1;
            // the firmware is supposed to be correctly updated at this step
        } else {
            // abort the update process
            deleteFirmware(&copyFirmware);
        }
    }
}