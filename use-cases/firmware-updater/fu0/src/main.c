#include "firmware_updater.h"
#include "lazart.h"

void initialize(void) { }

int oracle()
{
#ifdef P1
    // a trigger request occurred and either the received firmware was corrupted
    // or loaded at a bad address return (triggered==1 & (iscorrupted==1 |
    // loadAddress != LOAD_ADDRESS)) ;
    return ((triggered == 1) & ((loadAddress != LOAD_ADDRESS) & (iscorrupted == 1)));
#elif P2
    // a trigger request occurred and the received firmware was corrupted
    return ((triggered == 1) & (iscorrupted == 1));
#elif P3
    // a trigger request occurred and the received firmware is loaded at a bad
    // adddress
    return ((triggered == 1) & !iscorrupted & (loadAddress != LOAD_ADDRESS));
#elif P4
    // no trigger request occurred and a new firmware was correctly updated
    return ((triggered == 0) & (updated == 1) & (loadAddress == LOAD_ADDRESS));
#elif P5
    // a trigger request occurred and a new (craft by hand) firmware was correctly
    // updated
    return ((triggered == 1) & (updated == 1) & (loadAddress == LOAD_ADDRESS));
#endif
}

int main()
{
    initialize();
    firmware_updater();
    _LZ__ORACLE(oracle());

    return 0;
}
