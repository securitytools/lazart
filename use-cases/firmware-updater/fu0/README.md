# Firmware updater v0

 - [Wiki](https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/Experimentation-and-use-cases)

`fu0` program simulates the process of loading a new firmware from a source to replace the existing one.

The firmware is simulated by a list of `tpage` objects that holds the byte array corresponding to the codes.

## Folder structure

The `fu0` program's folder is structured as:
 - `src/`: 
   - `main.c`: main analysis file.
   - `firmware_updater.c`: studied program.
   - `firmware_updater.h`: studied program's header.
 - `campaign/`: results folders for analysis.
 - `experimentation.py`: experimentation script (use `-h` for more information).

## Attack objectives

Five attack objectives are studied for this program:
 - `p1`: a trigger request occurred and either the received firmware was corrupted, or loaded at a bad address.
 - `p2`: a trigger request occurred and the received firmware was corrupted.
 - `p3`: a trigger request occurred and the received firmware is loaded at a bad address.
 - `p4`: no trigger request occurred and a new firmware is correctly updated.
 - `p5`: a trigger request occurred and a new (craft by hand) firmware was correctly updated.

## Inputs

`fu0` program uses fixed input depending on the selected attack objectives.

## Attack models

`fu0` example uses combination of *Test Inversion* and *Data load mutation* fault models.

Test inversion is applied on the whole program (all functions) and data load fault model is applied on `loadFirmware`.