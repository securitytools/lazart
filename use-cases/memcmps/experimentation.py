#!/usr/bin/python3

from copy import deepcopy
import time 

import lazart.exps.experimentations as exp
import lazart.exps.variants as vrt
from lazart.core.attack_model import functions_list, ti_model, data_model
from lazart.analysis.attack_redundancy import feq_rule, eq_rule, prefix_rule, sub_word_rule
from lazart.core.analysis import AnalysisFlag
from lazart.results.formats import Table
from lazart._internal_.util import str_time
import lazart.core.countermeasures as cm
import lazart.logs as log
from lazart.util.exec import execute
import lazart.results.results as _res
from lazart.results.formats import column
import lazart.exps.std_variants as sv

# Define variants parameters.
vl = vrt.VariantsList()

# Oracle
v_oracle = vl.add_variant_and_option(
    # Values
    vrt.Variant(["", "-DPHI_NFALSE "],
    ["true", "nfalse"]),
    # Parser...
    "oracle", 
    "the attack objective define used for compiler arg.",
    None, None, # descr & shortcut
    "true"
    )

# Program version 
v_version = vl.add_variant_and_option(
    vrt.Variant(["", "-DV2 ", "-DV3 "],
    ["v1", "v2", "v3"]),
    # Parser...
    "version", 
    "the version of the program to be used.",
    None, None
    )

# Attack model
ti = ti_model()
def gen_dl(val): return data_model({"vars": {
    "len": val, 
    "result": val
}})
data_br = gen_dl(0) # Byte reset
data_flip = gen_dl("flip") # Flip
data_bitflip = gen_dl("__sym__:bitflip_pred") # Bit flip
data_sym = gen_dl("__sym__") # Arbitrary
magic_fix = gen_dl(4660) # Inject TRUE_VAL (fix).
magic_fct = gen_dl("true_val") # Inject TRUE_VAL (fct).
magic_pred = gen_dl("__sym__:true_val_pred") # Inject TRUE_VAL (symbolic predicate).

v_model = vl.add_variant_and_option(
    vrt.Variant([ti, data_br, [ti, data_br],
        data_flip, [ti, data_flip],
        data_bitflip, [ti, data_bitflip],
        data_sym, [ti, data_sym],
        magic_fix, magic_fct, magic_pred],
    ["ti", "set0", "ti_set0", "flip", "ti_flip", "bflip", "ti_bflip", "sym", "ti_sym", "magic", "magic_fct", "magic_pred"]),
    "model", 
    "determines the fault models of the analysis.",
    None, None,
    "ti_sym"
)

# Use standard memcmps or stub.
v_use_stub = vl.add_variant_and_option(
    vrt.Variant(["", "-DMEMCMP_STUB -D_LZ__NO_STD "],
                ["std", "stub"]),
                "library",
                "determines if standard `memcmp` function is called or if a stub is used",
                None, None,
                "std"
)

# Standard variants.
v_sym = sv.entries(vl) # Symbolic inputs (default 'sym').
v_size = sv.int_range(vl, [4, 2, 3, 5, 6, 10], "size", "Size of array to be compared.", value_fmt="-DSIZE={} ") # Array size (default '4').
v_mut_value = sv.mut_value(vl) # Keep injected value (default 'yes').
v_cm = sv.countermeasures(vl) # Automated countermeasures (default 'none').
v_det_mode = sv.det_mode(vl) # Detection mode.
v_flag = sv.aa_type(vl) # Analysis flag.
v_red_rule = sv.red_rule(vl) # Redundancy definition (generate useless variation without EqRedAnalysis).
v_eq_rule = sv.eq_rule(vl) # Equivalence definition (generate useless variation without EqRedAnalysis).
v_eae = sv.eae(vl) # KLEE's error emitting mode.

params = vl.install_exp_script(vl) # Install Lazart script with variant specific options.
log.info(vl)

MAX_ORDER = params.order if hasattr(params, "order") else 2

start = time.process_time()
log.info("variant combination: {0} => {1}".format(", ".join([str(i) for i in vl.factors()]), vl.combinations()))
campaign = exp.Experimentation(
    vl,
    ["src/memcmps.c", "src/main.c"],
    vrt.VariantLambda(lambda idx, ex: functions_list(["memcmps"], v_model.value(idx, ex))),
    path="campaign/", # Prefix for analysis folders.
    flags=v_flag,
    compiler_args = "-Wall -I commons " + v_oracle + v_sym + v_det_mode + v_version + v_size,
    wolverine_args="-I",
    klee_args=v_eae,
    max_order=MAX_ORDER,    
    countermeasures=v_cm,
    params=params,
    execute_params={
        "eq_rule": v_eq_rule,
        "red_rule": v_red_rule
    },
    remove_fix_variants_names=True # don't use name of variant that are fixed.
)
log.debug("{0} analysis created.".format(len(campaign.exps)))

# RUN 
def at_start(a, e, r): print(f"running {a.name()}")
try:
    results = campaign.run(exp.run_callback, at_start, None, no_save=True, no_report=True, no_display=True)
except InterruptedError as e:
    print("interrupted.")
    exit(0)

# DISPLAY
print("\n\n")
dse_time = sum(r[_res.Metrics.TDSE] for r in results) # Compute before stringification.
exp.print_and_save(campaign, MAX_ORDER, results)
total_time =  dse_time + (time.process_time() - start)
print(f"campaign ({len(campaign.exps)}) completed in {str_time(total_time)}.")