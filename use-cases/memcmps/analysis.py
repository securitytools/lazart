#!/usr/bin/python3

from lazart.lazart import *
import glob

# Parse CLI parameters
params = install_script()

# Data Load symbolic mutation on several programs variable.
data_sym = data_model({"vars": {
                    "len": "__sym__", # len is faulted using symbolic value.
                    "result": "__sym__" # results is faulted using symbolic value.
                }})

# Attack model: combine DL (`data_sym`) and TI models on `memcmps` function.
am = functions_list(["memcmps"], [ti_model(), data_sym]) 

a = read_or_make(["src/memcmps.c", "src/main.c"], # Source files
    am, # Attack model
    path="results", # Path in which analysis file will be stored
    flags=AnalysisFlag.EqRedAnalysis, # Attack analysis with equivalence & redundancy.
    compiler_args="-Wall -DV3 -DSYM", # Compiler arguments
    wolverine_args="-t mut", # Full wolverine output for mutation.
    params=params, # Pass CLI params to the analysis
    klee_args="-emit-all-errors --write-smt2s" # KLEE's arguments (multiple path will be generated for the same error location).
) 

execute(a, s_fct= lambda trace: trace.termination_is(Term.Correct, Term.Ptr, Term.Div, Term.Free), parse_rt_errs=True) # Execute analysis, print results and generate report. Custom RTE termination type are considered as successful attacks (s_fct), and RTE path are parsed by Lazart (parse_rt_errs)

# Optional verification of results by Lazart checking for commons errors.
verify.attack_analysis(a)
verify.traces_parsing(a)

# Print all traces 
"""for trace in all_traces(a, a.max_order()):
    print(trace.str() + ": " + trace.path)"""

for trace in all_traces(a, a.max_order()):
    if trace.termination_is(TerminationType.Correct):
        print(trace.str() + " " + trace.path)