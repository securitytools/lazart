# memcmps programs

 - [Wiki](https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/Experimentation-and-use-cases)

The `memcmps` function is a secure implementation of `memcmp` C function. Three versions of the function are available, selected using define macros:
 - version 1 (default): uses one mask and one call to `memcmp`.
 - version 2 (`V2`): uses one mask and three calls to `memcmp`.
 - version 3 (`V3`): uses two masks and four calls to `memcmp`.

## Folder structure

The project folder is organized as follows:
 - `src/`:
   - `main.c`:  main analysis file.
   - `memcmps.c`: studied program.
   - `memcmps.h`: studied program header and parameters.
 - `campaign/`: experimentation campaign results.
 - `experimentation.py`: [experimentation](https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/Experimentation-and-use-cases/#experimentation-module) script (use `-h` for more information)..
 - `results/`: results folder for analysis.
 - `analysis.py`: analysis script (use `-h` for more information).

## Attack objectives

Two attacks objective are studied:
 - returning exactly `TRUE_VAL` (by default).
 - returning anything but `FALSE_VAL` (with `PHI_NFALSE`).

## Inputs

The two entry arrays can be initialized using *fixed* or *symbolic* entry.

Default mode is *fixed entry*, one array is filled with '1' and the other is filled with '0'.

Symbolic mode is defined with the macro `SYM` and use symbolic array constrained to be unequal on at least one byte.

The macro `SIZE` allows to specify the size of input arrays (by default `4`).
The macro `TRUE_VAL`, `FALSE_VAL` and the masks can be redefined (either none is redefined, or all).

The macro `MEMCMP_STUB` specifies that the standard library function `memcmp` is not used and a custom implementation is used instead. 

## Attack models

The `verify_pin` collection uses a combination of *test inversion* and *data load mutation* fault models. 

*Test inversion* can be applied on `memcmps` function. 

*Data load mutation* is applied on variables `len` and `results`. Default script supports bit-flip, flip, symbolic value of magic value (default `TRUE_VAL`). Customize the fault model parameters and data model value predicates to specify custom behaviors.