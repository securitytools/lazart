#include "lazart.h"
#include "memcmps.h"
#include <stdio.h>

#ifdef PHI_NFALSE
int oracle(int result)
{
    return result != FALSE_VAL;
}
#else
int oracle(int result)
{
    return result == TRUE_VAL;
}
#endif

void initialize_sym(char s1[SIZE], char s2[SIZE])
{
    klee_make_symbolic(s1, SIZE * sizeof(char), "s1");
    klee_make_symbolic(s2, SIZE * sizeof(char), "s2");
}

void initialize(char s1[SIZE], char s2[SIZE])
{
    for (unsigned int i = 0; i < SIZE; i++) {
        s1[i] = '0';
    }
    for (unsigned int i = 0; i < SIZE; i++) {
        s2[i] = '1';
    }
}

void check_equals(char s1[SIZE], char s2[SIZE])
{
    int equal = 0;
    for (unsigned int i = 0; i < SIZE; i++) {
        equal += s1[i] == s2[i];
    }
    klee_assume(equal != SIZE);
}

int flip(int value)
{
    return ~value;
}

int true_val(int value)
{
    return TRUE_VAL;
}

int true_val_pred(int original, int value)
{
    return TRUE_VAL;
}

int bitflip_pred(int original, int value)
{
    return (original ^ value) == 1;
}

int main()
{
    char s1[SIZE], s2[SIZE];
#ifdef SYM
    initialize_sym(s1, s2);
#else
    initialize(s1, s2);
#endif

    int res = memcmps(s1, s2, SIZE);

    _LZ__ORACLE(oracle(res));
#ifdef SYM
    check_equals(s1, s2);
#endif

    return 0;
}