#ifndef MEMCMPS_H
#define MEMCMPS_H

#include <stdio.h>
#include <string.h>

#ifndef SIZE
#define SIZE 4
#endif

#ifndef MEMCMP_STUB
#include <string.h>
#endif

// If defined, all other mask values should be defined.
#ifndef TRUE_VAL
#define TRUE_VAL 0x1234
#define FALSE_VAL 0x5678
#define MASK 0xABCD
#define MASK2 0xF4F4
#endif

// Stub version called by the secure program.
int memcmp_stub(const char* str1, const char* str2, size_t count);
// Program to verify.
int memcmps(char* a, char* b, int len);

#endif // MEMCMPS_H