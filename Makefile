## 	Makefile
#	Main Lazart Makefile.

export SRC_DIR=src
export USR_DIR=usr
export BIN_DIR=$(USR_DIR)/bin
export SHARE_DIR=$(USR_DIR)/share
export LIB_DIR=/lib

PUBLIC_PREFIX=/usr/local/soft/Lazart

.PHONY: build deploy undeploy public docs

build:
	make -C $(SRC_DIR)/wolverine
	make -C $(SRC_DIR)/cli

clean:
	make clean -C $(SRC_DIR)/wolverine
	make clean -C $(SRC_DIR)/cli

mrproper:
	make mrproper -C $(SRC_DIR)/wolverine
	make mrproper -C $(SRC_DIR)/cli

deploy: build
	@mkdir -p $(BIN_DIR) $(SHARE_DIR)
	@cp -r $(SRC_DIR)/wolverine/$(USR_DIR)/* $(USR_DIR)
	@cp -r $(SRC_DIR)/cli/$(USR_DIR)/* $(USR_DIR)
	@cp -r $(SRC_DIR)/cpp-api/* $(USR_DIR)
	@cp -r $(SRC_DIR)/python-api/src/*  $(LIB_DIR)/python-api

undeploy:
	@rm -rf $(BIN_DIR)/* $(SHARE_DIR)/*

docs:
	make docs -C $(SRC_DIR)/lazart

basebox:
	@docker buildx build --platform linux/amd64 -f=Dockerfile.base -t=local/lazart:without-lazart .

devbox: basebox
	@docker buildx build --platform linux/amd64 -f=Dockerfile.dev -t=local/lazart:dev .

prodbox: devbox
	@docker buildx build --platform linux/amd64 -f=Dockerfile.prod -t=local/lazart .

rmi:
	@docker rmi local/lazart local/lazart:dev local/lazart:without-lazart

box: devbox
	@vagrant up --provider=docker

unbox:
	@vagrant destroy -f

export: prodbox
	@docker save local/lazart:latest > lazart.tar
