#!/usr/bin/python3
"""The main command of Lazart Interactive Interface.

LII allows to start a Lazart script directly in python interactive mode, keeping standard
script options.

This command basically transfert all arguments to the started script.
"""

import argparse
import subprocess
from lazart.script import get_parser
import lazart.constants as constants
import lazart.logs as log

def set_configs():
    """ Returns a dictionary corresponding to the configuration of the program depending on the specified arguments.
        An argument parser is created to handle the different option and parameters.
        @returns the configuration dictionary.
    """
    arg_parser = get_parser(constants.lazint_banner)
    arg_parser.add_argument("script", nargs="?", default=None, help="A script that will be executed right after Lazint launch.")
    # TODO() --no-import: lazint is not imported
    # TODO() --working-directory: launch lazint with specific chwd
        
    args = arg_parser.parse_args()

    return args

""" Unpack config dict to generate argument string."""
def call_args(config):
    args = []

    if config.verbosity:
        args += ["--verbosity", config.verbosity]
    if config.archive:
        args += ["--archive"]
    if config.keep_dse:
        args += ["--keep-dse"]
    if config.max_int:
        args += ["--max-int", str(config.max_int)]
    if config.no_progress:
        args += ["--no-progress"]
    if config.restart:
        args += ["--restart", str(config.restart)]
    if config.restart_all:
        args += ["--restart-all"]
    if config.version:
        args += ["--version"]
    if config.debug:
        args += ["--debug"]
    if config.output_folder is not None:
        args += ["--output-folder", str(config.output_folder)]
    if config.define is not None:
        for define in config.define:
            args += ["--define", str(define)]

    if config.order:
        args += ["--order", str(config.order)]

    return args

""" Runs Python interpreter with Lazint commands imported in global namespace.
@param config the argparse result for the command.
    @attr workspace the optional workspace, used to initialize analysis.
"""
def run_in_interpreter(config):

    script = getattr(config, "script")
    if script == None or script == "":
        log.error("no script specified.")
        exit(1)
    # TODO() no import
    insts = "import lazart.constants as cts; from lazart.util.cli import *; print(cts.lazint_banner);"

    if not script is None:
        insts += "exec(open('{}').read())".format(str(script))

    args = ["python3", "-ic", "exec(\"" + insts + "\")"]
    args += call_args(config)

    subprocess.call(args)

if __name__ == "__main__":
    """ Main function of Lazint.
    """

    # Argument parsing.
    config = set_configs()

    run_in_interpreter(config)