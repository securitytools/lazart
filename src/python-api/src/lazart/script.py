""" The module *lazart.script* contains definitions of standard parser and utility for **Lazart**'s scripts.

The options are returned to the caller (max_order for instance) and other are saved into lazart.options (recompute mode, output-folder, etc).

:since: 3.0
:author: Etienne Boespflug
"""

# Python
import argparse
import typing as ty

# Lazart
import lazart.constants as cts
import lazart.options as options
from lazart.logs import VerbosityLevel
import lazart.logs as log
from lazart._internal_.extensions import StepID

# Internal

import lazart._internal_.args as args
import lazart.util.cli as cli

_verbosity_choices = ['error', 'warning', 'quiet', 'info', 'verbose', 'debug', 'dev']
""" List[str]: The list of the name of each verbosity level."""

def get_parser(banner = "Lazart's script.") -> argparse.ArgumentParser:    
    """
    Returns a standard parser for *Lazart*'s script. 

    It includes all features provided by this API (log verbosity, recomputing mode, etc) that are available for scripts.

    :param banner: the banner to be displayed for help, defaults to "Lazart's script."
    :type banner: str, optional
    :return: the created standard parser
    :rtype: argparse.ArgumentParser
    """
    
    parser = argparse.ArgumentParser(description=banner)

    parser.add_argument("-V", "--verbosity", type=str, choices=_verbosity_choices, help="Set the verbosity level ('error', 'warning', 'quiet', 'info', 'verbose', 'debug' or 'dev') determining the number of information displayed by Lazart. The default verbosity is used if not specified")
    parser.add_argument("-P", "--no-progress", action="store_true", help="Do not print progress during long operations (allow for easier log display).")
    parser.add_argument("-A", "--archive", action="store_true", help="Prevents Lazart to add a .gitignore to the analysis. All the results folders will be included in the version control.")
    parser.add_argument("-D", "--debug",  action="store_true", help="Enable debug mode.")
    parser.add_argument("-d", "--define", type=str, action="append", help="Define a value for current Lazart execution. If the string contains a ':', the string is parsed as 'attribute:value'.")
    parser.add_argument("--max-int", type=str, help="The maximum value used in algorithms (such as minimum searches).")
    parser.add_argument("--keep-dse", action="store_true", help="Do not remove raw DSE output in analysis folders.")
    # Analysis param override
    parser.add_argument("-O", "--output-folder", type=str, help="The folder in which analysis files will be generated. If not specified, use analysis specified one.")
    parser.add_argument("-o", "--order", type=int, help="Defines the maximum order that will be used for the script. Analysis usually works on order-2 to order-5")
    parser.add_argument("-r", "--restart-all", action="store_true", help="Tells the command to ignore cached results. Shortcut for -R=start.")
    parser.add_argument("-R", "--restart", type=str, help="Defines from which step the analysis should restart. Accept step id (int) or standard step name: ['start', 'compile', 'run', 'traces', 'analysis']")

    parser.add_argument("-v", "--version", action="store_true", help="Prints the version of Lazart.")

    return parser

def handle_lazart_defines():
    """
    Verifies that *standard defines* are recognized, print warnings otherwise. See *Lazart*'s wiki for a full list of *standard defines*.
    
    **_List_**:
     - `lz.log.debug`: enables specific detailed debugs logs for development.
     - `lz.log.debug.atk_redundancy`: enable debug logs for `lazart.analysis.attack_redundancy`.
    
    """    
    d = options.defines()    

    for define in d:
        if define.startswith("lz."):
            # Specific debug
            if define.startswith("lz.log.debug."):
                args.expect_type(d[define], bool, ": for script define '{}'".format(define))

            # Reserved miscellaneous
            elif define.startswith("lz.misc."):
                pass
            else:
                log.warning("unknown Lazart's define '{}'. Defines prefixed with 'lz.' are reserved.")

def install_lazart(args: dict) -> argparse.Namespace:
    """
    Installs *Lazart*'s globals (logs, options) from arguments.

    :param args: program arguments.
    :type args: dict
    :return: `True` if the installation succeed, `False` otherwise.
    :rtype: argparse.Namespace
    """
    ret = argparse.Namespace()

    # Logs
    choices = [] # Unused

    if args.version:
        cli.version()
        exit(0)

    vb = VerbosityLevel.Info
    if args.verbosity is not None:  
        choices.append("--verbosity")        
        if args.verbosity == "error": vb = log.VerbosityLevel.Error
        if args.verbosity == "warning": vb = log.VerbosityLevel.Warning
        if args.verbosity == "quiet": vb = log.VerbosityLevel.Quiet
        if args.verbosity == "info": vb = log.VerbosityLevel.Info
        if args.verbosity == "verbose": vb = log.VerbosityLevel.Verbose
        if args.verbosity == "debug": vb = log.VerbosityLevel.Debug
        if args.verbosity == "dev": vb = log.VerbosityLevel.Dev

    if args.debug:
        vb = log.VerbosityLevel.Dev
        options.set_debug_mode(True)

    lf = cts.logfile
    
    log.init(vb, lf)

    if len(choices) > 1: # Unused
        log.error("error: invalid argument, both " + ",".join(choices) + " specified.")

    # Options
    if args.archive:
        options.set_archive(True)
    if args.keep_dse:
        options.set_keep_dse_out(True)
    if args.max_int is not None:
        options.set_max_int(int(args.max_int))
    if args.no_progress:
        options.set_print_progress(False)

    # Recover
    restart = options.max_int()
    if args.restart_all:
        restart = int(StepID.Start)

    if args.restart:   
        print("restart present")
        if args.restart_all:
            log.error("invalid argument, both --restart and --restart-all specified.")

        if args.restart == "start" or args.restart == str(int(StepID.Start)): restart = int(StepID.Start)
        elif args.restart == "compile" or args.restart == str(int(StepID.Compile)): restart = int(StepID.Compile)
        elif args.restart == "run" or args.restart == str(int(StepID.Run)): restart = int(StepID.Run)
        elif args.restart == "traces" or args.restart == str(int(StepID.Traces)): restart = int(StepID.Traces)
        elif args.restart == "analysis" or args.restart == str(int(StepID.Analysis)): restart = int(StepID.Analysis)
        elif args.restart.isdigit():
            restart = int(args.restart)
            if restart > options.max_int(): 
                log.warning("invalid int value for --restart parameter, integer greater than MAXINT. Fixed to MAXINT.")
                restart = options.max_int()
        else:
            log.warning("ignoring invalid value '{}' for --restart argument.".format(args.restart))
    
    vars(ret)["restart"] = restart
    # Defines
    if args.define is not None:
        
        for define in args.define:
            raw = str(define)
            value = True
            key = raw
            if ":" in define:
                sl = raw.split(":")
                if len(sl) != 2:
                    print("error: invalid define syntax '{}'. Use 'key:value'.".format(raw))
                key = sl[0]
                value = sl[1]
            
            if key in options.defines():
                log.warning("warning: '{}' already defined, ignoring value '{}'.".format(key, value))
            else:
                options.defines()[key] = value
    
    handle_lazart_defines()

    if args.order is not None:
        if args.order < 0:
            log.error("invalid argument, 'order' have to be positive.")
        vars(ret)["order"] = args.order


    if args.output_folder:
        vars(ret)["output_folder"] = args.output_folder
    return ret

def install_script(banner: str = "Lazart's script.") -> argparse.Namespace:
    """
    Installs the common script arg parser and parses arguments.

    :param banner: the banner of the script.
    :type banner: str, optional
    :raises Exception: if **Lazart**'s installation fail.
    :return: the dictionary of argument parsing parameters.
    :rtype: argparse.Namespace
    """
    parser = get_parser(banner)

    args = parser.parse_args()

    return install_lazart(args)

def install_custom_script(parser_callback: ty.Callable[[argparse.Namespace], argparse.ArgumentParser], banner: str = "Lazart's script.") -> ty.Tuple[bool, argparse.Namespace]:
    """
    Installs the common script arg parser with custom features and parses arguments.

    :param parser_callback: a function returning a custom parser from the standard parser.
    :type parser_callback: Callable[[argparse.Namespace], argparse.ArgumentParser]
    :param banner: the banner of the script.
    :type banner: str, optional
    :return: the dictionary of argument parsing parameters.
    :rtype: Tuple[bool, argparse.Namespace]
    """
    parser = get_parser(banner)
    parser = parser_callback(parser)

    args = parser.parse_args()
    return (install_lazart(args), args)