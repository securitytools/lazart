""" The module *lazart.tests.regression* contains definitions for **regression tests**.

:since: 3.1
:author: Etienne Boespflug
"""

# Python
import traceback
import typing as ty
import pprint as pprint
from datetime import datetime

# Lazart
import lazart.logs as log
from lazart.logs import VerbosityLevel
import lazart.options as options

# Internal
import lazart._internal_.color as colors
import lazart._internal_.args as args

# Core
from lazart.core.analysis import Analysis, AnalysisFlag

# Analysis
from lazart.analysis.hotspots_analysis import hotspots_results

# Results 
from lazart.results.results import Metrics

# Util
from lazart.util.exec import execute

# Tests
import lazart.tests.test as _test 

def _passed(indent: str, msg: str):
    """
    Utility function printing green message with indent for successful tests.

    :param indent: the indentation string.
    :type indent: str
    :param msg: the message to be printed.
    :type msg: str
    """
    log.quiet(indent + colors.green + msg + colors.reset)

def _failed(indent: str, msg: str):
    """
    Utility function printing red message with indent for failed tests.

    :param indent: the indentation string.
    :type indent: str
    :param msg: the message to be printed.
    :type msg: str
    """
    log.quiet(indent + colors.red + msg + colors.reset)

def _log_test(indent: str, msg: str, passed: bool):
    """
    Utility function printing a colored message depending on the test success.

    :param indent: the indentation string.
    :type indent: str
    :param msg: the message to be printed.
    :type msg: str
    :param passed: boolean determining if the test was successful.
    :type passed: bool
    """
    if passed:
        _passed(indent, msg + " passed.")
    else:
        _failed(indent, msg + " failed.")

def generate_regression_test(analysis: Analysis, metrics: Metrics, path: str, name: str, **kwargs) -> bool:
    """
    Generates the regression test from the specified analysis.

    *note that some example are not deterministic (due to DSE engine) and cannot be used raw.*

    :param analysis: the analysis parameters of the test.
    :type analysis: Analysis
    :param metrics: the result metrics of the analysis.
    :type metrics: Metrics
    :param path: the path in which the *.lzt* test file will be saved.
    :type path: str
    :param name: the name of the test.
    :type name: str
    :raises Exception: in case of unsupported analysis type is provided.
    :return: `True` if the generation was successful, `False` otherwise.
    :rtype: bool
    """
    content = "---\n"
    content += "name: \"{}\"\n".format(name)
    content += "type: regression\n"
    content += f"date: {datetime.today().strftime('%Y-%m-%d')}\n"
    content += "analysis:\n"
    content += "  input_files: [{}]\n".format(",".join('"' + s + '"' for s in analysis.input_files()))
    content += "  compiler_args: {}\n".format(analysis.compiler_args())
    content += "  link_args: {}\n".format(analysis.linker_args())
    content += "  max_order: {}\n".format(analysis.max_order())
    content += "  flags: {}\n".format(analysis.flags().value)
    content += "  attack_model: {}\n".format(analysis.attack_model())
    content += "  tasks: {}\n".format(analysis.tasks())
    content += "  countermeasures: {}\n".format(analysis.countermeasures())
    if analysis.klee_args() != "":
        content += "  klee_args: {}\n".format(analysis.klee_args())
    if analysis.wolverine_args() != "":
        content += "  wolverine_args: {}\n".format(analysis.wolverine_args())

    full_trace = args.get_or_throw(kwargs, "full_trace", bool, False)
    hs = args.get_or_throw(kwargs, "hs", bool, True)
    full_arr = args.get_or_throw(kwargs, "full_arr", bool, False)
    no_arr = args.get_or_throw(kwargs, "no_arr", bool, False)
    full_static = args.get_or_throw(kwargs, "full_static", bool, True)
    exec = args.get_or_throw(kwargs, "exec", bool, True)
    cov = args.get_or_throw(kwargs, "cov", bool, False)
    klee_exec = args.get_or_throw(kwargs, "klee_exec", bool, False)

    if analysis.flags().isset(AnalysisFlag.DetectorOptimization):
        raise Exception("Unsupported analysis type: Detector optimization.")

    content += "expected:\n"
    if full_trace:
        content += "  traces: {}\n"
        # TODO, register full traces ? 
    
    expected_set = Metrics.SET_PROGRAM 
        
    if full_static:
        expected_set.extend([Metrics.Branches, Metrics.Instructions, Metrics.BB])
    if exec:
        expected_set.extend(Metrics.SET_EXEC_FULL)
    if cov:
        expected_set.extend(Metrics.SET_COV_FULL)
    if klee_exec:
        expected_set.extend(Metrics.SET_EXEC_KLEE)

    if no_arr:
        expected_set.extend(Metrics.SET_ATK_SIMPLE)
    elif full_arr:
        expected_set.extend(Metrics.SET_ATK_FULL)
    else:
        expected_set.extend(Metrics.SET_ATK_TOT)
    
    # Add actual values:
    for item in expected_set:
        value = metrics[item]
        if item in Metrics.GROUP_SERIES: # Lists
            content += f"  {item}: [{','.join([str(i) for i in value])}]\n"
        else:
            content += f"  {item}: {value}\n"
        
    if hs:
        hr = hotspots_results(analysis)
        hr_rd = hr.reverse_dict()
        content += "  hotspots:\n"
        for key, values in hr_rd.items():
            row = []
            for value in values:
                (fc, tc, mc) = value
                row.append("[{}, {}, {}]".format(fc, tc, mc))
            content += "    '{}': [{}]\n".format(key, ",".join(row))
    
    # Execute
    exec_params = kwargs.get('execute', {})
    if len(exec_params) > 0:
        content += "execute:\n"
        for k, v in exec_params.items():
            content += f"  {k}: {v} \n"
    with open(path, "w+") as file:
        file.write(content)

    
def compare_hotspots_results(test, expected: ty.Dict[str, ty.Any]) -> bool:
    """
    Compares hotspot analysis results of the specified test and returns the result.
    
    :param test: the regression test to be verified.
    :type test: RegressionTest
    :param expected: the expected hotspots result reverse dict.
    :type expected: Dict[str, Any]
    :return: `True` if the results were correct, `False` otherwise.
    :rtype: bool
    """
    success = True

    hr = hotspots_results(test.analysis)
    hr_rd = hr.reverse_dict()

    for key, value in expected.items():
        if key not in hr_rd:
            log.quiet(colors.brown + "     missing injection point '{}'.".format(key) + colors.reset)
            success = False

        # Values
        for order in range(test.analysis.max_order() + 1):
            (fc, tc, mc) = hr_rd[key][order]
            str_val = "[{}, {}, {}]".format(fc, tc, mc)
            (fc2, tc2, mc2) = value[order]
            str_val2 = "[{}, {}, {}]".format(fc2, tc2, mc2)
            if str_val != str_val2:
                log.quiet(colors.brown + "     invalid value '{}' for injection point '{}', expected '{}'.".format(str_val, key, str_val2) + colors.reset)
                success = False

    for key in hr_rd:
        if key not in expected:
            log.quiet(colors.brown + "     unexpected injection point '{}'.".format(key) + colors.reset)
            success = False

    _log_test("  ", " hotspots tests", success)

    return success

class RegressionTest(_test.Test):
    """
    The RegressionTest class represents a regression test.

    It holds the corresponding analysis parameters and the expected results and provides methods to run the test.
    """
    def __init__(self, name: str, analysis: Analysis, expected: ty.Dict[str, object], execute: ty.Dict[str, object]):
        """        
        **__init__ (ctor):**  Init the RegressionTest object with the specified name, analysis and expected results.

        **Kwargs**:
         - `path` (`str`): path of the analysis in which intermediate files and reports will be stored. *default*: *automatic empty folder*.
         - *see `analysis` and `execute` documentation*.

        :param name: the name of the test.
        :type name: str
        :param analysis: the analysis parameters.
        :type analysis: Analysis
        :param expected: the dictionary of expected results.
        :type expected: Dict[str, object]
        :param execute: the dictionary of parameters to the `execute` command for the test. 
        :type execute: Dict[str, object]
        """
        super().__init__(name)
        self.analysis = analysis #: Analysis: The analysis parameters.
        self.expected = expected #: Dict[str, object]: The dictionary of expected results. Test verifications depend on the available key in this dictionary.
        self.execute = execute #: Dict[str, object]: parameters to pass to execute function.

    def run(self, path: str):
        """        
        Run the test and return a boolean determining if the test was successful.

        :param path: override path for analysis.
        :type path: str
        :return: `True` if the test was successful, `False` otherwise.
        :rtype: bool
        """
        vb = log.current_level()
        log.set_verbosity(VerbosityLevel.Warning)
        pp = options.print_progress()
        options.set_print_progress(False)
        success = True

        def log_exception(msg: str, err: Exception, rethrow = False):
            """
            Logs an exception with the specified and print the stack trace. Set the local variable `success` to `False`.

            :param msg: the message to be printed. 
            :type msg: str
            :param err: the exception to be log.
            :type err: Exception
            :param rethrow: if `True`, the exception is rethrown, defaults to False
            :type rethrow: bool, optional
            :raises err: if `rethrow` is `True`.
            """
            log.warning("exception occurred during execution of {} tests: {}.".format(msg, err))
            traceback.print_exc() 
            global success
            success = False
            if rethrow:
                raise err
            
        res = None
        
        try:
            #log.set_verbosity(VerbosityLevel.Dev)
            self.analysis._path = path
            #print(self.analysis)

            self.execute["no_display"] = True
            res = execute(self.analysis, **self.execute)

            success = True
            
        except Exception as err:
            log.warning(str(err) + ", during analysis execution for test {}".format(self.name))
            success = False
            traceback.print_exc() 

        if res is None:
            log.warning(f"cannot find the results for the test {self.name}.")
            return False
        
        log.set_verbosity(vb)

        # Actual Check
        for item, v in self.expected.items():

            if item == "traces":
                continue
            if item == "hotspots": 
                try:
                    success = success and compare_hotspots_results(self, v)
                except Exception as err:
                    success = False
                    log_exception("hotspots tests ", err, False)
                continue

            if item not in res:
                log.warning(f"expected key {item} is not found.")
                success = False
                continue

            # STD metrics
            v_res = res[item]
            if item in Metrics.GROUP_SERIES:
                v = [str(i) for i in v] # Stringify
                if len(v) == len(v_res):
                    if set(v) == set(v_res):
                        continue
                log.warning(f"invalid value {v_res} for '{item}', expected {v}.")
                success = False

            else:
                if str(v) != str(v_res):
                    success = False
                    log.warning(f"invalid value {v_res} for '{item}', expected {v}.")

        _log_test("", "test {}".format(self.name), success)
                
        log.set_verbosity(vb)
        options.set_print_progress(pp)

        return success