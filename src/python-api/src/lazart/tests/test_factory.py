""" The module *lazart.tests.test_factory* contains definitions to find and run tests.

Note that only *regression* tests use the factory, unit test are based on `unitest` package.

:since: 3.1
:author: Etienne Boespflug
"""

# Python
import time
import os
import fnmatch
import glob
import yaml
import traceback
import typing as ty

# Lazart
import lazart.logs as log

# Internal
from lazart._internal_.util import str_time as _str_time
import lazart._internal_.color as colors

# Core
from lazart.core.analysis import Analysis, AnalysisFlag

# Tests
from lazart.tests.regression import RegressionTest

full_print = True #: bool: Boolean determining if all output will be printed.

def find_files(directory: str, pattern: str):
    """
    Recursively find all files in the specified directory matching the pattern (unix-style).

    :param directory: the root directory in which files will be searched.
    :type directory: str
    :param pattern: the pattern (unix-style) for the files.
    :type pattern: str
    :yield: the files following the patterns.
    :rtype: str
    """
    for root, dirs, files in os.walk(directory):
        for basename in files:
            if fnmatch.fnmatch(basename, pattern):
                filename = os.path.join(root, basename)
                yield filename


def parse_test(lzt_path: str) -> RegressionTest:
    """
    Parse the .lzt test file at `lzt_path` and return the corresponding RegressionTest object.

    :param lzt_path: the path of the test.
    :type lzt_path: str
    :return: the parsed test object.
    :rtype: RegressionTest
    """
    log.debug("parsing {}".format(lzt_path))

    def abort(msg):
        log.warning(str(msg) + " when parsing test {}.".format(lzt_path))
        traceback.print_exc() 
        return None

    try:

        with open(lzt_path, "r") as file:
            try:
                obj = yaml.safe_load(file)
            except yaml.YAMLError as err:
                return abort(err)

        # Regression test parsing                
        if obj["type"] != "regression":
            return abort(f"unknown test type {obj['type']}")
        
        # Build analysis
        analysis_args = obj["analysis"]

        if "glob" in analysis_args:
            glob_path = analysis_args["glob"]
            input_files = glob.glob(glob_path + "/*.c")
            del analysis_args["glob"]
        else:
            input_files = analysis_args["input_files"]
            del analysis_args["input_files"]
        attack_model = analysis_args["attack_model"]
        del analysis_args["attack_model"]

        # Corrections
        if "flags" in analysis_args:
            try:
                analysis_args["flags"] = AnalysisFlag(analysis_args["flags"])
            except Exception as e:
                return abort(f"unsupported analysis type: {e}")
            
            flags = analysis_args["flags"]

            if flags.isset(AnalysisFlag.DetectorOptimization) or flags.isset(AnalysisFlag.CMPlacement):
                return abort("unsupported analysis type for regression {flags} CCPO")

        analysis = Analysis(input_files, attack_model, **analysis_args)

        exec_params = {}
        if "execute" in obj:
            exec_params = obj["execute"]

        rt = RegressionTest(obj["name"], analysis, obj["expected"], exec_params)        
        if "fast" in obj and not obj["fast"]:
            rt.fast = False

        return rt
    except KeyError as err:
        return abort("Error occurred when parsing test {}: {}.".format(lzt_path, err))

def run_tests(tests: ty.List[RegressionTest], fast: bool = False):
    """
    Run a list of tests, printing progress and errors.

    :param tests: the list of tests to be run.
    :type tests: ty.List[RegressionTest]
    :param fast: determines if slows tests should be ran, defaults to False
    :type fast: bool, optional
    """
    start = time.process_time()
    passed = 0
    error = 0

    #log.set_verbosity(log.VerbosityLevel.Dev)

    i = 0
    for test in tests:
        i = i + 1

        if fast and not test.fast:
            continue
        
        percentage = ((passed + error) / len(tests)) * 100
        if full_print:
            log.rquiet("\rrunning test {} [{}/{}] {}%...".format(test.name, passed + error + 1, len(tests), int(percentage)))

        if test.run("build/analysis-results" + str(i)):
            passed = passed + 1
        else:
            error = error + 1

    end = time.process_time()
    duration = end - start
    if error == 0:
        log.info(colors.reset + colors.green + "\r{}/{} tests passed in {}.".format(str(passed), len(tests), _str_time(duration)) + colors.reset)
    else:
        log.info(colors.reset + colors.red + "\r{}/{} tests passed in {} ({} errors).".format(str(passed), len(tests), _str_time(duration), str(error)) + colors.reset)


def read_folder(path: str) -> ty.List[RegressionTest]:
    """
    Recursively parse all tests in the folder `path` and return the list of RegressionTest.

    :param path: the root folder in which the test will be searched.
    :type path: str
    :return: the list of all found tests.
    :rtype: List[RegressionTest]
    """
    # Read all folders  

    tests = []

    for filename in find_files(path, '*.lzt'):
        t = parse_test(filename)
        if t is not None:
            tests.append(t)

    return tests

def lazart_tests(path: str, fast: bool):
    """
    Read and run all test in the folder `path` and display the results. 

    :param path: the root folder in which the test will be searched.
    :type path: str
    :param fast: long test (long: true) are ignored.
    :type fast: bool
    """
    # regression
    tests = read_folder(path)
    run_tests(tests, fast)
