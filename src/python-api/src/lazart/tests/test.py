""" The module *lazart.tests.test* contains general definitions for Lazart regression tests.

:since: 3.1
:author: Etienne Boespflug
"""

class Test:
    """
    The Test class is a virtual base class for all tests.
    """

    def __init__(self, name: str):
        """        
        **__init__ (ctor):**  Init the Test object with the specified name.

        :param name: the name of the test.
        :type name: str
        """
        self.name = name #: str: The name of the test.
        self.fast = True #: bool: Flag determining if the test is fast to run. Default to `True`. 

    def run(self, path: str) -> bool:
        """
        Run the test and return a boolean determining if the test was successful.

        :param path: override path for analysis and results.
        :type path: str
        :return: `True` if the test was successful, `False` otherwise.
        :rtype: bool
        """

        return False
