""" The module *lazart.core.io* contains definitions for saving and reading **Lazart**'s analysis and extensions.

:since: 3.0
:author: Etienne Boespflug 
"""

# Python
import argparse
import os
import typing as ty

# Lazart
import lazart.constants as constants
import lazart.options as options

# Internal
import lazart._internal_.extensions as _ext
import lazart._internal_.folder as _folder

# Core
from lazart.core.analysis import Analysis
    
def save(analysis: Analysis):
    """
    Saves the analysis to its folder.

    Each extension is saved in its own file in '.lazart/'. 

    *Note:* :class:`~lazart.analysis.attack_redundancy.AttackRedundancyResults` *is not saved in current version.*

    :param analysis: the analysis to be saved.
    :type analysis: Analysis
    """
    analysis.save()

    for extension in analysis._extensions:
        extension.save_disk()

def read(root_folder: str, **kwargs) -> Analysis:
    """
    Reads and returns the analysis stored in the specified folder.

    *Note:* :class:`~lazart.analysis.attack_redundancy.AttackRedundancyResults` *is not saved in current version.*

    :param root_folder: the path to the folder in which the analysis is saved.
    :type root_folder: str
    :raises Exception: if the operation fail (folder doesn't exists or the object is not an analysis).
    :return: the analysis read from the folder.
    :rtype: Analysis
    """
    
    obj = _ext.unpickle(os.path.join(root_folder, constants.lazart_dir, constants.analysis_file))
    if type(obj) is not Analysis:
        raise Exception("invalid object type '{}'".format(type(obj)))

    restart = kwargs.get("restart", options.max_int())
    for extension in obj._extensions:
        extension.analysis = obj # reassign
        extension.load_disk(restart)
    
    return obj

def read_or_make(input_files: ty.List[str], attack_model: dict, **kwargs) -> Analysis:
    """
    Tries to read the analysis at the specified path and return it. If no analysis can be found in the specified folder, a new analysis is created with the specified parameters.
    `params` keyword argument is used to override parameters with CLI's provided values.

    ***Notes:*** no check are made to ensure that the selected folder (from keyword argument or script's argument) matches the specified analysis parameters.*

    **Kwargs**:
        - `params` (`dict`): the params dictionary contains additional parameters for the analysis from the CLI.
        - *see constructor*: :class:`~lazart.core.analysis.Analysis` for other analysis parameters. 

    :param input_files: the list of path to source file of the analysis.
    :type input_files: List[str]
    :param attack_model: the attack model of the analysis.
    :type attack_model: dict
    :return: the read or newly created analysis.
    :rtype: Analysis
    """
    restart = options.max_int()

    # Replace params by actual params.
    if "params" in kwargs:
        params = kwargs.get("params", argparse.Namespace())
        
        if "restart" in params:
            restart = int(params.restart)

        if "output_folder" in params:
            if "path" in kwargs:
                kwargs["path"] = params.output_folder

        if "order" in params:
            kwargs["max_order"] = params.order

        # TODO(): other arguments ? Enforce values ?        
       
    path = str(os.path.abspath(kwargs.get("path", ".")))
    
    analysis = None
    if restart <= 0 or not os.path.exists(_folder.lazart_file(path, constants.analysis_file)):
        # Make analysis
        analysis = Analysis(input_files, attack_model, **kwargs)
        analysis.clean()
    else:
        # Read analysis
        analysis = read(path, restart=restart)

    # TODO: detect different params ? 

    return analysis