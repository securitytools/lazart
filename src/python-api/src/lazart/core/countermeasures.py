""" The module *lazart.core.countermeasures* contains definitions for *systematic countermeasure* handling in **Lazart**.

:since: 4.0
:author: Etienne Boespflug 
"""

# Python 
import typing as ty

# Lazart
import lazart.logs as log

# Internal
import lazart._internal_.util as _util

def cm_lm(params: dict = {}) -> dict:
    """
    Returns a countermeasure object with *Load Multiplication* type with specified parameters.

    :param params: additional parameters for the countermeasure, defaults to {}
    :type params: dict, optional
    :return: representing the countermeasure.
    :rtype: dict
    """
    return _util.create_typed_dict("load-multiplication", params)

def cm_tm(params: dict = {}) -> dict:
    """
    Returns a countermeasure object with *Test Multiplication* type with specified parameters.

    :param params: additional parameters for the countermeasure, defaults to {}
    :type params: dict, optional
    :return: representing the countermeasure.
    :rtype: dict
    """
    return _util.create_typed_dict("test-multiplication", params)
    
def cm_sec_swift(params: dict = {}) -> dict:
    """
    Returns a countermeasure object with *SecSwift CF* type with specified parameters.

    :param params: additional parameters for the countermeasure, defaults to {}
    :type params: dict, optional    
    :return: representing the countermeasure.
    :rtype: dict
    """
    return _util.create_typed_dict("sec_swift", params)

def verify_countermeasure(cm: dict) -> bool:
    """
    Verify that a model dictionary is well-formed for **Wolverine**.

    TODO: more verification needed.

    :param cm: the countermeasure object to be checked.
    :type cm: dict
    :return: `True` if the countermeasure is valid, `False` otherwise.
    :rtype: bool
    """
    if "type" not in cm:
        log.warning("no type specified for cm {}.".format(cm))
        return False

    valid = True
    
    if cm["type"] == "test-multiplication" or cm["type"] == "tm":
        valid = valid# and _args.warning_unknown_fields(model, ["name", "type", "vars", "all", "exclude", "use-sym-fct", "use-sym"], "model {}".format(model))         
         
        if "on" not in cm:
            log.warning("required 'on' field for cm {}.".format(cm))

        if "cm_fct" in cm:
            pass

        if "depth" in cm:
            pass

        if "ccps" in cm:
            pass
    elif cm["type"] == "load-multiplication" or cm["type"] == "lm":

         
        if "on" not in cm:
            log.warning("required 'on' field for cm {}.".format(cm))

        if "cm_fct" in cm:
            pass

        if "depth" in cm:
            pass

        if "ccps" in cm:
            pass

    elif cm["type"] == "sec_swift":
        valid = valid# and _args.warning_unknown_fields(model, ["name", "type", "vars", "all", "exclude", "use-sym-fct", "use-sym"], "model {}".format(model)) 
        if "on" not in cm:
            log.warning("required 'on' field for cm {}.".format(cm))
        if "cm_fct" in cm:
            pass
        if "depth" in cm:
            pass
        if "ccps" in cm:
            pass

    else:
        log.warning("unknown countermeasure type '{}' for cm {}.".format(cm["type"], cm))
        valid = False

    return valid

def verify_countermeasures(cms: ty.List[dict]) -> bool:
    """
    Verifies that a countermeasure list is correct, checking each countermeasure object.

    :param cms: the list of countermeasures objects to be checked.
    :type cms: List[dict]
    :return: `True` if the countermeasures are valid, `False` otherwise.
    :rtype: bool
    """
    correct = True

    if len(cms) > 1:
        log.warning("chaining countermeasures is not supported for now.")

    for cm in cms:
        correct = correct and verify_countermeasure(cm)

    return correct

# Utils 
def secswift_data_model() -> dict:
    """
    Returns the default data fault model for SecSwift countermeasures which attacks GSR and RTS with symbolic data injection.

    :return: the default fault model for SecSwift CF
    :rtype: dict
    """
    from lazart.core.attack_model import data_model
    return data_model({"vars": {
                "_LZ__ss_GSR":"__sym__",
                "_LZ__ss_RTS":"__sym__"
            }})