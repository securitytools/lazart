""" The module *lazart.core.attack_model* provides functions to parse and verify the attack model of an analysis.

:since: 3.2
:author: Etienne Boespflug 
"""

# Python
import copy
import typing as ty

# Internal
import lazart._internal_.args as _args
import lazart._internal_.util as util

# Lazart
import lazart.logs as log

def ti_model(params: dict = {}) -> dict:
    """
    Returns a model object with *test-inversion* type.

    :param params: additional parameters for the model, defaults to {}.
    :type params: dict, optional
    :return: a dictionary representing the fault model.
    :rtype: dict
    """
    return util.create_typed_dict("test-inversion", params)

def data_model(params: dict = {}) -> dict:
    """
    Returns a model object with *data-load* type.

    :param params: additional parameters for the model, defaults to {}
    :type params: dict, optional
    :return: a dictionary representing the fault model.
    :rtype: dict
    """
    return util.create_typed_dict("data", params)

def fault_space_list(fs_list: ty.List[str], models:  ty.Union[dict, ty.List[dict]], fs_name = "functions") -> dict:
    """
    Returns a dictionary corresponding to the fault space associating the fault models `models` for each fault-space object in `fs_list` (can be used for basic blocks or functions).

    :param fs_list: the list of the fault space objects to be associated with models.
    :type fs_list: List[str]
    :param models: a model or a list of fault models.
    :type models: Union[dict, List[dict]]
    :param fs_name: the name of the fault space type, used for error reporting messages, defaults to "functions"
    :type fs_name: str, optional
    :return: the fault space dictionary.
    :rtype: dict
    """
    ret = {}

    if type(models) is dict:  # noqa: E721
        models = [models]
    if type(models) is not list:  # noqa: E721
        log.warning("unknown value type for model '{0}'. ")
        return {fs_name: ret}

    for fct in fs_list:
        ret[fct] = {"models": models}

    return {fs_name: ret}

def functions_list(fct_list: ty.List[str], models: ty.Union[dict, ty.List[dict]]) -> dict:
    """
    Returns a dictionary corresponding to the fault space associating the fault models `models` for each function in `fct_list`.

    :param fct_list: the list of the functions to be associated with models.
    :type fct_list: List[str]
    :param models: a model or a list of fault models.
    :type models: Union[dict, List[dict]]
    :return: the fault space dictionary.
    :rtype: dict
    """ 
    return fault_space_list(fct_list, models, "functions")

def bb_list(bb_names: ty.List[str], models: ty.Union[dict, ty.List[dict]]) -> dict:
    """
    Returns a dictionary corresponding to the fault space associating the fault models `models` for each basic block in `bb_names`.

    :param bb_names: the list of the basic blocks to be associated with models.
    :type bb_names: List[str]
    :param models: a model or a list of fault models.
    :type models: Union[dict, List[dict]]
    :return: the fault space dictionary.
    :rtype: dict
    """ 
    return fault_space_list(bb_names, models, "basic-blocks")

def clean_duplicate_model(models: ty.List[dict]) -> dict:
    """ 
    TODO: **Not implemented**.
    """
    return models

def merge_am(fs1: dict, fs2: dict):
    ret = copy.deepcopy(fs1)
    if "functions" in fs2:
        if "functions" not in ret:
            ret["functions"] = {}
        ret["functions"].update(fs2["functions"]) # TODO incorrect, need merge ? 
        # TODO remove dup

    if "basic-blocks" in fs2:
        if "basic-blocks" not in ret:
            ret["basic-blocks"] = {}
        ret["basic-blocks"].update(fs2["basic-blocks"])
        # TODO remove dup

    return ret

def empty_model() -> dict:
    """
    Returns an empty fault space dictionary.

    :return: an empty fault space dictionary.
    :rtype: dict
    """
    return {
    }

def _verify_data_value(value, msg: str) -> bool:
    """
    Helper function to verify that a data model value is correct. `msg` corresponds to the model description to be printed on the console when an error is detected.

    TODO: for now only check type (scalar or string) but not value.

    :param value: the value to be
    :type value: 
    :param msg: the description of the model to be printed on errors.
    :type msg: str
    :return: `True` if the value is valid, `False` otherwise.
    :rtype: bool
    """
    if isinstance(value, int):
        return True
    if isinstance(value, str):
        return True
    log.warning("expected string or scalar value for {}.".format(msg))
    return False

def verify_model(model: dict) -> bool:
    """
    Verify that a model dictionary is well-formed for **Wolverine**.

    :param model: The model to be verified. 
    :type model: dict
    :return: `True` if the model is valid, `False` otherwise.
    :rtype: bool
    """
    if "type" not in model:
        log.warning("no type specified in model {}.".format(model))
        return False

    valid = True

    if "name" in model:        
        if not isinstance(model["name"], str):
            log.warning("error, expected str for the name in model.")
            valid = False
    
    if model["type"] == "test-inversion":
        _args.warning_unknown_fields(model, ["name", "type", "inline", "protected_detectors"], "model {}".format(model))
    elif model["type"] == "data":
        _args.warning_unknown_fields(model, ["name", "type", "inline", "vars", "all", "exclude", "use-sym-fct", "use-sym"], "model {}".format(model))
         
        if "all" in model:
            valid = valid and _verify_data_value(model["all"], "in field 'all' for model {}".format(model))

        if "vars" in model:
            pass

        if "exclude" in model:
            pass

    else:
        log.warning("unknown model type '{}' for model {}.".format(model["type"], model))
        valid = False

    return valid

def verify_attack_model(attack_model: dict) -> bool:
    """
    Verify that an attack model dictionary is well-formed for **Wolverine**.

    :param attack_model: the attack model to be verified.
    :type attack_model: dict
    :return: `True` if the attack model is valid, `False` otherwise.
    :rtype: bool
    """
    correct = True

    def verify_fault_space_section(fs_field_name, fs_name, attack_model):
        for fs, fs_am in attack_model[fs_field_name].items():
            if not isinstance(fs_am, dict):
                log.warning(f"expected dict for fault space in {fs_name} '{fs}' in {attack_model}.")
                correct = False
                continue

            if "models" not in fs_am:
                log.warning(f"expected 'models' field for fault space in {fs_name} '{fs}' in {attack_model}.")
                correct = False
                continue

            if not isinstance(fs_am["models"], list):
                log.warning(f"expected list type for 'models' field for fault space in {fs_name} '{fs}' in {attack_model}.")
                correct = False
                continue
                
            for model in fs_am["models"]:
                if not verify_model(model):
                    log.warning(f"invalid model '{model}' in {fs_name} '{fs}' in {attack_model}.")
                    correct = False


    if "functions" in attack_model:
        verify_fault_space_section("functions", "function", attack_model)

    if "basic-blocks" in attack_model:
        verify_fault_space_section("basic-blocks", "basic block", attack_model)

    return correct
