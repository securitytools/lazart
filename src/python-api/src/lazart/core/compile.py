""" The module *lazart.core.compile* contains definitions for the **preprocessing, compilation and countermeasure step**.

:since: 3.0
:author: Etienne Boespflug 
"""

# Python
import os
import subprocess
import datetime
import glob
import time
import typing as ty

# Lazart
import lazart.logs as log
import lazart.constants as constants

# Internal
import lazart._internal_.util as util
from lazart._internal_.util import str_time as _str_time
import lazart._internal_.extensions as _ext
import lazart._internal_.folder as _folder

# Core
from lazart.core.analysis import Analysis, AnalysisFlag

def _disassemble(source_bc: str, source_ll: str, **kwargs) -> bool:
    """
    Disassembles the bytecode at the path `source_bc` and output in `source_ll` path.

    **Kwargs**:
     - `args` (`List[str]`): a list of additional arguments to the command. *default*: `[]`.

    :param source_bc: the path to the bytecode to be disassembled.
    :type source_bc: str
    :param source_ll: the path to put the output the disassembled LLVM file.
    :type source_ll: str
    :return: `True` if the operation succeeded, `False` otherwise.
    :rtype: bool
    """
    args = kwargs.get("args", [])
    
    cmd = [constants.dis_cmd, source_bc, "-o", source_ll]

    if len(args) > 0:
        cmd.extend(args.split(" "))    
    log.verbose(" => " + " ".join(cmd)) 

    return util.exec_cmd(cmd) == 0

def _generate_graphs(analysis : Analysis, generate_pdf: bool, functions: ty.List[str] = None) -> bool:
    """
    Generates .dot Control Flow Graphs (CFG) for each function of the attack model. 

    TODO: *deprecated*.

    :param analysis: the analysis for which generate the CFG.
    :type analysis: Analysis
    :param generate_pdf: boolean indicating if the PDF version should be saved too.
    :type generate_pdf: bool
    :param functions: the list of functions to generate, default to `None`.
    :type functions: List[str], optional
    :return: `True` if the operation succeeded, `False` otherwise.
    :rtype: bool
    """
    graph_dir = os.path.join(analysis.path(), constants.graphs_folder)
    old_dir = _folder.check_dir(graph_dir)
    main_bc = os.path.relpath(analysis.path()) + "/" + constants.preprocessed_bc

    _clean_graph_dir(analysis)

    # Generate extended DOT files.

    cmd = ["opt", "-dot-cfg", main_bc]
    log.verbose(" => " + " ".join(cmd))
    util.exec_cmd(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    for dot_file in glob.glob("*.dot"):
        print("df = " + dot_file)
        basename, ext = os.path.splitext(dot_file)
        os.rename(dot_file, basename + ".ll" + ext)

    # Generate PDF for studied functions.
    if generate_pdf:
        if functions == None:
            functions = []
            am = analysis.attack_model()
            if type(am) != str:  # noqa: E721
                if "functions" in am:
                    for fct in am["functions"]:
                        if fct != "__all__" and not fct.startswith("_LZ__"):
                            functions.append(fct)

        if "main" not in functions: functions.append("main")
        #if "countermeasure" not in functions: functions.append("countermeasure")
        #if "oracle" not in functions: functions.append("oracle")

        for function in functions:
            cmd = ["dot", "-Tpdf", constants.dot_file.format(function), "-o", constants.pdf_graph_file.format(function)]

            log.debug("  => " + " ".join(cmd))

            util.exec_cmd(cmd)

    old_dir.revert()

    return True

def _link(source_bc: str, **kwargs) -> bool:
    """
    Links all .bc files inside the current directory and saves the bytecode to the specified output file.

    **Kwargs**:
     - `args` (`List[str]`): a list of additional arguments to the command. *default*: `[]`.

    :param source_bc: the path to the main.bc in which labels will be restored.
    :type source_bc: str
    :return: `True` if the operation succeeded, `False` otherwise.
    :rtype: bool
    """
    args = kwargs.get("args", [])
    cmd = [constants.linker_cmd] + glob.glob("*.bc") + ["-o", source_bc]
    if len(args) != 0:
        cmd += args
    log.verbose(" => " + " ".join(cmd))
    
    subprocess.call(" ".join(cmd).split())
    
    return util.exec_cmd(cmd) == 0

def _compile_to_llvm(analysis: Analysis, input_files: ty.List[str], **kwargs) -> bool:
    """
    Compiles the specified input_files with clang. Adds standard lazart defines and options.

    **Kwargs**:
     - `args` (`ty.List[str]`): a list of additional arguments to the command. *default*: `[]`.

    :param analysis: the analysis to be compiled.
    :type analysis: Analysis
    :param input_files: the list of input files of the analysis.
    :type input_files: List[str]
    :return: `True` if the operation succeeded, `False` otherwise.
    :rtype: bool
    """
    args = kwargs.get("args", [])
    
    cmd = [constants.compiler_cmd, "-c", "-g"] + input_files + glob.glob(os.path.join(constants.lazart_src_path, "*")) + ["-emit-llvm"] + constants.lazart_defines_args.split(" ") + ["-D_LZ__FAULT_LIMIT=" + str(analysis.max_order())]
    
    cmd += ["-I", constants.klee_include_path] # Klee path
    cmd += ["-I", constants.lazart_include_path] # Lazart API

    cmd.extend(args.split(" "))
        
    # Analysis dependent defines
    if analysis.flags().isset(AnalysisFlag.AttackAnalysisOnly):
        cmd.append(str("-D" + constants.attack_analysis_define))
    if analysis.flags().isset(AnalysisFlag.DetectorOptimization):
        cmd.append(str("-D" + constants.ccpo_define))

    log.verbose(" => " + " ".join(cmd)) 
    
    return util.exec_cmd(cmd) == 0

def _clean_graph_dir(analysis: Analysis):
    """
    Clean the graph directory of the analysis.

    :param analysis: the analysis to be clean.
    :type analysis: Analysis
    """
    graph_dir = os.path.join(analysis.path(), constants.graphs_folder)

    for it in glob.glob(os.path.join(graph_dir, "*")):
        os.remove(it)

def _clean_local_dir():
    """
    Clean local directory for compilation step, removing ".bc" files.
    """
    for it in glob.glob("*.bc"):
      os.remove(it)

class CompileResults(_ext.AnalysisStep):
    """ 
    The CompileResults class holds information about the *compilation and preprocessing step*.
    """ 
    def __init__(self, analysis: Analysis, duration: float, date: datetime.date, **kwargs):
        """
        **__init__ (ctor):** Constructs a new CompileResults object with the specified data.

        :param analysis: the compiled analysis.
        :type analysis: Analysis
        :param duration: the duration of the compilation.
        :type duration: float
        :param date: the date of the compilation.
        :type date: datetime.date
        """
        self._analysis = analysis #: Analysis: The analysis object to which the *extension* is associated. See :func:`~lazart._internal_.extensions.register`.

        self._duration = duration #: float: The duration of the compilation.
        self._date = date #: datetime.date: The date of the compilation.

    # G/S

    def duration(self) -> float:
        """
        Returns the duration of the compilation (as a number of seconds).

        :return: the duration of the compilation.
        :rtype: float
        """
        return self._duration

    def date(self) -> datetime.date: 
        """
        Returns the date of the compilation.

        :return: the date of the compilation.
        :rtype: datetime.date
        """
        return self._date

    def __str__(self):
        """
        Returns the string representing the CompileResults.

        :return: the string representation of the object.
        :rtype: str
        """              
        ret = "Compilation results: {}\n".format(self._analysis._name)
        ret += " - compile-date: {}".format(str(self.date())) + "\n"
        ret += " - compile-duration: {}".format(_str_time(self.duration())) + "\n"
        return ret

def compile(analysis : Analysis, **kwargs) -> CompileResults:
    """
    Executes the *preprocessing and compilation* step for the analysis and returns the compilation information.

    This function uses log to display progress, reduce verbosity level to reduce output.

    :param analysis: the analysis to be compiled.
    :type analysis: Analysis
    :raises Exception: if a substep failed.
    :return: the results of the compilation.
    :rtype: CompileResults
    """
    
    _folder.make_analysis_folder(analysis)
    
    start = time.process_time()
    _clean_local_dir()
    
    main_bc_path = os.path.relpath(analysis.path()) + "/" + constants.source_bc
    main_ll_path = analysis.path() + "/" + constants.source_ll

    log.verbose("Compiling program (" + analysis.name() + " )...")
    
    if not _compile_to_llvm(analysis, analysis.input_files(), args=analysis.compiler_args()):
        raise Exception("operation failed.")

    if not _link(main_bc_path, args=analysis.linker_args()):
        raise Exception("operation failed.")

    # if not kwargs.get("no_graph", False) and not _generate_graphs(analysis, True):
    #    raise Exception("operation failed.")

    if not _disassemble(main_bc_path, main_ll_path, args=analysis.dis_args()):
        raise Exception("operation failed.")

    end = time.process_time()
    duration = end - start
    log.info("compilation process ended in {}.".format(_str_time(duration)))
    _clean_local_dir()

    return CompileResults(analysis, duration, datetime.datetime.now())
    
class CompileStep(_ext.AnalysisStep):
    """
    Subclass of AnalysisStep for the *compilation and preprocessing step*, see :class:`lazart._internal_.extensions.AnalysisStep`.
    """
    def __init__(self, analysis: Analysis):   
        """
        **__init__ (ctor):** Constructs a new CompileStep object associated to the specified analysis.

        :param analysis: the analysis of the step.
        :type analysis: Analysis
        """     
        super(CompileStep, self).__init__(analysis, constants.compile_ext,  _ext.StepID.Compile)

    def execute(self, **kwargs):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        return compile(self.analysis, **kwargs)

    def init(self):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        self.analysis.init()
        
    def clean(self):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        _folder.remove_file(_folder.lazart_file(self.analysis.path(), constants.compile_ext))
        _folder.remove_file(os.path.join(self.analysis.path(), constants.source_bc)) # main.bc
        _folder.remove_file(os.path.join(self.analysis.path(), constants.source_ll))

def compile_results(analysis: Analysis, **kwargs) -> CompileResults:
    """
    Returns the results of the compilation for the analysis.

    Uses extension (traces_ext, see :class:`lazart._internal_.extensions.AnalysisStep`) system to reuse disk/RAM cache if available.
    This function uses log to display progress, reduce verbosity level to reduce output.   

    :param analysis: the analysis
    :type analysis: Analysis
    :return: the compilation results.
    :rtype: CompileResults
    """
    step = _ext.get_step(CompileStep(analysis))
    
    return step.get_results(**kwargs)

class CompileStepExternal(CompileStep):
    # TODO: doc (interopt)
    def __init__(self, analysis, main_bc_path):   
        super(CompileStep, self).__init__(analysis, constants.compile_ext,  _ext.StepID.Compile)
        self.main_bc_path = main_bc_path
        
    def execute(self, **kwargs):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        # Verify bc.

        if not os.path.exists(self.main_bc_path):
            log.warning(f"cannot find path: {self.main_bc_path}")

        return CompileResults(self.analysis, 0, datetime.datetime.now())

def external_compilation(a, main_bc_path: str, **kwargs):
    # TODO: doc (interopt)
    step = _ext.get_step(CompileStepExternal(a, main_bc_path))

    return step.get_results(**kwargs)