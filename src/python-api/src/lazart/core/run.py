""" The module *lazart.core.run* contains definitions for the mutation and Dynamic Symbolic Execution step. It includes wolverine and klee calls.

:since: 3.0
:author: Etienne Boespflug 
"""

# Python
import os
import subprocess as sp
import glob
import time
import datetime
import yaml 
import typing as ty

# Lazart

import lazart.options as options
import lazart.logs as log
import lazart.constants as constants

# Internal

import lazart._internal_.util as util
from lazart._internal_.util import str_time as _str_time
import lazart._internal_.extensions as _ext
import lazart._internal_.args as _args
import lazart._internal_.folder as _folder
import lazart._internal_.color as color

# Core

from lazart.core.analysis import Analysis

def _run_dse(analysis: Analysis) -> bool:
    """    
    Runs the *Dynamic Symbolic Execution* step for the analysis.

    Renames the output 'klee-out-0' to Lazart's format (default 'apply_X_test').

    :param analysis: the analysis on which the DSE will be run.
    :type analysis: Analysis
    :return: true if the step has been manually interrupted, false otherwise.
    :rtype: bool
    """
    cmd = [constants.dse_cmd]
    
    if analysis.klee_args() != "":
        cmd.extend(analysis.klee_args().split(" "))
    # Clean call
    cmd = list(filter(lambda x: x != "" and x != " ", cmd))

    cmd.extend(["./" + constants.mutated_bc])

    log.verbose(" => " + " ".join(cmd))
    
    interrupted = False

    ec = -1
    try:
        ec = util.exec_cmd(cmd, stdout=sp.PIPE, stderr=sp.PIPE)
    except KeyboardInterrupt:
        interrupted = True
        
        log._raw(color.red + "keyboard interrupt: " + color.reset + " DSE ending.")
        
    if os.path.exists("klee-out-0"): # will throw in trace folder.
        os.rename("klee-out-0", constants.dse_folder)
    else:
        log.verbose("waiting for KLEE...")
        time.sleep(1)
        if os.path.exists("klee-out-0"): # will throw in trace folder.
            os.rename("klee-out-0", constants.dse_folder)
        else:
            here = glob.glob("./*")
            log.debug(f"glob: [{', '.join(here)}].")
            log.error(f"DSE failed (code {ec}).")
    return interrupted

def _link():
    """
    Links the last LLVM bytecode ('main.mut.bc').
    """
    cmd = [constants.linker_cmd, "main.mut.bc", "-o=" + constants.mutated_bc]

    log.debug(" => " + " ".join(cmd))

    util.exec_cmd(cmd, stdout=sp.DEVNULL, stderr=sp.DEVNULL)

def _build_replay():
    """
    Builds Klee's replay executable for current directory.
    """
    cmd = [constants.compiler_cmd, constants.mutated_bc, "-L", constants.klee_lib_path, "-I", constants.klee_include_path, "-lkleeRuntest", "-o", constants.replay_bc]

    log.debug(" => " + " ".join(cmd))

    util.exec_cmd(cmd, stdout=sp.DEVNULL, stderr=sp.DEVNULL)
    
def copy_am(analysis: Analysis):
    """
    Copy the specified attack model into the am.yaml file of the analysis. Assumes that `analysis._am` is `str`.

    :param analysis: the analysis corresponding to the attack model to be copied.
    :type analysis: Analysis
    """
    cmd = ["cp", analysis._am, os.path.join(analysis.path(), "am.yaml")]
    
    ret = util.exec_cmd(cmd, stdout=sp.STDOUT, stderr=sp.STDOUT)
    if ret != 0:
        log.error("operation failed: {}.".format(ret))

def generate_am(analysis: Analysis, filename: str, **kwargs):
    """
    Generate the attack model file `am.yaml` am.yaml file of the analysis. Assume that `analysis._am` is `dict`.

    :param analysis: the analysis for which the attack model is generated.
    :type analysis: Analysis
    :param filename: the file path for the file.
    :type filename: str
    """
    tasks = analysis._tasks
    tasks["countermeasures"] = analysis.countermeasures()

    am = {
        "version": constants.version_string,
        "gen": "python",
        "tasks": tasks,
        #"out": analysis._out,
        "mode": "le",
        "fault-space": analysis._am
    }

    with open(filename, 'w') as outfile:
        yaml.dump(am, outfile, default_flow_style=False)


def _wolverine_error_code(err_code: int) -> str:
    """
    Returns the string description of the error code returned by Wolverine.

    :param err_code: the error code.
    :type err_code: int
    :return: the description of the error.
    :rtype: str
    """
    if err_code == 0:
        return "success."
    if err_code == 1:
        return "invalid program arguments"
    if err_code == 2:
        return "cannot open source bytecode"
    if err_code == 3:
        return "invalid input attack model YAML file"
    if err_code == 4:
        return "input module broken"
    if err_code == 5:
        return "output module broken"
    if err_code == 6:
        return "cannot save module"
    return "unknown error, please look at the log file"
    
def _run_wolverine(analysis: Analysis, **kwargs):
    """    
    Runs *Wolverine* for the specified analysis.

    :param analysis: the analysis for which the bytecode will be mutated.
    :type analysis: Analysis
    """

    if isinstance(analysis._am, str):
        am_file = constants.attack_model
        copy_am(analysis)
    else:
        am_file = constants.attack_model
        generate_am(analysis, am_file)

    # wolverine am.yaml example.bc -o example_mut.bc -V debug
    cmd = [constants.wolverine_cmd, am_file, "main.bc", "-o", constants.mutated_bc, "-V", str(min(log._current_level.value + 1, 6)), "-f", str(analysis.max_order())]
    if len(analysis.wolverine_args()) > 0:
        cmd.extend(analysis.wolverine_args().split(" ")) 

    log.info(" => " + " ".join(cmd))

    ec, out = util.exec_cmd_pair(cmd)

    if "lz.debug.wolv" in options.defines():
        log.dev(out.decode("utf-8"))
    else:
        log.quiet(out.decode("utf-8"))

    if ec != 0:
        log.error("mutation failed: {}.".format(_wolverine_error_code(ec)))
        return    

    ret = util.exec_cmd(["llvm-dis", constants.mutated_bc], stdout=sp.STDOUT, stderr=sp.STDOUT)  
    if ret != 0:
        log.error("cannot disassemble mutant: {}.".format(ret))

class KleeResults:
    """
    The KleeResults class is a data structure containing information about KLEE's execution for a specific analysis.
    """
    def __init__(self, folder: str):
        """
        **__init__ (ctor):** Constructs a new KleeResults for the specified `order` by parsing Klee's output `folder`.

        :param folder: the folder containing Klee's output.
        :type folder: str
        """

        self.valid = False #: bool: Boolean determining if the parsing of Klee's results was successful. 
        self.klee_stats = dict() #: dict: A dictionary associating klee-stats key to values. Views klee-stats's help.

        self.valid = False #: bool: Boolean determining if the parsing of Klee's results was successful. 
        self.instructions = None
        self.full_branches = None
        self.partial_branches = None
        self.num_branches = None
        self.user_time = None
        self.num_states = None
        self.malloc_usages = None
        self.num_queries = None
        self.num_query_constructs = None
        self.wall_time = None
        self.covered_instructions = None
        self.uncovered_instructions = None
        self.query_time = None
        self.solver_time = None
        self.cex_cache_time = None
        self.fork_time = None
        self.resolve_time = None
        self.query_cex_cache_misses = None
        self.query_cex_cache_hits = None
        self.array_hash_time = None
        self.elapsed = None
        self.explored_paths = None
        self.avg_construct_per_query = None
        self.total_queries = None
        self.valid_queries = None
        self.invalid_queries = None
        self.query_cex = None
        self.total_instructions = None
        self.completed_paths = None
        self.generated_tests = None
        self.partial_paths = None

        self.klee_stats = dict() #: dict: A dictionary associating klee-stats key to values. Views klee-stats's help.

        try:
            # Warning
            warn_file_path = os.path.join(folder, "warnings.txt")
            if not os.path.exists(warn_file_path):
                log.warning(warn_file_path + " not found.")
                self.warnings = "<no-file>" #: str: the string corresponding to KLEE's warnings.
            else:
                with open(warn_file_path) as file:
                    self.warnings = file.read()

            # Messages
            msg_file_path = os.path.join(folder, "messages.txt")
            if not os.path.exists(msg_file_path):
                log.warning(msg_file_path + " not found.")
                self.messages = "<no-file>" #: str: the string corresponding to KLEE's messages.
            else:
                with open(msg_file_path) as file:
                    self.messages = file.read()

            self._klee_stats()          

            self.valid = True          

            # Parse info file.
            infos_file_path = os.path.join(folder, "info")
            if not os.path.exists(infos_file_path):
                log.warning(infos_file_path + " not found.")
            else:
                with open(infos_file_path) as info_file:
                    lines = info_file.readlines()

                    self.searchers = []
                    in_searchers = False
                    current_searcher = ""
                    for line in lines:
                        if line.startswith("BEGIN searcher"): in_searchers = True
                        if line.startswith("END searcher"): in_searchers = False
                        
                        if in_searchers:
                            if line.startswith("<"):
                                current_searcher = line.split(">")[0][1:]
                            else:
                                self.searchers.append(current_searcher + "_" + line)
                        else:
                            if line.startswith("Elapsed: "):
                                self.elapsed = util.parse_klee_duration(line[9:-1])
                            if line.startswith("KLEE: done: explored paths"):
                                self.explored_paths = int(line.split("= ")[1])
                            if line.startswith("KLEE: done: avg. constructs per query"):
                                self.avg_construct_per_query = int(line.split("= ")[1])
                            if line.startswith("KLEE: done: total queries"):
                                self.total_queries = int(line.split("= ")[1])
                            if line.startswith("KLEE: done: valid queries"):
                                self.valid_queries = int(line.split("= ")[1])
                            if line.startswith("KLEE: done: invalid queries"):
                                self.invalid_queries = int(line.split("= ")[1])
                            if line.startswith("KLEE: done: query cex "):
                                self.query_cex = int(line.split("= ")[1])
                            if line.startswith("KLEE: done: total instructions"):
                                self.total_instructions = int(line.split("= ")[1])
                            if line.startswith("KLEE: done: completed paths"):
                                self.completed_paths = int(line.split("= ")[1])
                            if line.startswith("KLEE: done: generated tests"):
                                self.generated_tests = int(line.split("= ")[1])
                            if line.startswith("KLEE: done: partially completed paths"):
                                self.partial_paths = int(line.split("= ")[1])

            self.valid = True
        except IndexError as e:
            log.warning("an error occurred during parsing of Klee's data:" + str(e))
            self.valid = False
        
        except Exception as e:
            log.warning("an error occurred during parsing of Klee's data:" + str(e))
            self.valid = False
        
    def _klee_stats(self):
        """
        Uses klee-stats tool to extract execution, coverage and static data about the DSE execution.
        """
        cmd = ["klee-stats", "--table-format", "csv", "--print-all", "dse_out/"]
        (rc, txt) = util.exec_cmd_pair_wait(cmd)

        log.verbose(" => " + " ".join(cmd))

        if rc != 0:
            log.warning(f"cannot parse time metrics (code {rc}) : {txt}.")
        else:

            out = txt.decode("utf-8")

            args = out.split("\n")
            if len(args) != 3 or args[2] != "":
                log.error("unexpected format for klee-stats.")
            headers = args[0].split(",")
            values = args[1].split(",")
            
            if len(headers) != len(values):
                log.error("headers and values does not match.")
            
            for i in range(len(headers)):
                self.klee_stats[headers[i]] = values[i]       

    def klee_value(self, key: str) -> float:
        """
        Returns the value of KLEE's execution data identified by `key`.

        :param key: the key of the value.
        :type key: str
        :return: the value of the specified key inside KLEE's execution data.
        :rtype: float
        """
        if not self.valid: return float(-1)
        value = getattr(self, key)
        if value is None:
            return float(-1)
        return float(value)

def _parse_ip_dict(analysis: Analysis) -> ty.Dict[str, dict]:
    """
    Parse the 'injection_points.yaml' from Wolverine to gather a dictionary associating each IP name to its data.

    :param analysis: the analysis from which the file will be read.
    :type analysis: Analysis
    :return: the information about IPs.
    :rtype: Dict[str, dict]
    """
    ip_path = constants.injection_points

    if not os.path.exists(ip_path):
        log.error("cannot find the file '{}' (cd: '{}').".format(ip_path, os.curdir))
    
    with open(ip_path, "r") as stream:
        try:
            content = yaml.safe_load(stream)
            
            ip_dict = dict()
            
            for item in content:
                if item["id"] in ip_dict:
                    log.warning("ip '{}' already exists in the list.")
                    continue

                ip_dict[str(item["id"])] = item

            return ip_dict
        except yaml.YAMLError as e:
            log.error("cannot parse the file '{}': {}.".format(constants.injection_points, e))

    return None # required ? 
     
def _parse_ccp_dict(analysis: Analysis) -> ty.Dict[str, dict]:
    """
    Parse the 'ccp_list.yaml' from Wolverine to gather a dictionary associating each detector id to its data.

    :param analysis: the analysis from which the file will be read.
    :type analysis: Analysis
    :return: the information about detectors.
    :rtype: Dict[str, dict]
    """

    ccp_path = constants.detector_data
    if not os.path.exists(ccp_path):
        log.error("cannot find the file '{}'.".format(constants.detector_data))
    
    with open(ccp_path, "r") as stream:
        try:
            content = yaml.safe_load(stream)
            
            ccp_dict = dict()
            
            for item in content:
                if item["id"] in ccp_dict:
                    log.warning("detector '{}' already exists in the list.")
                    continue


                ccp_dict[str(item["id"])] = item

            return ccp_dict
        except yaml.YAMLError as e:
            log.error("cannot parse the file '{}': {}.".format(constants.injection_points, e))

    return None # required ? 
    
class RunResults:
    """
    The RunResults class holds information about the *mutation and DSE step*.
    """
    def __init__(self, analysis: Analysis, klee_results: KleeResults, ip_dict: ty.Dict[str, dict], det_dict : ty.Dict[str, dict], duration: float, date: datetime.date, **kwargs):
        """**__init__ (ctor):** Constructs a new RunResults object with the specified data.

        **Kwargs**:
         - `interrupted` (`bool`): determines if the DSE step has been interrupted prematurely by the user. *default*: `False`.

        :param analysis: the analysis to be ran.
        :type analysis: Analysis
        :param klee_results: information about Klee's execution.
        :type klee_results: KleeResults
        :param ip_dict: the dictionary of data about each injection point (generated by Wolverine).
        :type ip_dict: Dict[str, dict]
        :param det_dict: the dictionary of data about each detector (generated by Wolverine).
        :type det_dict: Dict[str, dict]
        :param duration: the duration of the compilation.
        :type duration: float
        :param date: the date of the compilation.
        :type date: datetime.date
        """
        self._analysis = analysis #: Analysis: The analysis object to which the *extension* is associated. See :func:`~lazart._internal_.extensions.register`.

        self._duration = duration #: float: The duration of the compilation.
        self._date = date #: datetime.date: The date of the compilation.
        self._klee_results = klee_results #: KleeResults: Holds information about Klee's execution.
        self._ip_dict = ip_dict #: Dict[str, dict]: Dictionary associating the YAML data of an IP with its identifier.
        self._det_dict = det_dict #: Dict[str, dict]: Dictionary associating the YAML data of a detector with its identifier.
        self._interrupted = _args.get_or_throw(kwargs, "interrupted", bool, False) #: bool: Determines if the DSE has been interrupted by the user.

    def interrupted(self) -> bool:
        """
        Returns `True` if the DSE execution has been interrupted during execution, `False` otherwise.

        :return: the interruption boolean.
        :rtype: bool
        """
        return self._interrupted

    def date(self) -> datetime.date:
        """
        Returns the date of the *mutation and DSE step* execution.

        :return: the date of the execution of the step.
        :rtype: datetime.date
        """
        return self._date

    def duration(self) -> float:
        """
        Returns the duration of the *mutation and DSE step*.

        :return: the duration of the step.
        :rtype: float
        """
        return self._duration

    def valid(self) -> bool:
        """
        Returns `True` if klee's information are valid (i.e. if the parsing of Klee's results information has been successful), `False` otherwise.

        :return: returns `True` if Klee's information are valid.
        :rtype: bool
        """
        return self._klee_results.valid

    def get_ip_data(self, ip_id: str) -> dict:
        """ 
        Returns the data about the specified injection point. It corresponds to the raw data available in injection_points.yaml (generated by Wolverine).

        :param ip_id: the identifier of the injection point.
        :type ip_id: str
        :return: the data dictionary of the IP.
        :rtype: dict
        """
        if ip_id in self._ip_dict:
            return self._ip_dict[ip_id]
        return None

    def get_detector_data(self, det: str) -> dict:
        """ 
        Returns the data about the specified detector. It corresponds to the raw data available in ccp_list.yaml (generated by Wolverine).

        :param det: the identifier of the detector.
        :type det: str
        :return: the data dictionary 
        :rtype: dict
        """
        if det in self._det_dict:
            return self._det_dict[det]
        return None 

    def __str__(self):
        """
        Returns the string representing the RunResults.

        :return: the string representation of the object.
        :rtype: str
        """              
        ret = "Run results: {}\n".format(self._analysis._name)
        ret += " - run-date: {}".format(str(self.date())) + "\n"
        ret += " - run-duration: {}".format(_str_time(self.duration())) + "\n"
        if self._interrupted:
            ret += " - interrupted: yes\n"
        for ip, value in self._ip_dict.items():
            ret += "   - {}: {}".format(ip, value)
        return ret

def run(analysis : Analysis, **kwargs) -> RunResults:
    """
    Executes the *mutation and DSE* step for the analysis and returns the corresponding information object.

    Requires that the *compilation step* (:func:`~lazart.core.compile.compile`) has been executed for this analysis.

    This function uses log to display progress, reduce verbosity level to reduce output.

    :param analysis: the analysis to be run.
    :type analysis: Analysis
    :return: the results of the step execution.
    """       
    if not _ext.has_extension(analysis, constants.compile_ext):
        log.error("the analysis need to be compiled before run.")

    #start = time.process_time()
    start = time.perf_counter()

    # Enter directory for klee and wolverine.
    old_dir = _folder.check_dir(analysis.path())
   
    _run_wolverine(analysis, eq="-leq")
    ip_dict = _parse_ip_dict(analysis)
    det_dict = _parse_ccp_dict(analysis)

    _link()
    _build_replay()
    log.info("running DSE...")
    interrupted = _run_dse(analysis)
    
    klee_results = KleeResults(constants.dse_folder)
    
    # Retrieve old directory
    old_dir.revert()

    end = time.perf_counter()
    
    duration = end - start
    log.info("Run process ended in {}.".format(_str_time(duration)))
    return RunResults(analysis, klee_results, ip_dict, det_dict, duration, datetime.datetime.now(), interrupted=interrupted)
    
class RunStep(_ext.AnalysisStep):
    """
    Subclass of AnalysisStep for the *mutation and DSE step*, see :class:`lazart._internal_.extensions.AnalysisStep`.
    """
    def __init__(self, analysis: Analysis):
        """
        **__init__ (ctor):** Constructs a new RunStep object associated to the specified analysis.

        :param analysis: the analysis of the step.
        :type analysis: Analysis
        """
        super(RunStep, self).__init__(analysis, constants.run_ext, _ext.StepID.Run)

    def execute(self, **kwargs):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        return run(self.analysis, **kwargs)
    
    def clean(self):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        _folder.remove_file(_folder.lazart_file(self.analysis.path(), constants.run_ext))

        _folder.remove_folder(os.path.join(self.analysis.path(), constants.dse_folder))
        _folder.remove_file(os.path.join(self.analysis.path(), constants.replay_bc))
        _folder.remove_file(os.path.join(self.analysis.path(), constants.mutated_bc))
        _folder.remove_file(os.path.join(self.analysis.path(), constants.mutated_ll))
        _folder.remove_file(os.path.join(self.analysis.path(), constants.injection_points))
        _folder.remove_file(os.path.join(self.analysis.path(), constants.attack_model))


def run_results(analysis: Analysis, **kwargs) -> RunResults:
    """
    Returns the results of the mutation and DSE for the analysis.

    Uses extension (run_ext, see :class:`lazart._internal_.extensions.AnalysisStep`) system to reuse disk/RAM cache if available.
    This function uses log to display progress, reduce verbosity level to reduce output. 

    :param analysis: the analysis
    :type analysis: Analysis
    :raises Exception: if no compilation information are available.
    :return: the compilation results.
    :rtype: RunResults
    :dev-note: Should be merged with `run` ?
    """
    step = _ext.get_step(RunStep(analysis))
    
    return step.get_results(**kwargs)