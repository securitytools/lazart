""" The module *lazart.core.fault_model* contains definitions for the different *fault models* supported by **Lazart**.

:since: 3.0
:author: Etienne Boespflug 
"""
# Python
from enum import Enum

class FaultModel(Enum):
    """ 
    This enumeration corresponds to the type of fault model available in Lazart. 
    """
    TI = 0
    """ Shortcut for `TestInversion`."""
    TestInversion = 0
    """ *Test Inversion* model. 
    The attacker is able to invert the branch during conditional branching instructions."""
    DL = 1
    """ Shortcut for `DataLoad`."""
    DataLoad = 1
    """ *Data load mutation* model.
    The attacker is able to change the value read during a `load` instruction."""
    SC = 2
    """ Shortcut for `Switch Call`."""
    SwitchCall = 2
    """ *Switch call* model. 
    The attacker is able to call another function during a `call` instruction.
    This model uses code instrumentation and is not applied by **Wolverine**."""
    JMP = 3
    """ Shortcut for `Jump`."""
    Jump = 3
    """ *Jump* model. 
    The attacker is able to force a jump to another control point during execution.
    This model uses code instrumentation and is not applied by **Wolverine**.
    """ 
    User = 1000
    """ Special value for user defined models."""

    def name(self) -> str:
        """
        Returns the string name of the fault model.

        :return: The string corresponding to the fault model.
        :rtype: str
        """
        if self.value == FaultModel.TI.value: return "TI"
        elif self.value == FaultModel.DL.value: return "DL"
        elif self.value == FaultModel.SC.value: return "SC"
        elif self.value == FaultModel.JMP.value: return "JMP"
        elif self.value == FaultModel.User.value: return "USR"
        else:
            return "<error>"