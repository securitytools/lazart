""" The module *lazart.core.traces* contains definitions for traces parsing and manipulation.

:since: 3.0
:author: Etienne Boespflug 
"""

# Python
from enum import Enum
import subprocess as sp
import os
from os import path
import traceback
import time
import types
import typing as ty
import glob
import copy
import re

# Lazart
import lazart.logs as log
import lazart.constants as constants
import lazart.options as options

# Internal
import lazart._internal_.extensions as _ext
import lazart._internal_.util as util
import lazart._internal_.folder as _folder
import lazart._internal_.args as _args
import lazart._internal_.color as color

# Core
from lazart.core.analysis import Analysis
from lazart.core.run import run_results, RunResults
from lazart.core.fault_model import FaultModel

## Traces Events
class UserEvent:
    """ The UserEvent class corresponds to an user-defined event inside a trace.
    """
    def __init__(self, data: str):
        """
        **_init (ctor):** Initializes an user event with the specified string data.

        :param data: the data parsed in the event.
        :type data: str
        """
        self.data = data #: str: The data parsed in the event.

    def str(self, **kwargs) -> str:
        """
        Returns the string representation of the object.
        
        :return: the string representation of the object.
        :rtype: str
        """
        return str(self)

    def __str__(self) -> str:
        return "EVENT(" + self.data + ")"

class BasicBlock:
    """ Represents a basic block traversal (without fault or ccp triggered) in a trace.
    """
    def __init__(self, name: str):
        """
        **__init__ (ctor):** Initializes a basic block object with the BB identifier 'name'.

        :param name: the name of the basic block.
        :type name: str
        """
        self.name = name #: str: The name of the BasicBlock.

    def str(self, **kwargs) -> str:
        """
        Returns the string representation of the object.
        
        :return: the string representation of the object.
        :rtype: str
        """
        return str(self)

    def __str__(self):
        return "BB(" + self.name + ")"

class Detection:
    """The Detection class represents the triggering of a countermeasure.
    """
    def __init__(self, ccp: str, det_ptr: ty.Dict[str, ty.Dict]):
        """
        **__init__ (ctor):** Initializes a detection event with the detector identifier.

        :param ccp: the triggered ccp string identifier.
        :type ccp: str
        :param det_ptr: the detector information.
        :type det_ptr: Dict[str, Dict]
        """
        self.ccp = ccp #: str: The detector identifier (Countermeasure Check Point (CCP)).
        self._det_ptr = det_ptr #: Dict[str, Dict]: The detector data (from run step).

    def str(self, **kwargs) -> str:
        """
        Returns the string representation of the object.
        
        :return: the string representation of the object.
        :rtype: str
        """
        return str(self)

    def __str__(self):
        return "CM(" + self.ccp + ")"

class Fault:
    """ A Fault event signal that a fault has been triggered, specifying the injection
    point and the fault model.
    Additional data are store depending on the model.
    """
    def __init__(self, ip : str, model: FaultModel, ip_ptr: ty.Dict):
        """
        **__init__ (ctor):** Creates a Fault object with the specified injection point identifier and
        fault model.

        :param ip: the injection point of the fault.
        :type ip: str
        :param model: the fault model of the fault.
        :type model: FaultModel
        :param ip_ptr: the injection point information.
        :type ip_ptr: Dict
        """
        self.injection_point = ip #: str: The Injection Point identifier.
        self.model = model #: FaultModel: The fault model of the fault.
        self._ip_ptr = ip_ptr #: Dict: The injection point data (from run step).

    def __data_key_str(key : str) -> str:
        """
        Returns the actual key from key name (with internal suffix).

        :param key: the name of the key.
        :type key: str
        :return: the complete data key name.
        :rtype: str
        """
        return "data_" + key

    def add_data(self, key : str, data: object):
        """
        Adds fault-model-dependent data to the Fault object.

        :param key: the name of the data.
        :type key: str
        :param data: the payload (whatever).
        :type data: object
        :raises Exception: if key already exists.
        """
        if hasattr(self, Fault.__data_key_str(key)):
            raise Exception("the key '{}' already exists.")
        setattr(self, Fault.__data_key_str(key), data)

    def get_data(self, key : str) -> object:
        """
        Returns the model-dependent data from the Fault object. Returns
        None if the data is not present.

        :param key: the name of the data to get.
        :type key: str
        :return: the data store for key.
        :rtype: object
        """
        
        if not hasattr(self, Fault.__data_key_str(key)):
            return None
        return getattr(self, Fault.__data_key_str(key))

    def rebind(self, rr: RunResults):
        """
        Binds the injection point's data to the Fault object using run step.

        :param rr: the result object of the *Mutation and DSE step*.
        :type rr: RunResults
        """
        self._ip_ptr = rr.get_ip_data(str(self.injection_point))
    
    def unbind(self):
        """
        Unbinds IPs data for the event.

        TODO: not implemented.
        """
        pass

    def str(self, **kwargs) -> str:
        """
        Returns the string representation of the object, supporting specific keyword arguments.

        **Kwargs**:
         - `details` (`bool`): Determines if fault-model's specific data are displayed. *default*: `True`.
         - `loc` (`bool`): Determines if location data are displayed. *default*: `True`.
         - `fm` (`bool`): Determines if the corresponding fault model type is displayed. *default*: `True`.
        
        :return: the string representation of the object.
        :rtype: str
        """
        details = _args.get_or_throw(kwargs, "details", bool, True)
        loc = _args.get_or_throw(kwargs, "loc", bool, True)
        fm = _args.get_or_throw(kwargs, "fm", bool, True)

        details_str = ""
        if details:
            if self.model.value == FaultModel.TI.value:
                details_str = ": {} => {}".format(self.get_data("origin_bb"), self.get_data("dest_bb")) 
            elif self.model.value == FaultModel.SwitchCall.value:
                details_str = ": {} => {}".format(self.get_data("origin_fct"), self.get_data("dest_fct"))     
            elif self.model.value == FaultModel.Jump.value:
                details_str = ": => {}".format(self.get_data("target"))          
            elif self.model.value == FaultModel.DataLoad.value:
                details_str = ": {} => {}".format(self.get_data("origin_value"), self.get_data("faulted_value")) 
            else:
                details_str = ""

        fm_str = ""
        if fm:
            fm_str = "|{}|".format(self.model.name())

        loc_str = ""
        if loc and self._ip_ptr is not None:
            loc_str = " [l:{}, bb:{}]".format(self._ip_ptr["loc"]["line"], self._ip_ptr["loc"]["basic_block"])
        else:
            loc_str = " [no-loc-info]"

        return "FAULT({}{}{}{})".format(self.injection_point, fm_str, details_str, loc_str)

    def __str__(self) -> str:
        return self.str(details=True, loc=False)

class TerminationType(Enum):
    """ This enum corresponds to the type of termination of a ktest. """
    Correct = 0
    """ Correct trace. """
    Assert = 1 
    """ An assertion failed """
    User = 2
    """ klee_assume violated. There is a problem with the input (invalid klee intrinsic calls) or the way KLEE is being used. """
    Model = 3
    """ KLEE was unable to keep full precision and is only exploring parts of the program state. For example, symbolic sizes to malloc are not currently supported, in such cases KLEE will concretize the argument. """
    Timeout = 4
    """ Timeout error. """
    Exec = 10
    """ There was a problem which prevented KLEE from executing the program; for example an unknown instruction, a call to an invalid function pointer, or inline assembly. """
    Abort = 11
    """ The program called abort() """
    External = 12
    """ External function called (not handled). Not documented in KLEE. """
    Ptr = 20
    """ Pointer error. """
    Free = 21
    """ Double or invalid free(). """
    Div = 22
    """ Division or modulus by zero was detected. """
    ReadOnly = 23
    """ Readonly memory violation. """
    Unknown = 100
    """ This termination type is unknown to Lazart. """

class Trace:
    """
    A Trace is an ordered set of trace events, which are `BasicBlock` traversal, `Detection`, `Fault` or `UserEvent`.
    CCP trigger or fault.
    In **Lazart**, traces are symbolic traces. Symbolic traces can map to several concrete traces
    depending on symbolic input and concolic execution.

    Traces are usually considered by order (i.e. the number of fault injected in the trace).
    """
    def __init__(self, order : int, events: ty.List[ty.Union[BasicBlock, Detection, Fault, UserEvent]], name : str, path : str, termination_type: TerminationType, **kwargs):
        r"""
        **__init__ (ctor):** Constructs a new Trace object with specified parameters.

        :param order: the order (i.e. faults count) of the trace.
        :type order: int
        :param events: ordered list of trace events.
        :type events: List[Union[BasicBlock, Detection, Fault, UserEvent]]
        :param name: the name of the trace (unique, not enforced).
        :type name: str
        :param path: the path of the ktest file.
        :type path: str
        :param termination_type: the type of termination (Correct, Error, Oracle violation...)
        :type termination_type: TerminationType
        """
        self.events = events #: list(Event): A list of events compounding the trace.
        self.name = name #: str: Name of the trace (unique, not enforced).
        self.path = path #: str: Path to the ktest file corresponding to this trace.
        self.order = order #: int: The order of the trace (I.e. the number of triggered faults in the trace).
        self.termination_type = termination_type #: TerminationType: The type of termination of the trace (Correct, Error, Oracle violation...).
        self.partial_parsing = False #: bool: Determines if the trace as been parsed using the ktest-tool technique (see :class:`~lazart.core.traces._parse_ktest_tool`) that is incomplete, default to `False`.

    def has_detection(self, cm_name: str) -> bool:
        """
        Returns true if the trace contains at least one trigger from 
        the ccp (detector) with the identifier 'ccp_name'.

        :param cm_name: the string identifier of the ccp.
        :type cm_name: str
        :return: true if the specified ccp is triggered.
        :rtype: bool
        """
        for e in self.events:
            if type(e) is Detection:
                if e.trigger_point == cm_name:
                    return True
        return False

    def execution_path(self) -> ty.List[BasicBlock]:
        """
        Returns a list of all basic blocks triggered in the trace.

        :return: the list of triggered basic blocks.
        :rtype: List[BasicBlock]
        """
        ep = []
        for event in self.events:
            if type(event) is BasicBlock:
                ep.append(event)

        return ep

    def faults(self) -> ty.List[Fault]:
        """
        Returns a list of all triggered faults in the trace.

        :return: the list of triggered faults.
        :rtype: List[Fault]
        """
        faults = []
        for event in self.events:
            if type(event) is Fault:
                faults.append(event)

        return faults

    def ips(self) -> ty.List[str]:
        """
        Returns faults's IPs list (with duplicates).

        :return: the list of names of each IP on which a fault has been injected in the trace.
        :rtype: List[str]
        """
        return [str(f.injection_point) for f in self.faults()]

    def triggered(self) -> ty.List[Detection]:
        """
        Returns a list of all detectors triggered in the trace.

        :return: the list of triggered detectors.
        :rtype: List[Detection]
        """
        cm = []
        for event in self.events:
            if type(event) is Detection:
                cm.append(event)

        return cm

    def first_triggered(self) -> Detection:
        """
        Returns the first triggered CCP, None otherwise.

        :return: Returns the first triggered CCP, None otherwise.
        :rtype: Detection
        """
        for event in self.events:
            if type(event) is Detection:
                return copy.deepcopy(event)#event)
        return None

    def detected(self) -> bool:
        """
        Returns true if the trace contains at least one CCP trigger, false
        otherwise.

        :return: true if the trace is detected, false otherwise.
        :rtype: bool
        """
        return self.first_triggered() is not None

    def termination(self) -> TerminationType:
        """
        Returns the termination type for the trace.

        :return: the termination type of the trace.
        :rtype: TerminationType
        """
        return self.termination_type

    def termination_is(self, *argv) -> bool:
        """
        Returns true if the termination type of the trace is in the specified
        list, false otherwise.

        :return: true if the termination type of the trace is in the specified list, false otherwise.
        :rtype: bool
        """
        for tt in argv:
            if tt == self.termination_type:
                return True
        return False

    def events(self) -> ty.List[UserEvent]:
        """
        Return the list (without copy), of all events (`UserEvent`) in the trace.

        :return: the list of user events in the trace.
        :rtype: List[UserEvent]
        """
        ret = []
        for event in self.events:
            if type(event) is UserEvent:
                ret.append(event)
        return ret

    def has_event(self, predicate) -> bool:
        """
        Returns the first event satisfying the specified predicate, returns `None` otherwise.

        :param predicate: predicate for filtering events.
        :type predicate: lambda(string) -> bool
        :return: the first satisfying event.
        :rtype: UserEvent
        """
        elist = self.has_events(predicate)
        if len(elist) > 0:
            return True
        return False

    def has_events(self, predicate: ty.Callable[[UserEvent], bool]) -> ty.List[UserEvent]:
        """
        Returns the list of all events satisfying the specified lambda.

        :param predicate: predicate for filtering events.
        :type predicate: Callable[[UserEvent], bool]
        :return: the list of all events satisfying the predicate.
        :rtype: List[UserEvent]
        """
        ret = []
        for event in self.events:
            if type(event) is UserEvent:
                if predicate(event.data):
                    ret.append(event)
        return ret  

    def rebind(self, rr: RunResults):
        """
        Rebinds the IPs data for each `Fault` event in the trace.

        :param rr: the result object of the *Mutation and DSE step*.
        :type rr: RunResults
        """
        for event in self.events:
            if type(event) is Fault:
                event.rebind(rr)

    def unbind(self):
        """
        Unbinds IPs data and detector data for each event in the trace.

        TODO:  not implemented.
        """
        pass
        
    def replay_str(self, analysis) -> str:
        # TODO not implemented ! 
        return "" 

    def satisfies(self) -> bool:
        """
        Returns true if the trace satisfies the oracle in it's basic definition
        (i.e. termination_is(TerminationType.Correct)).

        :return: true if the trace satisfies, false otherwise.
        :rtype: bool
        """
        return self.termination_is(TerminationType.Correct)

    def conclusive(self) -> bool:
        """
        Returns true if the trace is conclusive depending on its termination type, false 
        otherwise.

        :return: true if the trace is conclusive, false otherwise.
        :rtype: bool
        """
        return self.termination_type.value != TerminationType.Timeout.value and self.termination_type.value != TerminationType.Exec.value and self.termination_type.value != TerminationType.Model.value

    def faults_str(self, sep : str = ",") -> str:
        """
        Returns the string corresponding to the list of triggered faults.

        :param sep: separator for faults strings, defaults to ","
        :type sep: str, optional
        :return: the string corresponding to all triggered faults.
        :rtype: str
        """
        l = []

        for fault in self.faults():
            l.append(str(fault.injection_point))
        
        return sep.join(l)

    def det_str(self, sep : str = ",") -> str:
        """
        Returns the string corresponding to the list of detector triggered in the trace.

        :param sep: separator for detection strings, defaults to ","
        :type sep: str, optional
        :return: the string corresponding to all triggered detectors.
        :rtype: str
        """
        l = []

        for ccp in self.triggered():
            ccp.append(str(ccp.trigger_point))
        
        return sep.join(l)

    def str(self, **kwargs) -> str:
        """
        Returns the string representation of the object, supporting specific keyword arguments.

        **Kwargs**:
         - `details` (`bool`): Determines if fault-model's specific data are displayed. *default*: `True`.
         - `loc` (`bool`): Determines if location data are displayed. *default*: `True`.
         - `fm` (`bool`): Determines if the corresponding fault model type is displayed. *default*: `True`.

        :param c: determines if the string will use UNIX's colors.
        :type c: bool
        :return: the string representation of the trace.
        :rtype: str
        """
        c = _args.get_or_throw(kwargs, "color", bool, True)
        def h(s: str) -> str:
            if c:
                return s
            return ""
        ret = "(<" + str(self.order) + "> " + self.name + ": ["
        e_strs = []
        for event in self.events:
            event_s = ""
            if type(event) is Detection:
                event_s += h(color.yellow)
            if type(event) is Fault:
                event_s += h(color.purple)
            if type(event) is BasicBlock:
                event_s += h(color.light_blue)

            event_s += event.str(**kwargs) + h(color.reset)
            e_strs.append(event_s)
        ret += ", ".join(e_strs) + "]"
        ret += ": " + str(self.termination_type.name) + ")"
        return ret
        

    def __str__(self) -> str:
        """
        Returns the string representation of the object, supporting specific keyword arguments.

        **Kwargs**:
         - -> *see `str` method for* :class:`~lazart.core.Fault`.
        
        :return: the string representation of the Trace.
        :rtype: str
        """
        return "(<" + str(self.order) + "> " + self.name + ": [" + ", ".join(str(e) for e in self.events) + "]: " + str(self.termination_type.name) + ")"


def _parse_ktest_tool(analysis: Analysis, path: str) -> Trace:
    """
    Parses a .ktest file from KLEE to construct a Trace. Only `Fault` are generated and order is not preserved. Model specific data are not gathered.

    :param analysis: the analysis of the trace.
    :type analysis: Analysis
    :param path: the path of the ktest file.
    :type path: str
    :return: the parsed trace.
    :rtype: Trace
    """
    if not os.path.exists(path):
        log.warning(path + " not found.")
        return []

    out = util.check_output(["ktest-tool", path])
    if out is None:
        return []
    
    rr = run_results(analysis)
    
    events = []
    last_id = 0
    ip = None
    for line in out.split('\n'):
        if not line.startswith("object"):
            continue
        
        result = re.search('object (.+): (.+): (.+)', line)
        if result is None:
            log.warning("bad syntax when trying to parse line: " + line)
            continue

        id = result.group(1)
        key = result.group(2)
        value = result.group(3)

        if key.startswith("name"):
            last_id = id
            ip = value[1:-1]
        elif key.startswith("int"):
            if value != "0":
                if ip.isdigit(): # Exclude other symbolic variables
                    if id != last_id:
                        log.warning("strange behavior in ktest-tool format, maybe corrupted")

                    ip = result.group(1)
                    value = result.group(2)

                    ip_ptr = rr.get_ip_data(ip)

                    type = FaultModel.DL
                    if ip_ptr is not None:
                        if ip_ptr["type"] != "data":
                            type = FaultModel.TI
                    
                    fault = Fault(ip, type, ip_ptr)

                    events.append(fault)

    return events

def _parse_trace(analysis: Analysis, ktest_path: str,  state: TerminationType, **kwargs) -> Trace:
    """
    Parses a .ktest file from KLEE to construct a Trace. Only `Fault` are generated and order is not preserved. Model specific data are not gathered.

    **Kwargs**:
        - `trace_event_parser` (`Callable[int, List[UserEvent], RunResults]`): user-defined parsing function for user-defined fault models.

    :param analysis: the analysis of the trace.
    :type analysis: Analysis
    :param ktest_path: the path of the ktest file.
    :type ktest_path: str
    :param state: the termination state for the trace.
    :type state: TerminationType
    :raises Exception: if an error prevented the trace to be parsed.
    :return: the parsed trace.
    :rtype: Trace
    """
    trace_fct = _args.get_or_throw(kwargs, "trace_event_parser", types.FunctionType, None, none_allowed=True)

    replay_cmd = "KTEST_FILE=" + ktest_path + " ./" + path.join(path.relpath(analysis.path()), constants.replay_bc)
    p = sp.Popen(replay_cmd, stdout=sp.PIPE, stderr=sp.PIPE, shell=True)
    out, _ = p.communicate()

    events = []
    
    faults = 0

    rr = run_results(analysis)
    
    lines = out.splitlines()
    for line in lines:
        line = str(line.decode('UTF-8').strip())

        if trace_fct is not None:
            (consume, line_faults) = trace_fct(line, events, rr)
            if line_faults > 0:
                faults = faults + 1
            if consume:
                continue

        if line.startswith("[TRACE]"):
            events.append(BasicBlock(line[8:]))
        elif line.startswith("[CM] triggered"): # idea: check blocking ? 
            det_id = line[15:]
            det_ptr = rr.get_detector_data(det_id)
            det = Detection(det_id, det_ptr)
            events.append(det)
        elif line.startswith("[FAULT]"):
            try:
                if line.startswith("[FAULT] [DL]"):

                    result = re.search('\[FAULT\] \[DL\] \[(.+)\] \[(.+)\]', line)
                    if result is None:
                        raise Exception("bad syntax")
                    ip = result.group(1)
                    value = result.group(2)

                    #ip_ptr = rr.get_ip_data(ip)
                    fault = Fault(ip, FaultModel.DL, None)

                    if value == "~": 
                        pass 

                    fault.add_data("origin_value", "~")
                    fault.add_data("faulted_value", value)

                    events.append(fault)
                    
                elif line.startswith("[FAULT] [SC]"): # Switch call    
                    result = re.search('\[FAULT\] \[SC\] \[(.+)\] \[(.*)\] by \[(.*)\]', line)
                    if result is None:
                        raise Exception("bad syntax")
                    ip = result.group(1)
                    or_fct = result.group(2)
                    dst_fct = result.group(3)

                    fault = Fault(ip, FaultModel.SC, None)
                    fault.add_data("origin_fct", or_fct)
                    fault.add_data("dest_fct", dst_fct)

                    events.append(fault)

                elif line.startswith("[FAULT] [JMP]"): # Jump 
                    result = re.search('\[FAULT\] \[JMP\] \[(.+)\] jump to (.*)', line)
                    if result is None:
                        raise Exception("bad syntax")
                    ip = result.group(1)
                    target = result.group(2)

                    fault = Fault(ip, FaultModel.JMP, None)
                    fault.add_data("target", target)

                    events.append(fault)

                else: # TI
                    result = re.search('\[FAULT\] \[TI\] \[(.+)\] to force (.*) to (.*)', line)
                    if result is None:
                        raise Exception("bad syntax")
                    ip = result.group(1)
                    or_bb = result.group(2)
                    dst_bb = result.group(3)

                    fault = Fault(ip, FaultModel.TI, None)
                    fault.add_data("origin_bb", or_bb) # should be loc.basic_block
                    fault.add_data("dest_bb", dst_bb)

                    events.append(fault)
                    
                faults = faults + 1
            except Exception as e:    
                log.warning("\ncannot parse line '" + line + "': " + str(e) + ".", clear=True)
                raise e
        elif line.startswith("[USER_EVENT] "):
            data = line[13:]
            events.append(UserEvent(data))
    
    partial = False
    if len(lines) == 0 or len(events) == 0:
        # Use other version....
        log.warning(f"unable to parse the ktest (no output) {os.path.basename(ktest_path)} (Term:{state}), using ktest-tool instead.", clear=True)

        events = _parse_ktest_tool(analysis, ktest_path)
        partial = True
        faults = 0
        for e in events:
            if type(e) is Fault:
                faults = faults + 1
    
    order = faults
    trace = Trace(order, events, "", ktest_path[:-6], state)
    trace.partial_parsing = partial
    return trace        

def all_traces(analysis: Analysis, order: int, **kwargs) -> ty.List[Trace]:
    """    
    Returns the list of all traces of the analysis with order lower or equal than specified
    and matching the optional `satisfies_fct` predicate.

    Requires that *traces parsing step* has been executed for this analysis.

    **Kwargs**:
     - `satisfies_fct` (`Callable[[Trace], bool]`): boolean predicate determining if a trace
        should be returned or not. *default*: `lambda trace: True`.

    :param analysis: the analysis in which the traces are searched.
    :type analysis: Analysis
    :param order: the order limit for the traces.
    :type order: int
    :return: the lists of all filtered traces for each order.
    :rtype: List[Trace]
    """
    s_fct = _args.get_satisfies_function(kwargs, lambda trace: True)

    all_traces = all_fault_traces(analysis, order, s_fct=s_fct)
    for trace in traces_list(analysis, 0):
            if s_fct(trace):
                all_traces.append(trace)
    
    return all_traces  

def traces(analysis: Analysis) -> ty.List[ty.List[Trace]]:
    """
    Returns the list of lists of Traces for each order.

    Requires that *traces parsing step* has been executed for this analysis.

    :param analysis: the analysis containing the traces.
    :type analysis: Analysis
    :return: the lists of all traces for each order.
    :rtype: List[List[Trace]]
    """        
    return traces_results(analysis)._traces

def traces_list(analysis: Analysis, order: int) -> ty.List[Trace]:
    """
    Returns the list of traces for a specified order.

    Requires that *traces parsing step* has been executed for this analysis.

    :param analysis: the analysis containing the traces.
    :type analysis: Analysis
    :param order: the desired order.
    :type order: int
    :return: the list of traces of specified order.
    :rtype: List[Trace]
    """
    if order > analysis.max_order():
        log.error("index out of range")
    return traces_results(analysis)._traces[order]

def get_traces(analysis: Analysis, s_fct: ty.Callable[[Trace], bool] = lambda trace: True) -> ty.Generator[Trace, None, None]:
    """
    Returns all the traces matching the predicate `s_fct`.

    Requires that *traces parsing step* has been executed for this analysis.

    :param analysis: the analysis containing the traces.
    :type analysis: Analysis
    :param s_fct: boolean predicate determining if a trace should be returned or not, *default*: `lambda trace: True`.
    :type s_fct: Callable[[Trace], bool]
    :return: the set of all traces matching the specified predicate. 
    :rtype: Generator[Trace, None, None]
    """
    trs = traces_results(analysis)._traces
    for traces in trs:
        for trace in traces:
            if s_fct(trace):
                yield trace

def all_fault_traces(analysis: Analysis, order: int, **kwargs) -> ty.List[Trace]:
    """
    Returns the list of all trace with lower or equal order than specified (and >= 1) and matching optional `satisfies_fct` predicate.

    Requires that *traces parsing step* has been executed for this analysis.

    **Kwargs**:
     - `satisfies_fct` (`Callable[[Trace], bool]`): boolean predicate determining if a trace
        should be returned or not. *default*: `lambda trace: True`.

    :param analysis: the analysis in which the traces are searched.
    :type analysis: Analysis
    :param order: the order of traces.
    :type order: int
    :return: the lists of all filtered traces for each order (>= 1).
    :rtype: List[Trace]
    """
    s_fct = _args.get_satisfies_function(kwargs, lambda trace: True)

    all_traces = []
    for i in range(1, order + 1):
        for trace in traces_list(analysis, i):
            if s_fct(trace):    
                all_traces.append(trace)

    return all_traces

def get_trace(analysis: Analysis, **kwargs) -> Trace:
    """
    Returns the first trace matching the optional ̀binary predicate `satisfies_fct`,
    `None` otherwise if no trace match the predicate.

    Requires that *traces parsing step* has been executed for this analysis.

    **Kwargs**:
     - `satisfies_fct` (`Callable[[Trace], bool]`): boolean predicate determining if a trace should be returned or not. *default*: `lambda trace: True`.

    :param analysis: the analysis containing the traces.
    :type analysis: Analysis
    :return: the first matching trace, `None` otherwise.
    :rtype: Trace
    """
    s_fct = _args.get_satisfies_function(kwargs, lambda trace: True)

    for traces in traces_results(analysis)._traces:
        for trace in traces:
            if s_fct(trace):
                return trace

    return None

def get_trace_by_name(analysis: Analysis, name: str) -> Trace:
    """
    Helper returning the trace matching the specified name, `None` otherwise.

    *note*: trace name uniqueness in an analysis is not enforced nor checked.    

    :param analysis: the analysis containing the traces.
    :type analysis: Analysis
    :type analysis: Analysis
    :param name: the name of the trace.
    :type name: str
    :return: the trace matching the specified name, `None` otherwise.
    :rtype: Trace
    """
    return get_trace(analysis, satisfies_fct=lambda trace: trace.name == name)


def reload_traces_buckets(analysis: Analysis) -> ty.List[ty.List[Trace]]:
    """
    Reload all traces buckets from the specified analysis and returns the corresponding
    list of list of traces for each order (see :class:`lazart.core.traces.TracesBuckets`).

    :param analysis: the analysis to which the traces belong.
    :type analysis: Analysis
    :return: the list of traces list of each order for this analysis.
    :rtype: List[List[Trace]]
    """
    traces = []

    rr = run_results(analysis)

    for i in range(analysis.max_order() + 1):
        traces_i = []
        for bucket in glob.glob(os.path.join(analysis.path(), constants.traces_bucket_folder + "/{}/*".format(i))):
            r = _ext.unpickle(bucket)
            traces_i.extend(r)

        for trace in traces_i:
            trace.rebind(rr)
            
            
        traces.append(traces_i)

    return traces

class TracesResults:
    """ 
    The TracesResults class holds the internal representation of the analysis's traces.
    It also contains information about the *traces parsing step* of Lazart.

    Traces are stored in list for each order.
    """ 
    def __init__(self, analysis: Analysis, traces: ty.List[ty.List[Trace]], ignored: int, duration: float, **kwargs):
        """
        **__init__ (ctor):** Constructs a new TracesResults object with an analysis, the traces lists for each order and the duration information.

        *Should not be called directly, prefer* :func:`traces_results`.
        """        
        self._analysis = analysis #: Analysis: The analysis object to which the *extension* is associated. See :func:`~lazart._internal_.extensions.register`.
        
        self._ignored = ignored #: int: the number of ktest files that have not been parsed.
        self._traces = traces #: List[List[Trace]]: The list of traces lists for each order.
        self._duration = duration #: float: the duration of the traces parsing step.
        self._interrupted = _args.get_or_throw(kwargs, "interrupted", bool, False) #: bool: Determines if the DSE has been interrupted by the user.

    def interrupted(self) -> bool:
        """
        Returns `True` if the step has been interrupted during execution, `False` otherwise.

        :return: the interruption boolean.
        :rtype: bool
        """
        return self._interrupted

    def ignored(self) -> int:
        """
        Returns the number of ktest from KLEE that have been ignored.

        :return: the ignored traces count.
        :rtype: int
        """
        return self._ignored

    def inconclusive_count(self) -> int: 
        """
        Compute the total of inconclusive traces.
        
        ***note***: *does not take into account the traces that has been parsed using the partial ktest-tool method (see :func:`lazart.core.traces._parse_ktest_tool).).*

        :return: the total number of trace satisfying 'not trace.conclusive()'.
        :rtype: int
        """
        total = 0
        for order in range(self._analysis.max_order() + 1):
            for traces in all_traces(self._analysis, order, s_fct=lambda t : not t.conclusive()):
                total + 1

        return total

    def conclusive_count(self): 
        """
        Compute the total of conclusive traces.

        :return: the total number of trace satisfying 'trace.conclusive()'.
        :rtype: int
        """
        return self.traces_count() - self.inconclusive_count()

    def duration(self, **kwargs) -> float:
        """
        Returns the duration of the trace parsing step.

        :return: the duration(s) of the trace parsing step.
        :rtype: float
        """
        return self._duration

    def traces_count(self) -> int:
        """
        Returns the total number of traces (for order 0 to analysis.max_order()).

        :return: the total number of traces.
        :rtype: int
        """
        total = 0
        for i in range(self._analysis.max_order() + 1):
            total += len(self._traces[i])
        return total

    def __str__(self) -> str:
        """
        Returns the string representing the TracesResults object.

        :return: the string representation of the object.
        :rtype: str
        """
        ret = "Traces results: {}\n".format(self._analysis._name)
        ret += "  - duration    : " + util.str_time(self._duration) + "\n"
        if self._interrupted:
            ret += " - interrupted: yes\n"
        
        # correctness
        if self.traces_count() > 0:
            ret += "  - correctness : {}% ({}/{})\n".format(self.conclusive_count() / self.traces_count() * 100, self.conclusive_count(), self.traces_count())
        else:
            ret += "  - correctness : N/A\n"
        for order in range(self._analysis.max_order() + 1):
            ret = ret + "  - {}-order: {} trace(s).\n".format(order, len(self._traces[order]))
        return ret

class TracesBuckets:
    """ 
    This class is intended to automatically save buckets of traces of specified
    size during traces parsing to avoid keeping to high number of traces in memory 
    and keep by order organization.
    """
    def __init__(self, bucket_size: int, analysis: Analysis):
        """
        **__init__ (ctor):** Constructs a new TracesBuckets object for an analysis parameterized by the size of a bucket.

        :param bucket_size: size of a trace bucket, full buckets are saved to disk.
        :type bucket_size: int
        :param analysis: the analysis relative to the trace.
        :type analysis: Analysis
        """
        self.traces = [] #: List[List[Trace]]: The list of current lists of traces for each order.
        self.bucket_counts = [] #: List[int]: The list of number of bucket for each order.
        self.count = 0 #: int: Total count of traces.
        self.analysis = analysis #: Analysis: The Analysis related to the traces.
        for i in range (analysis.max_order() + 1):
            self.traces.append([])
            self.bucket_counts.append(0)
        self.bucket_size = bucket_size #: int: Maximal size of a bucket.

    def add_trace(self, trace: Trace):
        """
        Adds a trace in the bucket corresponding to trace's order. Save the bucket if filled.

        :param trace: the trace to be added.
        :type trace: Trace
        """
        # renames, expect order
        order = trace.order
        if order >= len(self.traces):
            order = len(self.traces) - 1
            log.warning(f"invalid trace order {trace.order} ('{trace.name}/{trace.path}'), saved to {order}-order bucket.")

        trace.name = "t{}".format(self.count)
        self.traces[order].append(trace)
        self.count = self.count + 1
        
        if len(self.traces[order]) >= self.bucket_size:
            self._save_bucket(order)

    def _save_bucket(self, order: int):
        """
        Saves the bucket of specified order. 
        No bound checking.

        :param order: the order of the bucket to be saved.
        :type order: int
        """
        tl = self.traces[order]
        if len(tl) > 0:
            _ext.pickle_to(tl, os.path.join(self.analysis.path(), constants.traces_bucket_fmt).format(order, self.bucket_counts[order]))
            tl.clear()
            self.bucket_counts[order] = self.bucket_counts[order] + 1

    def save_remaining(self):
        """
        Save all non-empty buckets. Used to save remaining bucket when trace parsing ends.
        """
        for order in range(len(self.traces)):
            self._save_bucket(order)

    def total_buckets(self) -> int:
        """
        Returns the number of trace buckets among all orders.

        :return: the total buckets count.
        :rtype: int
        """
        return sum(self.bucket_counts)

def _parse_dse_folder(analysis : Analysis, traces_bucket: TracesBuckets, **kwargs):
    """
    Parses a Klee folder and returns all parsed trace from ktests.

    **Kwargs**:
     - `parse_rt_errs` (`trace -> bool`): boolean determining if traces with RTE termination should be considered. *default*: `False`.
     - `parse_all_errs` (`trace -> bool`): boolean determining if every traces should be considered. *default*: `False`.

    :param analysis: the analysis containing the DSE folder.
    :type analysis: Analysis
    :param traces_bucket: the traces buckets to be filled.
    :type traces_bucket: TracesBuckets
    """
    dse_folder = path.join(analysis.path(), constants.dse_folder)

    glob_str = path.join(dse_folder, "*.ktest")
    ktests = glob.glob(glob_str)
    errs = []
    errs_str = [path.join(dse_folder, "*.err"), path.join(dse_folder, "*.early")]
    for e in errs_str:
        errs.extend(glob.glob(e))
    
    i = 0 # counter for names

    parse_rt_errs = _args.get_or_throw(kwargs, "parse_rt_errs", bool, False)
    parse_all_errs = _args.get_or_throw(kwargs, "parse_all_errs", bool, False)

    def get_state(err_path: str) -> TerminationType:
        """
        Returns the termination type of the trace depending on a KLEE's error file's type.

        :param err_path: the path of the error file.
        :type err_path: str
        :return: the corresponding termination type.
        :rtype: TerminationType
        """
        if err_path.endswith(".early"): return TerminationType.Timeout
        if err_path.endswith(".ptr.err"): return TerminationType.Ptr
        if err_path.endswith(".read_only.err"): return TerminationType.ReadOnly
        if err_path.endswith(".free.err"): return TerminationType.Free
        if err_path.endswith(".abort.err"): return TerminationType.Abort
        if err_path.endswith(".assert.err"): return TerminationType.Assert
        if err_path.endswith(".div.err"): return TerminationType.Div
        if err_path.endswith(".user.err"): return TerminationType.User
        if err_path.endswith(".exec.err"): return TerminationType.Exec
        if err_path.endswith(".model.err"): return TerminationType.Model
        if err_path.endswith(".external.err"): return TerminationType.External
        log.error(f"Unknown Termination type: {err_path}.")
        return TerminationType.Unknown

    def should_parse(state: TerminationType) -> bool:
        """
        Determines if a termination type should be considered depending on kwargs.

        :param state: the termination type.
        :type state: TerminationType
        :return: true if the state should be parsed, false otherwise.
        :rtype: bool
        """
        if not parse_all_errs and state in [TerminationType.Timeout, TerminationType.User,  TerminationType.Exec, TerminationType.Model, TerminationType.Unknown, TerminationType.External]:
            return False
        if not parse_rt_errs and not parse_all_errs and state in [TerminationType.Ptr, TerminationType.Free, TerminationType.Abort, TerminationType.Assert, TerminationType.Div, TerminationType.ReadOnly]:
            return False
        return True

    if options.print_progress():  # c.f. %repo%/dev/tests/print_progress.py
        log.rinfo("\33[2K\rloading traces: 0%...")

    ignored = 0
    for ktest in ktests:
        
        name = path.basename(ktest).split(".")[0]

        state = TerminationType.Correct
        # check if has errors:
        err_match = [err_f for err_f in errs if name in err_f ]
        if len(err_match) > 1:
            log.error(f"unexpected multiple match for kests errors [{','.join(err_match)}].")
        if len(err_match) == 1:
            state = get_state(err_match[0])
            
            if not should_parse(state):
                ignored = ignored + 1
                continue

        if options.print_progress():  # c.f. %repo%/dev/tests/print_progress.py
            ktest_chars = len(str(len(ktest))) # Character count to print ktest number.
            percentage = (i / len(ktests)) * 100
            log.rinfo("\33[2K\rloading traces: {}% ({}/{}{})...       ".format(util.fixed_str_int(int(percentage), 2), util.fixed_str_int(i, ktest_chars), len(ktests), "" if ignored == 0 else ", skipped: " + str(ignored)))
        
        try:
            trace = _parse_trace(analysis, ktest, state, **kwargs)
            if trace != None:
                traces_bucket.add_trace(trace)
            else:
                ignored = ignored + 1
                
        except Exception as e:
            # Cannot parse
            log.warning("unable to parse the ktest " + os.path.basename(ktest) + ": " + str(e), clear=True)
            if log.current_level().value >= log.VerbosityLevel.Debug.value: traceback.print_exc()

        i = i + 1
    
    return ignored

def read_traces(analysis: Analysis, **kwargs) -> TracesResults:
    """
    Performs the *trace parsing step* gathering the traces from DSE and returning the created TracesResults object.

    Requires that the *mutation and DSE step* has been computed for this `analysis`.

    This function uses log to display progress, reduce verbosity level to
    reduce output.

    :param analysis: the analysis of the step.
    :type analysis: Analysis
    :return: the results object of the step.
    :rtype: TracesResults
    """
    if options.print_progress():  # c.f. %repo%/dev/tests/print_progress.py
        log.rinfo("loading traces...")
    else:
        log.info("loading traces...")

    max_order = analysis.max_order()

    parse_function = _parse_dse_folder # old version removed.   

    for order in range(max_order + 1):
        _folder.make_folder(os.path.join(analysis.path(), os.path.join(constants.traces_bucket_folder, str(order))))

    buckets = TracesBuckets(constants.traces_bucket_size, analysis)

    start = time.process_time()
    interrupted = False

    ignored = 0
    try:
        ignored = parse_function(analysis, buckets, **kwargs)
    except KeyboardInterrupt:
        interrupted = True
        log._raw(color.red + "keyboard interrupt: " + color.reset + " traces parsing ending.")
    
    end = time.process_time()
    duration = end - start

    buckets.save_remaining()

    traces = reload_traces_buckets(analysis)
    _folder.remove_folder(os.path.join(analysis.path(), constants.traces_bucket_folder))
        
    log.info("\rTraces loaded in {}{}.                            ".format(util.str_time(duration), " ({} ktests skipped)".format(ignored)) if ignored > 0 else "")
    
    return TracesResults(analysis, traces, ignored, duration, interrupted=interrupted)

class TracesStep(_ext.AnalysisStep):
    """
    Subclass of AnalysisStep for the *trace parsing step*, see :class:`lazart._internal_.extensions.AnalysisStep`.
    """
    def __init__(self, analysis: Analysis):
        """
        **__init__ (ctor):** Constructs a new TracesStep object associated to the specified analysis.

        :param analysis: the analysis of the step.
        :type analysis: Analysis
        """
        super(TracesStep, self).__init__(analysis, constants.traces_ext,  _ext.StepID.Traces)

    def execute(self, **kwargs):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        return read_traces(self.analysis, **kwargs)

    def init(self):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """        
        pass

    def save_disk(self):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        # TODO avoid saving ip_ptr directly....
        return super().save_disk()
    
    def load_disk(self, restart_from: int):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        super().load_disk(restart_from)

    def clean(self):
        """
        Clean trace parsing step removing traces buckets, and trace step.
        See :class:`lazart._internal_.extensions.AnalysisStep`. 
        """
        _folder.remove_file(_folder.lazart_file(self.analysis.path(), constants.traces_ext))

        _folder.remove_folder(os.path.join(self.analysis.path(), constants.traces_bucket_folder)) # Should not be required
        log.dev("cleaning traces...")

def traces_results(analysis : Analysis, **kwargs) -> TracesResults:
    """ Returns a TraceResults from an analysis object performing *Trace parsing step*.

    Requires that the *DSE and mutation step* has been computed for this `analysis`.
    
    Uses extension (traces_ext, see :class:`lazart._internal_.extensions.AnalysisStep`) system to reuse disk/RAM cache if available.
    This function uses log to display progress, reduce verbosity level to reduce output.    

    :param analysis: the Analysis object for which the traces will be parsed.
    :type analysis: Analysis
    :return: the results of trace parsing step.
    :rtype: TracesResults
    """
    step = _ext.get_step(TracesStep(analysis))
    
    return step.get_results(**kwargs)


