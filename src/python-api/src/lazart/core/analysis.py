""" The module *lazart.core.analysis* provides the class Analysis and other related definitions.

:since: 3.0
:author: Etienne Boespflug 
"""

# Python
import os
import pprint
import typing as ty

# Lazart
import lazart.logs as log
import lazart.constants as constants

# Internal
import lazart._internal_.extensions as _ext
import lazart._internal_.args as args
from lazart._internal_.enum import Flag
import lazart._internal_.folder as _folder

# Core
from lazart.core.countermeasures import verify_countermeasures
from lazart.core.attack_model import verify_attack_model
from lazart.core.tasks import verify_tasks

class AnalysisFlag(Flag):
    """
    AnalysisFlag holds the different flags for an analysis.
    Flags are used by common utility functions (report, exec) to customize behavior. 
    Only defined flags are valid, enforcing valid analysis combination.
    """
    # Attacks
    AttackAnalysisOnly = 1 << 1 #: Flag for *attacks analysis*.
    HSAnalysisOnly = 1 << 2 #: Flag for *hotspots analysis*.
    EqRedAnalysisOnly = 1 << 3  #: Flag for *equivalence and redundancy analysis*.

    DetectorOptimization = 1 << 6 #: Flag for *Detector Optimization* (CCPO).
    CMPlacement = 1 << 7 #; Flag for *Placement analysis*.
    AttackAnalysis = AttackAnalysisOnly | HSAnalysisOnly#: Flag for *attack analysis* with *hotspot analysis* (default).
    EqRedAnalysis = AttackAnalysisOnly | HSAnalysisOnly | EqRedAnalysisOnly #: Flag for equivalence and redundancy analysis including hotspots and simple attacks analysis.

    def isset(self, flag: int) -> bool:
        """
        Returns true if 'flag' is defined for this analysis flag.

        :param flag: the flag to be tested
        :type flag: int
        :return: true if the flag is defined.
        :rtype: bool
        """
        return bool(self & flag)

    def name(self) -> str:
        """
        Returns a string representing all defined flags.

        :return: a string representing all defined flags.
        :rtype: str
        """
        flags = []
        if self.isset(AnalysisFlag.AttackAnalysisOnly): flags.append("attacks-analysis")
        if self.isset(AnalysisFlag.HSAnalysisOnly): flags.append("hs-analysis")
        if self.isset(AnalysisFlag.EqRedAnalysisOnly): flags.append("eq-red-analysis")
        if self.isset(AnalysisFlag.DetectorOptimization): flags.append("ccp-optimization")
        if self.isset(AnalysisFlag.CMPlacement): flags.append("placement-analysis")

        return "[" + ", ".join(flags) + "]"


def _find_available_path(base_path: str) -> str:
    """
    Finds an available name for the analysis path (empty folder).

    :param base_path: the base name of the path.
    :type base_path: str
    :raises Exception: if the limit of try has been found.
    :return: the found path name.
    :rtype: str
    """
    i = 1

    for i in range(10000000):
        path = base_path + "_" + str(i)

        if not os.path.exists(path):
            return path

    log.error("cannot found an available path for the analysis, please specify one.")

class Analysis:
    """
    The Analysis class represents an analysis inside of *Lazart*. It holds parameters, flags, and options for an analysis. It is a central class that is used in several modules of *Lazart* to manipulate analysis and associated data.

    This class also implement the extension system (see :mod:`~lazart._internal_.extensions`) to internally cache analysis results inside the `Analysis` object.
    """

    def __init__(self, input_files: ty.List[str], attack_model: dict, **kwargs):
        """
        **__init__ (ctor):** Constructs a new Analysis object with the specified `max_order` (i.e. faults limit) and input source files.

        **Kwargs**:
         - `path` (`str`): path of the analysis in which intermediate files and reports will be stored. *default*: *automatic empty folder*.
         - `name` (`str`): the name of the Analysis. Used only for display purposes. *default*: `''`.
         - `flags` (`AnalysisFlag`): the flags of the analysis used by utility functions. *default*: `AnalysisFlag.AttackAnalysis`.
         - `max_order` (`AnalysisFlag`): the faults limit of the analysis. *default*: `2`.
         - `compiler_args` (`str`): the arguments to pass to the compiler. *default*: `''`.
         - `linker_args` (`str`): the arguments to pass to the linker. *default*: `''`.
         - `dis_args` (`str`): the arguments to pass to the disassembler. *default*: `''`.
         - `klee_args` (`str`): the arguments to pass to KLEE. *default*: `''`.
         - `wolverine_args` (`str`): the arguments to pass to Wolverine. *default*: `''`.
         - `tasks` (`Dict[str, List[str]]`): dictionary mapping tasks names (e.g. `rename_bb` or `add_trace`) to application space description (list of concerned functions).
         - `countermeasures` (`List[Dict]`): the list of countermeasures to be applied on the program before the analysis. *default*: `[]`. *only one countermeasure per analysis if currently supported.*
         - `extensions_names` (`str`): the list of extension names of the analysis. *default*: `[]`. Used mostly for copy operations.

        :param input_files: the list of each source files of the program to be compiled and analyzed.
        :type input_files: List[str]
        :param attack_model: the dictionary representing the attack model.
        :type attack_model: dict
        """

        # Positional
        self._input_files = [] #: List[str]: The list of each input source file absolute path.
        for inf in input_files:
            self._input_files.append(os.path.abspath(inf))
        if len(self._input_files) == 0:
            log.error("empty input file list.")
            
        self._am = attack_model #: dict: dictionary associating function or basic block names to sets of fault models. This dictionary follows the YAML structure of Wolverine's attack model file.
        if isinstance(self._am, str):
            self._am = os.path.abspath(self._am)
            if not os.path.exists(self._am):
                log.error("cannot find attack model file '{}'".format(self._am))
        elif not verify_attack_model(self._am):
            log.error("invalid attack model")

        # General
        self._init = False #: bool: Determines if the analysis folder has been initialized.

        self._path = os.path.abspath(args.get_or_throw(kwargs, "path", str, _find_available_path("lzresults"))) 
        """ str: The absolute path of the analysis folder.
        This folder contains intermediate files and reports.
        
        Uses overridden :func:`lazart.options.output_folder` if present (defined with common script command line). """
        self._name = args.get_or_throw(kwargs, "name", str, "")
        """ str: The name of the Analysis.
        Used only for display purposes."""
        self._flags = args.get_or_throw(kwargs, "flags", AnalysisFlag, AnalysisFlag.AttackAnalysis)
        """ AnalysisFlag: Optional flags for the Analysis to enable automatic behavior for utility and reporting functions.
        
        See :mod:`lazart.util.exec`, :mod:`lazart.results.report`, :mod:`lazart.util.verify`."""

        # New 
        self._max_order = args.get_or_throw(kwargs, "max_order", int, 2) #: int: The maximum faults count per trace for this analysis.
        if self._max_order < 0:
            log.warning("expected 'max_order' to be positive integer, value set to 4.")
            self._max_order = 2

        # Tasks
        self._tasks = args.get_or_throw(kwargs, "tasks", args.DictType(str, object), {"rename_bb":["__mut__"], "add_trace": ["__mut__"]}) 
        """: Dict[str, object]: a map of task name to task definition (see Wolverine's format).
         - `rename_bb`: *basic-block renaming* task that rename with numbers all basic-blocks in the required section that has not already a name.
         - `add_trace`:  add a trace for replay (using `printf`) at the start of all basic-blocks in the required section. Allowing to keep those basic-block branchings inside the traces.
         
        The definition is a list of function names on which the task will be performed. Use "__all__" to apply on all functions (except Lazart's and LLVM's internals) and "__mut__" to apply on every function that is associated with a fault model. 
        """
        if not verify_tasks(self._tasks):
            pass#log.warning(f"unexpected 'tasks' field: {self._tasks}")

        self._cms = args.get_or_throw(kwargs, "countermeasures", args.ListType(dict), []) 
        """ List[Dict[str, Any]]: list of countermeasures represented by their YAML description (see Wolverine's format) in form of dictionaries.
        """

        # Compilation
        self._compiler_args = args.get_or_throw(kwargs, "compiler_args", str, constants.compiler_default_args)
        """ str: The arguments to pass to the compiler."""
        self._linker_args = args.get_or_throw(kwargs, "linker_args", str, constants.linker_default_args)
        """ str: The arguments to pass to the linker."""
        self._dis_args = args.get_or_throw(kwargs, "dis_args", str, constants.dis_default_args)
        """ str: The arguments to pass to the disassembler."""

        if not verify_countermeasures(self._cms):
            log.error("invalid countermeasures")

        self._extensions_names = args.get_or_throw(kwargs, "extensions_names", args.ListType(str), [])
        """ List[str]: the list of extension names of the analysis.
        *Used for analysis copy operations.*"""

        # Run
        self._klee_args = args.get_or_throw(kwargs, "klee_args", str, constants.dse_default_args)
        """ str: The arguments to pass to Klee (Dynamic Symbolic Engine)."""
        self._wolverine_args = kwargs.get("wolverine_args", constants.wolverine_default_args)
        """ str: The arguments to pass to Wolverine (Mutation step). """

        self._extensions = [] # List[AnalysisStep]: corresponds to the list of all step objects extending the analysis.

    def init(self):
        """
        Init the analysis, setting the `_init` member flag and creating the analysis folder.

        :raises Exception: _description_
        """
        log.verbose("init analysis.")
        if self._init:
            raise Exception("analysis already initialized.")
        self._init = True
        if not os.path.exists(self.path()):
            os.makedirs(self.path())    
        _folder.make_lazart_dir(self.path())
    
    def save(self):
        """
        Save or update analysis and extension data to the analysis folder (.lazart).
        """             
        cpy = Analysis(self.input_files(), self._am,
            path=self._path,
            flags=self._flags,
            name=self._name,
            max_order=self._max_order,
            compiler_args=self._compiler_args,
            linker_args=self._linker_args,
            dis_args=self._dis_args,
            wolverine_args=self._wolverine_args,
            klee_args=self._klee_args,
            tasks=self._tasks,
            countermeasures=self._cms,
           )        
        cpy._extensions=self._extensions

        for ext in self._extensions:
            ext.analysis = None

        _ext.pickle_to(cpy, _folder.lazart_file(self.path(), constants.analysis_file)) # Update ext list
    
        for ext in self._extensions:
            ext.analysis = self

    def clean(self):
        """
        Clean the analysis, removing the analysis folder.
        Remove the analysis folder.
        """
        _folder.remove_folder(self.path())

    def clean_cache(self):
        """
        Clean cache for each extension in reverse order.
        """
        for i in range(len(self._extensions) - 1, -1, -1):
            _ext.clean_extension(self, self._extensions[i].name, True)
        self._extensions = []
        self._extensions_names = []

    # Getters

    def max_order(self) -> int:
        """
        Returns the maximum order (i.e. fault limit) of the analysis.

        :return: the max_order of the analysis.
        :rtype: int
        """
        return self._max_order

    def name(self) -> str: 
        """
        Returns the name of the analysis.

        :return: the name of the analysis.
        :rtype: str
        """
        return self._name

    def path(self) -> str: 
        """
        Returns the path of the analysis (folder in which computation are put).

        :return: the path in which the analysis is computed.
        :rtype: str
        """
        return self._path

    def flags(self) -> AnalysisFlag:
        """
        Returns the flags of the analysis.

        :return: the flags of the analysis.
        :rtype: AnalysisFlag
        """
        return self._flags

    def input_files(self) -> ty.List[str]:
        """
        Returns the list of absolute paths of the input source files.

        :return: the list of input source files of the analysis.
        :rtype: List(str)
        """
        return self._input_files

    def compiler_args(self) -> str: 
        """
        Returns the arguments to pass to the compiler.

        :return: the arguments to pass to the compiler.
        :rtype: str
        """
        return self._compiler_args

    def linker_args(self) -> str:
        """
        Returns the arguments to pass to the linker.

        :return: the arguments to pass to the linker.
        :rtype: str
        """        
        return self._linker_args

    def dis_args(self) -> str:
        """
        Returns the arguments to pass to the disassembler.

        :return: the arguments to pass to the disassembler.
        :rtype: str
        """
        return self._dis_args

    def wolverine_args(self) -> str:
        """
        Returns the arguments to pass to the mutation tool Wolverine.

        :return: the arguments to pass to Wolverine.
        :rtype: str
        """
        return self._wolverine_args

    def klee_args(self) -> str:
        """
        Returns the arguments to pass to the DSE engine Klee.

        :return: the arguments to pass to the DSE engine.
        :rtype: str
        """
        return self._klee_args

    def attack_model(self) -> dict:
        """
        Returns the attack model of the analysis, using Wolverine YAML format.

        :return: the attack model dictionary.
        :rtype: dict
        """
        return self._am

    def tasks(self) -> ty.Dict[str, ty.List[str]]:
        """  
        Returns the list of tasks in the analysis.

        :return: the list of task applied for the analysis.
        :rtype: Dict[str, List[str]]
        """
        return self._tasks

    def countermeasures(self) -> ty.Dict[str, ty.Any]:
        """  
        Returns the list of countermeasure description dict of the analysis.

        :return: the list of countermeasures applied for the analysis.
        :rtype: Dict[str, CMDict]
        """
        return self._cms

    def __str__(self) -> str:
        """
        Returns the string representing the Analysis.

        :return: the string representation of the object.
        :rtype: str
        """              
        ret = "Analysis: {}\n".format(self.name())
        
        ret += " - path: " + self.path() + "\n"
        ret += " - flags: " + str(self.flags()) + "\n"
        ret += " - order: " + str(self.max_order()) + "\n"
        # AM
        if isinstance(self._am, str):
            ret += " - attack-model: {}\n".format(self._am)
        else:
            ret += " - attack-model: {}\n".format(pprint.pformat(self._am, indent=4))
        ret += f" - tasks: {self.tasks()}\n"
        if len(self.countermeasures()) == 0:
            ret += "   - auto-countermeasures: none\n"
        else:
            ret += "   - auto-countermeasures:\n"
            for cm in self.countermeasures():
                ret += "      - {}\n".format(pprint.pformat(cm, indent=4    ))
        # Params
        tmp_list = []
        for file in self._input_files:
            tmp_list.append(os.path.basename(file))
        ret += " - input-files: [{}]\n".format(", ".join(os.path.basename(f) for f in self._input_files))
        ret += " - compiler: {} {}\n".format(constants.compiler_cmd, self.compiler_args())
        ret += " - linker: {} {}\n".format(constants.linker_cmd, self.linker_args())
        ret += " - disassembler: {} {}\n".format(constants.dis_cmd, self.dis_args())
        ret += " - wolverine args: {}\n".format(self.wolverine_args())
        ret += " - DSE: klee {}\n".format(self.klee_args())

        return ret