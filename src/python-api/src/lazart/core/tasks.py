""" The module *lazart.core.tasks* contains definitions for *tasks* performed by wolverine such as `add_trace` or `rename_bb`.

:since: 4.0
:author: Etienne Boespflug 
"""

# Python
import typing as ty

# Internal
import lazart._internal_.args as _args

# Lazart
import lazart.logs as log


def verify_space(fct: ty.List[str]) -> bool:
    """
    Verifies a list of function name (including '__mut__' and '__all__' keywords) and report any error.

    :param fct: the list of function (see Wolverine's format).
    :type fct: List[str]
    :return: `True` if the list is well-formed, `False` otherwise.
    :rtype: bool
    """
    if not _args.expect_type(fct, _args.ListType(str), None, " for task's application space."):
        return False
    
    # Cannot verify that function exists here, made by Wolverine.

def verify_tasks(tasks: ty.Dict[str, object]) -> bool:
    """
    Verifies the *tasks* dictionary and check each task (except countermeasures tasks).

    :param tasks: the map of tasks objects to be checked.
    :type tasks: Dict[str, object]
    :return: `True` if the countermeasures are valid, `False` otherwise.
    :rtype: bool
    """
    correct = True

    for name, space in tasks.items():
        standard_tasks = ["add_trace", "rename_bb", "countermeasures"]
        if name not in standard_tasks:
            log.warning(f"unknown task type '{name}'.")

        # Check space: list of str.
        correct = correct | verify_space(space)

    return correct