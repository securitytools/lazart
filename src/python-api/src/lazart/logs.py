""" The module *lazart.logs* contains definitions for **Lazart**'s logging system (file and console logging).

Most operations use the verbosity level of current execution to decide if some information should be displayed or not.

Verbosity level can be modified thought access functions or thought scripts's command line argument (see :mod:`lazart.script`).

:since: 3.0
:author: Etienne Boespflug
:dev-note: Reexported by **lazint.py**.  
"""

# Python
from enum import Enum
import typing as ty

# Lazart
import lazart.options as options

# Internal
import lazart._internal_.color as color
import lazart._internal_.util as util

class VerbosityLevel(Enum):
    """
    The *verbosity level* is an enumeration corresponding to a level of display of Lazart tool.

    Logging calls use a minimal verbosity level and the current Lazart execution user-specified verbosity level is compared against to determines if the message should be displayed.
    """
    Error = 0 #: Only errors are printed. Lowest *verbosity level*.
    Warning = 1 #: Only errors and warnings are printed.
    Quiet = 2 #: Only essential information are printed. 
    Info = 3 #: Normal verbosity level.
    Verbose = 4 #: More information are printed.
    Debug = 5 #: Debug information are printed.
    Dev = 6 #: All information are printed, highest *verbosity level*.

_current_level = VerbosityLevel.Info
""" VerbosityLevel: The current *verbosity level* global variable.

*default*: `VerbosityLevel.Dev`"""
_log_file = None 
""" file: The log file of current execution.  
If `None`, **Lazart** will only print on console.

*default*: `None`
"""

def init(verbosity_level: VerbosityLevel, log_path: str):
    """
    Initializes the *verbosity level* and the *log file* globals.

    :param verbosity_level: the verbosity level value.
    :type verbosity_level: VerbosityLevel
    :param log_path: the path to the file in which **Lazart** will output logs. Use `None` to disable file logging.
    :type log_path: str
    """
    global _current_level
    _current_level = verbosity_level

    global _log_file  
    if not (log_path is None) and log_path != "":
        _log_file = open(log_path, "w") 

def set_verbosity(verbosity_level: VerbosityLevel):
    """
    Modifies the *verbosity level* global with the specified value.

    :param verbosity_level: the verbosity level value.
    :type verbosity_level: VerbosityLevel
    """
    global _current_level
    _current_level = verbosity_level

def current_level() -> VerbosityLevel:
    """
    Returns the current verbosity level.

    :return: the current verbosity level.
    :rtype: VerbosityLevel
    """
    return _current_level

# log functions

def _file_write(msg: str, write: bool):
    """
    Writes a message in the specified log file if it exists and if the write boolean parameter is set to `True`.

    :param msg: the message to log.
    :type msg: str
    :param write: boolean determining if the message should be written.
    :type write: bool
    """
    if not write:
        return

    global _log_file
    if _log_file is None:
        return

    _log_file.write(msg + "\n")

def _verify_defines(required_defines: ty.List[str]) -> bool:
    """
    Returns `True` if the specified *debug defines* are defined for **Lazart** (see, :func:`lazart.options.defines`), `False` otherwise. 

    *Debug defines* starts with the prefix `lz.log.debug`.

    :param required_defines: the list of required defines to be checked.
    :type required_defines: List[str]
    :return: `True` if all specified defines are defined, `False` otherwise.
    :rtype: bool
    """
    d = options.defines()
    
    for define in required_defines:
        # Defines.
        if define.startswith("lz.log.debug."):
            if not d.get(define, False):
                return False
    return True

def _raw(message: str, **kwargs):
    """
    Logs a message.

    Supports `print`'s *keyword arguments*.

    **Kwargs**:
     - `required_defines` (`List[str]`): the list of required defines to be checked before logging the message. *default*: `[]`.
     - `write_log_file` (`bool`): if `False`, nothing is printed in the log file. *default*: `True`.

    :param message: the message to be printed.
    :type message: str
    """
    if _verify_defines(kwargs.get("required_defines", [])):
        print(str(message), **util.remove_kwargs("write_log_file", **kwargs))
    _file_write(str(message), kwargs.get("write_log_file", True))

def error(message: str, **kwargs):
    """
    Logs a message with `Error` verbosity level. Throws an exception witht the specified verbosity level.

    Supports `print`'s *keyword arguments*.

    **Kwargs**:
     - `required_defines` (`List[str]`): the list of required defines to be checked before logging the message. *default*: `[]`.
     - `write_log_file` (`bool`): if `False`, nothing is printed in the log file. *default*: `True`.

    :param message: the message to be printed.
    :type message: str
    :raises Exception: always throws an exception with the specified message.
    """
    if(_current_level.value >= VerbosityLevel.Error.value):
        if _verify_defines(kwargs.get("required_defines", [])):
            print(("\r" if kwargs.get("clear", False) else "") + color.red + "error: " + color.reset + str(message), **util.remove_kwargs("clear", "write_log_file", "required_defines", **kwargs))
    _file_write("error: " + str(message), kwargs.get("write_log_file", True))
    raise Exception(message)

def warning(message: str, **kwargs):
    """
    Logs a message with `Warning` verbosity level.

    Supports `print`'s *keyword arguments*.

    **Kwargs**:
     - `required_defines` (`List[str]`): the list of required defines to be checked before logging the message. *default*: `[]`.
     - `write_log_file` (`bool`): if `False`, nothing is printed in the log file. *default*: `True`.

    :param message: the message to be printed.
    :type message: str
    """
    if(_current_level.value >= VerbosityLevel.Warning.value):
        if _verify_defines(kwargs.get("required_defines", [])):
            print(("\r" if kwargs.get("clear", False) else "") + color.brown + "warning: " + color.reset + str(message), **util.remove_kwargs("clear", "write_log_file", "required_defines", **kwargs))
    _file_write("warning: " + str(message), kwargs.get("write_log_file", True))

def quiet(message: str, **kwargs):
    """
    Logs a message with `Quiet` verbosity level.

    Supports `print`'s *keyword arguments*.

    **Kwargs**:
     - `required_defines` (`List[str]`): the list of required defines to be checked before logging the message. *default*: `[]`.
     - `write_log_file` (`bool`): if `False`, nothing is printed in the log file. *default*: `True`.

    :param message: the message to be printed.
    :type message: str
    """
    if(_current_level.value >= VerbosityLevel.Quiet.value):
        if _verify_defines(kwargs.get("required_defines", [])):
            print(color.reset + str(message), **util.remove_kwargs("write_log_file", "required_defines", **kwargs))
    _file_write(str(message), kwargs.get("write_log_file", True))

def info(message: str, **kwargs):
    """
    Logs a message with `Info` verbosity level.

    Supports `print`'s *keyword arguments*.

    **Kwargs**:
     - `required_defines` (`List[str]`): the list of required defines to be checked before logging the message. *default*: `[]`.
     - `write_log_file` (`bool`): if `False`, nothing is printed in the log file. *default*: `True`.

    :param message: the message to be printed.
    :type message: str
    """
    if(_current_level.value >= VerbosityLevel.Info.value):
        if _verify_defines(kwargs.get("required_defines", [])):
            print(color.reset + str(message), **util.remove_kwargs("write_log_file", **kwargs))
    _file_write(str(message), kwargs.get("write_log_file", True))

def verbose(message: str, **kwargs):
    """
    Logs a message with `Verbose` verbosity level.

    Supports `print`'s *keyword arguments*.

    **Kwargs**:
     - `required_defines` (`List[str]`): the list of required defines to be checked before logging the message. *default*: `[]`.
     - `write_log_file` (`bool`): if `False`, nothing is printed in the log file. *default*: `True`.

    :param message: the message to be printed.
    :type message: str
    """
    if(_current_level.value >= VerbosityLevel.Verbose.value):
        if _verify_defines(kwargs.get("required_defines", [])):
            print(color.reset + str(message), **util.remove_kwargs("write_log_file", **kwargs))
    _file_write(str(message), kwargs.get("write_log_file", True))

def debug(message: str, **kwargs):
    """
    Logs a message with `Debug` verbosity level.

    Supports `print`'s *keyword arguments*.

    **Kwargs**:
     - `required_defines` (`List[str]`): the list of required defines to be checked before logging the message. *default*: `[]`.
     - `write_log_file` (`bool`): if `False`, nothing is printed in the log file. *default*: `True`.

    :param message: the message to be printed.
    :type message: str
    """
    if(_current_level.value >= VerbosityLevel.Debug.value):
        if _verify_defines(kwargs.get("required_defines", [])):
            print(color.reset + str(message), **util.remove_kwargs("write_log_file", **kwargs))
    _file_write(str(message), kwargs.get("write_log_file", True))

def dev(message: str, **kwargs):
    """
    Logs a message with `Dev` verbosity level.

    Supports `print`'s *keyword arguments*.

    **Kwargs**:
     - `required_defines` (`List[str]`): the list of required defines to be checked before logging the message. *default*: `[]`.
     - `write_log_file` (`bool`): if `False`, nothing is printed in the log file. *default*: `True`.

    :param message: the message to be printed.
    :type message: str
    """
    if(_current_level.value >= VerbosityLevel.Dev.value):
        if _verify_defines(kwargs.get("required_defines", [])):
            print(color.reset + str(message), **util.remove_kwargs("write_log_file", **kwargs))
    _file_write(str(message), kwargs.get("write_log_file", True))

def rerror(message: str, **kwargs):
    r"""
    Helper for :func:`error` without line break (`end=''`). Allows the use of '\r' character to clean console.

    Doesn't print to *log file*. The message is ignored if :func:`lazart.options.print_progress` returns `False`.

    :param message: the message to be printed.
    :type message: str
    """
    if _verify_defines(kwargs.get("required_defines", [])):
        error(str(message), end="", write_log_file=False)

def rwarning(message: str, **kwargs):
    r"""
    Helper for :func:`warning` without line break (`end=''`). Allows the use of '\r' character to clean console.

    Doesn't print to *log file*. The message is ignored if :func:`lazart.options.print_progress` returns `False`.

    :param message: the message to be printed.
    :type message: str
    """
    if _verify_defines(kwargs.get("required_defines", [])):
        warning(str(message), end="", write_log_file=False)

def rquiet(message: str, **kwargs):
    r"""
    Helper for :func:`quiet` without line break (`end=''`). Allows the use of '\r' character to clean console.

    Doesn't print to *log file*. The message is ignored if :func:`lazart.options.print_progress` returns `False`.

    :param message: the message to be printed.
    :type message: str
    """
    if _verify_defines(kwargs.get("required_defines", [])):
        quiet(message, end="", write_log_file=False)
    
def rinfo(message: str, **kwargs):
    r"""
    Helper for :func:`info` without line break (`end=''`). Allows the use of '\r' character to clean console.

    Doesn't print to *log file*. The message is ignored if :func:`lazart.options.print_progress` returns `False`.

    :param message: the message to be printed.
    :type message: str
    """
    if _verify_defines(kwargs.get("required_defines", [])):
        info(str(message), end="", write_log_file=False)

def rverbose(message: str, **kwargs):
    r"""
    Helper for :func:`verbose` without line break (`end=''`). Allows the use of '\r' character to clean console.

    Doesn't print to *log file*. The message is ignored if :func:`lazart.options.print_progress` returns `False`.

    :param message: the message to be printed.
    :type message: str
    """
    if _verify_defines(kwargs.get("required_defines", [])):
        verbose(str(message), end="", write_log_file=False)

def rdebug(message: str, **kwargs):
    r"""
    Helper for :func:`debug` without line break (`end=''`). Allows the use of '\r' character to clean console.

    Doesn't print to *log file*. The message is ignored if :func:`lazart.options.print_progress` returns `False`.

    :param message: the message to be printed.
    :type message: str
    """
    if _verify_defines(kwargs.get("required_defines", [])):
        debug(str(message), end="", write_log_file=False)

def rdev(message: str, **kwargs):
    r"""
    Helper for :func:`dev` without line break (`end=''`). Allows the use of '\r' character to clean console.

    Doesn't print to *log file*. The message is ignored if :func:`lazart.options.print_progress` returns `False`.
    
    :param message: the message to be printed.
    :type message: str
    """
    if _verify_defines(kwargs.get("required_defines", [])):
        dev(str(message), end="", write_log_file=False)

def ferror(message: str, **kwargs):
    r"""
    Helper for :func:`error` without line break (`end=''`) and flushing the output. 

    Doesn't print to *log file*.

    :param message: the message to be printed.
    :type message: str
    """
    if _verify_defines(kwargs.get("required_defines", [])):
            error(str(message), flush=True, end="", write_log_file=False)

def fwarning(message: str, **kwargs):
    r"""
    Helper for :func:`warning` without line break (`end=''`) and flushing the output. 

    Doesn't print to *log file*.

    :param message: the message to be printed.
    :type message: str
    """
    if _verify_defines(kwargs.get("required_defines", [])):
            warning(str(message), flush=True, end="", write_log_file=False)

def fquiet(message: str, **kwargs):
    r"""
    Helper for :func:`quiet` without line break (`end=''`) and flushing the output. 

    Doesn't print to *log file*.

    :param message: the message to be printed.
    :type message: str
    """
    if _verify_defines(kwargs.get("required_defines", [])):
            quiet(message, flush=True, end="", write_log_file=False)
    
def finfo(message: str, **kwargs):
    r"""
    Helper for :func:`info` without line break (`end=''`) and flushing the output. 

    Doesn't print to *log file*.

    :param message: the message to be printed.
    :type message: str
    """
    if _verify_defines(kwargs.get("required_defines", [])):
            info(str(message), flush=True, end="", write_log_file=False)

def fverbose(message: str, **kwargs):
    r"""
    Helper for :func:`verbose` without line break (`end=''`) and flushing the output. 

    Doesn't print to *log file*.

    :param message: the message to be printed.
    :type message: str
    """
    if _verify_defines(kwargs.get("required_defines", [])):
            verbose(str(message), flush=True, end="", write_log_file=False)

def fdebug(message: str, **kwargs):
    r"""
    Helper for :func:`debug` without line break (`end=''`) and flushing the output. 

    Doesn't print to *log file*.

    :param message: the message to be printed.
    :type message: str
    """
    if _verify_defines(kwargs.get("required_defines", [])):
            debug(str(message), flush=True, end="", write_log_file=False)

def fdev(message: str, **kwargs):
    r"""
    Helper for :func:`dev` without line break (`end=''`) and flushing the output. 

    Doesn't print to *log file*.

    :param message: the message to be printed.
    :type message: str
    """
    if _verify_defines(kwargs.get("required_defines", [])):
            dev(str(message), flush=True, end="", write_log_file=False)