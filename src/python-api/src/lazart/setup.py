# Python
import setuptools

if __name__ == "__main__":
    setuptools.setup(
        name="lazart",
        version="4.0",
        author="Etienne Boespflug",
        author_email="etienne.boespflug@gmail.com",
        description="The Lazart package.",
        packages=setuptools.find_packages(),
        classifiers=[
            "Programming Language :: Python :: 3",
            "License :: OSI Approved :: MIT License",
            "Operating System :: Unix only",
        ],
        python_requires='>=3.4',
    )