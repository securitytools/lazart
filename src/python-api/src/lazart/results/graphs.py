""" The module *lazart.results.graphs* provides definitions for graphs generation.

**Note: graph generation module is not migrated to Lazart 4.0 and should be used in current version.**

:since: 3.0
:author: Etienne Boespflug 
"""

import os
import pydot
from lazart.core.traces import BasicBlock, Detection, Fault, Trace
import lazart.logs as log
from lazart.core.analysis import Analysis
from lazart.analysis.attack_analysis import attacks_results
import lazart.constants as constants
import re
from lazart.analysis.attack_redundancy import AttackRedundancyGraph, AttackRedundancyNode, attacks_redundancy_results

# Util
def _get_node_label(node: pydot.Node)  -> str:
    """
    Parse the label of the node to returns the name of the basic block.

    TODO() DO NOT WORK for graph with custom text (for instance with graph generated with -graph-cfg) in. 

    :param node: the node to be parsed.
    :type node: pydot.Node
    :return: the name of the node's basic block.
    :rtype: str
    """
    label = node.get_label()
    result = label.split("|")[0][2:] if "|" in label else label[2:-2]
    return result


# Redundancy Graph

def generate_redundancy_graph(analysis: Analysis):
    """
    Generates the redundancy graph in the `analysis` folder.

    :param analysis: the considered analysi.
    :type analysis: Analysis
    """
    log.finfo("generating redundancy graph...")
    arr = attacks_redundancy_results(analysis)

    dot_graph = pydot.Dot(graph_type='digraph', splines="ortho", rankdir='LR')

    def get_or_make(graph: pydot.Graph, node: AttackRedundancyNode) -> pydot.Node:
        """
        Returns the node in `graph` associated to `node` in equivalence / redundancy graph.

        :param graph: the dot graph in which the node is searched.
        :type graph: pydot.Graph
        :param node: the node in redundancy graph.
        :type node: AttackRedundancyNode
        :return: the corresponding node in `graph`.
        :rtype: pydot.Node
        """
        for dot_node in graph.get_nodes():
            if dot_node.get_label() == node.trace_name():
                return dot_node
        
        n = pydot.Node(node.trace_name(), label=node.trace_name())
        graph.add_node(n)
        return n
 
    for key in arr.graph()._nodes:
        curr_node = arr.graph()._nodes[key]
        curr_dot_node = get_or_make(dot_graph, curr_node)

        for redundant in curr_node._edges:
            redundant_dot_node = get_or_make(dot_graph, redundant)
            try:
                dot_graph.add_edge(pydot.Edge(curr_dot_node, redundant_dot_node))
            except IndexError:
                break
    
    graph_folder = os.path.join(analysis.path(), constants.graphs_folder)
    file = os.path.join(graph_folder, constants.attack_redundancy_graph)
    #graph = pydot.graph_from_dot_data(dot_graph.getvalue())
    #graph[0].write_pdf(file)
    #dot_graph.to_string()
    dot_graph.write(file + ".dot")

    graph = pydot.graph_from_dot_file(file + ".dot")
    graph[0].write_pdf(file + ".pdf")
    log.rinfo(" done.\n")
    
# Fault graph.
def make_fault_graph(trace: Trace, graph: pydot.Graph) -> pydot.Graph:
    """
    Create a fault trace for the specified `trace` and function `graph`.

    TODO: interproc, update.
    
    :param trace: the trace to be traced on graph.
    :type trace: Trace
    :param graph: the graph on which the transition will be added.
    :type graph: pydot.Graph
    :return: the fault graph.
    :rtype: pydot.Graph
    """

    def node_from_label(graph: pydot.Graph, label: str) -> str:
        """
        Returns the name for the node with specified `label` in `graph`.

        :param graph: the rgaph in which the node will be searched.
        :type graph: pydot.Graph
        :param label: the label of the searched node.
        :type label: str
        :return: the found name, empty string otherwise.
        :rtype: str
        """
        for node in graph.get_nodes():
            if label == _get_node_label(node):
                name = node.get_name()
                return "" if name == None else name

    #def parseItem(item):
    #    return (False,item.split(" ")[1]) if item.startswith("[TRACE]") else (True,item.split(" ")[-1][:-1])

    # Make links.

    i = 0
    counter = 0
    src_node = None
    fault = None

    out_node = None # Contains the node representing other functions (if needed).

    while i < len(trace.events):
        item = trace.events[i]

        # BasicBlock
        if type(item) == BasicBlock:
            node = node_from_label(graph, item.name)

            print(f"- treating: {node} ({item})")

            if node == None:
                if out_node == None:
                    # Generate new node.
                    out_node = pydot.Node("other_fcts", label="Other functions", shape="rect")
                    graph.add_node(out_node)

                    print(f"     outnode generated: {node}")
                # Replace with other_fcts.
                node = out_node

            if src_node != None: 
                # Make link
                graph.add_edge(pydot.Edge(
                    src_node,
                    node,
                    label=counter,
                    color="red" if fault != None else "green"
                ))
                print(f"     added TI: {node}")

                # clean step
                src_node = node 
                fault = None
            else:
                src_node = node
                fault = None
            counter = counter + 1
        # Detection
        elif type(item) == Detection:
            pass
        # Fault
        elif type(item) == Fault:
            fault = item
        else:
            raise Exception("Unknown event.")

        i = i + 1

    return graph


def graph_ll(analysis: Analysis, function: str) -> pydot.Graph:
    """
    Return the graph of the specified function for analysis. The graph contains all instructions in basic block, for simple graphs see :func:`lazart.results.graphs.graph`.

    :param analysis: the considered analysis.
    :type analysis: Analysis
    :param function: the considered function name.
    :type function: str
    :return: the graph corresponding to the specified function.
    :rtype: pydot.Graph
    """
    graph_folder = os.path.join(analysis.path(), constants.graphs_folder)
    graph_filename = constants.dot_ll_file.format(function)
    graph_file = os.path.join(graph_folder, graph_filename)

    if not os.path.exists(graph_file):
        log.error("graph file not found for function '{}' : {}.".format(function, graph_file))
        return None
    return pydot.graph_from_dot_file(graph_file)[0]

def graph(analysis: Analysis, function: str) -> pydot.Graph:
    """
    Return the graph of the specified `function` for analysis. The graph contains only the basic blocks names, for detailled graphs see :func:`lazart.results.graphs.graph_ll`.

    :param analysis: the considered analysis.
    :type analysis: Analysis
    :param function: the considered function name.
    :type function: str
    :return: the graph corresponding to the specified function.
    :rtype: pydot.Graph
    """
    graph_folder = os.path.join(analysis.path(), constants.graphs_folder)
    graph_filename = constants.dot_file.format(function)
    graph_file = os.path.join(graph_folder, graph_filename)

    if not os.path.exists(graph_file):
        log.error("graph file not found for function '{}' : {}.".format(function, graph_file))
        return None
    return pydot.graph_from_dot_file(graph_file)[0]

def generate_trace_graph(analysis: Analysis, function: str, trace: Trace, **kwargs):
    """
    Generates the trace graph, including transition and faults, for the specified `function`.

    TODO: interproc, update.

    **Kwargs**:
        - `graph` (`str`): the folder path in which the graph will be generated. *default*: `constants.graphs_folder`.

    :param analysis: the considered analysis.
    :type analysis: Analysis
    :param function: the considered function name.
    :type function: str
    :param trace: the trace to generate the graph.
    :type trace: Trace
    """
    graph_folder = kwargs.get("graph", os.path.join(analysis.path(), constants.graphs_folder))

    g = graph(analysis, function)

    make_fault_graph(trace, g)

    # TODO ptional ? 

    trace_name = os.path.basename(trace.path)

    file = os.path.join(graph_folder, "graph.order{}.{}.{}.pdf".format(trace.order, trace_name, function))
    g.write_pdf(file)
    
        
def generate_attacks_fault_graphs(analysis: Analysis, function: str):
    """
    Generates the trace graphs for all attacks for the specified function in the `analysis` folder.

    TODO: interproc, update.

    :param analysis: the considered analysis.
    :type analysis: Analysis
    :param function: the considered function name.
    :type function: str
    """
    log.finfo("generating attacks fault graphs...")
    graph_folder = os.path.join(analysis.path(), constants.graphs_folder)

    for order in range(1, analysis.max_order() + 1):
        for trace in attacks_results(analysis).attacks(order):
            g = graph(analysis, function)

            make_fault_graph(trace, g)

            trace_name = os.path.basename(trace.path)

            file = os.path.join(graph_folder, "graph.order{}.{}.{}.pdf".format(trace.order, trace_name, function))
            g.write_pdf(file)
    log.rinfo(" done.\n")

# TP Graphs.

def generate_detector_graph(analysis: Analysis, function: str):
    """
    Generates the detectors graph for the specified function in the `analysis` folder.

    TODO: interproc, update

    :param analysis: the considered analysis.
    :type analysis: Analysis
    :param function: the considered function name.
    :type function: str
    """
    graph_folder = os.path.join(analysis.path(), constants.graphs_folder)

    graph = graph_ll(analysis, function)
    log.finfo("generating trigger points graph...")

    # For each BB, keep the name of the Basic block and check for countermeasure call.
    for node in graph.get_nodes():
        label = node.get_label()
        bb = label.split(":")[0][2:] if ":" in label else label[2:-2]
        #print("bb = " + bb)

        label = "{}\n".format(bb)

        for line in node.get_label().splitlines():
            p = re.compile("countermeasure\(i32 (.+)\)")
            tp = p.search(line)
            if tp != None:
                #print("tp = " + str(tp.group(1)))
                label += "TP (id: {})".format(tp.group(1))
            else:
                p = re.compile(r"\*\)\(i32 (.+)\)")
                tp = p.search(line)

                if tp!= None:
                    #print("tp = " + str(tp.group(1)))
                    label += "TP (id: {})".format(tp.group(1))

                else:
                    pass#print("NOPE: " + line)
        
        node.set_label(label)
             
    file = os.path.join(graph_folder, constants.trigger_point_graph.format(function))
    graph.write_png(file)
    log.rinfo(" done.\n")