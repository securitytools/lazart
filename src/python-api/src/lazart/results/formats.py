""" The module *lazart.results.formats* provides definitions for results formatting.

:since: 3.0
:author: Etienne Boespflug 
"""

# Python
from tabulate import tabulate
import typing as ty

# Lazart
import lazart.logs as log
import lazart._internal_.args as _args

def column(tas: ty.List[str], tbs : ty.List[str], headers: ty.Tuple[ty.List[str], ty.List[str]] = ([], []), **kwargs) -> str:
    """
    Returns a string corresponding to the two column print of string convertible items in `tas` to the first column and `tbs`. Support Table type for item in `tas` and `tbs`.
    Use chaining (`column(column(list_a, list_b), list_b)`) to handle more columns.

    Headers are provided for each item in `tas` and `tbs`. Empty headers are ignored.

    **Kwargs**:
         - `padding` (`str`): padding inserted between the two columns. *default*: `" "`.
         - `max_col` (`int`): Maximum character size for the final string. Column B is placed under column A if the limit is exceeded. *default*: `200000`.

    :param tas: the list of strings or tables to be on the first column.
    :type tas: List[str]
    :param tbs: the list of strings or tables to be on the second column.
    :type tbs: List[str]
    :param headers: pair of headers for each column, follows the indexing of `tas` and `tbs`, defaults to ([], [])
    :type headers: Tuple[List[str], List[str]], optional
    :return: the string with values on two columns.
    :rtype: str
    """

    a = []
    b = []
    l_size = 0
    r_size = 0
    (a_h, b_h) = headers
    padding = _args.get_or_throw(kwargs, "padding", str, " ")
    max_col = _args.get_or_throw(kwargs, "max_col", int, 200000)

    for i in range(len(tas)):
        ta = tas[i] 
        a.append("")
        if i < len(a_h): 
            if a_h[i] != "":
                a.append(a_h[i])
        sta = str(ta).splitlines()
        l_size = max(l_size, len(sta[0]))
        a.extend(sta)

    for i in range(len(tbs)):
        tb = tbs[i] 
        b.append("")
        if i < len(b_h): 
            if b_h[i] != "":
                b.append(b_h[i])
        stb = str(tb).splitlines()
        r_size = max(r_size, len(stb[0]))
        b.extend(stb)

    if l_size + len(padding) + r_size > max_col:
        a.extend(b)
        return '\n'.join(a)

    padding = "    "
    for i in range(max(len(a), len(b))):
        if i >= len(a):
            a.append(" " * l_size + padding + b[i])
        elif i >= len(b):
            pass
        else:
            a[i] = a[i].ljust(l_size) + padding + b[i]

    return '\n'.join(a)

class Table:
    """
    The Table class holds table data and provides methods to generate formatted table with parameterizable formats.
    """
    def __init__(self, columns: int):
        """        
        **__init__ (ctor):** Constructs a new Table object with the specified column count and empty content.


        :param columns: the number of column of the table.
        :type columns: int
        """
        self._table = [] #: List[List[str]]: The table content. *default*: `[]`.
        self._header = [] #: List[str]: The header row. *default*: `[]`.
        self._columns = columns #: int: the number of columns in the table.
        self._exportable = True #: bool: Determines if the table is exportable. Exportable table should not contains UNIX coloration code. *default*: `True`.
        self._title = None #: str: The title of the table. *default*: `None`.

    @classmethod
    def fromData(columns: int, table: ty.List[ty.List[str]], header: ty.List[str]):
        """
        Factory method creating a table with the specified data.

        :param columns: the number of columns of the table.
        :type columns: int
        :param table: the table content
        :type table: List[List[str]]
        :param header: the header row.
        :type header: List[str]
        :return: the created Table object.
        :rtype: Table
        """
        t = Table(columns)
        t.set_header(header)
        t._table = table

        return t

    def __lshift__(self, other):
        """
        Left shift operator overload merging the table with the table `other`. All columns in `other` are placed after self's ones.
        The higher rows count between the two table is kept. If empty cells exists, empty strings are used.

        :param other: the table to be appended to self.
        :type other: Table
        :return: the table merging self and other.
        :rtype: Table
        """
        ret = self

        cols = self.columns() + other.columns()
        lines = max(self.rows(), other.rows())

        for i in range(lines):
            # Self part 
            if i < len(ret._table):
                row = ret._table[i]
            else:
                row = [""] * self.columns()
                ret._table.append(row)
            
            if i < len(other._table):
                row.extend(other._table[i])
            else:
                row.extend([""] * other.columns())

        ret._columns = cols
        ret._header.extend(other._header)
        return ret

    def __ror__(self, other: str) -> str:
        """
        Binary OR operator overload returning a string with two columns where self table is on left column and `other` table (or string) is on the second.

        :param other: the table or string to be on the second column.
        :type other: str
        :return: the string with `self` on the column and `other` on the right.
        :rtype: str
        """
        return column([self], [other])
        
    def columns(self) -> int: 
        """
        Returns the columns count.

        :return: the columns count.
        :rtype: int
        """
        return self._columns

    def rows(self) -> int: 
        """
        Returns the rows count.

        :return: The number of rows in the table.
        :rtype: int
        """
        return len(self._table)

    def add_row(self, row: ty.List[str]):
        """
        Add the specified row to the table.

        :param row: the row data.
        :type row: List[str]
        :return: the table.
        :rtype: Table
        """
        if len(row) != self._columns:
            log.error(f"invalid argument count ({len(row)}), expecting {self._columns}.")
            return None
        self._table.append(row)

        return self

    def set_header(self, header: ty.List[str]):
        """
        Set the header of the table.

        :param header: the header row.
        :type header: List[str]
        :return: the table.
        :rtype: Table
        """
        if len(header) is not self._columns:
            log.error("invalid argument count ({}), expecting {}).".format(len(header), self._columns))
            return None
        self._header = header 

        return self

    def get_tabulate(self, **kwargs) -> str:
        """
        Returns the `tabulate` object from the table.

        **Kwargs**:
         - `include_headers` (`bool`): predicate determining if the header row should be included to the table. *default*: `False`.
         - *forwards kwargs to tabulate*.
        
        :return: the tabulate object from the table.
        :rtype: str
        """
        # Parse kwargs and remove specific keys.
        include_headers = kwargs.pop("include_headers", False)
        
        # Generate
        if include_headers:
            tbl = tabulate(self._table, headers=self._header, **kwargs)
        else:
            tbl = tabulate(self._table, **kwargs)

        return tbl

    def _check_exportable(self):
        """
        Check if the exportable boolean is true and print an error message otherwise.
        """
        if not self._exportable:
            log.warning("exporting the table '" + str(self._title) + " which is marked as non-exportable. Results could contains terminal commands.")

    def exportable(self) -> bool: 
        """
        Returns the exportable attribute.

        :return: `True` if the table is exportable, `False` otherwise.
        :rtype: bool
        """
        return self._exportable

    def __str__(self):
        """
        Convert the table to a tabulate object with header and 'psql' format.

        :return: the tabulate object.
        :rtype: tabulate
        """
        return self.get_tabulate(include_headers=True, tablefmt="psql")

    def save(self, path: str):
        """
        Saves the table a the specified `path`, with header and 'tsv' format.      
        :param path: _description_
        :type path: str
        """
        with open(path, "w+") as file:
            file.write(self.get_tabulate(include_headers=True, tablefmt="tsv"))

    def grid(self):
        """
        Convert the table to a tabulate object with header and 'grid' format.

        :return: the tabulate object.
        :rtype: tabulate
        """
        return self.get_tabulate(include_headers=True, tablefmt="grid")
        
    def latex(self):
        """
        Convert the table to a tabulate object with header and 'latex' format.

        :return: the tabulate object.
        :rtype: tabulate
        """
        return self.get_tabulate(include_headers=True, tablefmt="latex")

    def latex_raw(self):
        """
        Convert the table to a tabulate object with header and 'latex_raw' format.

        :return: the tabulate object.
        :rtype: tabulate
        """
        return self.get_tabulate(include_headers=True, tablefmt="latex_raw")

    def html(self):
        """
        Convert the table to a tabulate object with header and 'html' format.

        :return: the tabulate object.
        :rtype: tabulate
        """
        return self.get_tabulate(include_headers=True, tablefmt="html")
