""" The module *lazart.results.results* provides facilities to manipulate analysis results and displays.

:since: 4.0
:author: Etienne Boespflug 
"""

import typing as ty

import lazart.core.compile as _compile
import lazart.core.analysis as _a
import lazart.core.run as _run
import lazart.core.traces as _tr
import lazart.analysis.attack_analysis as _aa
import lazart.analysis.attack_redundancy as _aar
import lazart.analysis.hotspots_analysis as _hs
import lazart._internal_.extensions as _ext
import lazart.constants as constants
from lazart.results.formats import Table, column
from lazart._internal_.util import str_time_fix
import lazart.logs as log
import lazart._internal_.args as _args

class Metrics(dict):
    """
    The Metrics class is a dictionary associating metrics string IDs to their value.    
    This class also contains metrics string IDs, and related class methods. 
    Metrics IDs are used by several table generation functions and Metrics constitute the holder of all metrics of an analysis.

    String values for KLEE's metrics corresponds to KleeResults's value keys and are used to request gathered values.

    ***Note:*** *for each identifier, the 'type' field in this documentation indicates the type of the metrics associated for the identifier.*
    """
    # Main
    Name = "Name" #: str: The name of the analysis. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    Instructions = "Instructions" #: int: Number of instructions in the LLVM module. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    IPs = "IPs" #: int: Number of Injection Points in the LLVM module. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    Dets = "Dets" #: int: Number of detectors in the LLVM module. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    Branches = "Branches" #: int: Number of branches in the LLVM module. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    BB = "BB" #: int: Number of Basic Blocks in the LLVM module. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    
    # AA
    Attacks = "Attacks" #: List[int]: Number of attacks for each order. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    Minimals = "Minimals" #: List[int]: Number of minimal attacks for each order. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    AttacksTotal = "AttacksTotal" #: int: Total number of attacks. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    MinimalsTotal = "MinimalsTotal" #: int: Total number of minimal attacks. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    Hotspots = "Hotspots" #: List[Dict[Tuple[int, int, int]]]: Raw Hotspots results. *(the 'type' field indicate the type of the metric corresponding to this ID)*

    # AA All 
    Isolated = "Isolated" #: List[int]: Number of isolated attacks for each order. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    IsolatedTotal = "IsolatedTotal" #: int: Total number of isolated attacks. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    Maximals = "Maximals" #: List[int]: Number of maximal attacks for each order. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    MaximalsTotal = "MaximalsTotal" #: int: Total number of maximals attacks. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    Centrals = "Centrals" #: List[int]: Number of central attacks for each order. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    CentralsTotal = "CentralsTotal" #: int: Total number of central attacks. *(the 'type' field indicate the type of the metric corresponding to this ID)*

    # Uniques 
    Uniques = "Uniques" #: List[int]: Number of unique attacks (equivalence classes) for each order. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    UniquesTotal = "UniquesTotal" #: int: Total number of unique attacks (equivalence classes). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    UniquesMin = "UniquesMin" #: List[int]: Number of unique minimal attacks (minimal equivalence classes) for each order. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    UniquesMinTotal = "UniquesMinTotal" #: int: Total number of unique minimal attacks (minimal equivalence classes). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    UniquesIsolated = "UniquesIsolated" #: List[int]: Number of unique isolated attacks (isolated equivalence classes) for each order. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    UniquesIsolatedTotal = "UniquesIsolatedTotal" #: int: Total number of unique isolated attacks (isolated equivalence classes). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    UniquesMaximals = "UniquesMaximals" #: List[int]: Number of unique maximal attacks (maximal equivalence classes) for each order. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    UniquesMaximalsTotal = "UniquesMaximalsTotal" #: int: Total number of unique maximal attacks (maximal equivalence classes). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    UniquesCentrals = "UniquesCentrals" #: List[int]: Number of unique central attacks (central equivalence classes) for each order. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    UniquesCentralsTotal = "UniquesCentralsTotal" #: int: Total number of unique central attacks (central equivalence classes). *(the 'type' field indicate the type of the metric corresponding to this ID)*

    Single = "Single"  #: List[int]: Number of single attacks (1 element equivalence classes) for each order. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    SingleTotal = "SingleTotal" #: int: Total number of single attacks (1 element equivalence classes). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    SingleMin = "SingleMin"  #: List[int]: Number of single minimal attacks (1 element minimal equivalence classes) for each order. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    SingleMinTotal = "SingleMinTotal" #: int: Total number of single minimal attacks (1 element minimal equivalence classes). *(the 'type' field indicate the type of the metric corresponding to this ID)*

    # Coverage
    State = "State" #: str: Determines the state of the analysis, indicating if an interruption occurred. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    IncompleteTraces = "IncompleteTraces" #: int: Total number of incomplete traces (either timeout termination or trace parsing error) among all orders. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    ICov = "ICov" #: str: String indicating the instructions coverage for the analysis (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    BCov = "BCov" #: str: String indicating the basic blocks coverage for the analysis (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    ICovCount = "ICount" #: int: Total number of covered LLVM instructions (reported by KLEE) *(the 'type' field indicate the type of the metric corresponding to this ID)*
    BCovCount = "BCount"  #: int: Total number of covered LLVM basic blocks (reported by KLEE) *(the 'type' field indicate the type of the metric corresponding to this ID)*
    FullBranches = "FullBranches" #: int: Total number of fully explored branches inside LLVM module (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    PartialBranches = "PartialBranches" #: int: Total number of partial explored branches inside LLVM module (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    CompletedPaths = "CompletedPaths" #: int: Total number of completed (fully explored) paths (reported by KLEE). Corresponds to Lazart's traces counts in default configuration. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    PartialPaths = "PartialPaths" #: int: Total number of partially explored paths (reported by KLEE).  *(the 'type' field indicate the type of the metric corresponding to this ID)*
    ExploredPaths = "ExploredPaths"#: int: Total number of completed (fully explored) paths (reported by KLEE) Corresponds to the number of Ktests generated by KLEE and to the sum `PartialPaths + CompletedPaths`. *(the 'type' field indicate the type of the metric corresponding to this ID)*

    # Exec
    Ktests = "Ktests" #: int: Number of Ktests generated by KLEE. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    Traces = "Traces" #: int: Total number of traces parsed by **Lazart**. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    ExecInstr = "Instrs" #: int: Total number of executed LLVM instructions (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    IgnoredTraces = "IgnoredTraces" #: int: Total number of ktests ignored by **Lazart**. *(the 'type' field indicate the type of the metric corresponding to this ID)*

    # Time
    TTotal = "TTotal" #: float: Total execution duration for the analysis. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    TCompile = "TCompile" #: float: Duration for the *preprocessing and compilation step*. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    TTraces = "TTraces"  #: float: Duration for the *traces parsing step*. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    TDSE = "Time(s)" #: float: Duration for the DSE (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    TAA = "TAA" #: float: Duration for the *attack analysis*. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    TAAR = "TAAR"  #: float: Duration for the *equivalence and redundancy analysis*. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    THS = "THS" #: float: Duration for the *hotspots analysis*. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    TCCPO = "TCCPO" #: float: Duration for the *detector optimization analysis*. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    TCCPOClass = "TCCPOClass" #: float: Duration for the *detectors classification analysis*. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    TCCPOSelection = "TCCPOSelection" #: float: Duration for the *detectors selection analysis*. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    TPlacement = "TPlacement" #: float: Duration for the *placement analysis*. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    TAnalysis = "TAnalysis" #: float: Duration for all analysis (for instance `TA+TAAR+THS` for standard attack analysis). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    TResults = "TResults" #: float: Duration for gathering of the Result dictionary from the analysis steps. *(the 'type' field indicate the type of the metric corresponding to this ID)*
    TLazart = "TLazart" #: float: Duration for all steps of **Lazart**, excluding `TResults` (`TCompile + TTraces + TAnalysis`). *(the 'type' field indicate the type of the metric corresponding to this ID)*

    # Klee
    Queries = "Queries" #: number of queries issued to the constraint solver (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    QueryConstructs = "QueryConstructs" #: number of query constructs for all queries send to the constraint solver (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    AvgSolverQuerySize = "AvgSolverQuerySize" #: average number of query constructs per query issued to the constraint solver (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    MaxActiveStates = "MaxActiveStates" #: maximum number of active states (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    AvgActiveStates = "AvgActiveStates" #: average number of active states (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    MaxMem = "MaxMem(MiB)" #: maximum memory usage (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    AvgMem = "AvgMem(MiB)" #: average memory usage (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    QCexCMisses = "QCexCMisses" #: Counterexample cache misses (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    QCexCHits = "QCexCHits" #: Counterexample cache hits (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    
    # TKlee
    TSolver = "TSolver(s)" #: str: Total solver time (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    TSolverPer = "TSolver(%)" #: str: Relative time spent in the solver chain wrt wall time (incl. caches and constraint solver) (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    TUser = "TUser(s)" #: str: Total user time (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    TUserPer = "TUser(%)" #: str: Relative user time wrt wall time (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    TResolve = "TResolve(s)" #: str: Time spent in object resolution (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    TResolvePer = "TResolve(%)" #: str: Relative time spent in object resolution wrt wall time (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    TQuery = "TQuery(s)" #: str: Time spent in the constraint solver (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    TArrayHash = "TArrayHash(s)" #: str: Time spent hashing arrays (if KLEE_ARRAY_DEBUG enabled, otherwise -1) (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    TCex = "TCex(s)" #: str: Time spent in the counterexample caching code (incl. constraint solver) (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    TCexPer = "TCex(%)" #: str: Relative time spent in the counterexample caching code wrt wall time (incl. constraint solver) (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    TFork = "TFork(s)" #: str: Time spent forking states (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*
    TForkPer = "TFork(%)" #: str: Relative time spent forking states wrt wall time (reported by KLEE). *(the 'type' field indicate the type of the metric corresponding to this ID)*    
          
    # Others
    ExpIndexes = "ExpIndexes" #: list[int]: indexes of experimentations (see :class:`lazart.exps.experimentation.Experimentation`). *(the 'type' field indicate the type of the metric corresponding to this ID)*

    # Groups (used for custom behavior depending on values type)
    GROUP_TIME = [TTotal, TAnalysis, TAA, TAAR, THS, TCCPO, TCCPOClass, TCCPOSelection, TPlacement, TLazart, TCompile, TTraces, TResults, TDSE, TArrayHash, TFork, TQuery, TResolve, TSolver, TUser] #: float: Group of all duration values. *(the 'type' field indicate the associated type for each value ID of the group)*
    GROUP_SERIES = [Attacks, Minimals, Isolated, Centrals, Maximals, Uniques, Single, UniquesMin, UniquesCentrals, UniquesIsolated, UniquesMaximals, Uniques, SingleMin] #: int: Group of values that are a list of value for each order. *(the 'type' field indicate the associated type for each value ID of the group)* 
    GROUP_TO_ADD =  [TTotal, TResults, TAnalysis, TAA, TAAR, THS, TCCPO, TCCPOClass, TCCPOSelection, TPlacement] #: float: Group of values that are not always generated and have to be added to the Results dictionary. *(the 'type' field indicate the associated type for each value ID of the group)* 

    # Tables sets
    SET_PROGRAM = [Name, IPs, Dets] #: Set of main static metrics.
    SET_PROGRAM_FULL = [Name, Instructions, Branches, IPs, Dets] #: Set of static metrics.

    SET_EXEC_KLEE = [Queries, QueryConstructs, AvgSolverQuerySize, AvgActiveStates, MaxActiveStates, AvgMem, MaxMem, QCexCHits, QCexCMisses] #: Set of KLEE's specific execution metrics.
    #SET_EXEC_EXT = [Traces, Ktests, ExecInstr ]
    SET_EXEC_FULL = [Traces, PartialPaths, IgnoredTraces, Ktests, CompletedPaths, ExploredPaths, ExecInstr] #: Set of execution metrics.

    SET_COV_FULL = [State, ICov, BCov, IncompleteTraces, FullBranches, PartialBranches] #: Set of coverage metrics.

    SET_TIME = [TTotal, TDSE, TLazart] #: Set of main performance metrics.
    SET_TIME_ATKS = [TAA, TAAR, THS] #: Set of robustness analysis performance metrics.
    SET_TIME_CCPO = [TCCPO, TCCPOClass, TCCPOSelection] #: Set of CCPO-related performance metrics.
    SET_TIME_LAZART = [TLazart, TDSE, TTraces, TAnalysis] #: Set of all Lazart-related performance metrics.
    SET_TIME_LAZART_FULL = [TLazart, TCompile, TDSE, TTraces, TAnalysis, TResults] #: Set of robustness analysis performance metrics.
    SET_TIME_KLEE = [TUser, TResolve, TSolver, TQuery, TCex, TFork, TArrayHash] #: Set of KLEE's performance metrics.
    SET_TIME_KLEE_PERC = [TUserPer, TResolvePer, TSolverPer, TCexPer, TForkPer] #: Set of KLEE's percentage usage metrics.

    SET_TIME_FULL = [TTotal] + SET_TIME_LAZART  #: Set of Lazart's performance metrics including total time.

    SET_ATK_SIMPLE = [Attacks, AttacksTotal] #: Set of attack analysis results without redundancy.
    SET_ATK = [Attacks, Minimals, Uniques, UniquesMin]  #: Set of main attack and redundancy metrics.
    SET_ATK_TOT = [Attacks, AttacksTotal, Minimals, MinimalsTotal, Uniques, UniquesTotal, UniquesMin, UniquesMinTotal] #: Set of main equivalence and redundancy metrics.
    SET_ATK_RED = [Attacks, Minimals, Maximals, Centrals, Isolated, Uniques, UniquesMin, Single] #: Set of all attack and redundancy metrics.
    SET_ATK_FULL = [Attacks, AttacksTotal, Minimals, MinimalsTotal, Maximals, MaximalsTotal, Centrals, CentralsTotal, Isolated, IsolatedTotal, Uniques, UniquesTotal, UniquesMin, UniquesMinTotal, Single, SingleTotal] #: Set of all equivalence and redundancy metrics.

    SET_FULL_NP = SET_EXEC_FULL + SET_COV_FULL + SET_TIME_FULL + SET_ATK_FULL #: Set of all metrics, excluding main set `SET_PROGRAM`.
    SET_FULL = SET_PROGRAM + SET_FULL_NP #: Set of all metrics (used for generation of `res.csv`).

    @classmethod
    def names(cls, res: str) -> ty.Tuple[str, str]:
        """
        Returns the names associated to the metric ID as a pair (short name, name).

        :param res: the metric ID.
        :type res: str
        :return: the pair of (short name, name) for this metric.
        :rtype: Tuple[str, str]
        """
        if res == Metrics.Name: return ("Name", "Program")
        if res == Metrics.Instructions: return ("Ins.", "Instructions")
        if res == Metrics.Branches: return ("BRs", "Branches")
        if res == Metrics.BB: return ("BBs", "Basic Blocs")
        if res == Metrics.IPs: return ("IPs", "IPs")
        if res == Metrics.Dets: return ("Dets", "Detectors")

        if res == Metrics.Attacks: return ("Atks.", "Attacks")
        if res == Metrics.Minimals: return ("Mins.", "Minimals")
        if res == Metrics.AttacksTotal: return ("T.Atks", "Tot. Attacks")
        if res == Metrics.MinimalsTotal: return ("T.Mins", "Tot. Minimals")
        
        if res == Metrics.Hotspots: return ("HS", "Hotspots")

        # Attacks Full
        if res == Metrics.Isolated: return ("Iso.", "Isolated")
        if res == Metrics.Maximals: return ("Maxs.", "Maximals")
        if res == Metrics.Centrals: return ("Cntr.", "Centrals")
        if res == Metrics.Single: return ("Sngl.", "Single")
        if res == Metrics.IsolatedTotal: return ("T.Isol", "Tot. Isolated")
        if res == Metrics.MaximalsTotal: return ("T.Maxs", "Tot. Maximals")
        if res == Metrics.CentralsTotal: return ("T.Cntr", "Tot. Centrals")
        if res == Metrics.SingleTotal: return ("T.Sngl", "Tot. Singles")


        # Uniques
        if res == Metrics.Uniques: return ("Un.", "Uniques")
        if res == Metrics.UniquesMin: return ("Un-M.", "UniquesMin")
        if res == Metrics.UniquesTotal: return ("T.Un.", "Tot. Uniques")
        if res == Metrics.UniquesMinTotal: return ("T.UM.", "Tot. UniquesMin")

        if res == Metrics.UniquesIsolated: return ("Un-I", "UniquesIsolated")
        if res == Metrics.UniquesMaximals: return ("Un-Mx", "UniquesMaximals")
        if res == Metrics.UniquesCentrals: return ("Un-Cn", "UniquesCentrals")
        if res == Metrics.UniquesIsolatedTotal: return ("T.UI.", "Tot. UniquesIsolated")
        if res == Metrics.UniquesMaximalsTotal: return ("T.UMx", "Tot. UniquesMaximals")
        if res == Metrics.UniquesCentralsTotal: return ("T.UC.", "Tot. UniquesCentrals")

        if res == Metrics.Single: return ("Sngl.", "Single")
        if res == Metrics.SingleTotal: return ("T.Sngl", "Tot. Singles")
        if res == Metrics.SingleMin: return ("Si-Min", "SingleMin")
        if res == Metrics.SingleTotal: return ("T.SM.", "Tot. SinglesMin")

        # Coverage
        if res == Metrics.State: return ("S", "State")
        if res == Metrics.IncompleteTraces: return ("ITr", "IncompleteTraces")
        if res == Metrics.ICov: return ("ICov(%)", "ICov(%)")
        if res == Metrics.BCov: return ("BCov(%)", "BCov(%)")
        if res == Metrics.ICovCount: return ("ICov", "ICov")
        if res == Metrics.BCovCount: return ("BCov", "BCov")
        if res == Metrics.FullBranches: return ("FBr", "FullBranches")
        if res == Metrics.PartialBranches: return ("PBr", "PartialBranches")
        if res == Metrics.CompletedPaths: return ("CP", "CompletedPaths")
        if res == Metrics.ExploredPaths: return ("EP", "ExploredPaths")
        if res == Metrics.PartialPaths: return ("PP", "PartialPaths")

        #Perf
        if res == Metrics.Ktests: return ("KT", "Ktests")
        if res == Metrics.Traces: return ("Trs", "Traces")
        if res == Metrics.IgnoredTraces: return ("IgT", "IgnoredTraces")

        if res == Metrics.TTotal: return ("TT", "TTotal")
        if res == Metrics.TCompile: return ("TC", "TCompile")
        if res == Metrics.TTraces: return ("TTr", "TTraces")
        if res == Metrics.TDSE: return ("TDSE", "TDSE")
        if res == Metrics.TAA: return ("TAA", "TAtkAnalysis")
        if res == Metrics.TAAR: return ("TAAR", "TRedEqAnalysis")
        if res == Metrics.THS: return ("THS", "THotspots")
        if res == Metrics.THS: return ("TCCPO", "TCCPO")
        if res == Metrics.THS: return ("TCCPOC", "TCCPOClass")
        if res == Metrics.THS: return ("TCCPOS", "TCCPOSelection")
        if res == Metrics.THS: return ("TPlacement", "TPlacement")

        if res == Metrics.TAnalysis: return ("TA", "TAnalysis")
        if res == Metrics.TLazart: return ("TLz", "TLazart")
        if res == Metrics.TResults: return ("TRes", "TResults")
        if res == Metrics.ExecInstr: return ("EI", "Executed Instructions")

        #TKlee
        if res == Metrics.TSolver: return ("TS", "TSolver")
        if res == Metrics.TSolverPer: return ("TS%", "TSolver(%)")
        if res == Metrics.TUser: return ("TU", "TUser")
        if res == Metrics.TUserPer: return ("TU%", "TUser(%)")
        if res == Metrics.TFork: return ("TF", "TFork")
        if res == Metrics.TForkPer: return ("TF%", "TFork(%)")
        if res == Metrics.TResolve: return ("TR", "TResolve")
        if res == Metrics.TResolvePer: return ("TR%", "TResolve(%)")
        if res == Metrics.TCex: return ("TCex", "TCex")
        if res == Metrics.TCexPer: return ("TCex%", "TCex(%)")
        if res == Metrics.TQuery: return ("TQ", "TQuerys")
        if res == Metrics.TArrayHash: return ("TAH", "TArrayHash")
        
        # Klee
        if res == Metrics.Queries: return ("Q", "Queries")
        if res == Metrics.QueryConstructs: return ("Qc", "QueryConstructs")
        if res == Metrics.AvgSolverQuerySize: return ("AvSQS", "AvgSolverQuerySize")
        if res == Metrics.MaxActiveStates: return ("MaxAS", "MaxActiveStates")
        if res == Metrics.AvgActiveStates: return ("AvAS", "AvgActiveStates")
        if res == Metrics.MaxMem: return ("MaxM", "MaxMem(MiB)")
        if res == Metrics.AvgMem: return ("AvM", "AvgMem(MiB)")
        if res == Metrics.QCexCMisses: return ("CexM", "QCexCMisses")
        if res == Metrics.QCexCHits: return ("CexH", "QCexCHits")

        return ("", "")

    @classmethod
    def name(cls, res: str) -> str:
        """
        Returns the short name associated to the specified metric ID.

        :param res: the requested metric's ID.
        :type res: str
        :return: the short name of the metric.
        :rtype: str
        """
        return Metrics.names(res)[0]
    
    @classmethod
    def full_name(cls, res: str) -> str:
        """
        Returns the full name associated to the specified metric ID.

        :param res: the requested metric's ID.
        :type res: str
        :return: the full name of the metric.
        :rtype: str
        """
        return Metrics.names(res)[1]
    
    # Object results.
    def __init__(self, base: ty.Dict[str, ty.Any] = dict()):
        super().__init__(base)

def get_aa_results(a: _a.Analysis) -> Metrics:
    """
    Returns all the metrics associated to the AttackAnalysis (attack, equivalence/redundancy, hotspots), if available.

    :param a: the analysis for which the metrics will be gathered.
    :type a: Analysis
    :return: the gathered metrics.
    :rtype: Metrics
    """
    has_aar = _ext.has_extension(a, constants.attack_redundancy_ext)

    attacks = []
    minimals = []
    maximals = []
    centrals = []
    isolated = []

    uniques = []
    singles = []
    min_singles = []
    un_minimals = []
    un_maximals = []
    un_centrals = []
    un_isolated = []

    # attacks
    aa = _aa.attacks_results(a)
    
    for i in range(a.max_order() + 1):
        if i == 0:
            attacks.append(0)
        else:
            attacks.append(0 if has_aar else len(aa.results[i]))
        minimals.append(0 if has_aar else None)
        uniques.append(0 if has_aar else None)
        maximals.append(0 if has_aar else None)
        centrals.append(0 if has_aar else None)
        isolated.append(0 if has_aar else None)
        singles.append(0 if has_aar else None)
        min_singles.append(0 if has_aar else None)
        un_minimals.append(0 if has_aar else None)
        un_maximals.append(0 if has_aar else None)
        un_centrals.append(0 if has_aar else None)
        un_isolated.append(0 if has_aar else None)

    uniques_only = []

    # fill redundant
    if has_aar:
        aar = _aar.attacks_redundancy_results(a)

        for key in aar.graph()._nodes:
            node = aar.graph().node(key)
            tr = _tr.get_trace_by_name(a, node._trace_name)

            is_min = not node.redundant
            is_max = len(node._edges) == 0
            is_strictly_unique = node.equivalents is None

            if is_min:
                minimals[tr.order] = minimals[tr.order] + 1
            if is_max:
                maximals[tr.order] = maximals[tr.order] + 1
            if is_min and is_max:
                isolated[tr.order] = isolated[tr.order] + 1
            if not is_min and not is_max:
                centrals[tr.order] = centrals[tr.order] + 1

            if is_strictly_unique:
                uniques_only.append(node)
                singles[tr.order] = singles[tr.order] + 1
                if is_min:
                    min_singles[tr.order] = min_singles[tr.order] + 1

            attacks[tr.order] = attacks[tr.order] + 1
    
        complete_eq_classes = aar.graph()._equivalence_classes + [[t] for t in uniques_only]
        # Loop equivalence groups, and uniques alone
        for eq_class in complete_eq_classes:
            if len(eq_class) < 1:
                log.warning("unexpected empty eq class.")
                continue
            tr = _tr.get_trace_by_name(a, eq_class[0].trace_name())
            if tr is None:
                log.warning("unexpected unknown trace.")
                continue

            node = aar.graph().node(tr.name)

            is_min = not node.redundant
            is_max = len(node._edges) == 0

            if is_min:
                un_minimals[tr.order] = un_minimals[tr.order] + 1
            if is_max:
                un_maximals[tr.order] = un_maximals[tr.order] + 1
            if is_min and is_max:
                un_isolated[tr.order] = un_isolated[tr.order] + 1
            if not is_min and not is_max:
                un_centrals[tr.order] = un_centrals[tr.order] + 1

            

            uniques[tr.order] = uniques[tr.order] + 1
    
        # DEBUG
        for i in range(len(attacks)):
            if i > 0 and attacks[i] != len(aa.results[i]):
                log.warning(f"different attacks counts (aa:{len(aa.results[i])}, aar:{attacks[i]}).")

    return Metrics({
        Metrics.Attacks: attacks,
        Metrics.Minimals: minimals,
        Metrics.Isolated: isolated,
        Metrics.Centrals: centrals,
        Metrics.Maximals: maximals,

        Metrics.Uniques: uniques,
        Metrics.UniquesMin: un_minimals,
        Metrics.UniquesIsolated: un_isolated,
        Metrics.UniquesMaximals: un_maximals,
        Metrics.UniquesCentrals: un_centrals,
        Metrics.UniquesIsolated: un_isolated,
        Metrics.Single: singles,
        Metrics.SingleMin: min_singles,

        Metrics.AttacksTotal: None if attacks[0] is None else sum(attacks),
        Metrics.MinimalsTotal: None if minimals[0] is None else sum(minimals),
        Metrics.IsolatedTotal: None if isolated[0] is None else sum(isolated),
        Metrics.CentralsTotal: None if centrals[0] is None else sum(centrals),
        Metrics.MaximalsTotal: None if maximals[0] is None else sum(maximals),

        Metrics.UniquesTotal: None if uniques[0] is None else sum(uniques),
        Metrics.UniquesMinTotal: None if un_minimals[0] is None else sum(un_minimals),
        Metrics.SingleTotal: None if singles[0] is None else sum(singles),
        Metrics.SingleMinTotal: None if min_singles[0] is None else sum(min_singles),
        Metrics.UniquesIsolatedTotal: None if un_isolated[0] is None else sum(un_isolated),
        Metrics.UniquesCentralsTotal: None if un_centrals[0] is None else sum(un_centrals),
        Metrics.UniquesMaximalsTotal: None if un_maximals[0] is None else sum(un_maximals)
    })

def get_program(a: _a.Analysis) -> Metrics:
    """
    Returns all static metrics and general data.

    :param a: the analysis for which the metrics will be gathered.
    :type a: Analysis
    :return: the gathered metrics.
    :rtype: Metrics
    """
    rr = _run.run_results(a)
    kr = rr._klee_results

    return Metrics({
        Metrics.Name: a.name(),
        Metrics.Instructions: kr.klee_stats.get("ICount", None) if kr.valid else None,
        Metrics.IPs: len(rr._ip_dict),
        Metrics.Branches: kr.klee_stats.get(Metrics.Branches, None) if kr.valid else None,
        Metrics.BB: kr.klee_stats.get(Metrics.BCovCount, None) if kr.valid else None,
        Metrics.Dets: len(rr._det_dict),
        Metrics.ExpIndexes: None if not hasattr(a, "_exp_indexes") else a._exp_indexes
    })

def get_coverage(a: _a.Analysis) -> Metrics:
    """
    Returns all coverage, completeness and correction related metrics.

    :param a: the analysis for which the metrics will be gathered.
    :type a: Analysis
    :return: the gathered metrics.
    :rtype: Metrics
    """
    rr = _run.run_results(a)
    kr = rr._klee_results
    tr = _tr.traces_results(a)

    state = "ok"
    if rr.interrupted():
        state = "int."
    elif tr.interrupted():
        state = "int."

    return Metrics({
        Metrics.State: state,
        Metrics.IncompleteTraces: tr.conclusive_count(),
        Metrics.ICov: kr.klee_stats.get("ICov(%)", None) if kr.valid else None,
        Metrics.BCov: kr.klee_stats.get("BCov(%)", None) if kr.valid else None,
        Metrics.FullBranches: kr.klee_stats.get("FullBranches", None) if kr.valid else None,
        Metrics.PartialBranches: kr.klee_stats.get("PartialBranches", None) if kr.valid else None,
        Metrics.ExploredPaths: kr.explored_paths if kr.valid else None,
        Metrics.PartialPaths: kr.partial_paths if kr.valid else None,
        Metrics.CompletedPaths: kr.completed_paths if kr.valid else None
    })
    
def get_performance(a: _a.Analysis) -> Metrics:
    """
    Returns all performance related metrics.

    :param a: the analysis for which the metrics will be gathered.
    :type a: Analysis
    :return: the gathered metrics.
    :rtype: Metrics
    """
    rr = _run.run_results(a)
    kr = rr._klee_results
    tr = _tr.traces_results(a)
    # todo ccpo etc 

    analysis_duration = float(0)
    aa_duration = -1
    if _ext.has_extension(a, constants.attacks_ext):
        aa_duration = _aa.attacks_results(a).duration()
        analysis_duration = analysis_duration + aa_duration
    aar_duration = -1
    if _ext.has_extension(a, constants.attack_redundancy_ext):
        aar_duration = _aar.attacks_redundancy_results(a).duration()
        analysis_duration = analysis_duration + aar_duration
    hs_duration = -1
    if _ext.has_extension(a, constants.hotspots_ext):
        hs_duration = _hs.hotspots_results(a).duration()
        analysis_duration = analysis_duration + hs_duration
    ccpo_duration = -1
    if _ext.has_extension(a, constants.ccpo_ext):
        ccpo_duration = _hs.hotspots_results(a).duration()
        analysis_duration = analysis_duration + ccpo_duration
    # TODO (CM): separate value for classification and selection ?
    
    total_duration = analysis_duration + tr.duration() + _compile.compile_results(a).duration()

    def get_val(key):
        return kr.klee_stats.get(key, None) if kr.valid else None
    tdse = get_val(Metrics.TDSE)
    if tdse is None and kr.valid:
        tdse = kr.elapsed
    if tdse is None: 
        tdse = 0
    tdse = float(tdse)
    
    ret = Metrics({
        Metrics.Ktests: kr.generated_tests if kr.valid else None,
        Metrics.Traces: sum([len(l) for l in tr._traces]),
        Metrics.IgnoredTraces: tr.ignored(),
        Metrics.TCompile: _compile.compile_results(a).duration(),
        Metrics.TTraces: tr.duration(),
        Metrics.TDSE: tdse,
        Metrics.TAnalysis: analysis_duration,
        Metrics.TLazart: total_duration,
        Metrics.ExecInstr: get_val(Metrics.ExecInstr),
        Metrics.TAA: aa_duration,
        Metrics.TAAR: aar_duration,
        Metrics.THS: hs_duration
    })

    for key in (Metrics.SET_EXEC_KLEE + Metrics.SET_TIME_KLEE):
        ret[key] = kr.klee_stats.get(key, None) if kr.valid else None

    return ret

def get_all_metrics(a: _a.Analysis) -> Metrics:  
    """
    Returns all metrics for the specified analysis.

    TODO: not implemented for CCPO.

    :param a: the analysis for which the metrics will be gathered.
    :type a: Analysis
    :return: the gathered metrics.
    :rtype: Metrics
    """  
    metrics = Metrics()
    metrics.update(get_program(a))
    if a.flags().isset(_a.AnalysisFlag.DetectorOptimization) or a.flags().isset(_a.AnalysisFlag.CMPlacement):
        pass
    else:
        metrics.update(get_aa_results(a))
    metrics.update(get_performance(a))
    metrics.update(get_coverage(a))
    return metrics

def to_string(results: ty.List[Metrics], **kwargs) -> ty.List[Metrics]:
    """
    Returns `results` with all metrics inside being converted to string.

    :param results: the metrics to be converted to string.
    :type results: List[Metrics]
    :return: the modified metrics.
    :rtype: List[Metrics]
    """ 
    for r in results:
        for to_add in Metrics.GROUP_TO_ADD:
            if to_add not in r:
                r[to_add] = None
        for key, item in r.items():

            if key in Metrics.GROUP_TIME and r[key] is not None:
                v = float(r[key]) 
                if v < float(0) and (key not in [Metrics.TArrayHash, Metrics.TAAR]):
                    log.warning(f"unexpected negative duration value '{v}' for '{key}'.")
                    r[key] = "N/A"
                else:
                    r[key] = str_time_fix(v, show_ms=True)

            if item is None:
                # Errors
                r[key] = "err"
            if key == Metrics.Name and item == "":
                r[Metrics.Name] = "unnamed"
            elif key in Metrics.GROUP_SERIES:
                r[key] = ["err" if v is None else str(v) for v in r[key]]
            elif key == Metrics.ExpIndexes:
                pass
            else:
                r[key] = str(r[key])

    return results


def append_result(results: ty.Dict[str, str], header: ty.List[str], rows: ty.List[str], res_type: str, max_order: int, **kwargs):
    """
    Updates `header` and `rows` from a specified Metric dictionary `results` to be used as a Table. 
    `max_order` is used for `GROUP_SERIES` metrics needing one column per order.

    **Kwargs**:
        - `full_name` (`bool`): determines if the full names will be used in header instead of short names. *default*: `False`.

    :param results: the metrics from which the table will be constructed.
    :type results: Dict[str, str]
    :param header: the list of header for each column. Can be `None`.
    :type header: List[str]
    :param row: the list of current 
    :type row: List[str]
    :param res_type: The metric ID or metrics set ID to be added.
    :type res_type: str
    :param max_order: the number of order considered for `GROUP_SERIES` metrics.
    :type max_order: int
    """
    fn = _args.get_or_throw(kwargs, "full_name", bool, False)

    # Series
    if res_type in Metrics.GROUP_SERIES:
        result_list = results[res_type]
        for i in range(max_order + 1):
            if header is not None:
                header.append(f"{i}F")
            rows.append(result_list[i])
    else:
        if header is not None:
            header.append(Metrics.full_name(res_type) if fn else Metrics.name(res_type))
        rows.append(results[res_type])

def append_results(results: ty.Dict[str, str], header: ty.List[str], row: ty.List[str], res_types: ty.List[str], max_order: int, **kwargs):
    """
    Calls :func:`lazart.results.results.append_result` for each metric ID in `res_types`.

    :param results: the metrics from which the table will be constructed.
    :type results: Dict[str, str]
    :param header: the list of header for each column. Can be `None`.
    :type header: List[str]
    :param row: the list of current 
    :type row: List[str]
    :param res_types: The list of metric IDs or metrics set IDs to be added.
    :type res_types: List[str]
    :param max_order: the number of order considered for `GROUP_SERIES` metrics.
    :type max_order: int
    """

    for res_type in res_types:
        append_result(results, header, row, res_type, max_order, **kwargs)

def custom_table(results: ty.List[ty.Dict[str, str]], max_order: int, res_types: ty.List[str], **kwargs) -> Table:
    """
    Returns a Table from the stringified metric dictionaries in the list `results`, for all metric ID in `res_types`. 
    Each metric is on a column and each analysis results in `results` on rows, header table is generated depending on `res_types`.
    `max_order` is used for `GROUP_SERIES` metrics needing one column per order.

    **Kwargs**:
        - `add_index` (`bool`): if true, the index of each analysis (in `results`) is used as first row. *default*: `False`.
        - `add_name` (`bool`): if true, the name of each analysis (in `results`) is used as first or second row (after "ID" if any). *default*: `True`.

    :param results: the list of metrics dictionaries for each analysis, to be used as rows values.
    :type results: List[Dict[str, str]]
    :param max_order: the number of order considered for `GROUP_SERIES` metrics.
    :type max_order: int
    :param res_types: The metric ID or metrics set ID to be added.
    :type res_types: List[str]
    :return: the table of metrics for each analysis.
    :rtype: Table
    """
    header = []
    rows = []

    add_index = _args.get_or_throw(kwargs, "add_index", bool, False)
    add_name = _args.get_or_throw(kwargs, "add_name", bool, True)
    
    if add_index: header.append("ID")
    if add_name: header.append("Name")

    for i in range(len(results)):
        h = header if i == 0 else None
        row = []

        if add_index: row.append(str(i))
        if add_name: row.append(results[i][Metrics.Name])

        append_results(results[i], h, row, res_types, max_order, **kwargs)

        rows.append(row)
        
    tbl = Table(len(header))
    for row in rows: 
        tbl.add_row(row)
    tbl.set_header(header)

    return tbl


## Standard tables
def aar_rows_table(results: ty.List[ty.Dict[str, str]], max_order: int, **kwargs) -> Table:
    """
    Returns the attacks redundancy and equivalence analysis table for the specified analysis.

    **Kwargs**:
        - `add_index` (`bool`): if true, the index of each analysis (in `results`) is used as first row. *default*: `False`.
        - `add_name` (`bool`): if true, the name of each analysis (in `results`) is used as first or second row (after "ID" if any). *default*: `True`.
        - `full` (`bool`): if true, full metrics for equivalence and redundancy classes are displayed. *default*: `True`.

    :param results: the list of metrics dictionaries for each analysis, to be used as rows values.
    :type results: List[Dict[str, str]]
    :param max_order: the number of order considered for `GROUP_SERIES` metrics.
    :type max_order: int
    :return: the table of metrics for each analysis.
    :rtype: Table
    """
    header = []
    rows = []

    has_aar = results[0][Metrics.MinimalsTotal] != "err"
    has_aar_full = results[0][Metrics.MinimalsTotal] != "err"
    force_all = _args.get_or_throw(kwargs, "force_all", bool, False) # determine if only computed value will be used.

    add_index = _args.get_or_throw(kwargs, "add_index", bool, False)
    add_name = _args.get_or_throw(kwargs, "add_name", bool, True)
    full = _args.get_or_throw(kwargs, "full", bool, False)
    if add_index: header.append("ID")
    if add_name: header.append("Name")

    header.extend(["Attacks"] + [str(i) + "F" for i in range(max_order + 1)] + ["Total"])

    for i in range(len(results)):
        res = results[i]
        row = []

        if add_index: row.append(str(i))
        if add_name: row.append(results[i][Metrics.Name])
        empty_row = [""] * len(row)


        row_atk = ["atk:"]
        
        row_atk_eq = ["eq:"]
        row_single = ["sgl:"]
        row_min = ["min:"]
        row_min_eq = ["mineq:"]
        row_min_sgl = ["minsgl:"]
        
        row_max = ["max:"]
        row_max_eq = ["maxeq:"]
        row_centrals = ["cntr:"]
        row_centrals_eq = ["cntreq:"]
        row_iso = ["isol:"]
        row_iso_eq = ["isoleq:"]
        
        append_results(res, None, row_atk, [Metrics.Attacks, Metrics.AttacksTotal], max_order)
        append_results(res, None, row_atk_eq, [Metrics.Uniques, Metrics.UniquesTotal], max_order)
        append_results(res, None, row_single, [Metrics.Single, Metrics.SingleTotal], max_order)
        append_results(res, None, row_min_sgl, [Metrics.SingleMin, Metrics.SingleMinTotal], max_order)
        append_results(res, None, row_min, [Metrics.Minimals, Metrics.MinimalsTotal], max_order)
        append_results(res, None, row_min_eq, [Metrics.UniquesMin, Metrics.UniquesMinTotal], max_order)
        append_results(res, None, row_max, [Metrics.Maximals, Metrics.MaximalsTotal], max_order)
        append_results(res, None, row_centrals, [Metrics.Centrals, Metrics.CentralsTotal], max_order)
        append_results(res, None, row_iso, [Metrics.Isolated, Metrics.IsolatedTotal], max_order)

        append_results(res, None, row_max_eq, [Metrics.UniquesMaximals, Metrics.UniquesMaximalsTotal], max_order)
        append_results(res, None, row_centrals_eq, [Metrics.UniquesCentrals, Metrics.UniquesCentralsTotal], max_order)
        append_results(res, None, row_iso_eq, [Metrics.UniquesIsolated, Metrics.UniquesIsolatedTotal], max_order)

        rows.append(row + row_atk)
        if full:
            if force_all or has_aar:
                rows.append(empty_row + row_atk_eq)
                rows.append(empty_row + row_single)
                rows.append(empty_row + row_min)
                rows.append(empty_row + row_min_eq)
                rows.append(empty_row + row_min_sgl)
                if force_all or has_aar_full:
                    rows.append(empty_row + row_max)
                    rows.append(empty_row + row_max_eq)
                    rows.append(empty_row + row_centrals)
                    rows.append(empty_row + row_centrals_eq)
                    rows.append(empty_row + row_iso)
                    rows.append(empty_row + row_iso_eq)
        else:
            if force_all or has_aar:
                rows.append(empty_row + row_atk_eq)
                rows.append(empty_row + row_min)
                rows.append(empty_row + row_min_eq)
            
    tbl = Table(len(header))
    for row in rows: 
        tbl.add_row(row)
    tbl.set_header(header)

    return tbl

def program_table(results: ty.List[ty.Dict[str, str]], max_order: int, **kwargs) -> Table:
    """
    Generates table for main program and static metrics. See :func:`lazart.results.results.custom_table`.
    """
    return custom_table(results, max_order, Metrics.SET_PROGRAM_FULL, **kwargs)

def time_table(results: ty.List[ty.Dict[str, str]], max_order: int, **kwargs) -> Table:
    """
    Generates table for full performance related metrics. See :func:`lazart.results.results.custom_table`.
    """
    return custom_table(results, max_order, Metrics.SET_TIME_FULL, **kwargs)

def exec_table(results: ty.List[ty.Dict[str, str]], max_order: int, **kwargs) -> Table:
    """
    Generates table for full execution metrics. See :func:`lazart.results.results.custom_table`.
    """
    return custom_table(results, max_order, Metrics.SET_EXEC_FULL, **kwargs)

def cov_table(results: ty.List[ty.Dict[str, str]], max_order: int, **kwargs) -> Table:
    """
    Generates table for full coverage metrics. See :func:`lazart.results.results.custom_table`.
    """
    return custom_table(results, max_order, Metrics.SET_COV_FULL, **kwargs)

def exec_full_table(results: ty.List[ty.Dict[str, str]], max_order: int, **kwargs) -> Table:
    """
    Generates table for full execution metrics, including main program and static metrics. See :func:`lazart.results.results.custom_table`.
    """
    return custom_table(results, max_order, Metrics.SET_PROGRAM_FULL + Metrics.SET_EXEC_FULL + Metrics.SET_COV_FULL, **kwargs)

def full_table(results: ty.List[ty.Dict[str, str]], max_order: int, **kwargs) -> Table:
    """
    Generates table for all metrics metrics, used for results logging. See :func:`lazart.results.results.custom_table`.
    """
    return custom_table(results, max_order, Metrics.SET_PROGRAM_FULL + Metrics.SET_EXEC_FULL + Metrics.SET_COV_FULL + Metrics.SET_TIME_FULL + Metrics.SET_TIME_ATKS + Metrics.SET_TIME_KLEE + Metrics.SET_EXEC_KLEE + Metrics.SET_ATK_FULL, full_name=True, **kwargs)

def full_table_ccpo(results: ty.List[ty.Dict[str, str]], max_order: int, **kwargs) -> Table:
    """
    Generates table for all metrics metrics for CCPO analysis, used for results logging. See :func:`lazart.results.results.custom_table`.
    """
    return custom_table(results, max_order, Metrics.SET_PROGRAM_FULL + Metrics.SET_EXEC_FULL + Metrics.SET_COV_FULL + Metrics.SET_TIME_FULL + Metrics.SET_TIME_KLEE + Metrics.SET_EXEC_KLEE, full_name=True, **kwargs)

def main_table(results: ty.List[ty.Dict[str, str]], mo: int, **kwargs) -> Table:
    """
    Generates the string corresponding to several metrics table printed on two columns.
    Forwards kwargs to :func:`lazart.results.formats.column`.
    """
    
    an = _args.get_or_throw(kwargs, "add_name", bool, False if len(results) == 1 else True)

    tb_prgm = program_table(results, mo, add_name=an)
    tb_exec = exec_table(results, mo, add_name=an)
    tb_time = time_table(results, mo, add_name=an)
    tb_cov = cov_table(results, mo, add_name=an)
    tb_aa = aar_rows_table(results, mo, add_name=an)
    tb_aa_full = aar_rows_table(results, mo, full=True, add_name=an)
    tb_full = full_table(results, mo, add_name=True)
    tb_full = tb_full


    return column([tb_prgm, tb_exec, tb_time, tb_cov, tb_aa], [tb_aa_full], (["Programs:", "Execution:", "Time metrics:", "Coverage:", "Attacks analysis:"], ["Full attacks results"]), padding="       ", **kwargs)

def attack_analysis_htable(analysis: _a.Analysis, metrics: ty.List[Metrics]) -> Table:
    """
    Returns a table with a column for each order and a total column and attacks count for first row and minimal attacks count for second row.

    :param analysis: the analysis from which the results are from.
    :type analysis: _a.Analysis
    :param metrics: the list of stringified metrics for each analysis.
    :type metrics: List[Metrics]
    :return: the horizontal table for robustness analysis.
    :rtype: Table
    """
    tbl = Table(3 + analysis.max_order())
    
    header = ["Order"]
    attack_line = ["attacks"]
    redundant_line = ["non-redundant"]
    for order in range(analysis.max_order() + 1):
        header.append("{}-order".format(str(order)))
        attack_line.append(metrics[0][Metrics.Attacks][order])
        redundant_line.append(metrics[0][Metrics.Minimals][order])
    
    header.append("Total")
    attack_line.append(metrics[0][Metrics.AttacksTotal])
    redundant_line.append(metrics[0][Metrics.MinimalsTotal])

    tbl.set_header(header)
                
    tbl.add_row(attack_line)
    has_aar = _ext.has_extension(analysis, constants.attack_redundancy_ext)
    if has_aar:
        tbl.add_row(redundant_line)

    return tbl
