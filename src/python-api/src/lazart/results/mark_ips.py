"""
TODO() In development module, should not be used.
"""

from lazart.core.analysis import Analysis
import lazart._internal_.args as args
import lazart.logs as log
import os
import lazart.core.run as run

def mark_ips(analysis: Analysis, **kwargs):

    output_folder = args.get_or_throw(kwargs, "output_folder", str, None) # if None, inline.
    comment_str = args.get_or_throw(kwargs, "comment_str", str, "//") # default to C.

    input_files = analysis.input_files()
    files = args.get_or_throw(kwargs, "files", args.ListType(str), input_files) # files

    def find_file(file_name):
        for file in files:
            if file.endswith(file_name): # TODO() can break
                return file
        return None
        
    print("files" + ",".join(files))
    ip_map = dict() #: Dict[str, List[dict]]: map file names to ip data.
    for _, ip in run.run_results(analysis)._ip_dict.items():
        print(f"{id} => {ip}")
        if "loc" in ip:
            if "file" in ip:
                file = find_file(ip["file"])

                if file is not None:
                    if file not in ip_map:
                        ip_map[file] = []
                    ip_map[file].append(ip)

    log.info(f"ip_map = {ip_map}") # TODO

    # Create output folder.
    if output_folder is None: # inline modification.
        log.error("not implemented")
    else:
        for file_name in files:
            if file_name not in ip_map:
                log.debug(f"no ip for {file_name}.")
                return
            
            log.info(f"working on {file_name}")
            new_name = file_name.replace("/", ".").replace("\\", ".")
            new_name = os.path.join(output_folder, new_name)

            try:
                with open(file_name, "r+") as file:
                    lines = file.read().splitlines()
                    print("lines = " + str(lines))

                    for ip in ip_map[file_name]:
                        try:
                            line = int(ip["loc"]["line"])
                            lines[line] = lines[line] + comment_str + f"IP{ip['id']}({ip['type']})"
                        except Exception as e:
                            log.warning(f"cannot add ip {ip['id']} in file {file_name}: {e}")

                    with open(new_name, "w") as file:
                        for l in lines:
                            file.write(l + '\n')

            except FileNotFoundError as e:
                log.warning(f"cannot add ip {ip['id']} in file {file_name}: file not found {e}")
            except Exception as e:
                log.warning(f"cannot add ip {ip['id']} in file {file_name}: {e}")

# TODO() hotspot version ? 