""" The module *lazart.results.report* provides definitions for generating report for an analysis.

:since: 3.1
:author: Etienne Boespflug 
"""

# Python
import datetime
import os
import json

# Lazart
import lazart.constants as _cts

# Internal 
import lazart._internal_.util as util
import lazart._internal_.extensions as _ext

# Core
from lazart.core.analysis import Analysis, AnalysisFlag
import lazart.core.run as _run
import lazart.core.compile as cr
import lazart.core.traces as tr

# Analysis
import lazart.analysis.ccpo as ccpo
from lazart.analysis.ccpo_classification import triggering_points, CCPOClassificationResults
from lazart.analysis.ccpo_selection import find_optimizable_traces, ccpo_selection
from lazart.analysis.attack_analysis import attacks_results
from lazart.analysis.hotspots_analysis import hotspots_results
import lazart.analysis.attack_redundancy as ar
import lazart.results.results as _res
from lazart.results.results import Metrics

# Results
from lazart.results.formats import Table

# Util
import lazart.util.verify as verify
import lazart.results.results as res

def _analysis_content(analysis: Analysis, metrics: Metrics) -> str:
    """
    Returns a text corresponding to the analysis parameters report.

    :param analysis: the analysis to be reported.
    :type analysis: Analysis
    :param metrics: the stringified metrics for the analysis.
    :type metrics: Metrics
    :return: the analysis parameters report.
    :rtype: str
    """
    s = "## Analysis\n\n"

    if analysis.name() != "":
        s += f"Name: *{analysis.name()}*.\n"
    s += f"Path: *{analysis.path()}*.\n"
    s += f"Flags: *{analysis.flags()}*.\n"
    s += f"Order (max faults): *{analysis.max_order()}*.\n"

    s += "\n"
    if isinstance(analysis.attack_model(), str):
        s += f"Attack-model: `{analysis.attack_model()}`.\n"
    else:
        s += f"Attack-model: `{json.dumps(analysis.attack_model(), indent=4, sort_keys=True)}`.\n"
    s += "\n"
    s += "Tasks:\n"
    for name, task in analysis.tasks().items():
        s += " - name: `{task}`.\n"
    s += f" - auto-countermeasures: `{analysis.countermeasures()}`.\n"

    return s + "\n"

def _compilation_content(analysis: Analysis, metrics: Metrics) -> str:
    """
    Returns a text corresponding to the analysis compilation report.

    :param analysis: the analysis to be reported.
    :type analysis: Analysis
    :param metrics: the stringified metrics for the analysis.
    :type metrics: Metrics
    :return: analysis compilation report.
    :rtype: str
    """
    if not _ext.has_extension(analysis, _cts.compile_ext): return ""

    s = "## Compilation Information\n\n"

    c = cr.compile_results(analysis)

    s += f"Compiled at: `{c.date()}`.\n"
    s += f"Duration: `{util.str_time(c.duration())}`.\n"    
    s += f"Input files: {', '.join([str('`' + os.path.basename(f) + '`') for f in analysis.input_files()])}\n"
    s += f"Compiler: `{_cts.compiler_cmd}`.\n - args: `{analysis.compiler_args()}`.\n"
    s += f"Linker: `{_cts.linker_cmd}`.\n - args: `{analysis.linker_args()}`.\n"
    s += f"Disassembler: `{_cts.dis_cmd}`.\n - args: `{analysis.dis_args()}`.\n"

    return s + "\n"

def _run_content(analysis: Analysis, metrics: Metrics) -> str:
    """
    Returns a text corresponding to the analysis run (mutation & DSE) report.
    
    :param analysis: the analysis to be reported.
    :type analysis: Analysis
    :param metrics: the stringified metrics for the analysis.
    :type metrics: Metrics
    :return: analysis run report.
    :rtype: str
    """
    if not _ext.has_extension(analysis, _cts.run_ext): return ""

    rr = _run.run_results(analysis)
    s = "## DSE Information\n\n"

    s += f"Run at {rr._date}\n"
    s += f"Wolverine args: {analysis.wolverine_args()}.\n"
    s += f"DSE Engine: Klee (args: {analysis.klee_args()})\n"

    s += "\n"
    exec_tbl = res.custom_table(metrics, analysis.max_order, Metrics.SET_PROGRAM_FULL + Metrics.SET_EXEC_FULL + Metrics.SET_COV_FULL, full_name=True, add_name=False)
    s += "Execution metrics:\n " + str(exec_tbl) + "\n"
    
    # Data table
    if rr.valid():
        wng = rr._klee_results.warnings
        s += "\n\n"
        if wng != "":
            s += "***Warnings:*** *" + str(wng) + "*\n\n"

    else:
        s += "Corrupted Klee's report.\n"

    return s

def _attack_analysis_content(analysis: Analysis, metrics: Metrics) -> str:
    """
    Returns a text corresponding to the attack analysis report.
    
    :param analysis: the analysis to be reported.
    :type analysis: Analysis
    :param metrics: the stringified metrics for the analysis.
    :type metrics: Metrics
    :return: attack analysis report.
    :rtype: str
    """
    if not _ext.has_extension(analysis, _cts.attacks_ext): return "\n"

    s = "## Attack Analysis\n\n"
    # Tests 
    s += ""
    
    l = []
    for trace in tr.traces_list(analysis, 0):
        if trace.termination_is(tr.TerminationType.Correct):
            l.append(trace)
    s += " - 0-faults attacks: " + str(len(l)) + "\n"

    suspicious = []

    for trace in l:
        if trace.termination_is(tr.TerminationType.Correct):
            suspicious.append(trace)

    for trace in suspicious:
        s += "    - " + trace.name + ": " + str(trace.termination_type) + "\n"

    s += "\n"

    # Attack
    s += "### Attacks\n\n" + str(res.attack_analysis_htable(analysis, metrics)) + "\n\n"    

    return s

def _aar_content(analysis: Analysis, metrics: Metrics) -> str:
    """
    Returns a text corresponding to the attack equivalence and redundancy report.
    
    :param analysis: the analysis to be reported.
    :type analysis: Analysis
    :param metrics: the stringified metrics for the analysis.
    :type metrics: Metrics
    :return: attack equivalence and redundancy analysis report.
    :rtype: str
    """
    if not _ext.has_extension(analysis, _cts.attack_redundancy_ext): return ""

    s = "#### Attack Redundancy Analysis\n\n"
    s += ar.attacks_redundancy_results(analysis).table().get_tabulate(include_headers=True, tablefmt="simple") + "\n\n"
    
    return s


def _hotspot_content(analysis: Analysis, metrics: Metrics) -> str:
    """
    Returns a text corresponding to the hotspots analysis report.
    
    :param analysis: the analysis to be reported.
    :type analysis: Analysis
    :param metrics: the stringified metrics for the analysis.
    :type metrics: Metrics
    :return: attack hotspots analysis report.
    :rtype: str
    """
    if not _ext.has_extension(analysis, _cts.hotspots_ext): return ""
    
    s = "### Hotspots Analysis\n\n"
    s += hotspots_results(analysis).table().get_tabulate(include_headers=True, tablefmt="simple") + "\n\n"

    return s

def _ccpa_results_table(ccpa_r: CCPOClassificationResults, analysis) -> str:
    """    
    Returns a text corresponding to the CCPO results table..

    :param ccpa_r: result object for CCPO classification.
    :type ccpa_r: CCPOClassificationResults
    :param analysis: the analysis to be reported.
    :type analysis: Analysis
    :return: ccpo results table report.
    :rtype: str
    """
    header = ["Fault limit", "Total CCP", "Inactive", "Repetitive", "Removable", "Removed", "Ratio"]

    data_table = Table(len(header))
    data_table.set_header(header)

    ccps = triggering_points(analysis)
    total_ccp = len(ccps)

    # values

    def add_order_values(order: int):
        inactive = len(ccpa_r.inactives(order))
        repetitive = len(ccpa_r.repetitives(order))
        removable = inactive + repetitive

        optimizable = find_optimizable_traces(analysis, ccpa_r.repetitives(order), max_order=order)
        
        if total_ccp == 0:
            percent = 0
        else:
            percent = 100.0 / float(total_ccp)  * float(removable)

        if len(optimizable) == 0:  
            removed = str(removable)
            ratio = "{:.2f}".format(percent) + "%"
        else:
            removed = "< " + str(removable)
            ratio = "{:.2f}".format(percent) + "%"
        
        row = [str(order) + "-order", str(total_ccp), str(inactive), str(repetitive), str(removable), str(removed), str(ratio)]

        data_table.add_row(row)
    
    for i in range(1, analysis.max_order() + 1):
        add_order_values(i)

    return data_table

def _metrics_table(analysis: Analysis, results: dict) -> str:
    """
    Returns a text corresponding to performance and execution metrics report.
    
    :param analysis: the analysis to be reported.
    :type analysis: Analysis
    :param metrics: the stringified metrics for the analysis.
    :type metrics: Metrics
    :return: attack hotspots analysis report.
    :rtype: str
    """
    data_table = Table(3+ analysis.max_order())
    # header
    header = ["Fault limit"]
    for order in range(analysis.max_order() + 1):
        header.append("{}-order".format(str(order)))
    header.append("Total")

    data_table.set_header(header)

    # values
    rr = _run.run_results(analysis)

    if rr.valid:
        def add_value(msg, key):
            row = [msg]
            for order in range(analysis.max_order() + 1):
                row.append("N/A")
                
            row.append(rr._klee_results.klee_value(key))

            data_table.add_row(row)

        add_value("Explored paths", "explored_paths")
        add_value("Total instructions", "total_instructions")
        add_value("Generated tests", "generated_tests")
    
    # Valid traces.
    row = ["Correct traces"]    
    total = 0
    for order in range(analysis.max_order() + 1): 
        v = 0
        for trace in tr.traces_list(analysis, order):
            if trace.termination_is(tr.TerminationType.Correct):
                v = v + 1
        row.append(str(v))
        total += v
    row.append(str(total))
    data_table.add_row(row)

    # DSE Duration
    if rr.valid:
        row = ["DSE duration"]
        for order in range(analysis.max_order() + 1):
            row.append("N/A")
        row.append(util.str_time(rr._klee_results.elapsed) if rr.valid() else "nodata")
        data_table.add_row(row)

    # Trace parsing (2 modes)
    row = ["Trace parsing duration"]    
    for order in range(analysis.max_order() + 1):
        row.append("N/A")
    row.append(util.str_time(tr.traces_results(analysis).duration()))
    data_table.add_row(row)
    
    if analysis.flags().isset(AnalysisFlag.AttackAnalysisOnly):
        row = ["Attacks Analysis duration"]
        total = float(0)
        for order in range(analysis.max_order() + 1):
            row.append("N/A")
        row.append(util.str_time(attacks_results(analysis).duration()))
        data_table.add_row(row)

    if _ext.has_extension(analysis, _cts.attack_redundancy_ext):
        row = ["Redundancy Analysis duration"]
        total = float(0)
        for order in range(analysis.max_order() + 1):
            row.append("N/A")
        row.append(util.str_time(ar.attacks_redundancy_results(analysis).duration()))
        data_table.add_row(row)

    if analysis.flags().isset(AnalysisFlag.HSAnalysisOnly):
        row = ["Hotspots Analysis duration"]
        total = float(0)
        for order in range(analysis.max_order() + 1):
            row.append("N/A")
        row.append(util.str_time(hotspots_results(analysis).duration()))
        data_table.add_row(row)

    if analysis.flags().isset(AnalysisFlag.DetectorOptimization):
        row = ["CCPO classification"]
        total = float(0)
        for order in range(analysis.max_order() + 1):
            row.append("N/A")
        row.append(util.str_time(ccpo.ccpo(analysis).duration()))
        data_table.add_row(row)

    return data_table

def _ccpo_content(analysis: Analysis) -> str:
    """
    Generates *CCPO* report content from the specified analysis.

    :param analysis: the analysis to be reported.
    :type analysis: Analysis
    :return: the generated report string.
    :rtype: str
    """
    if not _ext.has_extension(analysis, _cts.ccpo_ext): return ""

    s = "## CCP Optimization\n\n"

    ccps = triggering_points(analysis)
    s += "CCPs: " + ','.join(ccps) + "\n\n"
    ccpa_r = ccpo.ccpo(analysis).classification
    s += "### Local conclusions\n\n"
    s += str(ccpa_r.local_conclusions_table(exportable=True)) + "\n\n"
    s += "### Conclusions\n\n"
    s += str(ccpa_r.conclusions_table(exportable=True)) + "\n\n"
    
    # Results table.
    s += "### Results \n\n"

    for i in range(1, analysis.max_order() + 1):
        optimizables = find_optimizable_traces(analysis, ccpa_r.repetitives(i), max_order=i)

        if len(optimizables) == 0:
            s += "No optimizable traces in " + str(i) + "-order.\n"
        else:
            s += str(len(optimizables)) + " optimizable trace" + str("" if len(optimizables) == 1 else "s") + " in " + str(i) + "-order :\n"
            for trace in optimizables:
                triggered = trace.triggered()
                str_l = []
                for ccp in triggered:
                    str_l.append(ccp.ccp)
                s += " - " + trace.name + ": " + "[" + str(trace.termination_type) + "]: " + ",".join(str_l) + "\n"

    s += "\n"

    s += _ccpa_results_table(ccpa_r, analysis).get_tabulate(include_headers=True, tablefmt="simple") + "\n\n"
    
    return s

def _metrics_content(analysis: Analysis, metrics: Metrics) -> str:
    """
    Generates *CCPO* report content from the specified analysis.

    :param analysis: the analysis to be reported.
    :type analysis: Analysis
    :param metrics: the stringified metrics for the analysis.
    :type metrics: Metrics
    :return: the generated report string.
    :rtype: str
    """
    s = "### Metrics\n\n"

    s += _metrics_table(analysis, metrics).get_tabulate(include_headers=True, tablefmt="simple") + "\n\n"

    return s

def generate_report(path: str, analysis: Analysis, results: Metrics):
    """
    Generates the *report.md* file for the specified analysis at the specified path.

    The analysis flag is used to determines which content will be generated.

    :param path: the path in which the 'report.md' file will be saved.
    :type path: str
    :param analysis: the analysis to be reported.
    :type analysis: Analysis
    :param metrics: the stringified metrics for the analysis.
    :type metrics: Metrics
    """
    with open(path, "w+") as file:
        file.write("# Lazart report\n\n")

        file.write("Using Lazart version " + _cts.version_string + ".\n")
        file.write("Reported at *" + str(datetime.datetime.now()) + "*.\n\n")

        def write_section(s: str): 
            if s != "":
                file.write(s.encode("ascii", "ignore").decode("ascii"))

        write_section(_analysis_content(analysis, results))
        write_section(_compilation_content(analysis, results))
        write_section(_run_content(analysis, results))
        write_section(_metrics_content(analysis, results))
        
        write_section(_attack_analysis_content(analysis, results))
        write_section(_aar_content(analysis, results))
        write_section(_hotspot_content(analysis, results))
        write_section(_ccpo_content(analysis))
