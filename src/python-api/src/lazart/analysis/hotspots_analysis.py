""" The module *lazart.analysis.hotspot_analysis* contains definitions for *hotspot analysis*.

:since: 3.0
:author: Etienne Boespflug 
"""

# Python
import time
import typing as ty
from lazart.core.run import run_results

# Lazart
import lazart.logs as log
import lazart.constants as constants

# Internal
from lazart._internal_.util import str_time as _str_time
import lazart._internal_.extensions as _ext
import lazart._internal_.args as _args
import lazart._internal_.folder as _folder

# Core
from lazart.core.traces import Trace, traces_list
from lazart.core.analysis import Analysis

# Results
from lazart.results.formats import Table
 
class HotspotResults:
    """
    The HotspotResults class holds results of the *hotspot analysis*.
    
    *Hotspot analysis* focuses on how many time Injection Points has been triggered for each order.
    """
    def __init__(self, analysis: Analysis, results: ty.List[ty.Dict[str, ty.Tuple[int, int, int]]], duration, **kwargs): 
        """
        **__init__ (ctor):**  Constructs a new HotspotResults object with an analysis, the raw results and the duration information.

        :param analysis: the Analysis object from which the traces will be read.
        :type analysis: Analysis
        :param results: Hotspot data for each injection point for each order.
        :type results: List[Dict[str, Tuple[int, int, int]]]
        :param duration: duration of the analysis computing in millisecond.
        :type duration: float
        """
        self._analysis = analysis #: Analysis: The analysis object to which the *extension* is associated. See :func:`~lazart._internal_.extensions.register`.

        self.results = results
        """ List[Dict[str, Tuple[int, int, int]]]: The list, for each order, of dictionaries of information for each **Injection Point** (IP) by name.

        Each value in dictionary are a tuple (fc, tc, mc) corresponding to:
         - fc (int): number of attack traces in which the IP is faulted.
         - tc (int): total number of time the IP is faulted among all traces (take into account multiple triggering inside a same trace).
         - mc (int): maximum of triggers of the IP in one single trace among all traces. 
        """ 
        self._duration = duration #: float: The duration of the *attack analysis*.

    def duration(self) -> float:
        """
        Returns the duration in second of the hotspot analysis computations.

        :return: duration of the analysis computing in second.
        :rtype: float
        """
        return self._duration

    def __str__(self) -> str:
        """
        Returns the string representing the HotspotResults.

        :return: the string representation of the object.
        :rtype: str
        """      
        ret = "Hotspot results: {}\n".format(self._analysis.name())
        ret += "  - execution-time: {}\n".format(_str_time(self._duration))
        for order in range(self._analysis.max_order() + 1):
            ret = ret + "  - {}-order:\n".format(order)
            for key in self.results[order]:
                (total_count, trace_count, max_concurrent) = self.results[order][key]
                ret = ret + "      - '{}': {} (on {} traces); mc = {}.\n".format(key, str(total_count), str(trace_count), max_concurrent)
        return ret

    def all_injection_points(self) -> ty.Set[str]:
        """
        Returns the set of all IPs in the analysis.

        :return: the set of IPs.
        :rtype: Set[str]
        """
        keys = set()
        for order in range(self._analysis.max_order() + 1):
            for item in self.results[order]:
                keys.add(item)

        return keys

    def reverse_dict(self) -> ty.Dict[str, ty.List[ty.Tuple[str, str, str]]]:
        """
        Returns a dictionary associating for each IP, the list of tuples (fc, tc, mc) for each order.

        :return: the dictionary associating each IP to its hotspots results for each order.
        :rtype: Dict[str, List[Tuple[str, str, str]]]
        """
        d = dict()
        for key in self.all_injection_points():
            row = []
            for order in range(self._analysis.max_order() + 1):
                if key not in self.results[order]:
                    row.append((0, 0, 0))
                else:
                    row.append(self.results[order][key])
            d[key] = row
        return d

    def table(self, title = None):
        """ Returns the formatted table of HotspotAnalysis results.

        :param title: title of the table, defaults to None
        :type title: str, optional
        :return: the formatted table of the results.
        :rtype: lazart.util.Table
        """
        tbl = Table(3 + self._analysis.max_order())
        tbl._title = title

        # header
        header = ["Order\nIP", "Info"]
        for order in range(self._analysis.max_order() + 1):
            header.append("{}-order".format(str(order)))

        tbl.set_header(header)

        # hotspots
        rows = dict()

        rr = run_results(self._analysis)

        for order in range(self._analysis.max_order() + 1):
            for key in self.results[order]:
                if not key in rows:
                    ip_data = rr.get_ip_data(key)
                    if ip_data:
                        info_str = "{} [l:{}, bb:{}]".format(ip_data["type"], ip_data["loc"]["line"], ip_data["loc"]["basic_block"])
                    else:
                        info_str = "{}".format("nodata")
                    rows[key] = [key, info_str]
                    for _o in range(self._analysis.max_order() + 1): rows[key].append(0)
                (total_count, _, mc) = self.results[order][key]
                rows[key][order + 2] = str(total_count) + " (mc:{})".format(mc)


        # Optional ?
        for ip in rr._ip_dict:
            if ip not in rows:
                ip_data = rr.get_ip_data(ip)
                if ip_data:
                    info_str = "{} [l:{}, bb:{}]".format(ip_data["type"], ip_data["loc"]["line"], ip_data["loc"]["basic_block"])
                else:
                    info_str = "{}".format("nodata")
                rows[ip] = [ip, info_str]
                for _o in range(self._analysis.max_order() + 1): rows[ip].append(0)

        for key, row in sorted(rows.items()):
            tbl.add_row(row)

        return tbl

def _compute_mc(trace: Trace) -> ty.Dict[str, int]:
    """ 
    Returns a dictionary containing the number of triggering of each ip in `trace`.

    :param trace: a trace to compute.
    :type trace: Trace
    :return: a dict representing the 'max_concurrent' value for each IP in the trace.
    :rtype: Dict[str, int]
    """
    d = dict()

    for fault in trace.faults():
        ip = fault.injection_point
        if ip not in d:
            d[ip] = 1
        else:
            d[ip] = d[ip] + 1

    return d

def hotspots_analysis(analysis: Analysis, **kwargs) -> HotspotResults:
    """ Returns an AttackResults from an analysis object performing *Attacks
    Analysis*.

    Requires that the *traces parsing step* (see :func:`~lazart.core.traces.traces_results`) has been computed for this `analysis`.

    This function uses log to display progress, reduce verbosity level to reduce output.

    **Kwargs**:
     - `satisfies_fct` (`Callable[[Trace], bool]`): boolean predicate determining if a trace should be considered. *default*: `lambda trace: trace.satisfies()`.

    :param analysis: the Analysis object from which the traces will be read.
    :type analysis: Analysis
    :return: the computed hotspots results.
    :rtype: HotspotResults
    """
    results = []

    s_fct = _args.get_satisfies_function(kwargs, lambda trace: trace.satisfies())

    log.info("computing hotspots results."); 
    start = time.process_time()

    for order in range(analysis.max_order() + 1):
        log.fverbose("    order {}...".format(order))
        results.append(dict())
        for trace in traces_list(analysis, order):
            if s_fct(trace):
                d = _compute_mc(trace)

                for ip in d:
                    max_concurrent = d[ip]
                    if ip not in results[order]:
                        results[order][ip] = (0, 0, 0)
                    
                    (fc, tc, mc) = results[order][ip]

                    results[order][ip] = (fc + max_concurrent, tc + 1, max(mc, max_concurrent))

        log.rverbose(" done.\n") 
    end = time.process_time()
    duration = end - start
    log.info("hotspot analysis ended in {}.".format(_str_time(duration)))

    return HotspotResults(analysis, results, duration)


class HotspotsStep(_ext.AnalysisStep):
    """
    Subclass of HotspotsStep for the *hotspots analysis*, see :class:`lazart._internal_.extensions.AnalysisStep`.
    """
    def __init__(self, analysis):  
        """
        **__init__ (ctor):** Constructs a new HotspotsStep object associated to the specified analysis.

        :param analysis: the analysis of the step.
        :type analysis: Analysis
        """                  
        super(HotspotsStep, self).__init__(analysis, constants.hotspots_ext,  _ext.StepID.Analysis)

    def execute(self, **kwargs):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        return hotspots_analysis(self.analysis, **kwargs)

    def init(self):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        pass

    def save_disk(self):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        return super().save_disk()
        
    def clean(self):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        _folder.remove_file(_folder.lazart_file(self.analysis.path(), constants.hotspots_ext))

def hotspots_results(analysis: Analysis, **kwargs) -> HotspotResults:
    """ 
    Returns an HotspotResults from an analysis object performing *Hotspot
    Analysis*.

    Requires that the *traces* has been computed for this `analysis`.
    
    Uses extension (hotspots_ext, see :class:`lazart._internal_.extensions.AnalysisStep`) system to reuse disk/RAM cache if available.
    This function uses log to display progress, reduce verbosity level to reduce output.

    **Kwargs**:
     - `satisfies_fct` (`Callable[[Trace], bool]`): boolean predicate determining if a trace should be considered. *default*: `lambda trace: trace.satisfies()`.

    :param analysis: the Analysis object from which the traces will be read.
    :type analysis: Analysis
    :return: the computed hotspot results.
    :rtype: HotspotResults
    """

    step = _ext.get_step(HotspotsStep(analysis))
    
    return step.get_results(**kwargs)