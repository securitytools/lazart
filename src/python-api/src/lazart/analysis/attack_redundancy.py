""" 
The module *lazart.analysis.attack_redundancy* contains definitions for *attack equivalence-redundancy analysis*.

Fixing non-redundant attacks should fix corresponding relative attacks (or at least make them harder), depending on the chosen definition and *fault model*.

:since: 3.0
:author: Etienne Boespflug 
"""
# Python
import time
import typing as ty

# Lazart
import lazart.constants as constants
import lazart.logs as log
import lazart.options as options

# Core
from lazart.core.analysis import Analysis
from lazart.core.traces import BasicBlock, Fault, Trace, traces_list, get_trace_by_name

# Internal 
import lazart._internal_.extensions as _ext
from lazart._internal_.util import str_time as _str_time
import lazart._internal_.args as args
import lazart._internal_.folder as _folder

# Results
from lazart.results.formats import Table

# Graph Model

class AttackRedundancyNode:
    """
    AttackRedundancyNode represents a node of the *attack redundancy graph*.

    It holds the set of trace-nodes redundant to its trace.

    Each node is associated to an attack trace, identified by name.
    
    """
    def __init__(self, trace_name: str):
        """
        **__init__ (ctor):**  Constructs a AttackRedundancyNode for the trace identified by `trace_name`.

        :param trace_name: the name of the trace corresponding to this node.
        :type trace_name: str
        """
        self.redundant = False #: bool: Determines if the trace is redundant from at least another one.
        self.equivalents = None #: List[AttackRedundancyNode]: The set of all trace that are equivalent with this node.
        self._edges = set() # Set[AttackRedundancyNode]: The set of all traces that are redundant to this one.
        self._trace_name = trace_name #: str: The name of the trace corresponding to this node.
        self._cached_trace = None #: Trace: the trace associated to the node.

    def get_trace(self, analysis: Analysis) -> Trace:
        """
        Returns the trace corresponding to the node, cached on first request.

        :param analysis: the analysis associated to the trace.
        :type analysis: Analysis
        :return: the trace corresponding to the node.
        :rtype: Trace
        """
        if self._cached_trace is None:
            self._cached_trace = get_trace_by_name(analysis, self.trace_name())
        return self._cached_trace

    def trace_name(self) -> str:
        """
        The name of the trace corresponding to this node.

        :return: the name of the associated trace.
        :rtype: str
        """
        return self._trace_name

    def add_edge(self, redundant) -> bool:
        """
        Adds a node corresponding to a redundant attack to the set of edges (redundant attacks).

        :param redundant: The node corresponding to the redundant trace.
        :type redundant: AttackRedundancyNode
        :return: Returns True if the edge was added, False otherwise (duplicate).
        :rtype: bool
        """
        if redundant in self._edges:
            log.warning("the node {} already exists in {}'s redundant list.".format(redundant.trace_name(), self.trace_name()))
            return False
        self._edges.add(redundant)
        return True

    def have_edge(self, node) -> bool:
        """
        Check if this `node` is in the list of redundant traces.

        :param node: the node to be checked.
        :type node: AttackRedundancyNode
        :return: Returns `True` if this node is in the list of redundant traces, `False` otherwise.
        :rtype: bool
        """
        return node in self._edges

    def __str__(self) -> str:  
        """
        Returns the string representing the AttackRedundancyNode.

        :return: the string representation of the object.
        :rtype: str
        """            
        ret = "[{}, {}, {}]".format(self._trace_name, "redundant" if self.redundant else "not redundant", str(len(self._edges)))
        return ret

class AttackRedundancyGraph:
    """
    The AttackRedundancyGraph class represents a graph of redundancy and equivalence for attack traces.

    It contains a set of :class:`AttackRedundancyNode` containing list of redundant (`edge`) trace nodes and the associated equivalence class.

    """
    def __init__(self):
        """
        **__init__ (ctor):**  Constructs a new empty redundancy graph.
        """
        self._nodes = dict() # dictionary of all node of the graph.
        self._equivalence_classes = [] #: List[AttackRedundancyNode]: list of equivalence classes, containing list of equivalent nodes.

    def add_node(self, trace_name: str, node: AttackRedundancyNode):
        """
        Adds a node to the graph with the specified `trace_name`.

        :param trace_name: the name of the trace corresponding to the node.
        :type trace_name: str
        :param node: the node object to add.
        :type node: AttackRedundancyNode
        """
        if trace_name in self._nodes:
            log.error("the attack {} is already added.")
            return
        
        self._nodes[trace_name] = node

    def node(self, trace_name: str) -> AttackRedundancyNode:
        """
        Returns the node corresponding to the trace name. Returns `None` if the node doesn't exist.

        :param trace_name: the name of the trace of the node.
        :type trace_name: str
        :return: the node corresponding to the trace name, `None` other wise.
        :rtype: AttackRedundancyNode
        """
        if trace_name in self._nodes:
            return self._nodes[trace_name]
        return None

    def make_or_get(self, trace_name: str) -> AttackRedundancyNode:
        """
        Returns the node corresponding to the trace name if it exists, otherwise creates and returns a new node for this trace name.

        :param trace_name: the name of the trace of the node.
        :type trace_name: str
        :return: the node corresponding to the trace name.
        :rtype: AttackRedundancyNode
        """
        if trace_name in self._nodes:
            return self._nodes[trace_name]
        node = AttackRedundancyNode(trace_name)
        self._nodes[trace_name] = node
        return node

    def add_edge(self, base: AttackRedundancyNode, redundant: AttackRedundancyNode):
        """
        Adds a edge in the `base` node for the `redundant` trace node.

        :param base: the node corresponding to the trace to which `redundant` is redundant.
        :type base: AttackRedundancyNode
        :param redundant: the node corresponding to the trace which is redundant to `base`.
        :type redundant: AttackRedundancyNode
        """
        if not base.add_edge(redundant):
            return
        redundant.redundant = True

    def equivalent(self, a: AttackRedundancyNode, b: AttackRedundancyNode) -> ty.List[AttackRedundancyNode]:
        """
        Set `a` and `b` as equivalent traces, creating a new equivalent class if required.

        :param a: the first attack node.
        :type a: AttackRedundancyNode
        :param b: the second attack node.
        :type b: AttackRedundancyNode
        :return: the equivalence class of attacks as a list of nodes.
        :rtype: List[AttackRedundancyNode]
        """
        eq_class = None
        
        if a.equivalents is not None and b.equivalents is not None:
            if a.equivalents != b.equivalents:
                log.error("Invalid state, both {} and {} are in separate equivalence class and cannot be equivalent".format(a.trace_name(), b.trace_name()))
        elif a.equivalents is not None:
            a.equivalents.append(b)
            b.equivalents = a.equivalents
        elif b.equivalents is not None:
            b.equivalents.append(a)
            a.equivalents = b.equivalents
        else:
            eq_class = [a, b]
            self._equivalence_classes.append(eq_class)
            a.equivalents = eq_class
            b.equivalents = eq_class
        
        return a.equivalents

    def redundant_by(self, node: AttackRedundancyNode) -> ty.List[AttackRedundancyNode]:
        """
        Returns the list of redundant trace nodes for the specified `node`.

        `O(n)` complexity, as it need to search among all node wich one contains `node` as edge.

        :param node: the node for which the redundant nodes will be searched.
        :type node: AttackRedundancyNode
        :return: the list of all nodes redundant to `node`.
        :rtype: List[AttackRedundancyNode]
        """
        ret = []
        if not node.redundant:
            return ret
        for key in self._nodes:
            src = self._nodes[key]
            if src.have_edge(node):
                ret.append(src)
        return ret
        
# Predicates and algorithms

def prefix_rule(a: Trace, b: Trace):
    """
    Verifies if the trace `b` is **redundant** to `a` with respect to "SubWord" definition.

    **Prefix redundancy definition**: an attack A2 is *redundant* to an attack A1 if the the ordered set of fault of A2 is a prefix of the ordered set of faults of A1.
    
    example: `{A1 = [F1, F2, F3], A2 = [F1, F2]}`.

    :param a: the base trace. 
    :type a: Trace
    :param b: the trace to check to be redundant.
    :type b: Trace
    :return: `True` if `b` is redundant to `a`, `False` otherwise.
    :rtype: bool
    """
    if a == b:
        return False
    if(a.order >= b.order):
        return False

    a_faults = a.faults()
    b_faults = b.faults()
    
    for i in range(len(a_faults)):
        if a_faults[i].injection_point != b_faults[i].injection_point:
            return False

    return True

def sub_word_rule(a: Trace, b: Trace):
    """
    Verifies if the trace `b` is **redundant** to `a` with respect to "SubWord" definition.
    **SubWord redundancy definition**: an attack A2 is *redundant* to an attack A1 if the ordered set of faults of A2 is a sub-word of the ordered set of faults of A1.
    
    This definition includes :attr:`Prefix` definition. Longer to compute. 
    
    examples: `{A1: [F1, F2, F3, F4], A2 = [F2, F4]}` ; `{A1 = [F1, F2, F3], A2 = [F1, F2]}`

    :param a: the base trace. 
    :type a: Trace
    :param b: the trace to check to be redundant.
    :type b: Trace
    :return: `True` if `b` is redundant to `a`, `False` otherwise.
    :rtype: bool
    """
    if a == b:
        return False
    if(a.order >= b.order):
        return False

    a_faults = a.faults()
    b_faults = b.faults()

    def find(start_index: int, fault_list, required_fault):
        for i in range(start_index, len(fault_list)):
            if fault_list[i].injection_point == required_fault.injection_point:
                return i
        return -1

    b_index = 0
    for fault in a_faults:
        b_index = find(b_index, b_faults, fault)
        if b_index == -1:
            return False
        b_index = b_index + 1
    return True

def feq_rule(a: Trace, b: Trace) -> bool:
    """
    Verifies if the attacks `b` and to `a` are **fault-equivalent**.
    **Fault-equivalence**: two attacks A1 and A2 are equivalent if their sequence of fault IPs are equal.

    :param a: the first attack to be tested.
    :type a: Trace
    :param b: the second attack to be tested.
    :type b: Trace
    :return: True if `a` and `b` are fault-equivalent, false otherwise.
    :rtype: bool
    """
    if a == b:
        return False
    if a.order != b.order:
        return False
    
    a_faults = a.faults()
    b_faults = b.faults()
    for i in range(len(a_faults)):
        if a_faults[i].injection_point != b_faults[i].injection_point:
            return False

    return True

def eq_rule(a: Trace, b: Trace) -> bool:
    """
    Verifies if the attacks `b` and to `a` are **equivalent**.
    **Equivalence**: two attacks A1 and A2 are equivalent if their sequences of transitions (faults, detections and CF) are equal.

    :param a: the first attack to be tested.
    :type a: Trace
    :param b: the second attack to be tested.
    :type b: Trace
    :return: True if `a` and `b` are fault-equivalent, false otherwise.
    :rtype: bool
    """
    if a == b:
        return False
    if a.order != b.order:
        return False
    
    i = 0
    for i in range(len(a.events)):
        if i >= len(b.events):
            return False
        a_e = a.events[i]
        b_e = b.events[i]
        
        # TODO, handle user event ? Detection ? 
        if type(a_e) != BasicBlock and type(a_e) != Fault: log.error("Unexpected type...")
        if type(b_e) != BasicBlock and type(b_e) != Fault: log.error("Unexpected type...")
        if type(a_e) != type(b_e):
            return False
        
        if type(a_e) == BasicBlock:
            if a_e.name != b_e.name:
                return False
        if type(a_e) == Fault:
            if a_e.injection_point != b_e.injection_point:
                return False
    return True

class AttackRedundancyResults:
    """
    The AttackRedundancyResults class holds results of the *attacks redundancy analysis*.
    
    It holds the graph of equivalence and redundancy.
    """
    def __init__(self, analysis: Analysis, graph: AttackRedundancyGraph, duration: float):
        """
        **__init__ (ctor):**  Constructs a new AttackRedundancyResults object with an analysis, the equivalence/redundancy graph and the duration information.

        :param analysis: the Analysis object from which the traces will be read.
        :type analysis: Analysis
        :param graph: Hotspot data for each injection point for each order.
        :type graph: AttackRedundancyGraph
        :param duration: duration of the analysis computing in millisecond.
        :type duration: float
        """
        self._analysis = analysis #: Analysis: The analysis object to which the *extension* is associated. See :func:`~lazart._internal_.extensions.register`.

        self._graph = graph #: AttackRedundancyGraph: The redundancy graph of the analysis.
        self._duration = duration #: float: The duration of the *attack redundancy analysis*.
        self.lazy_count = 0 #: int: The number of lazy nodes.

    def graph(self) -> AttackRedundancyGraph: 
        """
        Returns the redundancy graph.

        :return: the redundancy graph.
        :rtype: AttackRedundancyGraph
        """
        return self._graph
    
    def representatives(self, predicate: ty.Callable[[Trace], bool] = lambda t: True, minimals_only = False) -> ty.List[Trace]:
        """
        Returns the list of a representative traces for each equivalence class in the graph, if the trace's representative verify `predicate`.

        :param predicate: boolean predicate determining if the equivalence_class should be considered, default `lambda t: True`.
        :type predicate: Callable[[Trace], bool]
        :param minimals_only: if `True`, only minimal (non-redundant) classes are considered.
        :type minimals_only: Callable[[Trace], bool]
        :return: the list of representative traces for specified orders.
        :rtype: List[Trace]
        """
        min = [] # List of traces.
        # Search equivalence classes.
        for eq_class in self.graph()._equivalence_classes:
            if len(eq_class) == 0: 
                log.warning("unexpected empty equivalence class.")
                continue
            representative = eq_class[0]
            if minimals_only and representative.redundant: continue
            t = representative.get_trace(self._analysis)
            if predicate(t):
                min.append(t)

        # Add singletons
        for k in self.graph()._nodes:
            node = self.graph()._nodes[k]
            if node.equivalents == None:
                t = node.get_trace(self._analysis)
                if predicate(t):
                    min.append(t)

        return min

    def duration(self) -> float:        
        """
        Returns the duration in second of the attacks redundancy analysis computations.

        :return: duration of the analysis computing in second.
        :rtype: float
        """
        return self._duration
    
    def __str__(self) -> str:
        """
        Returns the string representing the AttackRedundancyResults.

        :return: the string representation of the object.
        :rtype: str
        """      
        ret = "Attack redundancy results: {}\n".format(self._analysis.name())
        ret += "  - execution-time: {}\n".format(_str_time(self.duration()))
        ret += "  - data:\n"
        for key in self.graph()._nodes:
            ret += "    - " + str(self.graph().node(key)) + "\n"
            
        return ret

    def table(self, **kwargs) -> Table:
        """
        Returns the table of attack for each order, including redundant attacks.

        :param title: title of the table, defaults to None
        :type title: str, optional
        :return: the table corresponding to equivalence and redundancy results.
        :rtype: _type_
        """
          
        tbl = Table(2 + self._analysis.max_order())

        # header
        header = ["Order"]
        for order in range(self._analysis.max_order() + 1):
            header.append("{}-order".format(str(order)))

        tbl.set_header(header)

        # attacks
        attack_line = ["attacks"]
        redundant_line = ["non-redundant"]

        for i in range(self._analysis.max_order() + 1):
            attack_line.append(0)
            redundant_line.append(0)

        for key in self.graph()._nodes:
            node = self.graph().node(key)
            tr = get_trace_by_name(self._analysis, node._trace_name)
            attack_line[tr.order + 1] = attack_line[tr.order + 1] + 1
            if not node.redundant:
                redundant_line[tr.order + 1] = redundant_line[tr.order + 1] + 1
                
        tbl.add_row(attack_line)
        tbl.add_row(redundant_line)

        return tbl

def eq_red_analysis(analysis: Analysis, **kwargs) -> AttackRedundancyResults:
    """ Returns an AttackRedundancyResults from an analysis object performing *Attacks
    Redundancy Analysis*.    

    Requires that the *traces parsing step* (see :func:`~lazart.core.traces.traces_results`) has been computed for this `analysis`.
    This function uses log to display progress, reduce verbosity level to reduce output.

    **Kwargs**:
     - `eq_rule` (`Callable[[Trace, Trace], bool]`): binary predicate determining if two attacks are equivalent. *default*: `feq_rule` (fault-equivalence). 
     - `red_rule` (`Callable[[Trace, Trace], bool]`): binary predicate determining if an attack 'b' is redundant to an attack 'a'. *default*: `prefix_rule`. 
     - `lazy` (`bool`): boolean predicate determining if the complete graph should be computed. Otherwise, only the minimal attacks are researched. *default*: `False`.
     - `satisfies_fct` (`Callable[[Trace], bool]`): boolean predicate determining if a trace should be considered. *default*: `lambda trace: trace.satisfies()`.
     - `eq_range` (`List[int]`): two elements list ([min, max]) limit in which the equivalence analysis will be performed. *default*: `[1, max_order]`.
     - `red_range` (`List[int]`): two elements list ([min, max]) limit in which the redundancy analysis will be performed. *default*: `[1, max_order]`.
     - `eq_mode` (`str`): name of the mode of equivalence comparison to be performed ("0", "1+", "N+"), *default*: `"0"`: 
        - "0": indicates that each trace will be compared to other same order traces only.
        - "0+": indicates that each trace will be compared to other traces of order greater or equal than itself.
        - "1+": indicates that each trace will be compared to other traces of order strictly greater than itself.

        ***note:*** *equivalence and fault-equivalence require that "0" mode is used. However, this option can be used if a custom `eq_rule` is provided and required specific comparison mode.*
     - `red_mode` (`str`): name of the mode of equivalence comparison to be performed ("0", "1+", "N+"), *default*: `"1+"`: 
        - "0": indicates that each trace will be compared to other same order traces only.
        - "0+": indicates that each trace will be compared to other traces of order greater or equal than itself.
        - "1+": indicates that each trace will be compared to other traces of order strictly greater than itself.

        ***note:*** *prefix and sub-word definitions require that "1+" mode is used. However, this option can be used if a custom `red_rule` is provided and required specific comparison mode.*

    :param analysis: the analysis for which the equivalence-redundancy analysis will be performed.
    :type analysis: Analysis
    :return: the computed attacks equivalence and redundancy results.
    :rtype: AttackRedundancyResults
    """    
    log.verbose("computing attack redundancy and equivalence.")
    start = time.process_time()

    lazy = args.get_or_throw(kwargs, "lazy", bool, False) # lazy redundancy

    max_order = analysis.max_order() 
    eq_range = args.get_or_throw(kwargs, "eq_range", None, [1, max_order] ) # range for equivalence analysis for eq traces.
    eq_mode = args.get_or_throw(kwargs, "eq_mode", None, "0")
    red_range = args.get_or_throw(kwargs, "red_range", None, [1, max_order] ) # range for redundancy analysis for eq traces.
    red_mode = args.get_or_throw(kwargs, "eq_mode", None, "1+")

    eq_fct = args.get_or_throw(kwargs, "eq_rule", None, feq_rule)
    red_fct = args.get_or_throw(kwargs, "red_rule", None, prefix_rule)
    
    s_fct = args.get_satisfies_function(kwargs, lambda trace: trace.satisfies())
    full_debug_mode = options.defines().get("lz.log.debug.atk_redundancy", False)
    graph = AttackRedundancyGraph()

    lazy_count = 0

    for order in range(min(eq_range[0], red_range[0]), max(eq_range[1], red_range[1]) + 1):
        if full_debug_mode: 
            tmp = [x for x in traces_list(analysis, order) if s_fct(x)]
            log.quiet("  -> order {} traces: [{}].".format(order, ", ".join([t.name for t in tmp]) + "]"))
        
        i = 0
        for attack in [x for x in traces_list(analysis, order) if s_fct(x)]:
            if full_debug_mode: log.quiet("  - searching redundant for attacks {} ({}).".format(attack.name, ",".join([str(f.injection_point) for f in attack.faults()])))
            node = graph.make_or_get(attack.name)

            # Do ?
            do_eq = node.equivalents is None # Has some
            do_red = True
            if lazy:
                do_red = not node.redundant
          
            # Modes
            for order_target in range(order, max_order + 1):

                if order_target == order:
                    eq_in_range = eq_mode in ["0", "0+"]
                    red_in_range = red_mode in ["0", "0+"]
                elif order_target == order + 1:
                    eq_in_range = eq_mode in ["0+", "1", "1+"]
                    red_in_range = red_mode in ["0+", "1", "1+"]
                else:
                    eq_in_range = eq_mode in ["0+", "1+"]
                    red_in_range = red_mode in ["0+", "1+"]

                if not eq_in_range and not red_in_range:
                    continue

                if (not eq_in_range) and red_in_range and lazy and node.redundant:
                    if full_debug_mode: log.quiet("  - lazy for attack {}.".format(attack.name))
                    lazy_count = lazy_count + 1
                    continue

                targets = [x for x in traces_list(analysis, order_target) if s_fct(x)]
                if full_debug_mode: log.quiet("     -> eir: {}, rir {}, de {}, dr {}, targets {}: [{}].".format(eq_in_range, red_in_range, do_eq, do_red, order_target, ", ".join([t.name for t in targets]) + "]"))
                    
                for target in targets:
                    target_node = graph.make_or_get(target.name)

                    eq_result = None
                    eq_class = None 
                    if eq_in_range and do_eq:
                        eq_result = eq_fct(attack, target) 
                        if eq_result:
                            eq_class = graph.equivalent(node, target_node)
                            
                    red_result = None
                    if red_in_range and do_red:
                        red_result = red_fct(attack, target) 
                        if red_fct(attack, target):
                            graph.add_edge(node, target_node)

                    if full_debug_mode: log.quiet("           - target '{}' ({}): min: {}, red: {}, to {} ({}).".format(target.name, ",".join([str(f.injection_point) for f in target.faults()]), "n/a" if eq_result == None else "no" if not eq_result else "yes", "n/a" if red_result == None else "no" if not red_result else "yes", attack.name, ",".join([str(f.injection_point) for f in attack.faults()])))

                i = i + 1
                # TODO: display
                #percentage = (i / len(tl)) * 100
                #if not quiet and not full_debug_mode: log.rquiet("\33[2K\rrunning ARA: [{}/{}] {}% ({}/{})...".format(order, analysis.max_order() , int(percentage), i, len(tl)))
    
    end = time.process_time()
    duration = end - start
    log.info("\33[2K\rARA ended in {} (lazy: {}).".format(_str_time(duration), lazy_count))

    arr = AttackRedundancyResults(analysis, graph, duration)
    arr.lazy_count = lazy_count

    return arr

class AttacksRedundancyStep(_ext.AnalysisStep):
    """
    Subclass of AnalysisStep for the *equivalence and redundancy analysis*, see :class:`lazart._internal_.extensions.AnalysisStep`.
    """
    def __init__(self, analysis):    
        """
        **__init__ (ctor):** Constructs a new AttacksRedundancyStep object associated to the specified analysis.

        :param analysis: the analysis of the step.
        :type analysis: Analysis
        """                
        super(AttacksRedundancyStep, self).__init__(analysis, constants.attack_redundancy_ext,  _ext.StepID.Analysis)

    def execute(self, **kwargs):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        return eq_red_analysis(self.analysis, **kwargs)

    def init(self):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        pass

    def save_disk(self):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        return super().save_disk()

    def clean(self):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        _folder.remove_file(_folder.lazart_file(self.analysis.path(), constants.attack_redundancy_ext))
        
def attacks_redundancy_results(analysis: Analysis, **kwargs) -> AttackRedundancyResults:
    """ Returns an AttackRedundancyResults from an analysis object performing *Attacks
    Redundancy Analysis*.    

    Requires that the *traces* has been computed for this `analysis`.
    
    Uses extension system (attack_redundancy_ext, see :class:`AttacksRedundancyStep`) to reuse disk/RAM cache if available.
    This function uses log to display progress, reduce verbosity level to     reduce output.

    **Kwargs**:
     - `eq_rule` (`Callable[[Trace, Trace], bool]`): binary predicate determining if two attacks are equivalent. *default*: `feq_rule` (fault-equivalence). 
     - `red_rule` (`Callable[[Trace, Trace], bool]`): binary predicate determining if an attack 'b' is redundant to an attack 'a'. *default*: `prefix_rule`. 
     - `lazy` (`bool`): boolean predicate determining if the complete graph should be computed. Otherwise, only the minimal attacks are researched. *default*: `False`.
     - `satisfies_fct` (`Callable[[Trace], bool]`): boolean predicate determining if a trace should be considered. *default*: `lambda trace: trace.satisfies()`.
     - `eq_range` (`List[int]`): two elements list ([min, max]) limit in which the equivalence analysis will be performed. *default*: `[1, max_order]`.
     - `red_range` (`List[int]`): two elements list ([min, max]) limit in which the redundancy analysis will be performed. *default*: `[1, max_order]`.
     - `eq_mode` (`str`): name of the mode of equivalence comparison to be performed ("0", "1+", "N+"), *default*: `"0"`: 
        - "0": indicates that each trace will be compared to other same order traces only.
        - "0+": indicates that each trace will be compared to other traces of order greater or equal than itself.
        - "1+": indicates that each trace will be compared to other traces of order strictly greater than itself.

        ***note:*** *equivalence and fault-equivalence require that "0" mode is used. However, this option can be used if a custom `eq_rule` is provided and required specific comparison mode.*
     - `red_mode` (`str`): name of the mode of equivalence comparison to be performed ("0", "1+", "N+"), *default*: `"1+"`: 
        - "0": indicates that each trace will be compared to other same order traces only.
        - "0+": indicates that each trace will be compared to other traces of order greater or equal than itself.
        - "1+": indicates that each trace will be compared to other traces of order strictly greater than itself.

        ***note:*** *prefix and sub-word definitions require that "1+" mode is used. However, this option can be used if a custom `red_rule` is provided and required specific comparison mode.*

    :param analysis: the Analysis object from which the traces will be read.
    :type analysis: Analysis
    :return: the computed attacks equivalence and redundancy results.
    :rtype: AttackRedundancyResults
    """
    step = _ext.get_step(AttacksRedundancyStep(analysis))
    
    return step.get_results(**kwargs)

# Helpers 

def representatives(analysis: Analysis, predicate: ty.Callable[[Trace], bool] = lambda t: True) -> ty.List[Trace]:
    """
    Returns the list of minimal attacks matching the specified predicate. One representative per equivalence class is kept. 

    :param analysis: the considered analysis.
    :type predicate: Analysis
    :param predicate: boolean predicate determining if the equivalence_class should be considered, default `lambda t: True`.
    :type predicate: Callable[[Trace], bool]
    :return: the list of representative minimal traces.
    :rtype: List[Trace]
    """
    arr = attacks_redundancy_results(analysis)

    return arr.representatives(predicate)

def minimals(analysis: Analysis, predicate: ty.Callable[[Trace, AttackRedundancyNode], bool] = lambda t: True) -> ty.List[Trace]:
    """
    Returns the list of minimal attacks matching the specified predicate. One representative per equivalence class is kept. 

    :param analysis: the considered analysis.
    :type predicate: Analysis
    :param predicate: boolean predicate determining if the equivalence_class should be considered, default `lambda t: True`.
    :type predicate: Callable[[Trace], bool]
    :return: the list of representative minimal traces.
    :rtype: List[Trace]
    """
    arr = attacks_redundancy_results(analysis)

    return arr.representatives(predicate, True)


    