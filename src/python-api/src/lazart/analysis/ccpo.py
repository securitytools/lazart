""" The module *lazart.analysis.ccpo* contains definitions for **Countermeasure Check Points Optimization** (detectors optimization).

**Note: countermeasures analysis are not migrated to Lazart 4.0 for now and may by unusable.**

:since: 3.1
:note: **Not updated.**
:author: Etienne Boespflug
"""
# Lazart
import lazart.constants as constants

# Internal
import lazart._internal_.folder as _folder
import lazart._internal_.extensions as _ext

# Core
from lazart.core.analysis import Analysis

# Analysis
from lazart.analysis.ccpo_classification import CCPOClassificationResults, ccpo_classification, triggering_points
from lazart.analysis.ccpo_selection import ccpo_selection, CCPOSelectionResults


class CCPOResults:
    """ The CCPOResults class represents results for an *CCP Optimization* analysis.

    It holds the results of the two steps: *CCPO classification* and *CCPO selection*.
    """
    def __init__(self, analysis: Analysis, classification: CCPOClassificationResults, selection: CCPOSelectionResults, duration: float, **kwargs):
        """
        **__init__ (ctor):**  Constructs a new CCPOResults object from an analysis and results of classification and optional selection steps.

        :param analysis: the Analysis object from which the traces will be read.
        :type analysis: Analysis
        :param classification: the results of classification step.
        :type classification: CCPOClassificationResults
        :param selection: the results of selection step (`None` if no selection was computed).
        :type selection: CCPOSelectionResults
        :param duration: duration of the analysis computing in millisecond.
        :type duration: float
        """
        self._analysis = analysis #: Analysis: The analysis object to which the *extension* is associated. See :func:`~lazart._internal_.extensions.register`.

        self.classification = classification #: CCPOClassificationResults: The results of classification step.
        self.selection = selection #: CCPOSelectionResults: The results of selection step (`None` if no selection was computed).
        self._duration = duration  #: float: The duration of the *ccpo*.

    def duration(self) -> float:
        """
        Returns the duration in second of the attacks redundancy analysis computations.

        :return: duration of the analysis computing in second.
        :rtype: float
        """        
        return self._duration

    def __str__(self):
        """
        Returns the string representing the HotspotResults.

        :return: the string representation of the object.
        :rtype: str
        """     
        ret = "CCPO results:\n"
        tps = triggering_points(self._analysis)
        ret = ret + "   - {} CCPs: {}.\n".format(len(tps), "[" + ", ".join(str(t) for t in tps) + "]")
        return ret


def ccpo_analysis(analysis: Analysis, **kwargs) -> CCPOResults:
    """
    Runs CCPO analysis by executing the classification and selection steps.

    Requires that the *traces parsing step* (see :func:`~lazart.core.traces.traces_results`) has been computed for this `analysis`.

    This function uses log to display progress, reduce verbosity level to reduce output.

    **Kwargs**: *(forwarded to inner steps)*
     - `satisfies_fct` (`trace -> bool`): boolean predicate determining if a trace should be considered. *default*: `lambda trace: trace.satisfies()`. 
     - `det_list` (`List[str]`): the list of detectors to consider. *default*: `triggering_points(analysis)`.
     - `weight_fct` (`Callable[[str], int]`): weight_fct: the weight function for computing detector set weight. *default*: `lambda det: 1`.
     - `quiet` (`bool`): if true, output is reduced despite current verbosity level (**unused**). *default*: `False`.

    :param analysis: the Analysis object from which the traces will be read.
    :type analysis: Analysis
    :return: the computed CCPO results.
    :rtype: CCPOResults
    """
    
    classification_results = ccpo_classification(analysis, **kwargs)    
    selection_results = ccpo_selection(analysis, classification_results, **kwargs)

    duration = classification_results.duration() + sum(selection_results.duration())

    return CCPOResults(analysis, classification_results, selection_results, duration)


class CCPOStep(_ext.AnalysisStep):
    """
    Subclass of CCPOStep for the *CCPO analysis*, see :class:`lazart._internal_.extensions.AnalysisStep`.
    """
    def __init__(self, analysis: Analysis):  
        """
        **__init__ (ctor):** Constructs a new CCPOStep object associated to the specified analysis.

        :param analysis: the analysis of the step.
        :type analysis: Analysis
        """                              
        super(CCPOStep, self).__init__(analysis, constants.ccpo_ext,  _ext.StepID.Analysis)

    def execute(self, **kwargs):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        return ccpo_analysis(self.analysis, **kwargs)

    def init(self):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        pass

    def save_disk(self):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        return super().save_disk()       

    def clean(self):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        _folder.remove_file(_folder.lazart_file(self.analysis.path(), constants.ccpo_ext))

def ccpo(analysis: Analysis, **kwargs) ->  CCPOResults:
    """
    Returns an CCPOResults from an analysis object performing *CCPO* analysis.

    Requires that the *traces parsing step* (see :func:`~lazart.core.traces.traces_results`) and *CCPO classification step* (see :func:`~lazart.core.analysis.ccpo_classification`) has been computed for this `analysis`.
    
    Uses extension (ccpo_classification_ext, see :class:`lazart._internal_.extensions.AnalysisStep`) system to reuse disk/RAM cache if available.
    This function uses log to display progress, reduce verbosity level to reduce output.

    **Kwargs**: *(forwarded to inner steps)*
     - `satisfies_fct` (`trace -> bool`): boolean predicate determining if a trace should be considered. *default*: `lambda trace: trace.satisfies()`. 
     - `det_list` (`List[str]`): the list of detectors to consider. *default*: `triggering_points(analysis)`.
     - `weight_fct` (`Callable[[str], int]`): weight_fct: the weight function for computing detector set weight. *default*: `lambda det: 1`.
     - `quiet` (`bool`): if true, output is reduced despite current verbosity level (**unused**). *default*: `False`.

    :param analysis: the Analysis object from which the traces will be read.
    :type analysis: Analysis
    :return: the computed CCPO results.
    :rtype: CCPOResults
    """
    step = _ext.get_step(CCPOStep(analysis))
    
    return step.get_results(**kwargs)
