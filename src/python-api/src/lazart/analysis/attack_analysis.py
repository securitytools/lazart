""" The module *lazart.analysis.attack_analysis* contains definitions for *attack analysis*.

:since: 3.0
:author: Etienne Boespflug 
"""

# Python
import time
import typing as ty

# Internal
import lazart._internal_.args as _args
import lazart._internal_.extensions as _ext
import lazart._internal_.folder as _folder
import lazart.constants as constants

# Lazart
import lazart.logs as log
from lazart._internal_.util import str_time as _str_time

# Core
from lazart.core.analysis import Analysis
from lazart.core.traces import Trace, traces, traces_results


class AttackResults:
    """
    The AttackResults class holds results of the *attack analysis*.

    It include analysis computation time, and list of attacks traces.
    """
    def __init__(self, analysis: Analysis, results: ty.List[ty.List[Trace]], duration: float, **kwargs):
        """
        **__init__ (ctor):**  Init the AttackResults object with an analysis, the raw results and the duration information.

        *Should not be called directly, prefer* :func:`attacks_results`.

        :param analysis: the Analysis object from which the traces will be read.
        :type analysis: Analysis
        :param results: List of list of all attacks lists for each order.
        :type results: List[List[Trace]]
        :param duration: duration of the analysis computing in seconds.
        :type duration: float
        """
        self._analysis = analysis #: Analysis: The analysis object to which the *extension* is associated. See :func:`~lazart._internal_.extensions.register`.

        self.results = results #: List[List[Trace]]: The list of all attacks lists for each order.
        self._duration = duration #: float: The duration of the *attack analysis*.

    def duration(self) -> float: 
        """
        Returns the duration of the *attack analysis*.

        :return: the duration of the step.
        :rtype: float
        """
        return self._duration

    def attacks(self) -> ty.List[ty.List[Trace]]:
        """ Returns the list of all traces considered as attacks

        :return: the list by order of all traces lists corresponding to attack for this
        attacks analysis.
        :rtype: List[List[Trace]]
        """
        l = []
        for i in range(self._analysis.max_order() + 1):
            for trace in self.results[i]:
                l.append(trace)
        return l

    def attacks(self, order: int) -> ty.List[Trace]:
        """ 
        Returns the list (copy) of all traces considered as attacks for the specified order.

        :param order: the order (i.e. number of faults) for attacks.
        :type order: int
        :return: the list of all attacks of specified order.
        :rtype: List[Trace]
        """
        if order > self._analysis.max_order():
            log.error("order parameter out of range: " + str(order))
        l = []
        for trace in self.results[order]:
            l.append(trace)
        return l   
    
    def __str__(self) -> str:
        """
        Returns the string representing the AttackResults.

        :return: the string representation of the object.
        :rtype: str
        """      
        ret = ""
        ret += "  - execution-time: {}\n".format(_str_time(self._duration))
        for i in range(self._analysis.max_order() + 1):
            ret = ret + "  - {}-order: {} attack(s).\n".format(i, len(self.results[i]))
        return ret

def attack_analysis(analysis: Analysis, **kwargs) -> AttackResults:
    """ Returns an AttackResults from an analysis object performing *Attacks
    Analysis*.

    Requires that the *traces parsing step* (see :func:`~lazart.core.traces.traces_results`) has been computed for this `analysis`.

    This function uses log to display progress, reduce verbosity level to reduce output.

    **Kwargs**:
     - `satisfies_fct` (`Callable[[Trace], bool]`): boolean predicate determining if a trace should be considered. *default*: `lambda trace: trace.satisfies()`.

    :param analysis: the Analysis object from which the traces will be read.
    :type analysis: Analysis
    :return: the computed attacks results.
    :rtype: AttackResults
    """

    log.info("computing attack results.")
    traces_results(analysis) # verify that the step has been run.
    start = time.process_time()
    results = []
    
    s_fct = _args.get_satisfies_function(kwargs, lambda trace: trace.satisfies())

    # Attacks
    for order in range(analysis.max_order() + 1):
        log.fverbose("    order {}...".format(order))
        results.append([x for x in traces(analysis)[order] if s_fct(x)])
        log.rverbose(" done.\n")

    end = time.process_time()
    duration = end - start
    log.info("attacks analysis ended in {}.".format(_str_time(duration)))

    return AttackResults(analysis, results, duration)

class AttackAnalysisStep(_ext.AnalysisStep):
    """
    Subclass of AnalysisStep for the *attack analysis*, see :class:`lazart._internal_.extensions.AnalysisStep`.
    """
    def __init__(self, analysis: Analysis): 
        """
        **__init__ (ctor):** Constructs a new AttackAnalysisStep object associated to the specified analysis.

        :param analysis: the analysis of the step.
        :type analysis: Analysis
        """            
        super(AttackAnalysisStep, self).__init__(analysis, constants.attacks_ext, _ext.StepID.Analysis)

    def execute(self, **kwargs):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        return attack_analysis(self.analysis, **kwargs)

    def init(self):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        pass

    def save_disk(self):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        return super().save_disk()        

    def clean(self):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        _folder.remove_file(_folder.lazart_file(self.analysis.path(), constants.attacks_ext))
        log.dev("cleaning attacks...")

def attacks_results(analysis: Analysis, **kwargs) -> AttackResults:
    """ 
    Returns an AttackResults from an analysis object performing *Attacks Analysis*.

    Requires that the *traces parsing step* (see :func:`~lazart.core.traces.traces_results`) has been computed for this `analysis`.
    
    Uses extension (attacks_ext, see :class:`lazart._internal_.extensions.AnalysisStep`) system to reuse disk/RAM cache if available.
    This function uses log to display progress, reduce verbosity level to reduce output.

    **Kwargs**:
     - `satisfies_fct` (`Callable[[Trace], bool]`): boolean predicate determining if a trace should be considered. *default*: `lambda trace: trace.satisfies()`.

    :param analysis: the Analysis object from which the traces will be read.
    :type analysis: Analysis
    :return: the computed attacks results.
    :rtype: AttackResults
    """
    step = _ext.get_step(AttackAnalysisStep(analysis))
    
    return step.get_results(**kwargs)