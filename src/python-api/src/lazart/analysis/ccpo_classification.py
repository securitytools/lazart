""" The module *lazart.analysis.ccpo_classification* contains definitions for *classification step* of **Countermeasure Check Points Optimization**.

**Note: countermeasures analysis are not migrated to Lazart 4.0 for now and may by unusable.**

:since: 3.1
:note: **Not updated.**
:author: Etienne Boespflug
"""

# Python
from enum import Enum
import types
import time
import typing as ty

# Lazart
import lazart.options as options
import lazart.constants as constants

# Internal
import lazart._internal_.args as args
from lazart._internal_.util import str_time as _str_time
import lazart._internal_.extensions as _ext
import lazart.logs as log
import lazart._internal_.folder as _folder

# Core
from lazart.core.traces import Trace, traces_results, traces_list, TerminationType, traces
from lazart.core.analysis import Analysis

# Results
from lazart.results.formats import Table

def triggering_points(analysis: Analysis) -> ty.List[str]:
    """
    Returns the list of detectors of the analysis.

    TODO: not updated for new detector list 

    :param analysis: The analysis containing the detectors.
    :type analysis: Analysis
    :return: the list of detectors names.
    :rtype: List[str]
    """
    #Test
    """ccp_dict = run_results(analysis)._det_dict

    return ccp_dict.keys()"""

    
    # OLD change !
    if hasattr(analysis, '_cached_tp_list'):
        return analysis._cached_tp_list

    cms = set()
    for order in range(analysis.max_order() + 1):
        for trace in traces_list(analysis, order):
            for cm_trigger in trace.triggered():
                cms.add(cm_trigger.ccp)

    analysis._cached_tp_list = list(cms) # Caching

    return list(cms)

    # TODO get true results (cf in dse step)
    #"""

class CCPOClass(Enum):
    """
    The CCPOClass enumeration represents the class of a detector in classification.
    """
    Inactive = 0 #: The *detector* is inactive (never triggered). It can be removed.
    Repetitive = 1 #: The *detector* is repetitive (never trigger alone). *detector selection* is required in order to select repetitive to keep.
    Necessary = 2 #: The *detector* is necessary (triggers at least one time alone). Cannot be removed.

def _color_conclusion(conclusion: CCPOClass, color: bool) -> str:
    """
    Returns the string corresponding to the specified *CCPO class*. 
    Uses UNIX color codes if `color` is ̀ True`.

    :param conclusion: the *CCPO class* to return as string.
    :type conclusion: CCPOClass
    :param color: a boolean determining if the string should use UNIX color codes.
    :type color: bool
    :return: the string corresponding to the specified *CCPO class*.
    :rtype: str
    """
    if conclusion.value == CCPOClass.Inactive.value:
        if color:
            return log.color.red + "inactive" + log.color.reset
        else: return "inactive"
    if conclusion.value == CCPOClass.Repetitive.value:
        if color: 
            return log.color.brown + "repetitive" + log.color.reset
        else: return "repetitive"
    if color:
        return log.color.green + "necessary" + log.color.reset
    return "necessary"

class CCPOClassificationResults:
    """ The CCPOClassificationResults class represents results for an CCPO *classification* step.
    It holds the computed values for all detectors and provides functions to get conclusions and information about the analysis.
    """
    def __init__(self, analysis: Analysis, results: ty.List[ty.Dict[str, ty.Tuple[int, int, int, int, int]]], duration: float, **kwargs):
        """        
        **__init__ (ctor):**  Constructs a new CCPOClassificationResults object with an analysis, the computed raw results graph and the duration information.

        :param analysis: the Analysis object from which the traces will be read.
        :type analysis: Analysis
        :param results: List for each order of Dictionary of classification information for each detector: (total_triggers, trace_triggers, repetition_level, max_concurrent, best_rank).
        :type results: List[Dict[str, Tuple[int, int, int, int, int]]]
        :param duration: duration of the analysis computing in millisecond.
        :type duration: float
        """
        self._analysis = analysis #: Analysis: The analysis object to which the *extension* is associated. See :func:`~lazart._internal_.extensions.register`.

        self._results = results #: List[Dict[str, Tuple[int, int, int, int, int]]]: List for each order of Dictionary of classification information for each detector: (total_triggers, trace_triggers, repetition_level, max_concurrent, best_rank).
        self._duration = duration  #: float: The duration of the *ccpo classification*.

    def duration(self) -> float:
        """
        Returns the duration in second of the attacks redundancy analysis computations.

        :return: duration of the analysis computing in second.
        :rtype: float
        """        
        return self._duration

    def __str__(self):
        """
        Returns the string representing the HotspotResults.

        :return: the string representation of the object.
        :rtype: str
        """     
        ret = "CCPO results:\n"
        ret = ret + "   - detectors: {}.\n".format(len(triggering_points(self._analysis)))
        for order in range(self._analysis.max_order() + 1):
            if order == 0: continue
            ret = ret + "  - {}-order: \n".format(order)
            for cm in triggering_points(self._analysis):
                # (total_triggers, trace_triggers, repetition_level, max_concurrent, best_rank)
                (tot, trt, rl, mc, br) = self._results[order][cm]
                ret = ret + "       - '{}' : total:{}, traces: {}, repetition level: {}, max concurrent: {}, best rank: {}\n".format(cm, tot, trt, rl if rl != options.max_int() else "N/A", mc, br if br != options.max_int() else "N/A")

        return ret

    def results(self, order: int, det: str):
        """
        Returns the detector classification data for the specified detector.

        :param order: The required order for the results.
        :type order: int
        :param det: The detector identifier.
        :type det: str
        :return: The tuple of classification data for the specified order and detector: (total_triggers, trace_triggers, repetition_level, max_concurrent, best_rank).
        :rtype: (int, int, int, int, int)
        """
        return self._results[order][det]
    
    def total_triggers(self, order: int, det: str) -> int:
        """
        Returns the total triggers for the specified order and detector. Correspond to the total number of trigger of this detector on all traces of order `order`.

        *This value is not required to classify detector but can be used in weight function of detector selection step.*

        :param order: The required order for the results.
        :type order: int
        :param det: The detector identifier.
        :type det: str
        :return: the total trigger value for the specified order and detector.
        :rtype: int
        """
        (v, _, _, _, _) = self.results(order, det)
        return v

    def traces_triggers(self, order: int, det: str) -> int:
        """
        Returns the traces triggers for the specified order and detector. Corresponds to the number of traces of order lower or equals to `order` in which the detector is triggered.

        *This value is not required to classify detectors but can be used in weight function of detector selection step.*

        :param order: The required order for the results.
        :type order: int
        :param det: The detector identifier.
        :type det: str
        :return: the trace trigger value for the specified order and detector.
        :rtype: int
        """
        (_, v, _, _, _) = self.results(order, det)
        return v

    def local_repetition_level(self, order: int, det: str) -> int:
        """
        Returns the local repetition level for specified order and detector.

        :param order: The required order for the results.
        :type order: int
        :param det: The detector identifier.
        :type det: str
        :return: the local repetition level value for the specified order and detector.
        :rtype: int
        """
        (_, _, v, _, _) = self.results(order, det)
        return v

    def repetition_level(self, order: int, det: str) -> int:
        """
        Returns the minimum repetition level for the specified detector among all traces of order lower or equals to `order`.

        :param order: The required order for the results.
        :type order: int
        :param det: The detector identifier.
        :type det: str
        :return: the repetition level value for the specified order and detector.
        :rtype: int
        """
        best_rl = options.max_int()
        order = order + 1
        limit = self._analysis.max_order() + 1
        if order > limit:
            log.warning(f"order limit greater than analysis's max_order, set to {limit}.")
            order = limit
        for o in range(1, order):
            best_rl = min(self.local_repetition_level(o, det), best_rl)  
        return best_rl  

    def local_max_concurrent(self, order: int, det: str) -> int:
        """
        Returns the local max_concurrent value for specified order and detector. Corresponds to the maximum value of concurrent trigger of this detector among all traces with order equals to the specified `order`.

        *This value is not required to classify detector but can be used in weight function of detector selection step.*

        :param order: The required order for the results.
        :type order: int
        :param det: The detector identifier.
        :type det: str
        :return: the local max_concurrent value for the specified order and detector.
        :rtype: int
        """
        (_, _, _, v, _) = self.results(order, det)
        return v

    def max_concurrent(self, order: int, det: str) -> int:
        """
        Returns the max_concurrent value for specified order and detector. Corresponds to the maximum value of concurrent trigger of this detector among all traces with order lower or equals to the specified `order`.

        *This value is not required to classify detector but can be used in weight function of detector selection step.*

        :param order: The required order for the results.
        :type order: int
        :param det: The detector identifier.
        :type det: str
        :return: the local max_concurrent value for the specified order and detector.
        :rtype: int
        """
        best_mc = 0
        order = order + 1
        limit = self._analysis.max_order() + 1
        if order > limit:
            log.warning(f"order limit greater than analysis's max_order, set to {limit}.")
            order = limit
        for o in range(1, order):
            best_mc = max(self.local_max_concurrent(o, det), best_mc)  
        return best_mc  

    def local_best_rank(self, order: int, det: str) -> int:
        """
        Returns the local best_rank value for specified order and detector. Corresponds to the lower of ranking of detector trigger in a trace among all traces with order equals to the specified `order`.

        *This value is not required to classify detector but can be used in weight function of detector selection step.*

        :param order: The required order for the results.
        :type order: int
        :param det: The detector identifier.
        :type det: str
        :return: the local best rank value for the specified order and detector.
        :rtype: int
        """
        (_, _, _, _, v) = self.results(order, det)
        return v

    def best_rank(self, order: int, det: str) -> int:
        """
        Returns the best_rank value for specified order and detector. Corresponds to the lower of ranking of detector trigger in a trace among all traces with order lower or equals to the specified `order`.

        *This value is not required to classify detector but can be used in weight function of detector selection step.*

        :param order: The required order for the results.
        :type order: int
        :param det: The detector identifier.
        :type det: str
        :return: the local best rank value for the specified order and detector.
        :rtype: int
        """
        best_r = options.max_int()
        order = order + 1
        limit = self._analysis.max_order() + 1
        if order > limit:
            log.warning(f"order limit greater than analysis's max_order, set to {limit}.")
            order = limit
        for o in range(1, order):
            best_r = min(self.local_best_rank(o, det), best_r)  
        return best_r  

    def local_conclusion(self, order: int, det: str, **kwargs) -> CCPOClass:
        """
        Returns the local conclusion for the specified detector and order.

        :param order: The conclusion order.
        :type order: int
        :param det: The detector identifier.
        :type det: str
        :return: the local conclusion.
        :rtype: CCPOClass
        """
        if self.total_triggers(order, det) == 0: return CCPOClass.Inactive
        if self.local_repetition_level(order, det) > 0: return CCPOClass.Repetitive
        return CCPOClass.Necessary

    def conclusion(self, order: int, det: str, **kwargs) -> CCPOClass:
        """
        Returns the conclusion for the specified detector and order.

        :param order: The conclusion order.
        :type order: int
        :param det: The detector identifier.
        :type det: str
        :return: the local conclusion.
        :rtype: CCPOClass
        """
        maxc = CCPOClass.Inactive

        order = order + 1
        limit = self._analysis.max_order() + 1
        if order > limit:
            log.warning(f"order limit greater than analysis's max_order, set to {limit}.")
            order = limit
        for o in range(1, order):
            local = self.local_conclusion(o, det, **kwargs)
            if maxc.value < local.value:
                maxc = local
        
        return maxc  

    def ccps(self) -> ty.List[str]:
        """
        Returns the list of detectors in the detector classification.

        :return: the list of detectors.
        :rtype: List[str]
        """
        
        tps = []
        for order in range(1, self._analysis.max_order()):
            for key in self._results[order]:
                if key not in tps:
                    tps.append(key)
        return tps 

    def inactives(self, order: int) -> ty.List[str]:
        """
        Returns the list of *inactive* detectors for the specified order.

        :param order: The max number of faults.
        :type order: int
        :return: the list of inactive detectors.
        :rtype: List[str]
        """
        res = []
        for key in self._results[order].keys():
            if self.conclusion(order, key) == CCPOClass.Inactive:
                res.append(key)
        return res

    def repetitives(self, order: int) -> ty.List[str]:
        """
        Returns the list of *repetitive* detectors for the specified order.

        :param order: The max number of faults.
        :type order: int
        :return: the list of repetitive detectors.
        :rtype: List[str]
        """
        res = []
        for key in self._results[order].keys():
            if self.conclusion(order, key) == CCPOClass.Repetitive:
                res.append(key)
        return res
    
    
    def _table(self, str_fct: ty.Callable[[int, int, int], str], title=None, **kwargs) -> Table:
        """
        Internal function for table formatting.

        TODO(), verify det list.
         
        :param str_fct: The formatting function for results.
        :type str_fct: Callable[[int, int, int], str]
        :param title: The title of the table, defaults to None
        :type title: str, optional
        :return: the result table.
        :rtype: Table
        """
        tbl = Table(2 + self._analysis.max_order())
        tbl._title = title

        # header
        header = ["Det", "Infos"]
        for order in range(self._analysis.max_order()):
            header.append("{}-order".format(str(order + 1)))

        tbl.set_header(header)

        #rr = run_results(self._analysis)
        #ccps = rr._det_dict

        # data
        tps = self.ccps()

        #tps_wolv = [str(l) for l in triggering_points(self._analysis)]
        for tp in tps:
            infos = "no-data"
            """if tp in tps_wolv:
                cinfo = ccps[tp]
                infos = f": {cinfo['loc']['line']}, bb: {cinfo['loc']['basic_block']}, {', user.' if not cinfo['user_generated'] else '.'}"""
            data = [tp, infos]
            for order in range(1, self._analysis.max_order() + 1):
                data.append(str_fct(order, tp))
            tbl.add_row(data)

        return tbl
        
    def total_table(self) -> Table:
        """
        Returns the table of total triggers values.

        :return: the formatted result table.
        :rtype: Table
        """
        return self._table(lambda order, tp : str(self.total_triggers(order, tp)), "CCPO (total-trigger)")

    def local_conclusions_table(self, **kwargs) -> Table:
        """
        Returns the table of local conclusions.
            
        **Kwargs**:
        - `exportable` (`bool`): if `True`, coloration is disabled.

        :return: the formatted result table.
        :rtype: Table
        """
        exportable = kwargs.get("exportable", False)

        def l(order, tp):
            return _color_conclusion(self.local_conclusion(order, tp, **kwargs), not exportable)

        return self._table(l, "CCPO locals conclusions")

    def conclusions_table(self, **kwargs) -> Table:
        """
        Returns the table of conclusions.
            
        **Kwargs**:
        - `exportable` (`bool`): if `True`, coloration is disabled.

        :return: the formatted result table.
        :rtype: Table
        """
        exportable = kwargs.get("exportable", False)

        def l(order, tp):
            return _color_conclusion(self.conclusion(order, tp, **kwargs), not exportable)

        return self._table(l, "CCPO conclusions")

    def local_complete_table(self, **kwargs) -> Table:
        """
        Returns the table of CCPO classification local results.
            
        **Kwargs**:
        - `exportable` (`bool`): if `True`, coloration is disabled.

        :return: the formatted result table.
        :rtype: Table
        """
        exportable = kwargs.get("exportable", False)

        def color_white(s, color: bool):
            if color:
                return log.color.white + str(s) + log.color.reset
            return str(s)   

        def check_value(s):
            if s == options.max_int(): return "N/A"
            return s         

        def l(order, tp):
            return 'total triggers: {}\ntraces triggers: {}\nmax concurrent:       {}\nrepetition level: {}\nbest rank:    {}\nconclusion:    {}'.format(color_white(self.total_triggers(order, tp), not exportable), color_white(self.traces_triggers(order, tp), not exportable), color_white(self.local_max_concurrent(order, tp), not exportable), color_white(check_value(self.local_repetition_level(order, tp)), not exportable), color_white(check_value(self.local_best_rank(order, tp)), not exportable), _color_conclusion(self.local_conclusion(order, tp, **kwargs), not exportable))

        return self._table(l, "CCPO local results" + self._analysis.name())

    def complete_table(self, **kwargs) -> Table:
        """
        Returns the table of CCPO classifcation results.
            
        **Kwargs**:
        - `exportable` (`bool`): if `True`, coloration is disabled.

        :return: the formatted result table.
        :rtype: Table
        """
        exportable = kwargs.get("exportable", False)

        def color_white(s, color: bool):
            if color:
                return log.color.white + str(s) + log.color.reset
            return str(s)        
            
        def check_value(s):
            if s == options.max_int(): return "N/A"
            return s        

        def l(order, tp):
            return 'total triggers: {}\ntraces triggers: {}\nmax concurrent:       {}\nrepetition level: {}\nbest rank:    {}\nconclusion:    {}'.format(color_white(self.total_triggers(order, tp), not exportable), color_white(self.traces_triggers(order, tp), not exportable), color_white(self.max_concurrent(order, tp), not exportable), color_white(check_value(self.repetition_level(order, tp)), not exportable), color_white(check_value(self.best_rank(order, tp)), not exportable), _color_conclusion(self.conclusion(order, tp, **kwargs), not exportable))

        return self._table(l, "ICA results" + self._analysis.name())

    def dump_csv(self) -> str:
        """
        Returns the detector classification results with CSV (Comma Separated Value) format.

        :return: the CSV string for classification results.
        :rtype: str
        """
        s = "sep=,\n"
        for key in self.triggering_points():
            s += key
            for order in range(1, self._analysis.max_order() + 1):
                if key in self._results[order]:
                    s += "," + str(self.total_triggers(order, key)) # tot
                    s += "," + str(self.traces_triggers(order, key)) # tt
                    rp = self.local_repetition_level(order, key)
                    s += "," + str(rp if rp != options.max_int() else "N/A") # rp
                    s += "," + str(self.local_max_concurrent(order, key)) # mc
                    br = self.local_best_rank(order, key)
                    s += "," + str(br if br != options.max_int() else "N/A") # br
                else:
                    s += ", N/A, N/A, N/A, N/A, N/A"
            s += "\n"
        s += "\n\n"
        # Local conclusions
        for key in self.triggering_points():
            s += key 
            for order in range(1, self._analysis.max_order() + 1):
                if key in self._results[order]:
                    lc = self.local_conclusion(order, key)
                    if lc == CCPOClass.Inactive: s+= ", I"
                    elif lc == CCPOClass.Necessary: s += ", N"
                    else: s += ", R"
            s += "\n"
        return s

def compute_ranks(triggered: ty.List[str], det_list: ty.List[str]) -> ty.Dict[str, ty.Tuple[int, int, int]]:
    """
    Computes the ranks for the detectors in a trace.
    Returns the dict of values for each detector str as tuples (total_triggers, max_concurrent, best_rank).

    :param triggered: the list of detector string ID triggered in a trace.
    :type triggered: List[str]
    :param det_list: A list of detector to consider. detector not present are ignored.
    :type det_list: List[str]
    :return: The dictionary of ranks for each detector 
    :rtype:  Dict[str, Tuple[int, int, int]]
    """
    viewed = dict() # det -> (total_triggers, max_concurrent, best_rank)
    rank = 0

    for cm in triggered:
        tp = cm.ccp
        if tp in det_list:
            if tp not in viewed:
                viewed[tp] = (1, 1, rank)
            else:
                (tt, mc, best_rank) = viewed[tp]
                viewed[tp] = (tt + 1, mc + 1, best_rank)
            rank = rank + 1
    return viewed

def ccpo_classification_analysis(analysis: Analysis, **kwargs) -> CCPOClassificationResults:
    """ Returns a CCPOClassificationResults from an analysis object performing *CCPO Classification*.

    Requires that the *traces parsing step* (see :func:`~lazart.core.traces.traces_results`) has been computed for this `analysis`.

    This function uses log to display progress, reduce verbosity level to reduce output.

    **Kwargs**:
     - `satisfies_fct` (`Callable[[Trace], bool]`): boolean predicate determining if a trace should be considered. *default*: `lambda trace: trace.satisfies()`.
     - `det_list` (`List[str]`): the list of detectors to consider. *default*: `triggering_points(analysis)`.
     - `quiet` (`bool`): if true, output is reduced despite current verbosity level. *default*: `False`.

    :param analysis: the Analysis object from which the traces will be read.
    :type analysis: Analysis
    :return: the computed CCPO classification results.
    :rtype: CCPOClassificationResults
    """
    log.verbose("computing ccpo classification.")

    s_fct = args.get_or_throw(kwargs, "satisfies_fct", types.FunctionType, lambda trace: trace.termination_is(TerminationType.Correct))
    det_list = args.get_or_throw(kwargs, "det_list", list, None)
    if det_list is None:
        det_list = triggering_points(analysis)
    quiet = args.get_or_throw(kwargs, "quiet", bool, False)
    full_debug_mode = options.defines().get("lz.log.debug.ccpo_class", True) #False)
    
    results = []
    if not quiet: log.rinfo("starting ica")
    start = time.process_time()


    tr = traces_results(analysis) # verify trace step.
    max_order = analysis.max_order()

    # Count CM
    
    for order in range(max_order + 1):
        if order == 0: pass # do not compute for 0 faults.        

        # Create order n dict
        dict_n = dict()
        for cm in det_list:
            dict_n[cm] = (0, 0, options.max_int(), 0, options.max_int()) 
            # (total_triggers, trace_triggers, repetition_level, max_concurrent, best_rank)
            # best rank: sys.maxint = unset. => best_rank = best apparition order in traces.
            # max_concurrent: the maximum number of concurrent values.
        results.append(dict_n)
        tl = traces_list(analysis, order)

        if full_debug_mode: log._raw("order " + str(order) + " :" + str(len(tl)) + " traces")
        i = 0
        for trace in tl:
            if full_debug_mode: 
                tmp = []
                for det in trace.triggered():
                    tmp.append(det.ccp)

                log._raw("trace " + trace.name + "(" + str(trace.termination_type) + ": " + ",".join(tmp))            

            if not s_fct(trace):
                continue

            percentage = (i / len(tl)) * 100
            if not full_debug_mode and not quiet: log.rquiet("\rrunning ica: [{}/{}] {}% ({}/{})...".format(order, analysis.max_order(), int(percentage), i, len(tl)))
            cms = trace.triggered()
         
            if len(cms) == 0: 
                continue            

            # TODO, rank search could be optimized.

            # Compute values.
            viewed = compute_ranks(cms, det_list) # (total_triggers, max_concurrent, best_rank)

            if full_debug_mode: log._raw("\n\n" + str(trace))
            for key in viewed:
                (tot, trt, rl, mc, br) = dict_n[key]
                (trace_tot, trace_mc, trace_br) = viewed[key]

                if full_debug_mode: log._raw("old " + str(tot) + " " + str(trt) + " " + str(rl)  + " " + str(mc) + " " + str(br))
                if full_debug_mode: log._raw("modify " + str(key) + " " + str(trace_tot) + " " + str(trace_mc) + " " + str(trace_br))
                tot = tot + trace_tot
                trt = trt + 1
                rl = min(len(viewed.keys()) - 1, rl)
                mc = max(mc, trace_mc)
                br = min(br, trace_br)
                dict_n[key] = (tot, trt, rl, mc, br)
                if full_debug_mode: log._raw("new   " + str(tot) + " " + str(trt) + " " + str(rl)  + " " + str(mc) + " " + str(br))

    end = time.process_time()
    duration = end - start
    if not quiet: log.info("\rCCPO classification ended in {}.           ".format(_str_time(duration)))

    return CCPOClassificationResults(analysis, results, duration)


class CCPOClassificationStep(_ext.AnalysisStep):
    """
    Subclass of CCPOClassificationStep for the *CCPO analysis* classification step, see :class:`lazart._internal_.extensions.AnalysisStep`.
    """
    def __init__(self, analysis):  
        """
        **__init__ (ctor):** Constructs a new CCPOClassificationStep object associated to the specified analysis.

        :param analysis: the analysis of the step.
        :type analysis: Analysis
        """                        
        super(CCPOClassificationStep, self).__init__(analysis, constants.ccpo_classification_ext, _ext.StepID.Analysis)

    def execute(self, **kwargs):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        return ccpo_classification_analysis(self.analysis, **kwargs)

    def init(self):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        pass

    def save_disk(self):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        return super().save_disk()        

    def clean(self):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        _folder.remove_file(_folder.lazart_file(self.analysis.path(), constants.ccpo_classification_ext))

def ccpo_classification(analysis: Analysis, **kwargs) -> CCPOClassificationResults:
    """
    Returns an CCPOptimizationResults from an analysis object performing *CCPO Classification*.

    Requires that the *traces parsing step* (see :func:`~lazart.core.traces.traces_results`) and *CCPO classification step* (see :func:`~lazart.core.analysis.ccpo_classification`) has been computed for this `analysis`.
    
    Uses extension (ccpo_classification_ext, see :class:`lazart._internal_.extensions.AnalysisStep`) system to reuse disk/RAM cache if available.
    This function uses log to display progress, reduce verbosity level to reduce output.

    **Kwargs**:
     - `satisfies_fct` (`Callable[[Trace], bool]`): boolean predicate determining if a trace should be considered. *default*: `lambda trace: trace.satisfies()`.
     - `det_list` (`List[str]`): the list of detectors to consider. *default*: `triggering_points(analysis)`.
     - `quiet` (`bool`): if true, output is reduced despite current verbosity level. *default*: `False`.

    :param analysis: the Analysis object from which the traces will be read.
    :type analysis: Analysis
    :return: the computed CCPO classification results.
    :rtype: CCPOClassificationResults
    """
    step = _ext.get_step(CCPOClassificationStep(analysis))
    
    return step.get_results(**kwargs)

    


