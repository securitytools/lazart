""" The module *lazart.analysis.ccpo_selection* contains definitions for *selection step* of **Countermeasure Check Points Optimization**.

**Note: countermeasures analysis are not migrated to Lazart 4.0 for now and may by unusable.**

:since: 3.1
:note: **Not updated.**
:author: Etienne Boespflug
"""

# Python
import types
import typing as ty
from functools import reduce
import copy
from itertools import combinations

# Lazart
import lazart.logs as log
import lazart.constants as _constants
import lazart.options as options

# Internal
import lazart._internal_.extensions as _ext
import lazart._internal_.args as args
import lazart._internal_.folder as _folder

# Core
import lazart.core.traces as tr
from lazart.core.analysis import Analysis

# Analysis
from lazart.analysis.ccpo_classification import CCPOClassificationResults, ccpo_classification, triggering_points


def find_optimizable_traces(a: Analysis, detectors: ty.List[str], **kwargs) -> ty.List[tr.Trace]:
    """
    Returns the list of traces containing only repetitive detector triggers.

    **Kwargs**:
     - `satisfies_fct` (`Callable[[Trace], bool]`): boolean predicate determining if a trace should be considered. *default*: `lambda trace: trace.satisfies()`.
     - `max_order` (`int`): the maximum order considered for `GROUP_SERIES` metrics.. *default*: `a.max_order()`.

    :param a: the analysis.
    :type a: Analysis
    :param detectors: the list of detectors identifiers to consider.
    :type detectors: List[str]
    :return: the list of optimizable traces.
    :rtype: List[Trace]
    """    
    max_order = max(a.max_order(), args.get_or_throw(kwargs, "max_order", int, 0))
    at = tr.all_fault_traces(a)

    s_fct = args.get_or_throw(kwargs, "satisfies_fct", types.FunctionType, lambda trace: trace.termination_is(tr.TerminationType.Correct))

    trace_optimizable = []

    max_order = min(a.max_order() + 1, args.get_or_throw(kwargs, "max_order", int, a.max_order() + 1)) # TODO: strange magic here

    for trace in at:
        only_designed_det = True

        triggered = trace.triggered()

        if trace.order > max_order:
            continue

        if not s_fct(trace):
            continue

        if len(triggered) == 0:
            continue

        for det in triggered:
            tp = det.ccp
            
            if tp not in detectors:
                only_designed_det = False

        if only_designed_det:
            trace_optimizable.append(trace)

    return trace_optimizable 

class CCPOSelectionResults:
    """ 
    The CCPOSelectionResults class represents results for an CCPO *selection*.
    It holds the computed optimal set of detector to keep for each order and analysis information.
    """
    def __init__(self, analysis: Analysis, results: ty.List[ty.List[ty.List[str]]], duration: float, **kwargs):
        """
        **__init__ (ctor):**  Init the CCPOSelectionResults object with an analysis, the raw results and the duration information.

        :param analysis: the Analysis object from which the traces will be read.
        :type analysis: Analysis
        :param results: List of optimal sets for each order.
        :type results: List[List[List[str]]]
        :param duration: duration of the analysis computing in seconds.
        :type duration: float
        """
        self._analysis = analysis #: Analysis: The analysis object to which the *extension* is associated. See :func:`~lazart._internal_.extensions.register`.

        self._results = results #: List[List[List[str]]]: The optimal sets (i.e. the sets of detectors to be kept) for each order.
        self._duration = duration  #: float: The duration of the *ccpo*.

    def optimal_sets(self, order: int) -> ty.List[ty.List[str]]:
        """
        Returns the list of optimal sets for the specified order.

        :param order: the order (i.e. number of faults).
        :type order: int
        :return: the optimal sets for specified order.
        :rtype: List[List[str]]
        """
        return self._results[order]

    def duration(self) -> float:
        """
        Returns the duration in second of the attacks redundancy analysis computations.

        :return: duration of the analysis computing in second.
        :rtype: float
        """        
        return self._duration

    def __str__(self):
        """
        Returns the string representing the CCPOSelectionResults.

        :return: the string representation of the object.
        :rtype: str
        """    
        ret = ""
        for order in range(1, self._analysis.max_order() + 1):
            #print(self._results[order - 1])
            ret += "order {}: {}".format(order, [str(x) for x in self._results[order]]) + "\n"
        return ret


def _bruteforce_selection(analysis: Analysis, optimizable_traces: ty.List[tr.Trace], repetitives: ty.List[str], det_list: ty.List[str], weight_fct : ty.Callable[[str], int], **kwargs) -> ty.List[ty.List[str]]:
    """
    Performs brute-force selection and returns the computed optimal sets of detectors.

    :param analysis: the Analysis object from which the traces will be read.
    :type analysis: Analysis
    :param optimizable_traces: list of optimizable traces (containing only repetitive detector triggers).
    :type optimizable_traces: List[Trace]
    :param repetitives: list of repetitive detectors.
    :type repetitives: List[str]
    :param det_list: list of all detectors.
    :type det_list: List[str]
    :param weight_fct: the weight function for computing detector set weight.
    :type weight_fct: Callable[[str], int]
    :return: the computed optimal sets.
    :rtype: List[List[str]]
    """
    if len(repetitives) <= 1:
        log.error("selection requires at least 2 repetitives detectors (got {}).".format(len(repetitives)))

    full_debug_mode = options.defines().get("lz.log.debug.ccpo_selection", False)
    
    if options.defines().get("lz.log.debug.ccpo_selection.optimizables", False):
        log._raw("Optimizable traces:")
        for trace in optimizable_traces:
            log._raw("   " + trace.cstr(True))

    def verify(det_set: ty.List[str]) -> bool:
        """
        Test if a detector set is valid (i.e. if it doesn't induce additional attacks).

        :param det_set: The set of detector to be verified.
        :type det_set: List[str]
        :return: Returns `True` if the detectors set is valid, `False` otherwise.
        :rtype: bool
        """
        for trace in optimizable_traces:
            valid = False
            for det in trace.triggered():
                if det.ccp in det_set:
                    valid = True

            if not valid: return False     

        return True

    minimal_weight = None
    optimal_sets = [] # list(list(str))

    def set_weight(det_set: ty.List[str]) -> int: 
        """
        Returns the weight of a detector set.
        Uses the outer `weight_fct`.

        :param det_set: the detectors set.
        :type det_set: List[str]
        :return: the weight of detector set according to `weight_fct`.
        :rtype: int
        """
        total = 0
        for item in det_set:
            total += weight_fct(item)
        
        return total

    for i in range(1, len(repetitives)):
        if full_debug_mode: log._raw("size = {}".format(i))

        for comb in combinations(repetitives, i):
            if full_debug_mode: log._raw("set: " + ",".join(str(s) for s in comb))
            comb_l = list(comb)
            w = set_weight(comb_l)
            if minimal_weight is not None:
                if w > minimal_weight:
                    if full_debug_mode: log._raw("ignoring: w={}, m={}".format(w, m))
                    continue

            if verify(comb_l):
                if minimal_weight is not None and minimal_weight == w:
                    optimal_sets.append(comb_l)
                    if full_debug_mode: log._raw("  valid set")
                else: # mw > w
                    optimal_sets = [comb_l]
                    minimal_weight = w
                    if full_debug_mode: log._raw("  valid set (new mw: {}".format(minimal_weight))
            else:
                if full_debug_mode: log._raw("  invalid set")

    if len(optimal_sets) == 0:
        if full_debug_mode: log._raw("no optimal set found")
        return []

    return optimal_sets

def _selection_for_order(analysis: Analysis, classification: CCPOClassificationResults, weight_fct: ty.Callable[[str], int], order: int, **kwargs) -> ty.Tuple[ty.List[ty.List[str]], int]:
    """
    Performs selection for specified order.

    **Kwargs**:
     - `det_list` (`List[str]`): the list of detector to consider. *default*: `triggering_points(analysis)`.
     - `quiet` (`bool`): if true, output is reduced despite current verbosity level (**unused**). *default*: `False`.
     - *forwards kwargs to the actual selection function if called.*
     
    :param analysis: the Analysis object from which the traces will be read.
    :type analysis: Analysis
    :param classification: the classification results.
    :type classification: CCPOClassificationResults
    :param weight_fct: weight function for detectors.
    :type weight_fct: Callable[[str], int]
    :param order: order of the selection.
    :type order: int
    :return: the optimal sets (i.e. the lists of detectors to keep) and the duration (currently 0).
    :rtype: Tuple[List[List[str]], int]
    """
    det_list = args.get_or_throw(kwargs, "det_list", list, None)
    if det_list is None: det_list = triggering_points(analysis)
    quiet = args.get_or_throw(kwargs, "quiet", bool, False)
    full_debug_mode = options.defines().get("lz.log.debug.ccpo_selection", False)

    repetitives = classification.repetitives(order)
    inactives = classification.inactives(order)
    necessary = [x for x in det_list if x not in inactives and x not in repetitives]

    if full_debug_mode: log._raw("Selection (order={}):\nrepetitives: {}\ninactives: {}\nnecessary: {}".format(order, ",".join(repetitives), ",".join(inactives), ",".join(necessary)))

    optimal_sets = []
    
    if len(repetitives) == 0:
        # Easy case.
        if full_debug_mode: log._raw("No repetitives")
        optimal_sets.append([x for x in det_list if x not in inactives])
    elif len(repetitives) == 1:
        # Easy case too, the unique repetitive can be safely removed.
        if full_debug_mode: log._raw("1 repetitives")
        optimal_sets.append([x for x in det_list if x not in inactives and x not in repetitives])
    else:
        # Actual selection.
        if full_debug_mode: log._raw("2+ repetitives (actual selection)")
        optimizable_traces = find_optimizable_traces(analysis, repetitives, max_order=order)
        selected = _bruteforce_selection(analysis, optimizable_traces, repetitives, det_list, weight_fct, **kwargs)

        optimal_sets = [necessary + s for s in selected]
    
    if full_debug_mode: log._raw("optimal_set" + ",".join([str(x) for x in optimal_sets]))
    return (optimal_sets, 0)
            

def ccpo_selection_analysis(analysis: Analysis, classification: CCPOClassificationResults, **kwargs) -> CCPOSelectionResults:
    """ 
    Returns a CCPOSelectionResults from an analysis object performing *CCPO selection*.

    Requires that the *traces parsing step* (see :func:`~lazart.core.traces.traces_results`) and *CCPO classification step* (see :func:`~lazart.core.analysis.ccpo_classification`) has been computed for this `analysis`.

    This function uses log to display progress, reduce verbosity level to reduce output.

    **Kwargs**:
     - `det_list` (`List[str]`): the list of detector to consider. *default*: `triggering_points(analysis)`.
     - `weight_fct` (`Callable[[str], int]`): weight_fct: the weight function for computing detector set weight. *default*: `lambda det: 1`.
     - `quiet` (`bool`): if true, output is reduced despite current verbosity level (**unused**). *default*: `False`.
     - *forwards kwargs to the actual selection function if called.*

    :param analysis: the Analysis object from which the traces will be read.
    :type analysis: Analysis
    :param classification: the classification results.
    :type classification: CCPOClassificationResults
    :return: the computed CCPO selection results.
    :rtype: CCPOSelectionResults
    """

    if "det_list" in kwargs and kwargs.get("det_list") is None:
        kwargs["det_list"] = triggering_points(analysis)

    weight_fct = args.get_or_throw(kwargs, "weight_fct", types.FunctionType, lambda det: 1)
    
    results = [None]
    durations = [None]
    for order in range(1, analysis.max_order() + 1):
        (r, duration) = _selection_for_order(analysis, classification, weight_fct, order, **kwargs) 
        results.append(r)
        durations.append(duration)

    return CCPOSelectionResults(analysis, results, durations)

class CCPOSelectionStep(_ext.AnalysisStep):
    """
    Subclass of AnalysisStep for the *CPPO selection step*, see :class:`lazart._internal_.extensions.AnalysisStep`.
    """
    def __init__(self, analysis):        
        """
        **__init__ (ctor):** Constructs a new CCPOSelectionStep object associated to the specified analysis.

        :param analysis: the analysis of the step.
        :type analysis: Analysis
        """            
        super(CCPOSelectionStep, self).__init__(analysis, _constants.ccpo_selection_ext,  _ext.StepID.Analysis)

    def execute(self, **kwargs):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        return ccpo_selection_analysis(self.analysis, **kwargs)

    def init(self):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        pass

    def save_disk(self):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        return super().save_disk()
    
    def clean(self):
        """ See :class:`lazart._internal_.extensions.AnalysisStep`. """
        _folder.remove_file(_folder.lazart_file(self.analysis.path(), _constants.ccpo_selection_ext))
        log.dev("cleaning selection...")

def ccpo_selection(analysis: Analysis, classification_, **kwargs) -> CCPOSelectionResults:
    """ 
    Returns a CCPOSelectionResults from an analysis object performing *CCPO selection*.

    Requires that the *traces parsing step* (see :func:`~lazart.core.traces.traces_results`) and *CCPO classification step* (see :func:`~lazart.core.analysis.ccpo_classification`) has been computed for this `analysis`.
    
    Uses extension (ccpo_selection_ext, see :class:`lazart._internal_.extensions.AnalysisStep`) system to reuse disk/RAM cache if available.
    This function uses log to display progress, reduce verbosity level to reduce output.  

    **Kwargs**:
     - `det_list` (`List[str]`): the list of detectors to consider. *default*: `triggering_points(analysis)`.
     - `weight_fct` (`Callable[[str], int]`): weight_fct: the weight function for computing detector set weight. *default*: `lambda det: 1`.
     - `quiet` (`bool`): if true, output is reduced despite current verbosity level (**unused**). *default*: `False`.

    :param analysis: the Analysis object from which the traces will be read.
    :type analysis: Analysis
    :param classification: the classification results.
    :type classification: CCPOClassificationResults
    :return: the computed CCPO selection results.
    :rtype: CCPOSelectionResults
    """
    step = _ext.get_step(CCPOSelectionStep(analysis))

    return step.get_results(**dict(kwargs, classification=classification_))

class Node:
    """
    The Node class correspond to a Node of the Selection graph.

    TODO: To be imported from Lazart 3.

    :beta-feature: Yes.
    """
    def __init__(self, total_set, dets, traces, S):
        self._total_set = total_set
        self._dets = dets
        self._subnodes = []
        self._traces = traces
        self._S = S

    def weight(self):
        return [reduce(lambda x, y: self._S(x) + self._S(y), det) for det in self._dets]

    def min_weight(self, S):
        if self.valid():
            return self, self.weight()

        pair = (None, float("inf"))
        idx = self._total_set.index(self._dets[-1])

        for i in range (idx, len(self._total_set)):
            new_set = copy.deepcopy(self._dets)
            new_set.append(self._total_set[i])

            node = Node(self._total_set, new_set, traces, S)

            min_node, min_weight = node.min_weight(S)

        if min_weight < pair[1]:
            pair = min_node, min_weight
        return pair

    def valid(self):
        for trace in self._traces:
            for det in self._dets:
                if det in trace.triggered():
                    return True
        return False


def det_selection(repetitive_dets, traces, S):
    """
    TODO: To be imported from Lazart 3.

    :param repetitive_dets: _description_
    :type repetitive_dets: _type_
    :param traces: _description_
    :type traces: _type_
    :param S: _description_
    :type S: _type_
    """
    log.warning("Beta feature")
    base = copy.deepcopy(repetitive_dets)

    pair = (None, float("inf"))
    for R in base:
        node = Node(repetitive_dets, [R], traces, S)

        (min_node, min_weight) = node.min_weight(S)

        if min_weight < pair[1]:
            pair = min_node, min_weight

    if pair[0] is None:
        print("Bad")
    else:
        print("Best set: " + str(pair[0]) + " [" + str(pair[1]) + "]")