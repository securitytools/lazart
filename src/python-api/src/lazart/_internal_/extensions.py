""" The module *lazart._internal_.extension* provides features for *analysis extensions* such as i/o and utilities.

:since: 3.0
:author: Etienne Boespflug 
"""

# Python
import pickle
import typing as ty
from enum import IntEnum
from os import path

# Internal
import lazart._internal_.folder as folder

# Lazart
import lazart.logs as log


def has_extension(analysis, extension_name: str) -> bool:
    """
    Returns `True` if the analysis has the specified extension, `False` otherwise.

    :param analysis: The analysis to check.
    :type analysis: :class:`lazart.core.analysis.Analysis`
    :param extension_name: the name of the extension.
    :type extension_name: str
    :return: `True` if the analysis has the specified extension, `False` otherwise.
    :rtype: bool
    """
    attr = "_" + extension_name
    return hasattr(analysis, attr) and getattr(analysis, attr) is not None

def get_extension(analysis, extension_name: str) -> object:
    """
    Returns the extension object with the specified name in the analysis. 
    Returns `None` if no extension can be found with this name.

    :param analysis: the analysis containing the extension.
    :type analysis: :class:`lazart.core.analysis.Analysis`
    :param extension_name: the name of the extension.
    :type extension_name: str
    :return: the extension object with the specified name, `None` otherwise.
    :rtype: object
    """
    return getattr(analysis, "_" + extension_name, None)

def clean_extension(analysis, extension_name: str, full: bool = False):
    """
    Set the extension object of specified name to `None`.

    :param analysis: the analysis containing the extension.
    :type analysis: :class:`lazart.core.analysis.Analysis`
    :param extension_name: the name of the extension.
    :type extension_name: str
    """    
    if full:
        delattr(analysis, "_" + extension_name)
    else:
        setattr(analysis, "_" + extension_name, None)

def pickle_to(obj: object, filepath: str):
    """
    Saves an object to the specified path. The file is overridden.

    :param obj: the object to be saved.
    :type obj: object
    :param filepath: the path in which the object will be saved.
    :type filepath: str
    """
    serial = pickle.dumps(obj)
    with open(filepath, "wb") as file:
        file.write(serial)

def unpickle(filepath: str) -> object:
    """
    Loads the object saved to the specified path.

    :param filepath: the path from which the object will be saved.
    :type filepath: str
    :return: the loaded object.
    :rtype: obj
    """
    with open(filepath, "rb") as file:
        ret = pickle.load(file)
    return ret

def get_analysis_step_id(analysis) -> int:
    """ Returns the stepID for a new step for the specified analysis (i.e. the incremented step ID of the last extension).

    :param analysis: the analysis in which the extension will be stored.
    :type analysis: Analysis
    :return: the stepID for a new analysis extension.
    :rtype: int
    """
    if not analysis._extensions:
        log.error("calling without any extensions.")
    return analysis._extensions[-1].step_id + 1

class StepID(IntEnum):
    """
    This enumeration corresponds to the step ID of the standards steps of Lazart. Analysis step after `Analysis` are incremented.
    """
    Start = 0 
    """ Corresponds to the start step of an analysis (initialization). """
    Compile = 1000
    """ Corresponds to the *compilation step* of an analysis. """
    Run = 2000
    """ Corresponds to the *mutation and DSE step* of an analysis. """
    Traces = 3000
    """ Corresponds to the *traces parsing step* of an analysis. """
    Analysis = 4000
    """ Corresponds to the start ID value for analysis steps of an analysis. """

class AnalysisStep():
    """
    Base class for each step of an Analysis with Lazart. 
    
    This class aims to be inherited to create a new step allowing to enable auto-saving of the step inside Lazart's general function and recover an analysis to a specific step.
    """
    def __init__(self, analysis, extension_name : str, step_id : int):
        """
        **__init__ (ctor):** Constructs a new AnalysisStep object with an analysis, the extension name and the step id.

        :param analysis: the analysis to which the step is associated.
        :type analysis: :class:`lazart.core.analysis.Analysis`
        :param extension_name: the name of the step.
        :type extension_name: str
        :param step_id: the priority identifier of the step.
        :type step_id: int
        """
        self.analysis = analysis #:class:`lazart.core.analysis.Analysis`: Analysis: The analysis object to which the *extension* is associated. See :func:`~lazart._internal_.extensions.register`.
        self.name = extension_name #:str: name of the step used for display and caches/files names.
        self.step_id = step_id #int: the priority identifier of the step determining its execution order. Mainly used to allows user to recover analysis to a specific step.
        if step_id == StepID.Analysis:
            self.step_id = get_analysis_step_id(analysis)

    def has_results(self) -> bool:
        """
        Determines if the analysis has a result object (for this step) cached inside its analysis object.

        :return: true if a result object is cached, false otherwise.
        :rtype: bool
        """
        attr = "_" + self.name
        return hasattr(self.analysis, attr) and getattr(self.analysis, attr) is not None
    
    def get_results(self, **kwargs) -> ty.Any:
        """
        Returns the results object of the step cached in the analysis, if any. Otherwise, the step is executed, registered as analysis extension and saved to the disk, returning the newly created object.

        :return: the result object if cached, `None` otherwise.
        :rtype: ty.Any
        """
        if self.has_results():
            return getattr(self.analysis, "_" + self.name, None)

        self.init()
        #self.clean() # TODO ?
        obj = self.execute(**kwargs)
        self.register(obj)
        self.analysis.save()
        self.save_disk()

        return obj

    def execute(self, **kwargs) -> object:
        """
        Abstract method corresponding to the execution of the step with the specified kwargs arguments and returns a result object for the step.
        This method can be left not overridden if required.
        """
        pass

    def register(self, obj: ty.Any):
        """
        Register the extension inside it's analysis, adding it to the extension list, and cache the extension object `obj` as private member of the analysis object.

        :param obj: the extension object to be registered.
        :type obj: Any
        :raises Exception: if the extension is already registered.
        """
        ext_attr = "_" + self.name
        if hasattr(self.analysis, ext_attr) and getattr(self.analysis, ext_attr) is not None:
            raise Exception("trying to register an existing extension '{}'.".format(self.name))
        if self not in self.analysis._extensions:
            self.analysis._extensions.append(self)
        obj._analysis = self.analysis
        setattr(self.analysis, ext_attr, obj) # Caching

    ## DISK
    def load_disk(self, restart_from: int) -> bool:
        """
        Load the extension from the disk if it step ID is at least equal to `restart_from`.

        :param restart_from: the id of the step to recover the analysis.
        :type restart_from: int
        :return: true if the load was performed, false otherwise.
        :rtype: bool
        """
        if restart_from <= self.step_id: 
            log.verbose("ignoring cached data for " + self.name)
            self.clean()
            return False

        ext_attr = "_" + self.name
        filename = folder.lazart_file(self.analysis.path(), self.name)

        if not path.exists(filename):
            setattr(self.analysis, ext_attr, None)
            return False

        obj = unpickle(filename)
        setattr(self.analysis, ext_attr, obj)
        obj._analysis = self.analysis

        if self not in self.analysis._extensions:
            log.warning("unregistered extensions {}".format(self.name))

        return True

    def save_disk(self):
        """
        Save the extension to the disk inside the analysis folder.
        """
        if not self.has_results():
            log.error("already has result ?") # TODO ? 
        obj = self.get_results()
        obj._analysis = None

        pickle_to(obj, folder.lazart_file(self.analysis.path(), self.name))

        obj._analysis = self.analysis

        log.dev("saved disk : " + str(self))        

    def unbind(self):
        # TODO
        pass

    def rebind(self):
        # TODO
        pass
    
    # Folders
    def init(self):
        """ Init all files specific to the step in the analysis folder. """
        pass

    def clean(self):
        """ Clean all files specific to the step in the analysis folder. """
        pass

    def __str__(self) -> str:
            """
            Returns the string representing the object.

            :return: the string representation of the object.
            :rtype: str
            """                      
            return "step:{};id:{}".format(self.name, self.step_id)
    
def get_step(def_step_obj : AnalysisStep, **kwargs) -> AnalysisStep:
    """
    Returns the step object corresponding to the requested step type, if no corresponding step is found, the step object is registered as new extensions.

    :param def_step_obj: a step object corresponding to the step to get.
    :type def_step_obj: AnalysisStep
    :return: the corresponding step object if any, the newly registered `def_step_obj` encore.
    :rtype: AnalysisStep
    """

    for step in def_step_obj.analysis._extensions:
        if type(step) == type(def_step_obj):
            return step
    
    def_step_obj.analysis._extensions.append(def_step_obj)
    return def_step_obj