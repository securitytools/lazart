""" The module *lazart._internal_.util* contains miscellaneous utility functions for **Lazart**.

:since: 3.0
:author: Etienne Boespflug 
"""

# Python
import copy
import subprocess as sp
import sys
import typing as ty


def exec_cmd(cmds : ty.List[str], **kwargs) -> int:
    """
    Executes the specified commands and returns the *return code*.

    **Kwargs**:
     - `stdout`: if defined, the output of the command is displayed on console.

    :param cmds: list of strings corresponding to the command to be executed.
    :type cmds: List[str]
    :return: the *return code* of the command execution.
    :rtype: int
    """
    if not hasattr(kwargs, "stdout"):
        kwargs["stdout"] = sp.PIPE
    child = sp.Popen(cmds, **kwargs)
    child.communicate()[0]
    return child.returncode

def exec_cmd_pair_wait(cmd: ty.List[str]) -> ty.Tuple[int, str]:
    """
    Executes the command `cmd` and return the pair (return-code, output).
    Include wait call.

    :param cmd: the command to be executed.
    :type cmd: List[str]
    :return: the pair (return-code, output).
    :rtype: Tuple[int, str]
    """
    pop = sp.Popen(cmd, stderr=sp.STDOUT, stdout=sp.PIPE)
    pop.wait()
    return (pop.returncode, pop.communicate()[0])

def exec_cmd_pair(cmds : ty.List[str], **kwargs) -> int:
    """
    Executes the specified command and returns the pair (return-code, output).

    **Kwargs**:
     - `stdout`: if defined, the output of the command is displayed on console.

    :param cmds: list of strings corresponding to the command to be executed.
    :type cmds: List[str]
    :return: the pair (return-code, output).
    :rtype: Tuple[int, str]
    """
    if not hasattr(kwargs, "stdout"):
        kwargs["stdout"] = sp.PIPE
    child = sp.Popen(cmds, **kwargs)
    ret = child.communicate()[0]
    return (child.returncode, ret)

def check_output(cmds: ty.List[str]) -> str:
    """
    Returns the result of `subprocess.check_output(cmds)` if the command succeed, otherwise returns None.

    :param cmds: list of strings corresponding to the command to be executed.
    :type cmds: List[str]
    :return: the output of the command execution or None.
    :rtype: str
    """
    try:
        return sp.check_output(cmds).decode(sys.stdout.encoding)
    except sp.CalledProcessError:
        return None

def remove_kwargs(*args, **kwargs) -> dict:
    """
    Removes the specified keys in the keyword arguments dictionary and returns it.

    **Kwargs**: any `key` to remove. Values ignored.

    :param args: the list of key to be removed from the keywords arguments. 
    :type args: List[str]
    :return: the modified dictionary
    :rtype: dict
    """
    
    for arg in args:
        kwargs.pop(arg, None)
    return kwargs

def parse_klee_duration(s: str) -> float:
    """
    Parses string on format `HH:MM:SS` to a number of second.

    :param s: a string on format `HH:MM::SS`.
    :type s: str
    :return: the parsed number of second.
    :rtype: float
    """
    args = s.split(":")
    if len(args) != 3:
        raise ValueError("invalid format")

    v = 60 * 60 * int(args[0]) + 60 * int(args[1]) + int(args[2])

    return float(v) 

def str_time(sec: float, numeric: bool = False) -> str:
    """
    Returns a string from the specified number of second (float). Format with 'us', 'ms', 's', 'min' and 'h' depending on the value.

    :param sec: the duration (in second) to be converted to string.
    :type sec: float
    :param numeric: if True, hours, minutes and seconds are separated by semi-colons ':' instead of abbreviations, defaults to False
    :type numeric: bool, optional
    :return: the string corresponding to the duration.
    :rtype: str
    """
    if sec is None:
        return "nan"
    if sec < 0.001:
        return str(int(sec * 1000000)) + "us"
    if sec < 1:
        return str(int(sec * 1000)) + "ms"
    if sec < 60:
        return str(int(sec)) + "s"
    min = int(int(sec) / 60)
    if min < 60:
        if numeric:
            return "{}:{}".format(min, int(sec % 60))
        return "{}min and {}s".format(min, int(sec % 60))
    if numeric:
        return "{}:{}:{}".format(int(sec / 3600), min % 60, int(sec % 60))
    return "{}h {}min {}s".format(int(sec / 3600), min % 60, int(sec % 60))

def fixed_str_int(value: int, size: int) -> str:
    """
    Returns the string from integer value with fixed size.

    :param value: the integer to be converted to a fixed size string.
    :type value: int
    :param size: the size of the string.
    :type size: int
    :return: the fixed size string from the integer.
    :rtype: str
    """
    fmt = "0" + str(size) + "d"
    return format(value, fmt)

def str_time_fix(sec: float, show_ms: bool = False) -> str:
    """
    Returns a string from the specified number of second (float). Format with 'us', 'ms', 's', 'min' and 'h' depending on the value.

    :param sec: the duration (in second) to be converted to string.
    :type sec: float
    :param show_ms: determines if milliseconds are displayed, defaults to False
    :type show_ms: bool, optional
    :return: the string corresponding to the duration.
    :rtype: str
    """
    ms = int(sec * 1000.0) % 1000
    s = int(sec) % 60
    m = int(int(sec) / 60) % 60
    h = int(int(sec) / 3600)

    ms_str = "." + f"{ms:03d}" if show_ms else ""
    min_str = "" if m == 0 and h == 0 else f"{m:02d}:"
    h_str = "" if h == 0 else  f"{h}:"
    return f"{h_str}{min_str}{s:02d}{ms_str}"

def create_typed_dict(ty : str, params: dict) -> dict:
    """
    Returns the dictionary `params` adding the field "type" to the specified string `ty`.

    :param ty: the string description of the type.
    :type ty: str
    :param params: the dictionary to be changed.
    :type params: dict
    :return: the modified dictionary with `ty` as "type" key's value.
    :rtype: dict
    """
    ret = copy.deepcopy(params)
    ret["type"] = ty
    return ret