""" The module *lazart._internal_.color* provides global values for printing colored text on unix systems.

:since: 2.0
:author: Etienne Boespflug 
"""

def _cls_modifier(code: int) -> str:
    """
    Returns the string corresponding to the unix terminal console modifier for the specified `code`.
    *format*: `\033[CODE]m`.

    :param code: the code of the console modifier.
    :type code: int
    :return: the string for the specified code.
    :rtype: str
    """
    return "\033[" + str(code) + "m"

# Controls

reset = _cls_modifier(0)
""" str: String code which resets the console attributes. """
bold = _cls_modifier(1)
""" str: String code enabling *bold* font style which change the color. """
dark = _cls_modifier(2)
""" str: String code enabling *dark* font style. """
italic = _cls_modifier(3)
""" str: String code enabling *italic* font style. """
underline = _cls_modifier(4)
""" str: String code enabling *underline* font style. """
blink = _cls_modifier(5)
""" str: String code enabling *blink* font style. """
inverse = _cls_modifier(7)
""" str: String code enabling *inverse* font style."""
bold_off = _cls_modifier(24)
""" str: String code disabling *bold* font style which change the color. """
dark_off = _cls_modifier(22)
""" str: String code disabling *dark* font style. """
italic_off = _cls_modifier(23)
""" str: String code disabling *italic* font style. """
underline_off = _cls_modifier(21)
""" str: String code disabling *underline* font style. """
blink_off = _cls_modifier(25)
""" str: String code disabling *blink* font style. """
inverse_off = _cls_modifier(27)
""" str: String code disabling *inverse* font style."""

# Font colors

black = _cls_modifier(30) + bold_off
""" str: String code which set font color to *black*. Default color. """
dark_gray = _cls_modifier(30) + bold
""" str: String code which set font color to *dark gray*. """
red = _cls_modifier(31) + bold_off
""" str: String code which set font color to *red*. """
rose = _cls_modifier(31) + bold
""" str: String code which set font color to *rose*. """
green = _cls_modifier(32) + bold_off
""" str: String code which set font color to *green*. """
light_green = _cls_modifier(32) + bold
""" str: String code which set font color to *light green*. """
brown = _cls_modifier(33) + bold_off
""" str: String code which set font color to *brown*. """
yellow = _cls_modifier(33) + bold
""" str: String code which set font color to *yellow*. """
blue = _cls_modifier(34) + bold_off
""" str: String code which set font color to *blue*. """
light_blue = _cls_modifier(34) + bold
""" str: String code which set font color to *light blue*. """
magenta = _cls_modifier(35) + bold_off
""" str: String code which set font color to *magenta*. """
purple = _cls_modifier(35) + bold
""" str: String code which set font color to *purple*. """
cyan = _cls_modifier(36) + bold_off
""" str: String code which set font color to *cyan*. """
light_cyan = _cls_modifier(36) + bold
""" str: String code which set font color to *light cyan*. """
white = _cls_modifier(37) + bold_off
""" str: String code which set font color to *white*. """
light_gray = _cls_modifier(37) + bold
""" str: String code which set font color to *light gray*. """
fg_reset = _cls_modifier(39) + bold
""" str: String code which reset font color."""

# Background colors

bg_black = _cls_modifier(40)
""" str: String code which set background color to *black*. Default color. """
bg_red = _cls_modifier(41)
""" str: String code which set background color to *red*. """
bg_green = _cls_modifier(42)
""" str: String code which set background color to *green*. """
bg_yellow = _cls_modifier(43)
""" str: String code which set background color to *yellow*. """
bg_blue = _cls_modifier(44)
""" str: String code which set background color to *blue*. """
bg_magenta = _cls_modifier(45)
""" str: String code which set background color to *magenta*. """
bg_cyan = _cls_modifier(46)
""" str: String code which set background color to *cyan*. """
bg_white = _cls_modifier(47)
""" str: String code which set background color to *white*. """
bg_reset = _cls_modifier(49)
""" str: String code which reset background color."""
