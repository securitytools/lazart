""" The module *lazart._internal_.folder* provides definitions for manipulating **Lazart**'s analysis folders.

:since: 3.0
:author: Etienne Boespflug 
"""

# Python
import os
import os.path as path
import shutil

# Lazart
import lazart.constants as constants
import lazart.logs as log
import lazart.options as options


class Dir:
    """
    The Dir class stores a directory to revert current working directory.
    """
    def __init__(self, old_dir: str):
        """
        **__init__ (ctor):** Constructs a new Dir object with the specified current directory.

        :param old_dir: the directory which can be reverted as current directory.
        :type old_dir: str
        """
        self.old_dir = old_dir #: str: Old directory stored for revert operation.

    def revert(self):
        """
        Reverts the current directory to the one saved at the object construction.
        """
        os.chdir(self.old_dir)

class RDir:
    """
    The RDir class stores a directory to revert the current working directory after a with block.
    """
    def __init__(self, old_dir: str):
        """
        **__init__ (ctor):** Constructs a new RDir object with the specified current directory.

        :param old_dir: the directory which can be reverted as the current directory.
        :type old_dir: str
        """
        self.old_dir = old_dir  # Old directory stored for revert operation.

    def __enter__(self):
        """
        Called when entering the "with" block. It saves the current directory and
        changes to the stored old directory.
        """
        self.saved_dir = os.getcwd()  # Save the current directory
        os.chdir(self.old_dir)  # Change to the old directory
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        """
        Called when exiting the "with" block. It reverts the current directory
        back to the saved directory.
        """
        os.chdir(self.saved_dir)  # Revert to the saved directory

def check_dir(dir: str) -> Dir:
    """
    Changes the current directory to `dir` and return a :class:`Dir` object for revert.

    :param dir: the directory to set to current directory.
    :type dir: str
    :return: the `Dir` object corresponding to the old current directory.
    :rtype: Dir
    """
    old_dir = os.getcwd()
    make_folder(dir)
    os.chdir(dir)
    return Dir(old_dir)

def make_folder(dir: str):
    """
    Creates the specified folder and if it doesn't exists.

    :param dir: the folder to create.
    :type dir: str
    """
    if not os.path.exists(dir):
        os.makedirs(dir)

def lazart_dir(root_folder: str) -> str:
    """
    Returns the *Lazart*'s directory (.lazart) for the specified root folder. 

    :param root_folder: the *root analysis folder*.
    :type root_folder: str
    :return: the path of *Lazart*'s directory in the root folder.
    :rtype: str
    """
    return path.join(root_folder, constants.lazart_dir)

def lazart_file(root_folder: str, filename: str) -> str:
    """
    Returns the path of the *Lazart* file directory (.lazart/filename) for the specified root folder. 

    :param root_folder: the *root analysis folder*.
    :type root_folder: str
    :param filename: the name of the file.
    :type filename: str
    :return: the path of *Lazart*'s directory in the root folder.
    :rtype: str
    """
    return path.join(path.join(root_folder, constants.lazart_dir), filename)

def make_lazart_dir(root_folder: str):
    """
    Creates the *Lazart*'s directory (.lazart) for the specified root folder. 

    :param root_folder: the *root analysis folder*.
    :type root_folder: str
    """
    ld = lazart_dir(root_folder)
    make_folder(ld)

def make_analysis_folder(analysis, **kwargs):
    """
    Creates the *root analysis folder* and .gitignore file.

    **Kwargs**:
         - `archive` (`bool`): Prevents *Lazart* to add a .gitignore to the analysis. All the results will be included in the version control. *default*: `False`

    :param analysis: the analysis corresponding to the folder to be created.
    :type analysis: Analysis
    """
    make_folder(analysis.path())

    # Lazart ignore analysis by default, report always available.
    if not kwargs.get("archive", options.archive()):
        with open(os.path.join(analysis.path(), ".gitignore"), 'w') as file:
            file.write("*\n*/\n!.gitignore\n")

def remove_file(path: str):
    """
    Remove the file at the specified path if it exists.

    :param path: the path to the file to be removed.
    :type path: str
    """
    if os.path.exists(path):
        os.remove(path)

def remove_folder(folder: str):
    """
    Remove recursively a folder, without throwing if an error occurred. 

    :param folder: the folder to be removed.
    :type folder: str
    """
    try:
        shutil.rmtree(folder)
    except OSError as e:
        pass 

def remove_item(item: str):
    """
    Remove a file or a folder without throwing an exception in case of error.

    :param item: the file or folder path.
    :type item: str
    """
    if path.isfile(item):
        remove_file(item)
    elif path.isdir(item):
        remove_folder(item)
    else:
        log.warning("unknown file type: {}.".format(item))