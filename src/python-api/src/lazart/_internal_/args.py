""" The module *lazart._internal_.args* provides definitions for function arguments verification and user display.

:since: 3.0
:author: Etienne Boespflug 
"""

# Python
import types
import typing as ty

import lazart.constants as constants

# Lazart
import lazart.logs as log


class ListType():
    """
    The ListType class represents a parameterized list with fixed-type.
    """
    def __init__(self, t: type):
        """
        **__init__ (ctor):** Initializes the ListType object with the specified type parameter.

        :param t: the type items of the list.
        :type t: type
        """
        self.type = t #: type: The type parameter of the list type.

    def __str__(self) -> str:
        """
        Returns the string representing the ListType.

        :return: the string representation of the object.
        :rtype: str
        """        
        return "list({})".format(self.type)

class DictType():
    """
    The DictType class represents a parameterized dictionary with fixed-type.
    """
    def __init__(self, key: type, value: type):
        """
        **__init__ (ctor):** Initializes the DictType object with the specified type parameters for the key and the value.

        :param key: the type of the dictionary key.
        :type key: type
        :param value: the type of the dictionary values.
        :type value: type
        """
        self.key = key #: type: The type parameter of the dictionary key.
        self.value = value #: type: The type parameter of the dictionary value.

    def __str__(self) -> str:
        """
        Returns the string representing the DictType.

        :return: the string representation of the object.
        :rtype: str
        """      
        return "dict({}, {})".format(self.key, self.value)

def is_type(obj, required_type : ty.Union[types.FunctionType, ListType, DictType, object], **kwargs) -> bool:
    """
    Returns `True` if `obj` is of type `required_type`, `False` otherwise. 
    Uses special classes to represent complex types.

    **Kwargs**:
         - `deep_check` (`bool`): if `True`, the function is recursively called on container to verify items's types. *default*: `True`

    :param obj: the object to check.
    :type obj: object
    :param required_type: the type required for the object
    :type required_type: Union[types.FunctionType, ListType, DictType, object]
    :return: Returns `True` if `obj` is of type `required_type`, `False` otherwise.
    :rtype: bool
    """
    deep = kwargs.get("deep_check", True)

    if required_type is None: # No check
        return True
    # FunctionType
    if required_type is types.FunctionType:
        return callable(obj)
    # ListType
    if isinstance(required_type, ListType):        
        if not isinstance(obj, list): 
            return False
        if deep:
                for o in obj:
                    if not is_type(obj, required_type.type, deep_check=deep):
                        return False
        return True    
    
    # DictType
    if isinstance(required_type, DictType):
        if not isinstance(obj, dict):
            return False
        if deep:
            for k, v in obj.items():
                if not is_type(k, required_type.key, deep_check=deep):
                    return False
                if not is_type(v, required_type.value, deep_check=deep):
                    return False
        return True

    return isinstance(type(obj), type(required_type))

def expect_type(obj: object, required_type : ty.Union[types.FunctionType, ListType, DictType, object], key: str, message: str = "", **kwargs):
    """
    Verify that `obj` is of type `required_type`, and throw an error otherwise. 
    Uses special classes to represent complex types.

    **Kwargs**:
         - `deep_check` (`bool`): if `True`, the function is recursively called on container to verify items's types. *default*: `True`

    :param obj: the object to check.
    :type obj: object
    :param required_type: the type required for the object
    :type required_type: Union[types.FunctionType, ListType, DictType, object]
    :param key: the key of the argument.
    :type key: str
    :param message: the message to display if the type is incorrect. *defaults*: `""`
    :type message: str, optional
    """
    deep = kwargs.get("deep_check", True)

    if not is_type(obj, required_type, deep_check=deep):
        msg = "invalid type '{}' for '{}', expected '{}' {}.".format(type(obj), key, required_type, message)
        if kwargs.get("warning", False):
            log.warning(msg)
        else:
            log.error(msg)


def get_or_throw(d: dict, key: str, required_type: ty.Union[types.FunctionType, ListType, DictType, object], default: object, **kwargs) -> object:
    """
    Returns the value in the dictionary `kwargs` for the specified `key` if the type is correct, throws an error otherwise.

    **Kwargs**:
         - `deep_check` (`bool`): if `True`, `None` is considered as valid value. *default*: `False`

    :param kwargs: the dictionary in which the value is get.
    :type kwargs: dict
    :param key: the key of the argument.
    :type key: str
    :param required_type: the type required for the object
    :type required_type: Union[types.FunctionType, ListType, DictType, object]
    :param default: the default value if the key is not in the dictionary.
    :type default: object
    :return: the value of the argument if present, the default value otherwise.
    :rtype: object
    """
    none_allowed = kwargs.get("none_allowed", False)

    if not key in d:
        return default
    
    ret = d[key]

    if none_allowed and ret == None:
        pass
    else:
        expect_type(ret, required_type, key, ": when parsing kwargs argument")

    return ret

def forward(kwargs: dict, key: str, default = None) -> object:
    """
    Returns the value of `key` in dict if any, and the default value otherwise.

    :param kwargs: the dictionary.
    :type kwargs: dict
    :param key: the name of the key.
    :type key: str
    :param default: the default value for the return, defaults to None
    :type default: _type_, optional
    :return: `kwargs[key]` if present, `default` otherwise.
    :rtype: object
    """
    if not key in kwargs:
        return default
    return kwargs[key]

def unknown_fields(kwargs: dict, keys : ty.List[str]) -> ty.List[str]:
    """
    Search if all key inside `kwargs` are valid fields fields depending on the list of valid fields `key` and returns the list of unknown fields.

    :param kwargs: the dictionary of fields. 
    :type kwargs: dict
    :param keys: the list of field names allowed.
    :type keys: ty.List[str]
    :return: Returns the list of key in `kwargs` that are not in `keys`.
    :rtype: ty.List[str]
    """
    unknown = []
    for key in kwargs:
        if not key in keys:
            unknown.append(key)

    return unknown

def warning_unknown_fields(kwargs: dict, keys : ty.List[str], msg: str) -> bool:
    """
    Search if all key inside `kwargs` are valid fields fields depending on the list of valid fields `key`.
    Print a warning to stdout to signal any invalid field to the user, using `msg` as description of the current task.

    :param kwargs: the dictionary of fields. 
    :type kwargs: dict
    :param keys: the list of field names allowed.
    :type keys: ty.List[str]
    :param msg: the description of the current task used for warning display purposes.
    :type msg: str
    :return: Returns true if no unknown fields were found, false otherwise.
    :rtype: bool
    """
    res = unknown_fields(kwargs, keys)
    if len(res) > 0:
        log.warning("unexpected field {} in {}.".format("[" + ",".join(res) + "]", msg))
        return False
    return True

# Usual arguments.

def get_satisfies_function(kwargs: dict, default: object) -> object:
    """
    Helper for :func:`get_or_throw` for common `satisfies_fun` argument.

    :param kwargs: the dictionary of keyword arguments.
    :type kwargs: dict
    :param default: the default value if no argument is present in the dictionary.
    :type default: object
    :return: the value if the argument is in the dictionary, the default otherwise.
    :rtype: object
    """
    return get_or_throw(kwargs, constants.arg_satisfies_function, types.FunctionType, default)

def get_quiet(kwargs: dict, default: bool) -> bool:
    """
    Helper for :func:`get_or_throw` for common `quiet` argument forcing a 

    :param kwargs: the dictionary of keyword arguments.
    :type kwargs: dict
    :param default: the default value if no argument is present in the dictionary.
    :type default: bool
    :return: the value if the argument is in the dictionary, the default otherwise.
    :rtype: bool
    """
    return get_or_throw(kwargs, "quiet", bool, False)

def all_same_lengths(lists : ty.List[ty.List[ty.Any]], msg : str = None, ignore_none : bool = True) -> bool:
    """
    Returns true if the all lists inside `lists` are of same lengths, `False` otherwise.
    Print a warning (`msg`) to stdout to signal a size difference between two lists are found.

    :param lists: the list of lists to be checked.
    :type lists: List[List[object]]
    :param msg: message to be displayed when lists are not of the same lengths. No message is displayed if equal to `None`, defaults to None
    :type msg: str, optional
    :param ignore_none: if True, None is always considered as valid value, defaults to True
    :type ignore_none: bool, optional
    :return: True if the lists have same length, false otherwise.
    :rtype: bool
    """
    if len(lists) <= 1:
        return True

    base = len(lists[0])

    for i in range(1, len(lists)):
        l = lists[i]
        if ignore_none and l == None:
            continue

        if len(lists[i]) != base:
            if msg != None:
                log.warning(msg)
            return False
    
    return True