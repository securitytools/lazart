""" The module *lazart.exps.std_variants* defines several Variants for Lazart's Experimentation module. Those variant aims to be reused in /use-cases/examples programs's experimentation and user analysis.

:since: 4.0
:author: Etienne Boespflug 
"""
# Python
import typing as ty

# Lazart
import lazart.exps.variants as vrt
import lazart.core.countermeasures as cm
from lazart.core.traces import TerminationType
from lazart.core.analysis import AnalysisFlag
import lazart.analysis.attack_redundancy as ar

def entries(vl: vrt.VariantsList, shortcut : str = None) -> vrt.Variant:
    """
    Returns the standard variant 'entries' that corresponds to the predicate of the use of symbolic inputs.

    :param vl: the VariantList in which the variant will be added.
    :type vl: VariantsList
    :param shortcut: CLI's shortcut value (complete value is 'exp_entries'), defaults to None
    :type shortcut: str, optional
    :return: the created Variant.
    :rtype: Variant
    """
    return vl.add_variant_and_option(
        vrt.Variant(["", "-DSYM "], ["fix", "sym"]),
        # Parser...
        "entries", 
        "determines if concrete or symbolic entries are used.",
        None, shortcut,
        "fix")

def mut_value(vl: vrt.VariantsList, shortcut : str = None) -> vrt.Variant:
    """
    Returns the standard variant 'mut_value' that corresponds to the predicate of the printing of mutation value for DL model.

    :param vl: the VariantList in which the variant will be added.
    :type vl: VariantsList
    :param shortcut: CLI's shortcut value (complete value is 'exp_mut_value'), defaults to None
    :type shortcut: str, optional
    :return: the created Variant.
    :rtype: Variant
    """
    return vl.add_variant_and_option(
        vrt.Variant(["", "-D_LZ__NO_MUT_VALUE "], ["yes", "no"]),
        # Parser...
        "mut_value", 
        "determines injected value is gathered by Lazart.",
        None, shortcut,
        "yes")

def countermeasures_tm(vl: vrt.VariantsList, shortcut : str = None) -> vrt.Variant:
    """
    Returns the standard variant 'countermeasures_tm' that enumerates program variants using different version of Test Multiplication countermeasure up to depth 2 (tripling).

    :param vl: the VariantList in which the variant will be added.
    :type vl: VariantsList
    :param shortcut: CLI's shortcut value (complete value is 'exp_countermeasures_tm'), defaults to None
    :type shortcut: str, optional
    :return: the created Variant.
    :rtype: Variant
    """
    return  vl.add_variant_and_option(
        vrt.Variant([[], [cm.cm_tm(dict(depth=1, on=["__mut__"]))], [cm.cm_tm(dict(depth=2, on=["__mut__"]))]],
        ["none", "td", "tt"]),
        "countermeasures_tm", 
        "the TM countermeasures applied on the program.",
        None, shortcut,
        "none")

def countermeasures_lm(vl: vrt.VariantsList, shortcut : str = None) -> vrt.Variant:
    """
    Returns the standard variant 'countermeasures_lm' that enumerates program variants using different version of Load Multiplication countermeasure up to depth 2 (tripling).

    :param vl: the VariantList in which the variant will be added.
    :type vl: VariantsList
    :param shortcut: CLI's shortcut value (complete value is 'exp_countermeasures_lm'), defaults to None
    :type shortcut: str, optional
    :return: the created Variant.
    :rtype: Variant
    """
    return  vl.add_variant_and_option(
        vrt.Variant([[], [cm.cm_lm(dict(depth=1, on=["__mut__"]))], [cm.cm_lm(dict(depth=2, on=["__mut__"]))]],
        ["none", "ld", "lt"]),
        "countermeasures_lm", 
        "the LM countermeasures applied on the program.",
        None, shortcut,
        "none")

def countermeasures(vl: vrt.VariantsList, shortcut : str = None) -> vrt.Variant:
    """
    Returns the standard variant 'countermeasures' that enumerates program variants using different version of Test and Load Multiplication (not combined) up to depth 2 (tripling).

    :param vl: the VariantList in which the variant will be added.
    :type vl: VariantsList
    :param shortcut: CLI's shortcut value (complete value is 'exp_countermeasures'), defaults to None
    :type shortcut: str, optional
    :return: the created Variant.
    :rtype: Variant
    """
    return  vl.add_variant_and_option(
        vrt.Variant([[], [cm.cm_tm(dict(depth=1, on=["__mut__"]))], [cm.cm_tm(dict(depth=2, on=["__mut__"]))], [cm.cm_lm(dict(depth=1, on=["__mut__"]))], [cm.cm_lm(dict(depth=2, on=["__mut__"]))]],
        ["none", "td", "tt", "ld", "lt"]),
        "countermeasures", 
        "the countermeasures applied on the program.",
        None, shortcut,
        "none")

def det_mode(vl: vrt.VariantsList, shortcut : str = None) -> vrt.Variant:
    """
    Returns the standard variant 'det_mode' that corresponds to the predicate of the use of non-stopping detectors.

    :param vl: the VariantList in which the variant will be added.
    :type vl: VariantsList
    :param shortcut: CLI's shortcut value (complete value is 'exp_det_mode'), defaults to None
    :type shortcut: str, optional
    :return: the created Variant.
    :rtype: Variant
    """
    return vl.add_variant_and_option(vrt.Variant(["", "-D_LZ__FORCE_NS_DETECTORS"],
    ["stop", "no-stop"]),
    "det_mode", "determine if the detectors stop analysis or not.", 
    None, shortcut,
    "stop")

def eae(vl: vrt.VariantsList, shortcut : str = None) -> vrt.Variant:
    """
    Returns the standard variant 'eae' that corresponds to the predicate of the use of "--emit-all-errors" option for KLEE, allowing DSE to generates all paths for the same error's LLVM location.

    :param vl: the VariantList in which the variant will be added.
    :type vl: VariantsList
    :param shortcut: CLI's shortcut value (complete value is 'exp_eae'), defaults to None
    :type shortcut: str, optional
    :return: the created Variant.
    :rtype: Variant
    """
    return vl.add_variant_and_option(vrt.Variant(["", "--emit-all-errors"],
    ["neae", "eae"]),
    "eae", "determine if KLEE is used with --emit-all-errors options.",
    None, shortcut,
    "neae")

def timeout(vl: vrt.VariantsList, shortcut : str = None) -> vrt.Variant:
    """
    Returns the standard variant 'timeout' that corresponds to the timeout value passed to KLEE ("--max-time). Use "--watchdog" to enforce max time.

    :param vl: the VariantList in which the variant will be added.
    :type vl: VariantsList
    :param shortcut: CLI's shortcut value (complete value is 'exp_timeout'), defaults to None
    :type shortcut: str, optional
    :return: the created Variant.
    :rtype: Variant
    """
    return vl.add_variant_and_option(
    # Values
    vrt.Variant(["", "--max-time=1 ", "--max-time=10 ", "--max-time=60 ", "--max-time=300 ", "--max-time=1800 "],
    ["no", "1s", "10s", "1min", "5min", "30min"]),
    # Parser...
    "timeout", 
    "The timeout used for DSE, none by default.",
    None, shortcut,
    "no"
    )

def strategy(vl: vrt.VariantsList, shortcut : str = None) -> vrt.Variant:
    """
    Returns the standard variant 'strategy' that corresponds to the exploration strategy to be used by KLEE.

    :param vl: the VariantList in which the variant will be added.
    :type vl: VariantsList
    :param shortcut: CLI's shortcut value (complete value is 'exp_strategy'), defaults to None
    :type shortcut: str, optional
    :return: the created Variant.
    :rtype: Variant
    """
    return vl.add_variant_and_option(
    # Values
    vrt.Variant(["", "--search=dfs", "--search=bfs", "--search=random-state", "--search=random-path ", "--search=nurs:depth ", "--search=nurs:rp ", "--search=nurs:cpicnt "],
    ["std", "dfs", "bfs", "rstate", "rpath", "nurs-depth", "nurs-rp", "nurs-cpi"]),
    # Parser...
    "strategy", 
    "The timeout used for DSE. If any, depth first strategy will be used",
    None, None, # descr & shortcut
    "std"
    )

def traces_mode(vl: vrt.VariantsList, shortcut : str = None) -> vrt.Variant:
    """
    Returns the standard variant 'entries' that corresponds to a ternary predicate of the use of the several trace parsing mode options: lazy, rt (parse also RTE errors), all (parses all traces termination type).

    :param vl: the VariantList in which the variant will be added.
    :type vl: VariantsList
    :param shortcut: CLI's shortcut value (complete value is 'exp_traces_mode'), defaults to None
    :type shortcut: str, optional
    :return: the created Variant.
    :rtype: Variant
    """
    return vl.add_variant_and_option(vrt.Variant(["lazy", "rt", "full"],
    ["lazy", "rt", "full"]),
    "tp_mode", "determine which types of traces to be used.",
    None, shortcut,
    "lazy")

def aa_type(vl: vrt.VariantsList, shortcut : str = None) -> vrt.Variant:
    """Returns the standard variant 'entries' that corresponds to a the analysis flag to be used, using only attack analysis.

    :param vl: the VariantList in which the variant will be added.
    :type vl: VariantsList
    :param shortcut: CLI's shortcut value (complete value is 'exp_aa_type'), defaults to None
    :type shortcut: str, optional
    :return: the created Variant.
    :rtype: Variant
    """
    return vl.add_variant_and_option(vrt.Variant([AnalysisFlag.AttackAnalysis, AnalysisFlag.EqRedAnalysis],
    ["atk", "red"]),
    "aa_type", "determines which type of Attack Analysis will be performed.",
    None, shortcut,
    "red")

def red_rule(vl: vrt.VariantsList, shortcut : str = None) -> vrt.Variant:
    """Returns the standard variant 'red_rule' that to the redundancy definition to be used using equivalence&redundancy analysis.

    TODO: Their is no standard solution for now to forbid the experimentation to generate multiple analysis variation for this variant when working with AttackAnalysis mode (without redundancy). Use a custom variant with three cases [aa, aar1, aar2] or more for combining with eq_rule.

    :param vl: the VariantList in which the variant will be added.
    :type vl: VariantsList
    :param shortcut: CLI's shortcut value (complete value is 'exp_red_rule'), defaults to None
    :type shortcut: str, optional
    :return: the created Variant.
    :rtype: Variant
    """
    return vl.add_variant_and_option(
    vrt.Variant([ar.prefix_rule, ar.sub_word_rule],
    ["pre", "sw"]),
    "red_rule",
    "determines the redundancy rule to be used",
    None, shortcut,
    "pre")

def eq_rule(vl: vrt.VariantsList, shortcut : str = None) -> vrt.Variant:
    """Returns the standard variant 'eq_rule' that to the equivalence definition to be used using equivalence&redundancy analysis.

    TODO: Their is no solution for now to forbid the experimentation to generate multiple analysis variation for this variant when working with AttackAnalysis mode (without equivalence). Use a custom variant with three cases [aa, aar1, aar2] or more for combining with red_rule.

    :param vl: the VariantList in which the variant will be added.
    :type vl: VariantsList
    :param shortcut: CLI's shortcut value (complete value is 'exp_eq_rule'), defaults to None
    :type shortcut: str, optional
    :return: the created Variant.
    :rtype: Variant
    """
    return vl.add_variant_and_option(
    vrt.Variant([ar.eq_rule, ar.feq_rule],
    ["eq", "feq"]),
    "eq_rule",
    "determines the equivalence rule to be used",
    None, shortcut,
    "feq")

def int_range(vl: vrt.VariantsList, range: ty.List[int], name : str, descr: str, shortcut : str = None, **kwargs) -> vrt.Variant:
    """
    Returns a variant that corresponds to an integral value among `range`. Custom name and description has to be provided. 

    **Kwargs**:
        - `value_fmt` (`bool`): format string to generate string value. If `value_fmt` is empty, the value is integral, otherwise the value is a string.

    :param vl: the VariantList in which the variant will be added.
    :type vl: VariantsList
    :param range: possible integer values for the variant (non empty, first will be default).
    :type range: List[int]
    :param value_fmt: 
    :type name: str
    :param name: name of the variant.
    :type name: str
    :param descr: description of the variant.
    :type descr: str
    :param shortcut: CLI's shortcut value (complete value is 'exp_<name>'), defaults to None
    :type shortcut: str, optional
    :return: the created Variant.
    :rtype: Variant
    """
    names = [str(i) for i in range]
    value_fmt = kwargs.get("value_fmt", "")
    if value_fmt != "": 
        range = [value_fmt.format(i) for i in range]
    
    return vl.add_variant_and_option(
        vrt.Variant(range, names),
        name,
        descr,
        None, shortcut,
        names[0]
    )

def term(vl: vrt.VariantsList, shortcut : str = None) -> vrt.Variant:
    """
    Returns the standard variant 'term' that corresponds to type of termination type that are considered as successful attacks (for instance considering ptr errs as RTE).
    Trace parsing mode has to be modified accordingly using the key value "tp_mode".

    :param vl: the VariantList in which the variant will be added.
    :type vl: VariantsList
    :param shortcut: CLI's shortcut value (complete value is 'exp_term'), defaults to None
    :type shortcut: str, optional
    :return: the created Variant.
    :rtype: Variant
    """
    v_term = vl.add_variant_and_option(
    vrt.Variant([lambda trace: trace.satisfies(), 
                 lambda trace: trace.satisfies() or trace.termination_is(TerminationType.Timeout),
                 lambda trace: trace.satisfies() or trace.termination_is(TerminationType.Ptr, TerminationType.Free, TerminationType.Abort, TerminationType.Assert, TerminationType.Div, TerminationType.ReadOnly), # RTE
                 lambda trace: trace.satisfies() or trace.termination_is(TerminationType.Ptr, TerminationType.Free, TerminationType.Abort, TerminationType.Assert, TerminationType.Div, TerminationType.ReadOnly, TerminationType.Timeout)],
            ["std", "to", "rte", "both"]),
    "term", 
    "the termination type accepted as successful attacks.",
    ["only traces that verify instrumented attack objective (`_LZ__ORACLE`) are considered",
     "timeout are considered as successful attacks as well",
     "all runtime errors (RTE) are successful attacks.",
     "timeout and RTE are successful attacks"],
     shortcut,
     "std"
    )

    v_term.add_key_value("tp_mode", lambda v, indexes, exps: "lazy" if v.name(indexes, exps) == "std" else "rte" if v.name(indexes, exps) == "rte" else "full")
    
    return v_term