""" The module *lazart.exps.variants* defines the several Variant types, and the VariantList class, used to defines program's variants for an experimentation.

:since: 4.0
:author: Etienne Boespflug 
"""

# Internal 
from abc import abstractmethod
import argparse
from functools import reduce
import typing as ty
import lazart._internal_.args as _args
import lazart.logs as log
import lazart.script as script

class VariantExpression():
    """
    The VariantExpression abstract class corresponds to the base class of a variant expression, constituted of Variant or other expression and overloading arithmetic operators.
    """
    BinaryOpType = None #: type: The class type for binary operations overloading.

    @abstractmethod
    def value(self, indexes: ty.List[int], exp) -> ty.Any:
        """
        Returns the value of the variant expression for the specified experimentation exp and the indexes of the current variant being generated.

        :param indexes: the list of indexes for each variant of the experimentation.
        :type indexes: List[int]
        :param exp: the experimentation of the variant.
        :type exp: Experimentation
        :return: the variant's value for the specified indexes.
        :rtype: Any
        """
        pass

    def __add__(self, x):
        return VariantExpression.BinaryOpType(self, x)

    def __sub__(self, x):
        return VariantExpression.BinaryOpType(self, x, op="-")

    def __mul__(self, x):
        return VariantExpression.BinaryOpType(self, x, op="*")

    def __div__(self, x):
        return VariantExpression.BinaryOpType(self, x, op="/")

    __radd__ = __add__
    __rsub__ = __sub__
    __rmul__ = __mul__
    __rdiv__ = __div__ 

class VariantBinOp(VariantExpression):
    """
    The VariantBinOp class is a variant expression corresponding to a binary operation on two expression. This class allows to express several arithmetical operations.
    """
    def __init__(self, a: VariantExpression, b: VariantExpression, op: str = "+"):
        """
        **__init__ (ctor):** Constructs a new VariantBinOp object with two operands and an operator (default '+').

        :param a: the first operand of the binary operation.
        :type a: VariantExpression
        :param b: the second operand of the binary operation.
        :type b: VariantExpression
        :param op: the operator, defaults to "+"
        :type op: str, optional
        """
        self.a = a #: VariantExpression: the first operand.
        self.b = b #: VariantExpression: the second operand.
        self.op = op #: str: the operator ('+', '-', '*' or '/').

    def value(self, indexes: ty.List[int], exp) -> ty.Any:
        # inherited documentation.
        op1 = self.a
        op2 = self.b
        if isinstance(self.a, VariantExpression):
            op1 = self.a.value(indexes, exp)
        if isinstance(self.b, VariantExpression):
            op2 = self.b.value(indexes, exp)
        
        if self.op == "+":
            return op1 + op2
        if self.op == "-":
            return op1 - op2
        if self.op == "*":
            return op1 * op2
        if self.op == "/":
            return op1 / op2
        else:
            log.error("unknown operation " + self.op + ".")

# Rebind VariantExpression
_VBO_obj = VariantBinOp(None, None)
VariantExpression.BinaryOpType = type(_VBO_obj)

class VariantLambda(VariantExpression):
    """
    The VariantLambda class is a variant expression using a callback to returns the value.
    """
    def __init__(self, callback: ty.Callable[[ty.List[int], object], ty.Any]):
        """
        **__init__ (ctor):** Constructs a new VariantLambda object with the callback function to be called for getting value.

        :param callback: the callback returning the value depending on current indexes for the experimentation.
        :type callback: ty.Callable[[ty.List[int], Experimentation], ty.Any]
        """
        self.callback = callback

    def value(self, indexes: ty.List[int], exp) -> ty.Any:
        # inherited documentation.
        return self.callback(indexes, exp)

# Actual Variant Types
class VariantRelValue(VariantExpression):
    """
    The VariantRelValue class holds the a relative Variant and the value's key, returning as value the relative value for this key.
    """
    def __init__(self, variant, key: str):
        """
        **__init__ (ctor):** Constructs a new VariantRelValue object with the list of values and keys (names).

        :param variant: the relative variant.
        :type variant: AbstractVariant
        :param key: the relative value's key.
        :type key: str
        """
        self.key = key #: str: the key of the relative value.
        self.variant = variant #: List[str]: the list of the variant's keys (names).

    def value(self, indexes: ty.List[int], exp) -> ty.Any:
        # inherited documentation.
        return self.variant.relvalue(self.key, indexes, exp)

class AbstractVariant(VariantExpression):
    """
    The AbstractVariant class correspond to the base class of Variant classes.
    """
    def __init__(self):
        """
        **__init__ (ctor):** Constructs a new AbstractVariant.
        """
        self.key_values = dict() #: Dict[str, Callable[[Any, List[int]], Any]]: dictionary of callbacks for key values, returning a value depending on current Variant's value.

    def add_key_value(self, key: str, callback: ty.Callable[[ty.Any, ty.List[int]], ty.Any]):   
        """
        Add a new key value, using `callback` to return a value depending on the current Variant value. 
        
        Used to associate other values to the variant.

        :param key: the key name of the value.
        :type key: str
        :param callback: the function associating values depending on the variant's value.
        :type callback: Callable[[Any, List[int]], Any]
        """
        if key in self.key_values:
            log.warning("the key '{}' is already defined for the variant.".format(key))
            return
        
        self.key_values[key] = callback

    def valid_idx(self, idx: int) -> bool:
        """
        Predicate determining if the index `idx` is valid for the variant.

        :param idx: the index.
        :type idx: int
        :return: true if the index is valid, false otherwise.
        :rtype: bool
        """
        if not (0 <= idx < self.count()):
            log.warning("index ({0}) out of range (size = {1})".format(idx, self.count()))
            return False
        return True

    @abstractmethod
    def count(self) -> int:
        """
        Returns the number of variant's values.

        :return: the number of variant's values.
        :rtype: int
        """
        pass

    @abstractmethod
    def value(self, indexes: ty.List[int], exp) -> ty.Any:
        # inherited documentation.
        pass

    @abstractmethod
    def relvalue(self, key: str, indexes: ty.List[int], exp) -> ty.Any:
        """
        Returns the relative value for the specified `key` (see `self.key_value`).

        :param key: the requested value's key.
        :type key: str
        :param indexes: the list of indexes for each variant of the experimentation.
        :type indexes: List[int]
        :param exp: _description_
        :param exp: the experimentation of the variant.
        :type exp: Experimentation
        :return: the variant's relative value for the specified key and indexes.
        :rtype: Any
        """
        if key not in self.key_values:
            log.warning("unknown key '{}' for the variant.".format(key))

        cb = self.key_values[key]
        return cb(self, indexes, exp)
    
    def get(self, key: str) -> VariantRelValue:
        """
        Returns the relative value Variant depending on current value.

        :param key: the requested value's key.
        :type key: str
        :param indexes: the list of indexes for each variant of the experimentation.
        :type indexes: List[int]
        :param exp: the experimentation of the variant.
        :type exp: Experimentation
        :return: _description_
        :rtype: VariantRelValue
        """
        return VariantRelValue(self, key)

    @abstractmethod
    def name(self, indexes: ty.List[int], exp) -> str:
        """
        Returns the name of the value depending on the current Variant's value.

        :param indexes: the list of indexes for each variant of the experimentation.
        :type indexes: List[int]
        :param exp: the experimentation of the variant.
        :type exp: Experimentation
        :return: the name of the value.
        :rtype: str
        """
        pass

class Variant(AbstractVariant):
    """
    The Variant class is the standard Variant type, holding a list of values and their names.
    """
    def __init__(self, values: ty.List[ty.Any], names: ty.List[str]):
        """
        Create a new Variant with specified values and data.

        :param values: the possible values of the variant (ex: [1, 4, 3, 2], ["-DA", -DB"]).
        :type values: List[Any]
        :param names: the list of each name for variant value (will also be used for custom parsers).
        :type names: List[str]
        """
        super().__init__()
        
        self.values = values #: List[Any]: the possible values of the variant (ex: [1, 4, 3, 2], ["-DA ", -DB "]).
        self.names = names #: List[str]: the list of name of each value (unique, not enforced).

        _args.all_same_lengths([values, names], "not corresponding lengths of input lists")

    def count(self) -> int:     
        # inherited documentation.   
        return len(self.values)

    def value(self, indexes: ty.List[int], exp) -> ty.Any: 
        # inherited documentation.
        v_idx = exp.index_of(self, "cannot find variant ")

        value_idx = indexes[v_idx]

        if value_idx >= len(self.values):
            log.error("invalid value index found for variant " + str(v_idx) + " ({0}/{1}).".format(value_idx, len(self.values)))
        
        return self.values[value_idx]

    def update(self, allowed_values: ty.List[str]) -> bool:
        """
        Updates the values and name of the Variant depending on the actives ones (`allowed_value`). Used for applying CLI's arguments.

        :param allowed_values: the list of value names that are active.
        :type allowed_values: List[str]
        :return: true if the update is successful, false otherwise.
        :rtype: bool
        """
        diff = list(set(allowed_values).difference(self.names))
        if "__all__" in diff:
            return True
        if len(diff) > 0:
            log.warning("unknown values ({}) for variant.".format(",".join(diff)))
            return False

        values_cp = self.values
        names_cp = self.names
        self.values = []
        self.names = []

        correct = True

        for value in allowed_values:
            if value not in names_cp:
                log.warning("unknown value name '{}' for variant.".format(value))
                correct = False
                continue

            idx = names_cp.index(value)
            self.values.append(values_cp[idx])
            self.names.append(names_cp[idx])

        return correct 

    def name(self, indexes: ty.List[int], exp) -> str:
        # inherited documentation.
        v_idx = exp.index_of(self, "in variant.")
        idx = indexes[v_idx]
        if not self.valid_idx(idx):
            return "_unknown_"
        return self.names[idx]

class VariantsList():
    def __init__(self, **kwargs):
        """
        **__init__ (ctor):** Constructs a new variants list used to initialize an `Experimentation`.

        Variants can be added and first added list will be a priority parameter.
        """
        self.variants: ty.List[AbstractVariant] = [] #: List[AbstractVariant]: the list of variants. 
        self.variants_args: ty.List[(str, str, ty.List[str], str, str)] = [] #: List[(str, str, List[str], str, str)]: the list of data tuples associated with equivalent index in `self.variants`. Each tuple contains: (name, description, list of values descriptions, CLI arg shortcut, default value).

    def count(self) -> int:
        """
        Returns the number of variants in the list.

        :return: the variant count.
        :rtype: int
        """
        return len(self.variants)

    def factors(self) -> ty.List[int]:
        """
        Compute the list of program variant counts for each Variant in the list.

        :return: the list of factors.
        :rtype: List[int]
        """
        factors = []
        for v_idx, _ in enumerate(self.variants):
            factors.append(self.variants[v_idx].count())
        
        return factors

    def combinations(self) -> int:
        """
        Returns the total number of combinations of program variants to be generated. 

        :return: the total combination count.
        :rtype: int
        """
        return reduce(lambda x, y: x * y, self.factors())

    def __getitem__(self, idx: int) -> Variant:
        """
        Returns the Variant object at the specified index. No bound checking.

        :param idx: the index of 
        :type idx: int
        :return: the variant at the specified index.
        :rtype: Variant
        """
        assert type(idx) is int
        return self.variants[idx]

    def by_name(self, name: str) -> Variant:
        """
        Returns the variant in the list corresponding to the specified name.

        :param name: the name of the variant.
        :type name: str
        :return: the corresponding variant, `None` otherwise.
        :rtype: Variant
        """
        for i in range(len(self.variants_args)):
            if self.variants_args[i][0] == name:
                return self.variants[i]
        return None

    def index_by_name(self, name: str) -> int:
        """
        Returns the index of the variant of the specified `name`. Returns -1 if no variant corresponds.

        :param name: the requested variant's name.  
        :type name: str
        :return: the corresponding index, `-1` otherwise.
        :rtype: int
        """
        for i in range(len(self.variants_args)):
            if self.variants_args[i][0] == name:
                return i
        return -1

    def add_variant(self, v: AbstractVariant) -> AbstractVariant:
        """
        Add a variant in the list.

        :param v: the variant to be added.
        :type v: AbstractVariant
        :return: the added variant.
        :rtype: AbstractVariant
        """
        
        self.variants.append(v)
        self.variants_args.append(None)
        
        return v

    def add_variant_and_option(self, v: AbstractVariant, name: str, description: str, descriptions: ty.List[str] = None, shortcut: str = None, def_val: str = None) -> AbstractVariant:
        """
        Add a new variant to the list with argparse parameters.

        :param values: the possible values of the variant (ex: [1, 4, 3, 2], ["-DA", -DB"]).
        :type values: List[str]
        :param names: the list of each name for variant value (will also be used for custom parsers).
        :type names: List[str]
        :param name: the short name of the variant used for parser generation.
        :type name: str, optional
        :param description: the description of the variant for the arguments parser (with {0} containing the list of values names).
        :type description: str, optional
        :param descriptions: the list of description of each value of the variant.
        :type descriptions: List[str]
        :param shortcut: the shortcut arguments parser, if `None` no shortcut is used.
        :type shortcut: str, optional
        :return: the created variant
        :rtype: Variant
        """
        
        self.variants.append(v)
        self.variants_args.append( (name, description, descriptions, shortcut, def_val) )

        return v

    def get_parser(self, parser: argparse.ArgumentParser) -> argparse.ArgumentParser:
        """
        Returns the `argparse` parser for the VariantList, including standard `--priority` and variant specific's `--exp_variant` CLI options.

        :param parser: the parser to be modified.
        :type parser: argparse.ArgumentParser
        :return: the parser.
        :rtype: argparse.ArgumentParser
        """

        group = parser.add_argument_group('variants')
        group.add_argument('--priority', nargs='+', type=str, help='The priority of variant (by name). May be incomplete.')
        group.add_argument("-a", "--all", action="store_true", help="Force all variants to use all values (as if '__all__' was specified for each variant).")

        for i in range(len(self.variants)):
            v = self.variants[i]
            v_args = self.variants_args[i]

            if v_args is None:
                continue
            (name, description, all_descr, sc, _) = self.variants_args[i]
            
            full_name = "exp_" + name

            if type(v) is Variant: # Only for variants.
                if all_descr is not None: # Build detailed description.
                    names = v.names
                    formatted = []
                    for i in range(len(names)):
                        value_name = names[i]
                        formatted.append(f"'{value_name}': {'no description.' if i >= len(all_descr) else all_descr[i]}")
                    description = f"{description} ({', '.join(formatted)})"

            if isinstance(v, Variant):
                if sc is not None:
                    group.add_argument(sc, "--" + full_name, nargs='+', type=str, choices=v.names + ["__all__"], help=description)
                else:
                    group.add_argument("--" + full_name, nargs='+', type=str, choices=v.names + ["__all__"], help=description)

        return parser
    

    def install_exp_script(self, banner: str = "Experimentation script.") -> argparse.Namespace:
        """
        Installs the common script arg parser and parses arguments.

        :param banner: the experimentation giving the list of variant to be added as script argument.
        :type banner: Experimentation
        :param banner: the banner of the script.
        :type banner: str, optional
        :raises Exception: if **Lazart**'s installation fail.
        :return: the dictionary of argument parsing parameters.
        :rtype: argparse.Namespace
        """
        (params, cli_args) = script.install_custom_script(lambda parser: self.get_parser(parser))
        self.apply_cli_params(cli_args)

        return params

    def apply_cli_params(self, cli_args: dict) -> bool:
        """
        Applies the CLI arguments to the VariantList.

        :param cli_args: the CLI parameters.
        :type cli_args: dict
        :return: `False` if an error occurred, `True` otherwise.
        :rtype: bool
        """
        correct = True

        # Update variants
        if not getattr(cli_args, 'all', False): # Ignore variant updates when --all is defined.
            for i in range(len(self.variants)):
                variant = self.variants[i]

                if not isinstance(variant, Variant):
                    print("ignoring variant " + str(type(variant)))
                    continue

                def_value = None
                v_args = self.variants_args[i]
                if v_args is not None:
                    def_value = v_args[4]
                
                name = self.variants_args[i][0]
                if name is None:
                    continue

                full_name = "exp_" + name
                v_list = getattr(cli_args, full_name, None)

                if v_list is None:
                    if def_value is not None and def_value != "__all__":
                        variant.update([def_value])
                    continue
                
                variant.update(v_list)

        # Update priority
        priority = getattr(cli_args, "priority", None)

        if priority is not None and len(priority) > 0:
            new_vl = []
            new_v_args = []


            # Specified
            for value in priority:
                if value == "__all__":
                    continue
                idx = self.index_by_name(value)
                if idx < 0:
                    log.warning("unknown variant name '{}'.".format(value))
                    correct = False
                    continue
                
                new_vl.append(self.variants[idx])
                new_v_args.append(self.variants_args[idx])
                
            # Others
            for i in range(len(self.variants)):
                v = self.variants[i]
                args = self.variants_args[i]

                if args[0] is None or v not in new_vl:
                    new_vl.append(v)
                    new_v_args.append(args)
        
            self.variants = new_vl
            self.variants_args = new_v_args

        return correct

    def __str__(self) -> str:
        # Inherit documentation.
        ret = "variants:\n"
        for i in range(len(self.variants_args)):
            ret += f" - {self.variants_args[i][0]}: [{','.join(self.variants[i].names)}]\n"

        return ret
