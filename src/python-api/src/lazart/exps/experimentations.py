""" The module `lazart.exps.experimentations` defines the Experimentation class used to generate and run experimentations with several variant programs.

:since: 4.0
:author: Etienne Boespflug 
"""

# Python
import traceback
import typing as ty
import time
import copy
import os 

# Internal 
import lazart._internal_.args as _args
import lazart._internal_.util as util

# Lazart
from lazart.util.exec import execute 
import lazart.logs as log
from lazart.results.results import Metrics
from lazart.results.formats import Table
from lazart.core.analysis import Analysis
import lazart.results.results as res
from lazart.exps.variants import VariantsList, VariantExpression, Variant, AbstractVariant

class Experimentation():
    """
    The Experimentation class holds a list of variant (`VariantList`) and provides methods to run an experimentation.
    """
    def __init__(self, v_list: VariantsList, files: ty.List[str], attack_model: dict, **kwargs):
        """        
        **__init__ (ctor):** Constructs a new Experimentation object with the specified variant list

        **Kwargs**:
         - `execute_params` (`Dict[str, Any | VariantExpression]`): variant parameter to be passed to execute function. Does expand variants.
         - any kwargs supported by `Analysis` class, see corresponding documentation. Does expand variants. 

        :param v_list: the variants list of the experimentation.
        :type v_list: VariantsList
        :param files: the analysis file (may be a `VariantExpression`).
        :type files: List[str] | VariantExpression
        :param attack_model: the attack model of the experimentation (may be a `VariantExpression`).
        :type attack_model: dict | VariantExpression
        """
        self.variants: VariantsList = v_list #: VariantList: the list of the variants.
        self.exps = [] #: List[Analysis]: the list of experimentation's analysis (generated).
        self.path = _args.get_or_throw(kwargs, "path", str, "campaign/") #: the path of the experimentation campaign's analysis folders.

        self._compute_analysis(files=files, attack_model=attack_model, **kwargs)

    def expand(self, kwargs: dict, indexes: ty.List[int]) -> dict:
        """
        Expands the specified dictionary switching all VariantExpression values to their actual value depending on experimentation indexes.

        :param kwargs: the dictionary to be expanded.
        :type kwargs: dict
        :param indexes: the list of analysis's variant indexes.
        :type indexes: List[int]
        :return: the modified dictionary.
        :rtype: dict
        """
        for key, value in kwargs.items():
            if key == "execute_params": continue # Nothing to be done here.
            if isinstance(value, VariantExpression):
                val = value.value(indexes, self)
                kwargs[key] = val

        return kwargs
    
    def _compute_analysis(self, **kwargs):
        """
        Generates the analysis object from the experimentation's variants and parameters and fills the `self._exps` field.
        """
        # each param search for Expression
        fields = ["files", "attack_model", "flags", 
            "path", "name", "max_order",
            "compiler_args", "dis_args", "linker_args", "wolverine_args", "klee_args",
            "compiler", "linker", "disassembler",
            "tasks", "countermeasures",
            "execute_params"]
        other_fields = ["params", "remove_fix_variants_names"]
        _args.warning_unknown_fields(kwargs, fields + other_fields, "experimentation initialization")

        indexes = [0] * self.variants.count() # idx of each variant in the matrix
            
        def create_analysis(indexes: ty.List[int], exp: Experimentation, **kwargs) -> Analysis :
            """
            Create an Analysis for the specified variant's indexes and `exp` campaign.

            :param indexes: the list of analysis's variant indexes.
            :type indexes: List[int]
            :param exp: the experimentation campaign.
            :type exp: Experimentation
            :return: the created analysis.
            :rtype: Analysis
            """
            kwargs = copy.copy(kwargs) # do not interfere with future analysis created, deepcopy would lost variant links.
            self.expand(kwargs, indexes)    

            id = str(len(exp.exps))
            # Generate name and path.
            kwargs["name"] = kwargs.get("name", "") + id + "-" + self.name_of(indexes, **kwargs)
            kwargs["path"] = kwargs.get("path", "") + "results_" + id + "_" + self.name_of(indexes, **kwargs)
            
            # Create analysis and experimentation-specific bindings.
            analysis = Analysis(kwargs["files"], kwargs["attack_model"], **util.remove_kwargs("files", "attack_model", "execute_params", **kwargs))
            analysis._exp_indexes = copy.deepcopy(indexes)

            # Handle specific execute params, expanded and cached.
            exec_params = copy.copy(kwargs.get("execute_params", None))
            if exec_params != None:
                analysis._execute_params = self.expand(exec_params, indexes)

            return analysis

        def loop_i(idx: int, final_callback: ty.Callable[[ty.List[int]], None]):
            """
            Recursively generate all values for the variant at index `idx` and call recursively loop_i for next `idx`.
            Final callback is executed when all indexes has been generated, generating an analysis.

            :param idx: the current variant's index.
            :type idx: int
            :param final_callback: the function to be called to generate the analysis.
            :type final_callback: Callable[[List[int]]
            """
            if idx >= self.variants.count():
                final_callback(indexes)
                return
            
            for i in range(self.variants.variants[idx].count()):
                indexes[idx] = i
                loop_i(idx + 1, final_callback)
        
        def final_callback(exp_idx: ty.List[int]):
            """
            Create an analysis for the specified value index for each variant (`exp_idx`).

            :param exp_idx: the list of value's index for each variant of the campaign.
            :type exp_idx: List[int]
            """
            try:
                analysis = create_analysis(exp_idx, self, **kwargs)
                self.exps.append(analysis)
            except Exception as e:
                log.warning("an error occurred when trying to create the analysis '{1}': {0}.".format(e, exp_idx))
                traceback.print_exc() 
                self.exps.append(None)

        loop_i(0, final_callback) # Starting point of recursion.

    # Util 
    def index_of(self, variant: AbstractVariant, msg = "") -> int:
        """
        Returns the index of the specified variant and print the error message `msg` if none corresponds.

        :param variant: the variant.
        :type variant: AbstractVariant
        :param msg: the error message, defaults to ""
        :type msg: str, optional
        :return: the index of the variant.
        :rtype: int
        """
        v_idx = self.variants.variants.index(variant)
        if msg != "" and v_idx >= self.variants.count():
            log.error(msg + " (idx: " + str(v_idx) + ").")
        return v_idx

    def indexes_of(self, analysis: Analysis) -> ty.List[int]:
        """
        Returns the indexes list of the specified analysis (part of the experimentation).

        :param analysis: the analysis.
        :type analysis: Analysis
        :return: the list of variant indexes corresponding to this analysis.
        :rtype: List[int]
        """
        return analysis._exp_indexes

    def variant(self, v_idx: int) -> AbstractVariant:
        """
        Returns the variant at the index `v_idx`.

        :param v_idx: the requested index.
        :type v_idx: int
        :return: the variant at ̀ v_idx`.
        :rtype: AbstractVariant
        """
        return self.variants[v_idx]

    def variant_from_ilist(self, idx_list: ty.List[int]) -> Variant:
        """
        Returns the variant corresponding to the specified indexes list.

        TODO: not implemented.

        :param idx_list: the current indexes list.
        :type idx_list: List[int]
        :return: the variant corresponding to `idx_list`.
        :rtype: Variant
        """
        _args.all_same_lengths([self.variants, idx_list])
        return None

    def name_of(self, indexes: ty.List[int], **kwargs) -> str:
        """
        Returns the name of the analysis for the specified variants indexes.

        :param indexes: the list of values'indexes for each variant.
        :type indexes: List[int]
        :return: the name of the corresponding analysis.
        :rtype: str
        """
        return "_".join(self.names_of(indexes, **kwargs))

    def names_of(self, indexes: ty.List[int], **kwargs) -> ty.List[str]:
        """
        Constructs the name of the analysis corresponding to the specified `indexes`, concatenating the current value for each variant.

        **Kwargs**:
         - `remove_fix_variants_names` (`bool`): if `True`, variant that only has one value will not be part of the name. *default*: `True`.
         
        :param indexes: the current variant indexes.
        :type indexes: List[int]
        :return: the name of the analysis.
        :rtype: List[str]
        """
        remove_unused = _args.get_or_throw(kwargs, "remove_fix_variants_names", bool, True)
        names = []
        for i in range(self.variants.count()):
            v = self.variants.variants[i]
            if v.count() > 0: # No count for fix or such variants.
                if remove_unused and v.count() == 1:
                    continue
                v_args = self.variants.variants[i].names[indexes[i]]
                names.append(v_args)
        
        return names            

    def exp(self, id: int) -> Analysis:
        """
        Returns the analysis corresponding to the specified index. No bound checking.

        :param id: the index of the analysis.
        :type id: int
        :return: the analysis of the specified index.
        :rtype: Analysis
        """
        return self.exps[id]

    # Run
    def run(self, callback: ty.Callable[[Analysis, object, ty.List[Metrics]], ty.List[Metrics]], *args, **kwargs) -> ty.List[Metrics]:
        """
        Calls the specified `callback` on each analysis. The callback take as argument: the current analysis, the experimentation (`self`), the list of results and `*args` and `**kwargs` are forwarded.

        **Kwargs**:
         - `mode` (`str`): execution mode, only 'serial' is supported for now, default.

        :param callback: the callback to be called on each analysis 
        :type callback: Callable[[Analysis, object, List[Metrics]], List[Metrics]]
        :param args: additional callback arguments.
        :param kwargs: callback keywords arguments.
        :return: the list of metrics for each analysis.
        :rtype: List[Metrics]
        """
        mode = _args.get_or_throw(kwargs, "mode", str, "serial")
        if mode == "serial":
            return self.run_serial(callback, *args, **kwargs)
        elif mode == "parallel":
            return self.run_parallel(callback, *args, **kwargs)
        else:
            log.error("unknown parameter value for 'mode': " + str(mode))

    def run_serial(self, callback: ty.Callable[[Analysis, object, ty.List[Metrics]], ty.List[Metrics]], *args, **kwargs) -> ty.List[Metrics] :
        """
        Calls the specified `callback` on each analysis. The callback take as argument: the current analysis, the experimentation (`self`), the list of results and `*args` and `**kwargs` are forwarded.

        :param callback: the callback to be called on each analysis 
        :type callback: Callable[[Analysis, object, List[Metrics]], List[Metrics]]
        :param args: additional callback arguments.
        :param kwargs: callback keywords arguments.
        :return: the list of metrics for each analysis.
        :rtype: List[Metrics]
        """

        results = []
        for analysis in self.exps:
            callback(analysis, self, results, *args, **kwargs)
            # disconnect cache...

        return results

    def run_parallel(self, callback, *args, **kwargs):
        log.error("not implemented")

    def clean_folders(self):
        """
        Cleans all analysis of the experimentation.
        """
        for exp in self.exps:
            exp.clean()

# Util 
def append_indexes_infos(results: ty.List[Metrics], header: ty.List[str], rows: ty.List[ty.List[str]], campaign: Experimentation):
    """
    Add the analysis variants's names to the `rows` and the "Variant" column to the header. 

    :param results: list of metrics for each analysis.
    :type results: List[Metrics]
    :param header: header of the result table.
    :type header: List[str]
    :param rows: list of rows of the results table.
    :type rows: List[List[str]]
    :param campaign: experimentation campaign of all analysis.
    :type campaign: Experimentation
    """
    for i in range(campaign.variants.count()):
        v = campaign.variants.variants[i]
        if v.count() > 0:
            v_args = campaign.variants.variants_args[i]
            name = "Variant"
            if v_args != None:
                name = v_args[0]
            header.append(name)

            for j in range(len(results)):
                a_results = results[j]
                row = rows[j]
                
                indexes = a_results[res.Metrics.ExpIndexes]
                if indexes == None:
                    row.append("err")
                else:
                    v_args = campaign.variants.variants[i].names[indexes[i]]
                    row.append(v_args)       

def indexes_table(results: ty.List[Metrics], campaign: Experimentation, **kwargs):
    """
    Returns the table containing information about variants's value for each analysis of the campaign.

        **Kwargs**:
         - `add_index` (`bool`): if `True`, index column is added, default to `False`.
         - `add_name` (`bool`): if `True`, name column is added, default to `True`.

    :param results: list of metrics for each analysis.
    :type results: List[Metrics]
    :param campaign: experimentation campaign.
    :type campaign: Experimentation
    :return: the indexes Table.
    :rtype: Table
    """
    add_index = _args.get_or_throw(kwargs, "add_index", bool, False)
    add_name = _args.get_or_throw(kwargs, "add_name", bool, True)

    header = []
    rows = []
    if add_index: header.append("ID")
    if add_name: header.append("Name")

    for i in range(len(results)):
        row = []

        if add_index: row.append(str(i))
        if add_name: row.append(results[i][res.Metrics.Name])
        
        rows.append(row)

    append_indexes_infos(results, header, rows, campaign)

    tbl = Table(len(header))
    tbl.set_header(header)
    for row in rows: tbl.add_row(row)

    return tbl

def run_callback(analysis: Analysis, exp: Experimentation, results: ty.List[Metrics], at_start : ty.Callable[[Analysis, Experimentation, ty.List[Metrics]], None] = None, at_end : ty.Callable[[Analysis, Experimentation, ty.List[Metrics], Metrics], None] = None, *args, **kwargs):
    """
    Default run callback for an experimentation, called on each analysis.
    This function allows to specify another action `at_start` and `at_end` of the analysis.

    **Kwargs**:
        - `quiet` (`bool`): if true, logger is set to minimal verbosity during analysis execution, reporting only errors and warnings, default: `True`.
        - `clean_cache` (`bool`): clean analysis's cache after execution (can avoid high memory usage for big use cases), default: `True`.

    :param analysis: the analysis to be ran.
    :type analysis: Analysis
    :param exp: the experimentation campaign.
    :type exp: Experimentation
    :param results: the list of results for each analysis.
    :type results: List[Metrics]
    :param at_start: callback to be called before the current analysis execution, defaults to None
    :type at_start: Callable[[Analysis, Experimentation, List[Metrics]], None], optional
    :param at_end: callback to be called after the analysis execution, defaults to None
    :type at_end: Callable[[Analysis, Experimentation, List[Metrics], Metrics], None], optional
    """
    quiet = _args.get_or_throw(kwargs, "quiet", bool, True)
    clean_cache = _args.get_or_throw(kwargs, "clean_cache", bool, True)


    verb = log.current_level()
    if quiet: log.set_verbosity(log.VerbosityLevel.Warning) # Shut up all.

    start = time.process_time()
    analysis.clean()

    if at_start is not None:
        at_start(analysis, exp, results, *args)

    if hasattr(analysis, "_execute_params"): # find cached param
        kwargs.update(analysis._execute_params)
    execute(analysis, **kwargs)
    
    # New
    ttotal = time.process_time() - start
    start_r = time.process_time()
    a_results = res.get_all_metrics(analysis)
    a_results[res.Metrics.TTotal] = ttotal + a_results[res.Metrics.TDSE]
    a_results[res.Metrics.TResults] = time.process_time() - start_r

    results.append(a_results)    

    if at_end is not None:
        at_end(analysis, exp, results, a_results, *args)

    # End
    if clean_cache: analysis.clean_cache() 

    log.set_verbosity(verb)

def print_and_save(campaign: Experimentation, max_order: int, results: ty.List[Metrics]):
    """
    Print and save all analysis of an experimentation `campaign`, for the specified fault limit `max_order`. Analysis results are stored in `results` list.

    :param campaign: the experimentation campaign.
    :type campaign: Experimentation
    :param max_order: the fault limit (used to select how many results column will be used for metrics displayed by order), cannot exceed `analysis.max_order()`. 
    :type max_order: int
    :param results: the list of metrics for each analysis.
    :type results: List[Metrics]
    """
    results_s = res.to_string(results)

    tb_idx = indexes_table(results_s, campaign)

    print(f"Programs variants:\n{tb_idx}\n")
    print(res.main_table(results_s, max_order))

    tb_full = res.full_table(results_s, max_order, add_name=False)
    tb_full = tb_full << tb_idx
    tb_full.save(os.path.join(campaign.path, "res.csv"))
    tb_aa_full = res.aar_rows_table(results_s, max_order, full=True)
    tb_aa_full.save(os.path.join(campaign.path, "aar.csv"))

    # Hard save
    from lazart._internal_.extensions import pickle_to

    pickle_to(results_s, os.path.join(campaign.path, "results"))

