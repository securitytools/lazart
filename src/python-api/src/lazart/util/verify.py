""" The module *lazart.util.verify* provides functions to verify that an analysis does not contains suspicious results, which could indicate a bad configuration.

:since: 3.1
:author: Etienne Boespflug 
"""

# Python
import types

# Lazart
import lazart.logs as log 

# Internal
import lazart._internal_.args as args
import lazart._internal_.color as color

# Core
from lazart.core.analysis import Analysis, AnalysisFlag
from lazart.core.traces import traces_list, TerminationType, all_fault_traces

# Analysis
from lazart.analysis.ccpo_classification import ccpo_classification

def has_det_in_0_fault(a: Analysis, **kwargs) -> bool:
    """
    Tests if an analysis has detector triggers in 0 faults traces, which could indicate that the program or the attack objective are ill-formed.

    :param a: the analysis to be tested.
    :type a: Analysis
    :return: `True` if it exists 0-faults traces with CCP triggers.
    :rtype: bool
    """
    if not a.flags().isset(AnalysisFlag.DetectorOptimization):
        log.error("The analysis should be CCPO: " + str(a.flags()))
        return

    traces0 = traces_list(a, 0)
    b = False

    for trace in traces0:
        triggered = trace.triggered()

        if len(triggered) > 0:
            if trace.termination_is(TerminationType.Correct):
                print("detectors triggered (0 faults): " + str(trace))
    return b

def ccpo(a: Analysis):
    """
    Prints CCPO verification information. 

    :param a: the analysis to be verified.
    :type a: Analysis
    """
    if has_det_in_0_fault(a):
        log._raw(color.blue + "verify: found detectors in O faults traces" + color.reset)
    else:
        log._raw(color.blue + "verify: no detector in 0 faults traces" + color.reset)

def attack_analysis(a: Analysis):
    """
    Prints attacks analysis verification results. Having 0-faults attacks (satisfying oracle) may imply that the oracle is ill-formed.

    :param a: the analysis to be verified.
    :type a: Analysis
    """
    l = traces_list(a, 0)

    suspicious = []

    for trace in l:
        if trace.termination_is(TerminationType.Correct):
            suspicious.append(trace)

    for trace in suspicious:
        log._raw(color.blue + trace.name + ": " + str(trace.termination_type) + color.reset)

    if len(suspicious) > 0:
        log._raw(color.blue + "verify: this program present 0-faults attacks. Please verify the oracle or the source code." + color.reset)
    else:
        log._raw(color.blue + "verify: attacks tests passed"+ color.reset)

def traces_parsing(a: Analysis):
    """
    Prints trace parsing verification results. Verify that traces's fault count are correctly found by **Lazart**. 

    :param a: the analysis to be verified.
    :type a: Analysis
    """
    suspicious = []

    for o in range(1, a.max_order() + 1):
        for trace in traces_list(a, o):
            if not trace.faults():
                suspicious.append(trace)
    
    for trace in suspicious:
        log._raw(color.blue + trace.name + ": " + str(trace.termination_type) + color.reset)

    if len(suspicious) > 0:
        log._raw(color.blue + "verify: some traces are incorrectly detected as fault traces." + color.reset)
    else:
        log._raw(color.blue + "verify: traces tests passed" + color.reset)
