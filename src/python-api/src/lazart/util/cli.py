""" The module *lazart.util.cli* provides utility functions for lazart interactive mode.

:since: 3.0
:author: Etienne Boespflug 
"""

# Lazart
import lazart.constants as cts
import lazart.logs as log

def lz_help():
    """
    Print the help command of Lazart.
    """
    s = "Lazart v" + cts.version_string + "\n"
    s += "\n"
    s += "For more information, please consult the documentation: https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/home.\n\n"
    s += " commands:\n"
    s += "    - help:      shows Python interactive mode help.\n"
    s += "    - lz_help():  show this help message.\n"
    s += "    - version(): shows Lazart version.\n"
    s += "    - about():   shows information about authors.\n"
    s += "    - quit():    quit Lazart interactive mode.\n"
    log._raw(s)

def version():
    """
    Prints **Lazart** version.
    """
    log._raw("Lazart v" + cts.version_string)

def about():
    """
    Prints information about authors.
    """
    log._raw(f"Lazart Interactive Interface {cts.version_string}\nVERIMAG, Université de Grenoble Alpes\n\n")