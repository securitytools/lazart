""" The module *lazart.util.exec* provides functions for standard Lazart analysis execution.

:since: 3.0
:author: Etienne Boespflug 
"""

# Python
import os
import time

# Lazart
import lazart.constants as constants
import lazart.logs as log

# Internal
import lazart._internal_.extensions as _ext
import lazart._internal_.args as _args
import lazart._internal_.util as util

# Core
import lazart.core.compile
from lazart.core.analysis import Analysis, AnalysisFlag
from lazart.core.compile import compile_results
from lazart.core.run import run_results
from lazart.core.traces import traces_results

# Analysis
from lazart.analysis.attack_analysis import attacks_results
from lazart.analysis.hotspots_analysis import hotspots_results
from lazart.analysis.ccpo import ccpo
from lazart.analysis.attack_redundancy import attacks_redundancy_results

# Results
from lazart.results.report import generate_report
import lazart.results.results as _res

def __sep_line():
    """
    Print separation line between steps.
    """
    log.verbose("\n-----------------------------------------------\n")

def execute(analysis: Analysis, **kwargs) -> _res.Metrics:
    """
    Execute the specified analysis depending on its flags, running each required step, printing results and saving them into the analysis folder.

    **Kwargs**:
         - `no_analysis` (`bool`): if set, only *compilation*, *mutation and DSE* and *traces parsing* step are performed, ignoring actual analysis. It also implies that results will not be displayed, neither saved. *default*: `False`.
         - `no_save` (`bool`): if set, analysis caches are not saved in the analysis folder. *default*: `False`.
         - `no_reports` (`bool`): if set, result reports are not generated. *default*: `False`.
         - `no_display` (`bool`): if set, results are not displayed again after analysis execution. *default*: `False`.
         - `max_col` (`int`): the column limit for display (characters count of a line) used for placement of result tables on console. *default*: `os.get_terminal_size().columns` (reported terminal current size).

    :param analysis: the analysis to be executed
    :type analysis: Analysis
    :return: the metrics of the analysis.
    :rtype: _res.Metrics
    """

    start = time.process_time()           

    if not _ext.has_extension(analysis, constants.compile_ext): 
        __sep_line()
        compile_results(analysis)

    if not _ext.has_extension(analysis, constants.run_ext): 
        __sep_line()
        run_results(analysis)

    if not _ext.has_extension(analysis, constants.traces_ext): 
        __sep_line()
        traces_results(analysis, **kwargs)

    # Analysis
    if not _args.get_or_throw(kwargs, "no_analysis", bool, False):
        s_fct = _args.get_satisfies_function(kwargs, lambda trace: trace.satisfies())

        if analysis.flags().isset(AnalysisFlag.AttackAnalysisOnly):
            if not _ext.has_extension(analysis, constants.attacks_ext): 
                __sep_line()
                attacks_results(analysis, satisfies_fct=s_fct)

        if analysis.flags().isset(AnalysisFlag.EqRedAnalysisOnly):
            if not _ext.has_extension(analysis, constants.attack_redundancy_ext):          
                __sep_line()
                attacks_redundancy_results(analysis, satisfies_fct=s_fct, **util.remove_kwargs("satisfies_fct", **kwargs)) # weird ? 

        if analysis.flags().isset(AnalysisFlag.HSAnalysisOnly):
            if not _ext.has_extension(analysis, constants.hotspots_ext): 
                __sep_line()
                hotspots_results(analysis, satisfies_fct=s_fct)        

        if analysis.flags().isset(AnalysisFlag.DetectorOptimization):  
            if not _ext.has_extension(analysis, constants.ccpo_ext): 
                __sep_line()   
                ccpo(analysis, satisfies_fct=s_fct)

    ttotal = time.process_time() - start
    metrics = None

    no_display = _args.get_or_throw(kwargs, "no_display", bool, False)
    no_reports = _args.get_or_throw(kwargs, "no_reports", bool, False)
    no_save = _args.get_or_throw(kwargs, "no_save", bool, False)

    if not no_save or not no_display or not no_reports:       

        start_metrics = time.process_time()
        metrics = _res.get_all_metrics(analysis)
        metrics[_res.Metrics.TResults] = time.process_time() - start_metrics
        metrics[_res.Metrics.TTotal] = ttotal + metrics[_res.Metrics.TDSE]

        res_l = _res.to_string([metrics])

        mo = analysis.max_order()
        tb_prgm = _res.program_table(res_l, mo, add_name=False)
        tb_exec = _res.exec_table(res_l, mo, add_name=False)
        tb_time = _res.time_table(res_l, mo, add_name=False)
        tb_cov = _res.cov_table(res_l, mo, add_name=False)
        if analysis.flags().isset(AnalysisFlag.AttackAnalysisOnly):
            tb_aa = _res.aar_rows_table(res_l, mo, add_name=False)
            tb_aa_full = _res.aar_rows_table(res_l, mo, full=True, add_name=False)
        if analysis.flags().isset(AnalysisFlag.DetectorOptimization):
            tb_full = _res.full_table_ccpo(res_l, mo, add_name=False) # TODO: not implemented
        else:
            tb_full = _res.full_table(res_l, mo, add_name=False) # TODO: not implemented
        tb_hs = hotspots_results(analysis).table()

        max_col = _args.get_or_throw(kwargs, "max_col", int, os.get_terminal_size().columns)
        
        if not no_display:
            log.info(f"\n{analysis}")
            if analysis.flags().isset(AnalysisFlag.AttackAnalysisOnly):
                log.quiet(_res.column([tb_prgm, tb_aa, tb_hs], [tb_aa_full, tb_exec, tb_time, tb_cov], (["Program:", "Attacks analysis:", "Hotspots:"], ["Full attacks results", "Execution:", "Time metrics:", "Coverage:"]), padding="        ", max_col=max_col))
            elif analysis.flags().isset(AnalysisFlag.DetectorOptimization):
                cr = ccpo(analysis)
                log.quiet(f"{str(cr)}")
                loc_class = cr.classification.local_conclusions_table()
                full_class = cr.classification.conclusions_table()
                log.quiet(_res.column([tb_prgm, tb_exec, tb_time, tb_cov], [loc_class, full_class], (["Program:", "Execution:", "Time Metrics:", "Coverage:"], ["loc_class", "full_class"])))
                log.quiet(cr.selection)

        if not no_reports:
            tb_full.save(os.path.join(analysis.path(), "res.csv"))
            generate_report(os.path.join(analysis.path(), "report.md"), analysis, res_l)
            if analysis.flags().isset(AnalysisFlag.AttackAnalysisOnly):
                tb_aa_full.save(os.path.join(analysis.path(), "aar.csv"))
                tb_hs.save(os.path.join(analysis.path(), "hs.csv"))

        if not no_save:
            from lazart._internal_.extensions import pickle_to
            pickle_to(res_l, os.path.join(analysis.path(), "results"))

    return metrics   
