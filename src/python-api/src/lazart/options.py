""" This file contains definitions for **Lazart**'s options which directly affect **Lazart**'s behavior.

Those options can be modified thought access functions or thought scripts's command line argument (see :mod: `lazart.script`).

:since: 3.1
:author: Etienne Boespflug
"""

# Python
import sys

class Parameters():    
    """
    The Parameters class holds the options of Lazart.
    """
    
    def __init__(self):
        """
        **__init__ (ctor):** Constructs a new Parameters objects with default values.
        """
        self.archive = False
        """ bool: Prevents Lazart to add a .gitignore to the analysis. All the results will be included in the version control.
        *default:* `False` 
        """
        self.keep_dse_out = False
        """ bool: Determines if the DSE's temporary files (ktests) should be kept. 
        *default:* `False` 
        """ 
        self.debug = False
        """ bool: Determines if debug mode is active. 
        *default:* `False`
        """
        self.print_progress = True
        """ bool: Determines if progress should be display during long operations.
        *default:* `True`
        """
        self.defines = dict()
        """ dict: Dictionary of defines for this execution.
        *default:* `dict()`
        """ 
        self.max_int = sys.maxsize
        """ int: Maximum integer size.
        *default:* `sys.maxsize`
        """

    def __str__(self) -> str:
        """
        Returns the string representing the Parameters.

        :return: the string representation of the object.
        :rtype: str
        """      
        ret = "[archive={}, keep-dse={}, debug={}, output-folder={}, max_int={}, defines={}]".format(self.archive, self.keep_dse_out, self.debug, self.output_folder, self.max_int, self.defines)
        return ret

# global variable

_params = Parameters() 
""" parameters: The global variable parameters for *Lazart* options. 
Prefer getter / setter (e.g.: :func:`set_defines`, `defines`) method to modify and access options."""

def _get_params() -> Parameters:
    """
    Returns the global variable parameter of *Lazart*.

    Prefer getter / setter (e.g.: :func:`set_defines`, `defines`) method to modify and access options.

    :return: the global parameters.
    :rtype: Parameters
    """
    global _params
    return _params

# archive

def archive() -> bool:
    """
    Returns the value of the 'archive' *Lazart*'s option which prevents Lazart to add a .gitignore to the analysis. 
    All the results will be included in the version control.
    
    *default:* `False` 

    :return: the 'archive' option value.
    :rtype: bool
    """
    return _get_params().archive

def set_archive(b: bool):
    """
    Set the value of *Lazart*'s option 'archive'.

    :param b: the value to set.
    :type b: bool
    """
    _get_params().archive = b 

# keep_dse_out

def keep_dse_out():
    """
    Returns the value of the 'keep_dse_out' *Lazart*'s option which determines if the DSE's temporary files (ktests) should be kept.

    *default:* `False` 

    :return: the 'keep_dse_out' option value.
    :rtype: bool
    """
    return _get_params().keep_dse_out

def set_keep_dse_out(b: bool):
    """
    Set the value of *Lazart*'s option 'keel_dse_out'.

    :param b: the value to set.
    :type b: bool
    """
    _get_params().keep_dse_out = b

# debug_mode

def debug_mode(): 
    """
    Returns the value of the 'debug' *Lazart*'s option which determines if debug mode is active. 
    
    *default:* `False`

    :return: the 'debug_mode' option value.
    :rtype: bool
    """
    return _get_params().debug

def set_debug_mode(b: bool):
    """
    Set the value of *Lazart*'s option 'debug_mode'.

    :param b: the value to set.
    :type b: bool
    """
    _get_params().debug = b

# print_progress

def print_progress() -> bool:
    """
    Returns the value of the 'print_progress' *Lazart*'s option which determines if progress should be display during long operations.

    *default:* `True`

    :return: the 'print_progress' option value.
    :rtype: bool
    """
    return _get_params().print_progress

def set_print_progress(b: bool):
    """
    Set the value of *Lazart*'s option 'print_progress'.

    :param b: the value to set.
    :type b: bool
    """
    _get_params().print_progress = b

# defines

def defines(): 
    """
    Returns the dictionary of defines for this execution.
    
    *default:* `dict`

    :return: the dictionary of defines.
    :rtype: dict
    """
    return _get_params().defines

# max_int

def max_int(): 
    """
    Returns the value of the 'print_progress' *Lazart*'s option corresponding to the maximum integer size.
    
    *default:* `sys.maxsize`

    :return: the 'max_int' option value.
    :rtype: bool
    """
    return _get_params().max_int

def set_max_int(i: int): 
    """
    Set the value of *Lazart*'s option 'max_int'.

    :param b: the value to set.
    :type b: bool
    """
    _get_params().max_int = i


