"""
This file is the main file of Lazart.

It imports core features of Lazart into global namespace.

:since: 3.0
:author: Etienne Boespflug 
"""

# Lazart
import lazart.constants as cts
import lazart.options as options
from lazart.logs import init, error, VerbosityLevel
import lazart.logs as log
from lazart.script import install_script

# Core
from lazart.core.analysis import AnalysisFlag
from lazart.core.attack_model import ti_model, data_model, functions_list, bb_list, empty_model, merge_am
from lazart.core.fault_model import FaultModel
from lazart.core.compile import compile_results, compile
from lazart.core.run import run_results, run
from lazart.core.traces import Trace, traces_results, get_trace, get_traces, traces, traces_list, all_traces, all_fault_traces, TerminationType, get_trace_by_name
from lazart.core.io import read_or_make, save, read
from lazart.core.countermeasures import cm_tm, cm_sec_swift, cm_lm

# Analysis
from lazart.analysis.attack_analysis import attacks_results
from lazart.analysis.hotspots_analysis import hotspots_results
from lazart.analysis.ccpo import ccpo
from lazart.analysis.ccpo_classification import ccpo_classification, triggering_points, CCPOClass
from lazart.analysis.ccpo_selection import ccpo_selection
from lazart.analysis.attack_redundancy import attacks_redundancy_results, prefix_rule, sub_word_rule, eq_rule, feq_rule, minimals, representatives

# Results
from lazart.results.graphs import make_fault_graph, generate_attacks_fault_graphs, graph, generate_redundancy_graph, generate_detector_graph
from lazart.results.report import generate_report
from lazart.results.results import Metrics, get_all_metrics, append_result, append_results, custom_table, aar_rows_table, program_table, time_table, exec_table, cov_table, exec_full_table, full_table, full_table_ccpo, main_table, attack_analysis_htable

# Util
from lazart.util.exec import execute
import lazart.util.verify as verify 
import lazart.results.results as results

# Exp
import lazart.exps.experimentations as exps
import lazart.exps.variants as variants