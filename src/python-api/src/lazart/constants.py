""" The module *lazart.constants* contains values and parameters for **Lazart**.

:since: 3.0
:author: Etienne Boespflug
"""

# Version
version_major = 4 #: int: The major version number.
version_minor = 0 #: int: The minor version number.
version_patch = 0 #: int: The patch version number.
version_string = str(version_major) + "." + str(version_minor) + "." + str(version_patch) #: str: The version string 'MAJOR.MINOR.PATCH'.
version_tuple = (version_major, version_minor, version_patch) # (int, int, int): The version number tuple (comparable).

# Docker paths
klee_lib_path = "/home/build/lib/" 
""" str: Klee library path in **Lazart**'s docker.

Modify this value for custom installation.
"""
klee_include_path = "/home/klee/include"
""" str: Klee includes path in **Lazart**'s docker.

Modify this value for custom installation.
"""
lazart_include_path = "/opt/lazart/usr/includes"
""" str: **Lazart**'s includes path in **Lazart**'s docker.

Modify this value for custom installation.
"""
lazart_src_path = "/opt/lazart/usr/src"
""" str: **Lazart**'s sources path in **Lazart**'s docker.

Modify this value for custom installation.
"""
lazart_bin_path = "/opt/lazart/usr/bin"
""" str: **Lazart**'s binary path in **Lazart**'s docker.

Modify this value for custom installation.
"""

# Cpp API
lazart_defines_args = "-DLAZART -D_LZ__VERSION=" + version_string #: str: The arguments to pass to compiler containing **Lazart**'s defines.
attack_analysis_define = "_LZ__ATTACKS" #: str: **Lazart**'s define for attack analysis.
ccpo_define = "_LZ__CCPO" #: str: **Lazart**'s define for CCPO.    

# Lazint
lazint_banner = "Lazart Interactive Interface (supports for Lazart " + version_string + ")\n\nUse 'lz_help()' for more more information" #: str: **Lazart**'s banner for interactive mode.

# Values
# ------

#: int: Maximum size of a trace bucket in analysis folder.
traces_bucket_size = 64

# Defines
# -------

# Args
# ----
#: str: The key of the standard kwargs `satisfies_fct`.
arg_satisfies_function = "satisfies_fct"

# Commands
# --------

# Executables
#: str: Executable name for compiler.
compiler_cmd = "clang"
#: str: Executable name for linker.
linker_cmd = "llvm-link"
#: str: Executable name for disassembler.
dis_cmd = "llvm-dis"
#: str: Executable name for DSE engine (KLEE).
dse_cmd = "klee"
#: str: Executable name for mutation tool (Wolverine).
wolverine_cmd = "wolverine"

# Default arguments
#: str: Executable name for compiler.
compiler_default_args = ""
#: str: Executable name for linker.
linker_default_args = ""
#: str: Executable name for disassembler.
dis_default_args = ""
#: str: Executable name for DSE engine (KLEE).
dse_default_args = ""
#: str: Executable name for mutation tool (Wolverine).
wolverine_default_args = ""


# File naming
# -----------

#: str: Report location relative to analysis folder.
report = "report.md"
#: str: Absolute log file location in docker.
logfile = "/opt/lazart/logs.txt"

# Folders

#: str: Graph folder location.
graphs_folder = "graphs/"
#: str: Compilation folder (not used for now).
compile_folder = "compilation/"
#: str: Trace folder location.
traces_folder = "traces/"
#: str: Traces bucket folder.
traces_bucket_folder = "traces_buckets/"
#: str: Lazart hidden directory.
lazart_dir = ".lazart/"

# Binary Caches

#: str: Analysis binary location.
analysis_file = "analysis"
#: str: Compilation results binary location relative to `lazart_dir`. 
compile_ext = "compile_results"
#: str: Mutation and DSE results binary location relative to `lazart_dir`.
run_ext = "run_results"
#: str: Traces results binary location relative to `lazart_dir`.
traces_ext = "traces"
traces_bucket_fmt = "traces_buckets/{}/bucket{}.tr"
"""str: Traces bucket path.

:arg1: The order of the trace bucket.
:arg2: The bucket number for this order.
"""
#: str: Attack analysis binary location relative to `lazart_dir`.
attacks_ext = "attacks_results"
#: str: Attack redundancy binary location relative to `lazart_dir`.
attack_redundancy_ext = "attacks_redundancy_results"
#: Attack redundancy binary location relative to `lazart_dir`.
hotspots_ext = "hotspots_results"
#: CCPO location relative to `lazart_dir`.
ccpo_ext = "ccpo_results"
#: CCPO classification location relative to `lazart_dir`.
ccpo_classification_ext = "ccpo_class_results"
#: CCPO selection location relative to `lazart_dir`.
ccpo_selection_ext = "ccpo_selection_results"
#: Placement relative to `lazart_dir`.
placement_ext = "placement_results"

# Files
dse_folder = "dse_out" #: str: DSE folder path inside analysis folder.
replay_bc = "replay" #: str: Replay executable path inside analysis folder.
mutated_bc = "mutated.bc" #: str: Mutated LLVM-IR bytecode path inside analysis folder.
mutated_ll = "mutated.ll" #: str: Mutated LLVM-IR (textual) path inside analysis folder.
source_bc = "main.bc" #: str: Source LLVM-IR bytecode path inside analysis folder.
source_ll = "main.ll" #: str: Source LLVM-IR (textual) path inside analysis folder.
preprocessed_bc = "preprocessed.bc" #: str: Preprocessed LLVM-IR bytecode path inside analysis folder. 
protected_bc = "protected.bc" #: str: Preprocessed LLVM-IR (textual) path inside analysis folder.
injection_points = "injection_points.yaml" #: str: Wolverine's IPs data file path inside analysis folder.
detector_data = "ccp_list.yaml" #: str: Wolverine's detectors data file path inside analysis folder.
attack_model = "am.yaml" #: str: Attack model (YAML) path inside analysis folder.
dot_file = ".{}.dot" 
""" str: Naming pattern for CFG dot files relative inside analysis folder.

:arg1: The name of the function.
"""
dot_ll_file = ".{}.ll.dot"
""" str: Naming pattern for CFG dot files with complete LLVM code inside analysis folder.

:arg1: The name of the function.
"""
trigger_point_graph = "trigger_points.{}.png"
""" str: Naming pattern for the graph of triggering points.

:arg1: The name of the function.
"""
pdf_graph_file = "cfg.{}.pdf"
""" str: Naming pattern for the pdf CFG files.

:arg1: The name of the function.
"""
