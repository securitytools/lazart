/**
 * @file argparse.hpp
 * @author Etienne Boespflug (etienne.boespflug@gmail.com)
 * @brief Definitions relative to command line arguments parsing and storing.
 * @version 4.0
 * 
 * Contains interface for the args class containing parsed command line arguments and several function allowing to parse CLI arguments.
 */

#pragma once

#include <string>

#include "util/log.hpp"
#include "util/util.hpp"

namespace lazart { 
/**
 * The `lazart::cli` namespace contains CLI-specific functions and classes
 * used to parse arguments for the program and enhance user interaction.
 */
namespace cli {
/**
 * @brief The args struct holds CLI'argument parsing data. 
 */
struct args {
    /// @brief true, the parsing encountered some errors and the application should be stopped
    bool abort = false;
    /// @brief If true, the parsing required help and should stop the application.
    bool help = false;

    /// @brief  The path to the user defined am.yaml
    std::string attack_model_path = "";
    /// @brief  The path to the LLVM module (.bc).
    std::string module_path = "";
    /// @brief  Output for the mutated module (.bc).
    std::string mutated_module_path = "";
    /// @brief  Output folder for generated files.
    std::string output_folder = "";

    /// @brief If `true`, unused function will not be deleted from the final output module.
    bool no_clean_unused_function = false;
    /// @brief If `true`, no inlining will be performed on the final output module.
    bool no_inlining = false;

    /// @brief The verbosity level choosen for this analysis.
    util::optional<util::VerbosityLevel> verbosity;

    /// @brief Global fault limit for the fault models.
    int fault_limit = 2;

    /**
     * @brief Print a dump of the structure to the standard output.
     */
    void dump() const;

    /**
     * @brief Returns an \ref args object with abort flag set to `true`.
     * 
     * @return args an aborted \ref args object.
     */
    static args get_abort() 
    { 
        auto ret = args{};
        ret.abort = true;
        return ret;
    }
};

/**
 * @brief Parse the arguments of the program and returns the corresponding \ref args struct.
 *
 * @param argc the arguments count.
 * @param argv the arguments vector.
 * @return args the parsed arguments.
 */
args get_args(int argc, char* argv[]);
} // namespace cli
} // namespace lazart
