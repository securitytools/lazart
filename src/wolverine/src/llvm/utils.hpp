/**
 * @file utils.hpp
 * @author Etienne Boespflug (etienne.boespflug@gmail.com)
 * @brief Definition of several utility functions for LLVM.
 * @version 4.0
 * 
 * This file contains definitions of LLVM's utility functions.
 */
#pragma once 

#include <set>
#include <iostream>
#include <typeinfo>

#include <llvm/IR/Function.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/IRBuilder.h>
#include "llvm/Support/raw_ostream.h"

namespace lazart {
/**
 * @brief The xllvm namespace contains functions and tools for manipulating LLVM IR inside of Lazart.
 * 
 * note : xllvm stands for "eXtended LLVM". The namespace name lazart::llvm was rejected due to
 * potential conflict when lazart namespace is put on global scope.
 */
namespace xllvm {
    namespace details {
        /**
         * @brief Fill the set of function with all functions called inside the specified one. 
         * This function performs a recursive scan and call itself for each new callee.
         * 
         * @param f the function from which the callee search start.
         * @param set the set of function already discovered (possibly not empty). 
         */
        void callees(llvm::Function* f, std::set<llvm::Function*>& set);
    }

    /**
     * @brief Explore the call graph to compute the list of callees of the specified function.
     * 
     * @param f the function to which the callees will be searched.
     * @param include_intrinsic if true, intrinsic function will be included in the list.
     * @return std::set<llvm::Function*> the set of f's callees.
     */
    std::set<llvm::Function*> callees(llvm::Function* f, bool include_intrinsic = false);

    /**
     * @brief Returns the LLVM context corresponding to the instruction `inst`.
     * 
     * @param inst the instruction.
     * @return llvm::LLVMContext& the context of the instruction.
     */
    llvm::LLVMContext& get_context(llvm::Instruction* inst);

    /**
     * @brief Get the standard printf function.
     * 
     * @param mod the module in which the function will be searched.
     * @return llvm::Constant* the printf function.
     */
    llvm::Constant* get_printf(llvm::Module* mod);

    /**
     * @brief Get the KLEE's builtin `klee_is_replay` function.
     * 
     * @param mod the module in which the function will be searched.
     * @return llvm::Constant* the `klee_is_replay` function.
     */
    llvm::Constant* get_klee_is_replay(llvm::Module* mod);

    /**
     * @brief Get the KLEE's builtin `klee_make_symbolic` function.
     * 
     * @param mod the module in which the function will be searched.
     * @param data_type the argument type to the function.
     * @return llvm::Constant* the `klee_make_symbolic` function.
     */
    llvm::Constant* get_klee_make_symbolic(llvm::Module* mod, llvm::Type* data_type);

    /**
     * @brief Convert an object to a string, using `llvm::raw_string_ostream`.
     * 
     * @tparam T object type to be converted to string.
     * @param object the object to be converted to string.
     * @return std::string 
     */
    template<typename T>
    std::string to_string(const T& object) {
        std::string str;
        llvm::raw_string_ostream ss{ str };
        ss << object;
        return ss.str(); 
    }

    /**
     * @brief Return the function enclosing the specified LLVM value `v`.
     * 
     * @param v the value.
     * @return const llvm::Function* the enclosing function. 
     */
    const llvm::Function* get_enclosing_function(const llvm::Value* v);

    /**
     * @brief Returns the pair of boolean determining respectively if the module is valid and
     * if debug information are valid.
     * 
     * @todo params to log debug errors ? 
     * 
     * @param module the module to be verified.
     * @return std::pair<bool, bool> a pair of boolean determining the validity of the module and the debug information.
     */
    std::pair<bool, bool> verify_module(llvm::Module* module);
    
    /**
     * @brief Insert a call to the function `name` and arguments (`args`) before the instruction `inst`.
     * For Lazart's mutation functions see #call_mut_function.
     * 
     * @param name the name of the function to be called.
     * @param type the type (signature) of the function to be called.
     * @param args the arguments of the function call. 
     * @param inst the instruction before which the call will be inserted.
     * @param reg_name the name of the instruction call.
     * @param throws boolean indicating if the function can throws.
     * @return llvm::CallInst* the created call instruction.
     */
    llvm::CallInst* call_function(const std::string& name, llvm::FunctionType* type, std::vector<llvm::Value*> args, llvm::Instruction* inst, const std::string& reg_name = "", bool throws = false);

    /**
     * @brief Insert a call to the mutation function of the specified `name`, `type` (signature) and `args` (arguments) before the instruction `inst`.
     * 
     * @param name the name of the function to be called.
     * @param type the type (signature) of the function to be called.
     * @param args the arguments of the function call. 
     * @param inst the instruction before which the call will be inserted.
     * @param reg_name the name of the instruction call.
     * @return llvm::CallInst* the created call instruction.
     */
    llvm::CallInst* call_mut_function(const std::string& name, llvm::FunctionType* type, std::vector<llvm::Value*> args, llvm::Instruction* inst, const std::string& reg_name = "");

    /**
     * @brief Return a string corresponding to the location of the instruction `i` inside the original source code with the format `[line, column]`. Returns an error string if no location debug info are attached to the instruction.
     * 
     * @note prefer #xllvm::get_location if you need more information.
     * 
     * @param i the instruction to be located.
     * @return std::string the string corresponding to the location of `i` in the source file.
     */
    std::string get_location(llvm::Instruction* i);

    /**
     * @brief Return the string value of the specified `operand` argument of the function call `call_inst` string literal. Return an empty string if the function does not match de expected signature or if an error prevented to retrieve the string literal value.
     * 
     * @param call_inst the call instruction with the string argument. 
     * @param operand the index of the argument in the function call.
     * @param clean_null if true, any '\0' character is removed from the string.
     * @return std::string the string corresponding to the specified argument's string literal value if possible, an empty string otherwise.
     */
    std::string get_string_literal_arg(llvm::CallInst* call_inst, int operand = 0, bool clean_null = false);

    /**
     * @brief Return the name of the function called in the specified call instruction `c_inst`. Return an empty string if the name cannot be retrieved.
     * 
     * @param c_inst the call instruction.
     * @return std::string the name of the called function.
     */
    std::string get_called_fct_name(llvm::CallInst* c_inst);

    } // lazart::xllvm
} // lazart
