#pragma once 
/**
 * @file llvm_fwd.hpp
 * @author Etienne Boespflug (etienne.boespflug@gmail.com)
 * @brief Definition of forward declarations for LLVM API.
 * @version 4.0
 * 
 * This file contains forward declaration for commonly used LLVM's IR types. 
 */

namespace llvm {
    class Instruction;
    class LoadInst;
    class CallInst;
    class BasicBlock;
    class Constant;
    class Function;
    class Module;
    class MDNode;
    class StringRef;
    class Value;
    class Type;
    class LLVMContext;
    class DebugLoc;
} // llvm