#include "utils.hpp"

#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Module.h>
#include <llvm/Transforms/Utils/BasicBlockUtils.h>
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/DebugInfoMetadata.h"
#include "llvm/IR/Metadata.h"
#include <llvm/IR/Verifier.h>
#include <llvm/Support/raw_ostream.h>

#include <boost/algorithm/string/predicate.hpp>

#include <cstdlib>
#include <iostream>
#include <sstream>

#include "util/util.hpp"
#include "core/wolverine.hpp"

// Wolverine

#include "export/export.hpp"


using FT = llvm::FunctionType;
using Ty = llvm::Type;

void lazart::xllvm::details::callees(llvm::Function* f, std::set<llvm::Function*>& set) {
    if(f == nullptr)
        return;


    for(auto& bb : *f)
        for(auto& inst : bb) {
            //inst.print(llvm::errs());
            // Log().info("oc= " + std::string(inst.getOpcodeName()));
            if (llvm::isa<llvm::CallInst>(inst)) {
                // Log().info("New call inst");
                llvm::CallInst * callInst = llvm::dyn_cast<llvm::CallInst>(&inst);
                std::string fname;
                llvm::Function *cf;
                //https://stackoverflow.com/questions/53086557/casting-tail-call-void-i32-bitcast-to-an-llvmfunction-to-get-the-fnattr
                if(callInst->getCalledFunction())
                {
                    cf = callInst->getCalledFunction();
                    fname = cf->getName().str();
                } 
                else if(llvm::dyn_cast<llvm::Function>(callInst->getCalledValue()->stripPointerCasts()))
                {
                    cf = llvm::dyn_cast<llvm::Function>(callInst->getCalledValue()->stripPointerCasts());
                    fname = cf->getName().str();
                }

                if(boost::starts_with(fname, "llvm."))
                    continue;
                auto pair = set.insert(cf);
                if(pair.second) {
                    callees(cf, set);
                }
            }
        }
}


// https://stackoverflow.com/questions/21410675/getting-the-original-variable-name-for-an-llvm-value
const llvm::Function* lazart::xllvm::get_enclosing_function(const llvm::Value* v) 
{
    if (const llvm::Argument* arg = dyn_cast<llvm::Argument>(v))
        return arg->getParent();
    if (const llvm::Instruction* inst = dyn_cast<llvm::Instruction>(v))
        return inst->getParent()->getParent();
    return nullptr;
}

llvm::LLVMContext& lazart::xllvm::get_context(llvm::Instruction* inst)
{
    return inst->getParent()->getParent()->getContext();
}

std::string lazart::xllvm::get_called_fct_name(llvm::CallInst* cinst)
{
    std::string fname;

    //https://stackoverflow.com/questions/53086557/casting-tail-call-void-i32-bitcast-to-an-llvmfunction-to-get-the-fnattr
    if(cinst->getCalledFunction())
        return cinst->getCalledFunction()->getName().str();
    else if(llvm::dyn_cast<llvm::Function>(cinst->getCalledValue()->stripPointerCasts()))
        return llvm::dyn_cast<llvm::Function>(cinst->getCalledValue()->stripPointerCasts())->getName().str();
    return "";
}

std::pair<bool, bool> lazart::xllvm::verify_module(llvm::Module* module)
{
    bool debug_error = false;
    if(llvm::verifyModule(*module, &llvm::outs(), &debug_error))
    {
        Log().critical("broken module, aborting.");
        return {false, false};
    }
    return {true, !debug_error};
}

std::set<llvm::Function*> lazart::xllvm::callees(llvm::Function* f, bool include_intrinsic)
{
    if(f == nullptr)
        return {};

    std::set<llvm::Function*> set;
    details::callees(f, set);

    // Maybe should not be explored at all
    if(!include_intrinsic) {
        for(auto it = set.begin(); it != set.end();) {
            if((*it)->isIntrinsic())
                it = set.erase(it);
            else
                ++it;
        }
    }
    return set;
}

llvm::CallInst* lazart::xllvm::call_function(const std::string& name, llvm::FunctionType* type, std::vector<llvm::Value*> args, llvm::Instruction* inst, const std::string& reg_name, bool throws) {
    llvm::Module* mod = inst->getParent()->getParent()->getParent();
    auto callee = mod->getOrInsertFunction(name, type);
    auto fun = dyn_cast<llvm::Constant>(callee.getCallee());

    llvm::ArrayRef<llvm::Value*> arg_range(args);

    auto c = llvm::CallInst::Create(fun, arg_range, reg_name, inst);
    
    if(!throws)
        c->setDoesNotThrow();
    return c;
}

llvm::Constant* lazart::xllvm::get_klee_is_replay(llvm::Module* mod)
{
    auto i32_ty = Ty::getInt32Ty(mod->getContext());

    auto fun_ty = FT::get(i32_ty, {}, false);
    auto fct = mod->getOrInsertFunction("klee_is_replay", fun_ty);

    return dyn_cast<llvm::Constant>(fct.getCallee());;
}

llvm::Constant* lazart::xllvm::get_printf(llvm::Module* mod)
{
    auto pi8_ty = Ty::getInt8PtrTy(mod->getContext());
    auto i32_ty = Ty::getInt32Ty(mod->getContext());

    auto fun_ty = FT::get(i32_ty, {pi8_ty}, true); 
    auto fct = mod->getOrInsertFunction("printf", fun_ty);

    return dyn_cast<llvm::Constant>(fct.getCallee());;
}

llvm::Constant* lazart::xllvm::get_klee_make_symbolic(llvm::Module* mod, llvm::Type* data_type)
{
    auto pi8_ty = Ty::getInt8PtrTy(mod->getContext());
    auto i32_ty = Ty::getInt32Ty(mod->getContext());

    auto fun_ty = FT::get(i32_ty, true);  // Hack ? 
    auto fct = mod->getOrInsertFunction("klee_make_symbolic", fun_ty);

    return dyn_cast<llvm::Constant>(fct.getCallee());;
}

llvm::CallInst* lazart::xllvm::call_mut_function(const std::string& name, llvm::FunctionType* type, std::vector<llvm::Value*> args, llvm::Instruction* inst, const std::string& reg_name) {
    auto c = call_function(name, type, args, inst, names::wolverine_prefix + "fc_d" + reg_name, false);

    // do not work: https://stackoverflow.com/questions/54524188/create-debug-location-for-function-calls-in-llvm-function-pass
    if(c->getCalledFunction())
        c->getCalledFunction()->addAttribute(llvm::AttributeList::FunctionIndex, llvm::Attribute::NoInline);

    return c;
}

std::string lazart::xllvm::get_location(llvm::Instruction* inst)
{
    auto de = inst->getDebugLoc();
    if(!de)
        return "[no debug info]";
    return "[" + std::to_string(de.getLine()) + ", " + std::to_string(de.getCol()) + "]";
}

std::string lazart::xllvm::get_string_literal_arg(llvm::CallInst* callInst, int operand, bool clean_null) {
    if(llvm::ConstantExpr* pCE = dyn_cast<llvm::ConstantExpr>(callInst->getArgOperand(operand))) {
        if(llvm::GlobalVariable * pGV = dyn_cast<llvm::GlobalVariable>(pCE->getOperand(0))) {
            if(llvm::ConstantDataSequential * pCA = dyn_cast<llvm::ConstantDataSequential>(pGV->getInitializer())) {
                auto ret = pCA->getAsString().str();
                if(clean_null)
                    ret.pop_back(); // always \0 at end.
                return ret;
            }
        }
    }
    return "";
}