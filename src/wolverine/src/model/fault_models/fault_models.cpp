#include "model/fault_models/fault_models.hpp"

#include "util/util.hpp"

std::string lazart::model::fault_type_str(fault_type type)
{
    if(type == fault_type::TestInversion)
        return "ti";
    if(type == fault_type::DataLoad)
        return "data";
    if(type == fault_type::Jump)
        return "jump";
    if(type == fault_type::SwitchCall)
        return "sc";
    if(type == fault_type::User)
        return "user";
    Log().critical("unknown type " + std::to_string((int) type));
    return "<error>";
}

lazart::model::fault_type lazart::model::to_string(std::string& str) 
{
    if(str == "TI" || str == "TestInversion")
        return fault_type::TestInversion;
    if(str == "DL" || str == "DataLoad")
        return fault_type::TestInversion;
    if(str == "JMP" || str == "Jump")
        return fault_type::TestInversion;
    if(str == "SC" || str == "SwitchCall")
        return fault_type::TestInversion;

    return fault_type::User;
}
