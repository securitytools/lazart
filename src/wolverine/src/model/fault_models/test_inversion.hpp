/**
 * @file test_inversion.hpp
 * @author Etienne Boespflug (etienne.boespflug@gmail.com)
 * @brief This file defines the test inversion fault model.
 * 
 * @version 4.0
 */

#include "fault_models.hpp"

namespace lazart::model {

    /**
     * @brief Test inversion fault model.
     */
    class test_inversion : public fault_model
    {
    public:

        /**
         * @brief Construct a new `test_inversion` object with the specified name and parameters.
         * 
         * @param name the name of the fault model, possibly empty.
         * @param protected_trigger_fct the name of the detector function used to protect detection points (forbids fault forcing the countermeasure detection).
         * @param inl_ determines if the experimental inline mutation should be used. TODO: Not implemented.
         */
        test_inversion(const std::string& name, const std::vector<std::string>& protected_trigger_fct, bool inl_ = false) : fault_model(name), inl{inl_}, protected_trigger_fct_{ protected_trigger_fct } {}

        virtual std::string str() const override;
        fault_type get_fault_type() const override { return fault_type::TestInversion; }
        virtual bool should_mutate(llvm::Instruction& inst, Mutation& params) const override;
        virtual std::pair<llvm::Instruction*, lazart::mutation::fault> mutate(int ip_id, llvm::Instruction* inst, Mutation& params) const override;

        /// @brief Name of Lazart's TI mutation function.
        static constexpr auto ti_mut_fct = "_LZ__mut_ti";
        /// @brief Name of Lazart's TI mutation function with true branch protected.
        static constexpr auto ti_true_mut_fct = "_LZ__mut_ti_true";
        /// @brief Name of Lazart's TI mutation function with false branch protected.
        static constexpr auto ti_false_mut_fct = "_LZ__mut_ti_false";

        /// @brief Determines if the experimental inline mutation should be used. 
        bool inl;

    protected:
        /**
         * @brief List of detector function names to protect.
         */
        std::vector<std::string> protected_trigger_fct_;

        /**
         * @brief Apply test inversion mutation using the inline version (avoiding the call to a mutation function and separating the code for each IP).
         * 
         * @param ip_id the injection point id.
         * @param inst the load instruction to be mutated.
         * @param params the context of the mutation. 
         * @return std::pair<llvm::Instruction*, lazart::mutation::fault> pair corresponding to the next instruction to be considered by the mutation path and the data of the generated IP. 
         * @todo Not implemented.
         */
        std::pair<llvm::Instruction*, lazart::mutation::fault> mutate_inline(int ip_id, llvm::Instruction* inst, Mutation& params) const;
    };
    
} // lazart::model