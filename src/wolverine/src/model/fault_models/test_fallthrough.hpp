/**
 * @file test_fallthrough.hpp
 * @author Etienne Boespflug (etienne.boespflug@gmail.com)
 * @brief This file defines the test fallthrough fault model.
 * 
 * @todo NOT IMPLEMENTED, use instrumentation directives.
 * @version 4.0
 */

#include "fault_models.hpp"

namespace lazart::model {

    /// @brief NOT IMPLEMENTED.
    /// @todo NOT IMPLEMENTED.
    class test_fallthrough : public fault_model
    {
    public:
        enum class Mode 
        {
            ThenElse,
            ElseThen
            // Both ?
        };

        test_fallthrough(const std::string& name, Mode m) : fault_model(name), mode_{ m } {}

        virtual std::string str() const override;
        fault_type get_fault_type() const override { return fault_type::TestFallthrough; }
        virtual bool should_mutate(llvm::Instruction& inst, Mutation& params) const override;
        virtual std::pair<llvm::Instruction*, lazart::mutation::fault> mutate(int ip_id, llvm::Instruction* inst, Mutation& params) const override;
    
    private:
        Mode mode_;
    };
    
} // lazart::model