#include "test_fallthrough.hpp"

#include <iostream>
#include <sstream>

#include "core/wolverine.hpp"
#include "model/mutation.hpp"
#include "llvm/utils.hpp"
#include "util/util.hpp"
#include "mutation/metadata/metadata.hpp"

#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Module.h>
#include <llvm/Transforms/Utils/BasicBlockUtils.h>
#include "llvm/Support/raw_ostream.h"


bool lazart::model::test_fallthrough::should_mutate(llvm::Instruction& inst, Mutation& params) const
{
    if(llvm::BranchInst* bInst = llvm::dyn_cast<llvm::BranchInst>(&inst))
    {
        return bInst->getNumSuccessors() > 1;
    }
    return false;
}


std::pair<llvm::Instruction*, lazart::mutation::fault> lazart::model::test_fallthrough::mutate(int ip_id, llvm::Instruction* inst, Mutation& params) const
{ 
    Log().debug("    creating IP " + std::to_string(ip_id) + " (tf) at " + md::get_location(inst).str() + ".");

    llvm::BranchInst* bInst = llvm::dyn_cast<llvm::BranchInst>(inst);

    auto err_ret = std::make_pair<llvm::Instruction*, lazart::mutation::fault>(nullptr, {});
    return err_ret;
}

std::string lazart::model::test_fallthrough::str() const
{
    return "[TF '" + name_ + "': mode=" + ((mode_ == Mode::ThenElse) ? "te" : "et") + "]";
}
