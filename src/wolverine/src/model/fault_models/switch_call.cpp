#include "switch_call.hpp"

#include <iostream>
#include <sstream>
#include <algorithm>

#include "core/wolverine.hpp"
#include "model/mutation.hpp"
#include "llvm/utils.hpp"
#include "util/util.hpp"
#include "mutation/metadata/metadata.hpp"

#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Module.h>
#include <llvm/Transforms/Utils/BasicBlockUtils.h>
#include "llvm/Support/raw_ostream.h"

bool lazart::model::switch_call::should_mutate(llvm::Instruction& inst, Mutation& params) const
{
    if(llvm::CallInst* cinst = llvm::dyn_cast<llvm::CallInst>(&inst))
    {
        return std::find(source_calls.begin(), source_calls.end(), cinst->getName()) != source_calls.end();
    }
    return false;
}


std::pair<llvm::Instruction*, lazart::mutation::fault> lazart::model::switch_call::mutate(int ip_id, llvm::Instruction* inst, Mutation& params) const
{ 
    Log().debug("    creating IP " + std::to_string(ip_id) + " (ti) at " + md::get_location(inst).str() + ".");

    llvm::BranchInst* bInst = llvm::dyn_cast<llvm::BranchInst>(inst);

    auto err_ret = std::make_pair<llvm::Instruction*, lazart::mutation::fault>(nullptr, {});

    return err_ret;
}

std::string lazart::model::switch_call::str() const
{
    return "[SC '" + name_ + "']";
}
