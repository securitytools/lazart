/**
 * @file switch_call.hpp
 * @author Etienne Boespflug (etienne.boespflug@gmail.com)
 * @brief This file defines the switch call fault model.
 * 
 * @todo NOT IMPLEMENTED, usesinstrumentation directives.
 * @version 4.0
 */

#include "fault_models.hpp"

namespace lazart::model {

    /// @brief NOT IMPLEMENTED.
    /// @todo NOT IMPLEMENTED.
    class switch_call : public fault_model
    {
    public:
        switch_call(const std::string& name) : fault_model(name) {}

        virtual std::string str() const override;
        fault_type get_fault_type() const override { return fault_type::SwitchCall; }
        virtual bool should_mutate(llvm::Instruction& inst, Mutation& params) const override;
        virtual std::pair<llvm::Instruction*, lazart::mutation::fault> mutate(int ip_id, llvm::Instruction* inst, Mutation& params) const override;

    private:
        std::vector<std::string> target_function; // function switchable
        std::vector<std::string> source_calls; // function 
    };
    
} // lazart::model