#include "test_inversion.hpp"

#include <iostream>
#include <sstream>
#include <string>

#include "core/wolverine.hpp"
#include "model/mutation.hpp"
#include "llvm/utils.hpp"
#include "util/util.hpp"
#include "mutation/metadata/metadata.hpp"

#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Module.h>
#include <llvm/Transforms/Utils/BasicBlockUtils.h>
#include <llvm/Support/raw_ostream.h>

constexpr auto field = lazart::fields::mutation;

bool lazart::model::test_inversion::should_mutate(llvm::Instruction& inst, Mutation& params) const
{
    if(llvm::BranchInst* bInst = llvm::dyn_cast<llvm::BranchInst>(&inst))
    {
        return bInst->getNumSuccessors() > 1;
    }
    return false;
}

std::pair<llvm::Instruction*, lazart::mutation::fault> lazart::model::test_inversion::mutate_inline(int ip_id, llvm::Instruction* inst, Mutation& params) const
{
    Log().cyan().info("      creating IP " + std::to_string(ip_id) + " (ti) at " + md::get_location(inst).str() + ".", field);
    
    llvm::BranchInst* bInst = llvm::dyn_cast<llvm::BranchInst>(inst);
    auto md_dbg = bInst->getMetadata("dbg");

    auto err_ret = std::make_pair<llvm::Instruction*, lazart::mutation::fault>(nullptr, {});

    if(!bInst) 
    {
        Log().critical("Invalid instruction for test-inversion mutation ...");
        return err_ret;
    }
    if((bInst->getNumSuccessors() <= 1))
    {
        Log().critical("Invalid successor count for test-inversion mutation on branch instruction: ...");
        llvm::errs() << *inst;
        return err_ret;
    }    

    llvm::BasicBlock& bb = *bInst->getParent();
    llvm::Function* fun = bb.getParent();
    llvm::Module* mod = fun->getParent();

    llvm::IRBuilder<> builder(&bb);

    Log().critical("not implemented");
    exit(1); 
}

std::pair<llvm::Instruction*, lazart::mutation::fault> lazart::model::test_inversion::mutate(int ip_id, llvm::Instruction* inst, Mutation& params) const
{ 
    if(inl)
        return mutate_inline(ip_id, inst, params);

    Log().cyan().info("      creating IP " + std::to_string(ip_id) + " (ti) at " + md::get_location(inst).str() + ".", field);

    llvm::BranchInst* bInst = llvm::dyn_cast<llvm::BranchInst>(inst);
    auto md_dbg = bInst->getMetadata("dbg");

    auto err_ret = std::make_pair<llvm::Instruction*, lazart::mutation::fault>(nullptr, {});

    if(!bInst) 
    {
        Log().critical("Invalid instruction for test-inversion mutation ...");
        return err_ret;
    }
    if((bInst->getNumSuccessors() <= 1))
    {
        Log().critical("Invalid successor count for test-inversion mutation on branch instruction: ...");
        llvm::errs() << *inst;
        return err_ret;
    }    

    llvm::BasicBlock& bb = *bInst->getParent();
    llvm::Function* fun = bb.getParent();
    llvm::Module* mod = fun->getParent();

    llvm::IRBuilder<> builder(&bb);

    llvm::Value* v = bInst->getOperand(0);


    bool mute_true{ !lazart::tasks::is_ccp(cast<llvm::BasicBlock>(bInst->getOperand(2)), protected_trigger_fct_) };
    bool mute_false{ !lazart::tasks::is_ccp(cast<llvm::BasicBlock>(bInst->getOperand(1)), protected_trigger_fct_) };

    auto mutation_fct_name = "<error>";
    if(mute_false && mute_true)
        mutation_fct_name = ti_mut_fct;
    else if(!mute_true && mute_false)
        mutation_fct_name = ti_true_mut_fct;
    else if(!mute_false && mute_true)
        mutation_fct_name = ti_false_mut_fct;
    else {
        Log().warning("No edge can be set on IP, change 'protected_detectors' fields to disable CCP protection.");
        return err_ret;
    }
    
    // Add call
    auto int32Type = llvm::Type::getInt32Ty(mod->getContext());
    auto int8PtrType = llvm::Type::getInt8PtrTy(mod->getContext());
    llvm::Type *argsType3[] = {int32Type, int32Type, int8PtrType, int8PtrType, int8PtrType, int8PtrType};
    auto funType = llvm::FunctionType::get(int32Type, argsType3, false);

    std::vector<llvm::Value*> values;
    builder.SetInsertPoint(bInst);
    auto casted_br_result = builder.CreateIntCast(v, llvm::Type::getInt32Ty(mod->getContext()), true);
    values.push_back(casted_br_result);
    llvm::ConstantInt* fault_limit = llvm::ConstantInt::get(llvm::Type::getInt32Ty(mod->getContext()), params.fault_limit);
    values.push_back(fault_limit);

    // Create string args
    auto ip_prefix = lazart::names::ip_prefix + std::to_string(ip_id);

    llvm::Value* ip_id_str = builder.CreateGlobalStringPtr(std::to_string(ip_id), ip_prefix + "_id");
    
    llvm::Value* currentbb_strptr = builder.CreateGlobalStringPtr(bb.getName().str(), ip_prefix + "_current_bb");
    llvm::Value* targetA_strptr = builder.CreateGlobalStringPtr(bInst->getOperand(2)->getName().str(), ip_prefix + "_targetA");
    llvm::Value* targetB_strptr = builder.CreateGlobalStringPtr(bInst->getOperand(1)->getName().str(), ip_prefix + "_targetB");

    values.push_back(ip_id_str);
    values.push_back(currentbb_strptr);
    values.push_back(targetA_strptr);
    values.push_back(targetB_strptr);

    llvm::Instruction* mutation_fct = xllvm::call_mut_function(mutation_fct_name, funType, values, inst, std::to_string(ip_id));
    mutation_fct->setMetadata("dbg", md_dbg);
        

    auto casted_mutated_result = builder.CreateICmpNE(mutation_fct, llvm::ConstantInt::get(llvm::Type::getInt32Ty(mod->getContext()), 0, true));

    bInst->setOperand(0, (llvm::Value*)casted_mutated_result);

    // Fault
    lazart::mutation::fault fault = {
        .ip_id = std::to_string(ip_id),
        .ip_num = ip_id,
        .function = fun,
        .det_id = md::get_lz_cm_ip(bInst),
        .type = fault_type::TestInversion,
        .args = "null",
        .bb_name = bb.getName().str(),
        .loc = md::get_location(inst),
        .data_emitter = [=](YAML::Emitter& out) { 
            if(mutation_fct_name == ti_mut_fct)
                return;
            
            out << YAML::Key << "data" << YAML::BeginMap;
            out << YAML::Key << "edge" << YAML::Value << (mutation_fct_name == ti_true_mut_fct ? "true" : "false"); 
            out << YAML::EndMap; 
        }
    };

    return std::make_pair<llvm::Instruction*, lazart::mutation::fault>(dynamic_cast<llvm::Instruction*>(bInst), std::move(fault));
}

std::string lazart::model::test_inversion::str() const
{
    auto ret = "[TI '" + name_ + "'";
    if(inl)
        ret += ", inl";
    if(protected_trigger_fct_.size() != 0)
        ret +=", prot=[" + util::join(protected_trigger_fct_, ",") + "]";

    return ret + "]";
}
