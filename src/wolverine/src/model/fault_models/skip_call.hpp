/**
 * @file skip_call.hpp
 * @author Etienne Boespflug (etienne.boespflug@gmail.com)
 * @brief This file defines the skip call fault model.
 * 
 * @todo NOT IMPLEMENTED, USE INSTRUMENTATION DIRECTIVES.
 * @version 4.0
 */

#include "fault_models.hpp"

namespace lazart::model {

    /// @brief NOT IMPLEMENTED.
    /// @todo NOT IMPLEMENTED.
    class skip_call : public fault_model
    {
    public:
        skip_call(const std::string& name) : fault_model(name) {}

        std::string skiped_function;

        virtual std::string str() const override;
        fault_type get_fault_type() const override { return fault_type::TestInversion; }
        virtual bool should_mutate(llvm::Instruction& inst, Mutation& params) const override;
        virtual std::pair<llvm::Instruction*, lazart::mutation::fault> mutate(int ip_id, llvm::Instruction* inst, Mutation& params) const override;
    };
    
} // lazart::model