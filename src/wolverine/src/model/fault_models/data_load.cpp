#include "data_load.hpp"

#include <iostream>
#include <sstream>
#include <optional>

#include "model/mutation.hpp"
#include "llvm/utils.hpp"
#include "util/util.hpp"
#include "mutation/metadata/metadata.hpp"

#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/Type.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Module.h>
#include <llvm/Transforms/Utils/BasicBlockUtils.h>
#include "llvm/Support/raw_ostream.h"

constexpr auto field = lazart::fields::mutation;

// Data
std::pair<bool, lazart::model::data_load_model_value> lazart::model::data_load_model::mutation_value(const std::string& var_name) const
{
    if(values_.count(var_name)) {
           return {true, values_.at(var_name)};
    }
    if(all_.has_value())
        if(std::find(exclude_.begin(), exclude_.end(), var_name) == exclude_.end()) {
            return {true, all_.get()};
        }
    return {false, data_load_model_value{}};
}


bool lazart::model::data_load_model::should_mutate(llvm::Instruction& inst, Mutation& params) const
{
    if (llvm::LoadInst* linst = llvm::dyn_cast<llvm::LoadInst>(&inst))
    {
        auto var_name = md::get_var_name(linst);

        if(var_name == "")
            return false;

        return mutation_value(var_name).first;
    }
    return false;
}

std::string lazart::model::data_load_model::type_suffix(llvm::LLVMContext& ctx, llvm::Type* type)
{
    if(type == llvm::Type::getInt1Ty(ctx))
        return "i8";
    if(type == llvm::Type::getInt8Ty(ctx))
        return "i8";
    if(type == llvm::Type::getInt16Ty(ctx))
        return "i16";
    if(type == llvm::Type::getInt32Ty(ctx))
        return "i32";
    if(type == llvm::Type::getInt64Ty(ctx))
        return "i64";
    if(type == llvm::Type::getFloatTy(ctx)) {
        Log().warning("float type requested");
        return "f"; // never used.
    }
    Log().warning("unsupported llvm type: " + xllvm::to_string(type));
    return "__error__";
}

std::string lazart::model::data_load_model::select_mut_fct(llvm::Module* mod, llvm::Type* base_type, data_load_model_value val, std::vector<llvm::Type*>& types, std::vector<llvm::Value*>& values, Mutation& params) const
{
    std::string mutation_fct_suffix = "__error__";
    using value_t = data_load_model_value::value_t;
    auto value_type = val.type();
    auto ts = type_suffix(mod->getContext(), base_type);

    //auto int8PtrType = llvm::Type::getInt8PtrTy(mod->getContext());

    auto sym_fct_type = llvm::FunctionType::get(base_type, {base_type, base_type}, false);
    auto sym_fct_ptr_type = llvm::PointerType::get(sym_fct_type, 0);
    auto fct_type = llvm::FunctionType::get(base_type, {base_type}, false);
    auto fct_ptr_type = llvm::PointerType::get(fct_type, 0);


    if(value_type == value_t::FixedReal)
    {
        Log().critical("not implemented.");
        return "";
    }
    else if(value_type == value_t::FixedInt)
    {
        mutation_fct_suffix = "fix";
        values.push_back(llvm::ConstantInt::get(base_type, val.value_i()));
        types.push_back(base_type); // Fixed value
    }
    else if(value_type == value_t::Symbolic)
    {
        mutation_fct_suffix = "sym";
        if(!use_sym) // Do not use specific function for symbolic (use pred version).
        {
            mutation_fct_suffix = "sym_pred";
            values.push_back(llvm::ConstantPointerNull::get(llvm::PointerType::get(sym_fct_type, 0)));
            types.push_back(sym_fct_ptr_type);
        }
    }
    else
    {
        // Check predicate
        auto pred_fct_name = val.predicate();

        auto pred_fct = mod->getFunction(pred_fct_name); // TODO, typed variant !
        if(pred_fct == nullptr) // TODO check type ?
        {
            Log().critical("cannot find the function '" + pred_fct_name + "' for data mutation.");
            return "";
        }

        values.push_back(pred_fct); // TODO verify TYPE ?

        // Check version
        if(value_type == value_t::Predicate) // Non symbolic function
        {
            mutation_fct_suffix = "fct";
            auto t = pred_fct->getFunctionType();
            if(t != fct_type) {
                Log().critical("invalid function type generated, expected " + xllvm::to_string(*t) + ", got " + xllvm::to_string(fct_type));
            }
            types.push_back(fct_ptr_type);
        }
        else if(value_type == value_t::SymbolicConstraint)
        {
            if(use_sym_fct)
                mutation_fct_suffix = "sym_fct";
            else
                mutation_fct_suffix = "sym_pred";

            types.push_back(sym_fct_ptr_type);
        }
        else
        {
            Log().critical("unknown data value type..." + std::to_string(static_cast<int>(value_type)));
            return "";
        }
    }

    return "_LZ__mut_dl_" + ts + "_" + mutation_fct_suffix;
}
std::pair<llvm::Instruction*, lazart::mutation::fault> lazart::model::data_load_model::mutate_inline(int ip_id, llvm::Instruction* inst, Mutation& params) const
{
    auto err_ret = std::make_pair<llvm::Instruction*, lazart::mutation::fault>(nullptr, {});

    llvm::LoadInst* lInst = llvm::dyn_cast<llvm::LoadInst>(inst);
    auto md_dbg = lInst->getMetadata("dbg");


    if(!lInst)
    {
        Log().critical("invalid instruction for data-load mutation, expected load: " + xllvm::to_string(*inst) + ".");
        return err_ret;
    }

    llvm::Value* original = lInst->getPointerOperand();

    auto var_name = md::get_var_name(lInst).str();
    auto value_p = mutation_value(var_name);
    Log().cyan().info("      creating IP " + std::to_string(ip_id) + " (data)(var=" + var_name + ") at " + md::get_location(inst).str() + ".", field);

    // TODO ignroe lazart prefix (when we mute globals)

    if(!value_p.first)
    {
        Log().critical("invalid instruction to be mutated, no value found for '" + var_name + "' in model:  " + xllvm::to_string(*inst) + ".");
        return err_ret;
    }

    // Data
    auto& basic_block = *lInst->getParent();
    auto function = basic_block.getParent();
    auto mod = function->getParent();
    auto& context = params.context;

    Log().warning("bb: " + xllvm::to_string(basic_block));

    auto dataLayout = new llvm::DataLayout(mod);
    auto type = original->getType()->getPointerElementType();
    int bits = cast<llvm::IntegerType>(type)->getBitWidth();
    Log().warning("found " + std::to_string(bits) + " : " + xllvm::to_string(*inst));
    if(bits % 8 != 0) {
        Log().critical("unexpected data layout.");
    }
    int size = bits / 8;

    using Ty = llvm::Type;
    auto pi8_ty = Ty::getInt8PtrTy(context);
    auto i32_ty = Ty::getInt32Ty(context);

    auto fcount = mod->getGlobalVariable("_LZ__fault_count"); //, Type::getInt32Ty(getGlobalContext()));
    llvm::ConstantInt* fault_limit = llvm::ConstantInt::get(i32_ty, params.fault_limit);

    // Head

    auto ip_str = "ip" + std::to_string(ip_id);

    auto bb_tail = llvm::SplitBlock(&basic_block, lInst, nullptr, nullptr, nullptr); //, "head", true);
    std::string oname = basic_block.getName().str();
    auto bb_head = &basic_block;
    bb_head->setName(ip_str + "_h");
    bb_tail->setName(oname);

    auto bb_fault = llvm::BasicBlock::Create(context, ip_str + "_f", function, bb_tail);
    llvm::IRBuilder<> builder_fault(bb_fault);

    auto bb_inject = llvm::BasicBlock::Create(context, ip_str + "_i", function, bb_tail);
    llvm::IRBuilder<> builder_inject(bb_inject);

    auto bb_print = llvm::BasicBlock::Create(context, ip_str + "_p", function, bb_tail);
    llvm::IRBuilder<> builder_print(bb_print);

    auto bb_inject_end = llvm::BasicBlock::Create(context, ip_str + "_e", function, bb_tail);
    llvm::IRBuilder<> builder_inject_end(bb_inject_end);

    auto bb_normal = llvm::BasicBlock::Create(context, ip_str + "_n", function, bb_tail);
    llvm::IRBuilder<> builder_normal(bb_normal);

    llvm::IRBuilder<> builder_head(bb_head);

    auto old_br = bb_head->getTerminator();
    old_br->eraseFromParent();
    //auto bb_head_br = builder_head.CreateCondBr(icmp, bb_normal, bb_fault);

    auto new_var = builder_head.CreateAlloca(type, nullptr, "new_var"); // ->setMetadata("dbg", md_dbg) ?
    auto new_load = builder_head.CreateAlignedLoad(original, 4, "new_load"); // TODO handle  type
    auto fcount_load = builder_head.CreateAlignedLoad(fcount, 4, "fcount_load");
    auto icmp = builder_head.CreateICmpSGE(fcount_load, fault_limit, "flim_cmp");

    builder_head.CreateCondBr(icmp, bb_normal, bb_fault);

    // Fault
    auto inject = builder_fault.CreateAlloca(type, nullptr, "inject");

    {
        auto inject_str = builder_fault.CreateGlobalStringPtr(std::to_string(ip_id) + "_inj", lazart::names::ip_prefix + std::to_string(ip_id) + "_inj");

        builder_fault.CreateCall(xllvm::get_klee_make_symbolic(mod, type), {inject, llvm::ConstantInt::get(i32_ty, size) // TODO handle size
        , inject_str}, "sym_inject");
    }
    auto inject_loaded = builder_fault.CreateLoad(inject, "inject_loaded"); // TODO:  FUNCTION_KLEE_MAKE_SYM(inject, type);
    auto zero_const = llvm::ConstantInt::get(type, 0);
    auto fault_icmp = builder_fault.CreateICmpNE(inject_loaded, zero_const);
    builder_fault.CreateCondBr(fault_icmp, bb_normal, bb_inject);

    // Inject
    auto fcount_incr = builder_inject.CreateAdd(builder_inject.getInt32(1), fcount_load);
    auto fcount_store = builder_inject.CreateStore(fcount_incr, fcount);

    auto value = builder_inject.CreateAlloca(type);
    {
        // klee_make_sym(ptr, size, name)
        // void*, int32, pi8

        auto inject_str = builder_inject.CreateGlobalStringPtr(std::to_string(ip_id) + "_inj", lazart::names::ip_prefix + std::to_string(ip_id) + "_inj");

        builder_inject.CreateCall(xllvm::get_klee_make_symbolic(mod, type), {value, llvm::ConstantInt::get(i32_ty, 4 // TODO handle size
        ), inject_str}, "sym_value");
    }
    auto value_loaded = builder_inject.CreateLoad(value, "value_loaded");

    auto is_replay = builder_inject.CreateCall(xllvm::get_klee_is_replay(mod), {}, "is_replay");
    is_replay->setMetadata("dbg", md_dbg);
    auto inject_icmp = builder_inject.CreateICmpNE(builder_inject.getInt32(0), is_replay);
    builder_inject.CreateCondBr(fault_icmp, bb_inject_end, bb_print);

    {
        auto fmt = "%d";
        auto str = std::string("\n[FAULT] [DL] [") + fmt + "] [~]\n";
        auto fmt_str = builder_print.CreateGlobalStringPtr(str, lazart::names::ip_prefix + std::to_string(ip_id) + "_fmt");

        auto ip_id_str = builder_print.CreateGlobalStringPtr(std::to_string(ip_id), lazart::names::ip_prefix + std::to_string(ip_id) + "_id");

        std::vector<llvm::Value*> args{fmt_str, builder_print.getInt32(ip_id)};

        builder_print.CreateCall(xllvm::get_printf(mod), args, "print");
    }
    auto bb_print_term = builder_print.CreateBr(bb_inject_end);

    auto inject_store = builder_inject_end.CreateAlignedStore(value_loaded, new_var, 4, 0);
    builder_inject_end.CreateBr(bb_tail);

    auto normal_store = builder_normal.CreateAlignedStore(new_load, new_var, 4, 0);
    builder_normal.CreateBr(bb_tail);

    lInst->setOperand(0, (llvm::Value*)new_var);
    lInst->setName(ip_str + "_result");

    // Fault
    lazart::mutation::fault fault = {
        .function = function,
        .ip_id = std::to_string(ip_id),
        .det_id = md::get_lz_cm_ip(lInst),
        .ip_num = ip_id,
        .type = fault_type::DataLoad,
        .bb_name = basic_block.getName().str(),
        .loc = md::get_location(inst),
        .data_emitter = [=](YAML::Emitter& out) { // TODO: ugly
            out << YAML::Key << "data" << YAML::BeginMap;

            out << YAML::Key << "value" << YAML::Value << value_p.second.str(); // TODO
            out << YAML::Key << "impl" << YAML::Value << "inline";//mutation_fct_name;
            out << YAML::Key << "var" << YAML::Value << var_name;
            out << YAML::Key << "type" << YAML::Value << type_suffix(mod->getContext(), type);
            out << YAML::EndMap;
        }
    };

    return std::make_pair<llvm::Instruction*, lazart::mutation::fault>(std::move(inst), std::move(fault));
}

std::pair<llvm::Instruction*, lazart::mutation::fault> lazart::model::data_load_model::mutate(int ip_id, llvm::Instruction* inst, Mutation& params) const
{
    if(inl)
        return mutate_inline(ip_id, inst, params);
    Log().cyan().verbose("      creating IP " + std::to_string(ip_id) + " (data) at " + md::get_location(inst).str() + ".", field);
    
    auto err_ret = std::make_pair<llvm::Instruction*, lazart::mutation::fault>(nullptr, {});

    llvm::LoadInst* lInst = llvm::dyn_cast<llvm::LoadInst>(inst);
    auto md_dbg = lInst->getMetadata("dbg");
    
    if(!lInst)
    {
        Log().critical("invalid instruction for data-load mutation, expected load: " + xllvm::to_string(*inst) + ".");
        return err_ret;
    }

    llvm::Value* original = lInst->getPointerOperand();

    auto var_name = md::get_var_name(lInst).str();
    auto value_p = mutation_value(var_name);

    // TODO ignore lazart prefix (when we mute globals)
    if(!value_p.first)
    {
        Log().critical("invalid instruction to be mutated, no value found for '" + var_name + "' in model:  " + xllvm::to_string(*inst) + ".");
        return err_ret;
    }

    // Data
    auto& basic_block = *lInst->getParent();
    auto function = basic_block.getParent();
    auto mod = function->getParent();

    llvm::IRBuilder<> builder(&basic_block);
    builder.SetInsertPoint(inst);

    auto type = original->getType()->getPointerElementType();
    auto new_var = builder.CreateAlloca(type);

    auto new_load = builder.CreateAlignedLoad(original, 4, "");
    new_load->setMetadata("dbg", md_dbg);

    auto ip_id_str = builder.CreateGlobalStringPtr(std::to_string(ip_id), lazart::names::ip_prefix + std::to_string(ip_id) + "_id");

    auto int8PtrType = llvm::Type::getInt8PtrTy(mod->getContext());
    auto int32Type = llvm::Type::getInt32Ty(mod->getContext());

    // Add call
    // Creating arguments and types
    auto fault_limit = llvm::ConstantInt::get(int32Type, params.fault_limit);
    std::vector<llvm::Value*> values{ new_load, fault_limit, ip_id_str};
    std::vector<llvm::Type*> types{ type, int32Type, int8PtrType}; // Base types: original, fault limit, ip_id

    auto mutation_fct_name = select_mut_fct(mod, type, value_p.second, types, values, params);
    if(mutation_fct_name == "" || (mutation_fct_name.find("_error_") != std::string::npos))
        return err_ret;

    auto mutation_fct_type = llvm::FunctionType::get(type, types, false);

    auto mutation_fct = xllvm::call_mut_function(mutation_fct_name, mutation_fct_type, values, inst, std::to_string(static_cast<int>(value_p.second.type())) + "_" + std::to_string(ip_id));
    if(mutation_fct->getCalledFunction() == nullptr)
    {
        Log().critical("cannot find called function: " + mutation_fct_name);
        return err_ret;
    }

    mutation_fct->setMetadata("dbg", md_dbg);

    builder.CreateAlignedStore(mutation_fct, new_var, 4, 0)->setMetadata("dbg", md_dbg);

    inst->setOperand(0, (llvm::Value*)new_var);

    // Fault
    lazart::mutation::fault fault = {
        .function = function,
        .ip_id = std::to_string(ip_id),
        .det_id = md::get_lz_cm_ip(lInst),
        .ip_num = ip_id,
        .type = fault_type::DataLoad,
        .bb_name = basic_block.getName().str(),
        .loc = md::get_location(inst),
        .data_emitter = [=](YAML::Emitter& out) {
            out << YAML::Key << "data" << YAML::BeginMap;
            out << YAML::Key << "value" << YAML::Value << value_p.second.str();
            out << YAML::Key << "impl" << YAML::Value << mutation_fct_name;
            out << YAML::Key << "var" << YAML::Value << var_name;
            out << YAML::Key << "type" << YAML::Value << type_suffix(mod->getContext(), type);
            out << YAML::EndMap;
        }
    };

    return std::make_pair<llvm::Instruction*, lazart::mutation::fault>(std::move(inst), std::move(fault));
}


std::string lazart::model::data_load_model::str() const
{
    std::vector<std::string> contents;
    if(all_.has_value())
        contents.push_back("{all=" + all_.get().str() + "}");
    if(exclude_.size() > 0)
    {
        contents.push_back("{excludes: " + util::join(exclude_) + "}");
    }
    if(values_.size() > 0)
    {
        std::vector<std::string> strs;
        for(const auto& var: values_)
        {
            strs.push_back(var.first + "=" + var.second.str());
        }
        contents.push_back("{vars: " + util::join(strs) + "}");
    }

    std::vector<std::string> opts;
    if(use_sym_fct)
        opts.push_back("use-sym-fct");
    if(use_sym)
        opts.push_back("use-sym");

    std::string opts_str = (opts.size() > 0) ? ", {opts: " + util::join(opts) + "}" : "";

    return "[DL '" + name_ + "':" + util::join(contents) + opts_str + "]";
}
