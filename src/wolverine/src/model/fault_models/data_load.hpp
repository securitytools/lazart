/**
 * @file data_load.hpp
 * @author Etienne Boespflug (etienne.boespflug@gmail.com)
 * @brief This file defines the data load fault model.
 * 
 * @version 4.0
 */
#pragma once

#include "fault_models.hpp"

namespace lazart::model {
    /**
     * @brief The `data_load_model_value` holds the value of the data load injection, corresponding to a variant of several type (see #value_t).
     * 
     * @todo Use actual variant.
     */
    class data_load_model_value
    {
    public:
        /**
         * @brief The `value_t` enum enumerates the value types that can be selected for a data injection.
         * 
         */
        enum value_t
        {
            /// @brief Fixed integer value injection.
            FixedInt,
            /// @brief Fixed float value injection.
            /// @todo not implemented.
            FixedReal,
            /// @brief Injection transforming the original value with a function (holds the function name).
            Predicate,
            /// @brief Symbolic injection.
            Symbolic,
            /// @brief Symbolic injection constrained by a predicate (holds the predicate function).
            SymbolicConstraint
        };

        /**
         * @brief Construct a new `data_load_model_value` object with #FixedInt value type and the integral value.
         * 
         * @param value the integer value to be injected. 
         */
        explicit data_load_model_value(long value): type_{ FixedInt }, value_i_{ value } {}

        /**
         * @brief Construct a new `data_load_model_value` object with #FixedRead value type and the float value.
         * 
         * @param value the integer value to be injected. 
         * @note real mutation is unsupported in current version.
         */
        explicit data_load_model_value(double value): type_{ FixedReal }, value_f_{ value } {}

        /**
         * @brief Construct a new `data_load_model_value` object using #Predicate if #sym is false, #SymbolicConstraint otherwise. The predicate function name is stored.
         * 
         * @param value the predicate function name.
         * @param sym if true, uses #SymbolicConstraint type instead of #Predicate type.
         */
        explicit data_load_model_value(std::string value, bool sym = false): type_{ sym ? SymbolicConstraint : Predicate }, predicate_{ value } { }
        /**
         * @brief Construct a new `data_load_model_value` object with #Symbolic value type. 
         */
        explicit data_load_model_value(): type_{ Symbolic } {}

        /**
         * @brief Returns the type of the value.
         * 
         * @return value_t the value type.
         */
        value_t type() const { return type_; }

        /**
         * @brief Returns the string representation of the value, including the value type.
         * 
         * @return std::string the string representation of the data value.
         */
        std::string str() const 
        {
            if(type_ == FixedInt) return std::to_string(value_i_);
            if(type_ == FixedReal) return std::to_string(value_f_);
            if(type_ == Predicate) return predicate_;
            if(type_ == Symbolic) return "__sym__";
            if(type_ == SymbolicConstraint) return "__sym__:" + predicate_;
            return predicate_;
        }

        /**
         * @brief Returns the name of the predicate function. Print a warning if the value type is not #Predicate or #SymbolicConstraint.
         * 
         * @return std::string the name of the predicate function.
         */
        std::string predicate() {
            if(type_ != SymbolicConstraint && type_ != Predicate)
                Log().warning("unexpected call to predicate with data value: " + str());
            return predicate_;
        }

        /**
         * @brief Returns the integral value. Print a warning if the value is not #FixedInt.
         * 
         * @return long long the value to be injected.
         */
        long long value_i() {
            if(type_ != FixedInt)
                Log().warning("unexpected call to value_i with data value: " + str());
            return value_i_;
        }

    private:
        /// @brief The type of the value to be injected.
        value_t type_;
        /// @brief The integral value for #FixedInt.
        long long value_i_;
        /// @brief The real value for #FixedReal.
        double value_f_;
        /// @brief The function name for predicate mode.
        std::string predicate_;
    };

    /**
     * @brief Data load fault model. Holds name (for instrumentation directives) and provides standard methods (`should_mutate`, `mutate`). 
     */
    class data_load_model : public fault_model
    {
    public:
        /**
         * @brief Construct a new `data_load_model` object with the specified name and parameters.
         * 
         * @param all default mutation value for all variable in mutated section of the program, optional. 
         * @param vars map of variables to be mutated associated to their mutation values (see #value_t).
         * @param exclude list of variables to be excluded from the mutation. 
         * @param name the name of the fault model, possibly empty.
         * @param inl determines if the experimental inline mutation should be used. 
         * @param opt_use_sym_fct determines if the #SymbolicConstraint mode should use the function variant. See wiki for more information. Deprecated.
         * @param opt_use_sym determines if the #SymbolicConstraint should be symbolic or fall back to #Predicate. Deprecated.
         */
        data_load_model(util::optional<data_load_model_value> all, std::map<std::string, data_load_model_value> vars, std::vector<std::string> exclude, const std::string& name, bool inl = false,
            bool opt_use_sym_fct = false, bool opt_use_sym = false) : fault_model(name), 
            use_sym_fct { opt_use_sym_fct },
            use_sym{ opt_use_sym },
            inl{ inl },
            all_{ all },
            values_{ vars },
            exclude_ { exclude }
            {}

        virtual std::string str() const override;
        fault_type get_fault_type() const override { return fault_type::DataLoad; }
        virtual bool should_mutate(llvm::Instruction& inst, Mutation& params) const override;
        virtual std::pair<llvm::Instruction*, lazart::mutation::fault> mutate(int ip_id, llvm::Instruction* inst, Mutation& params) const override;

        /**
         * @brief Returns the suffix of the mutation function depending on the value type.
         * 
         * @param ctx the context of the mutation.
         * @param type the type of the variable.
         * @return std::string the type suffix of the mutation function.
         */
        static std::string type_suffix(llvm::LLVMContext& ctx, llvm::Type* type);

        // Options

        /**
         * @brief Determines if the #SymbolicConstraint mode should use the function variant.See wiki for more information. Deprecated.
         * 
         * @note Deprecated.
         */
        bool use_sym_fct; 

        /**
         * @brief Determines if the #SymbolicConstraint should be symbolic or fall back to #Predicate. Deprecated.
         * 
         * @note Deprecated.
         */
        bool use_sym;

        /// @brief Determines if the experimental inline mutation should be used. 
        bool inl;

    private:

        /**
         * @brief Returns the mutation value of the specified variable name depending on model parameters.
         * 
         * @param var_name the name of the variable. 
         * @return std::pair<bool, lazart::model::data_load_model_value> pair of a boolean indicating if a value exists and the mutation value to be used in exists.
         * @todo Use optional.
         */
        std::pair<bool, lazart::model::data_load_model_value> mutation_value(const std::string& var_name) const;

        /**
         * @brief Retuns the name of Lazart's mutation function to be used depending on the instruction pointer type and the model parameters.
         * 
         * @param mod the module of the mutation.
         * @param base_type the type of the `LLVM::Value` to be read by the load instruction.
         * @param val the mutation value.
         * @param types the mutation function argument types list, in which the value will be added.
         * @param values the mutation function argument values, in which the value will be added. 
         * @param params the context of the mutation. 
         * @return std::string the name of the mutation function to be used.
         */
        std::string select_mut_fct(llvm::Module* mod, llvm::Type* base_type, data_load_model_value val, std::vector<llvm::Type*>& types, std::vector<llvm::Value*>& values, Mutation& params) const;
    
        /**
         * @brief Apply data load mutation using the inline version (avoiding the call to a mutation function and separating the code for each IP).
         * 
         * @param ip_id the injection point id.
         * @param inst the load instruction to be mutated.
         * @param params the context of the mutation. 
         * @return std::pair<llvm::Instruction*, lazart::mutation::fault> pair corresponding to the next instruction to be considered by the mutation path and the data of the generated IP. 
         * @note Experimental version. Does not support all options. 
         */
        std::pair<llvm::Instruction*, lazart::mutation::fault> mutate_inline(int ip_id, llvm::Instruction* inst, Mutation& params) const;

        /**
         * @brief Default mutation value for all variables in mutated section of the program.
         * 
         * All variables will be mutated with this default value if they are not specified in #value_ or explicitly excluded in #exclude_.
         */
        util::optional<data_load_model_value> all_;
        /**
         * @brief Map of variables to be mutated associated to their mutation values (see #value_t).
         */
        std::map<std::string, data_load_model_value> values_;
        /**
         * @brief List of variables to be excluded from the mutation. 
         * 
         * Only exclude variable when #all_ ('all' field) is set. Value specified in #values_ ('vars' field) will be mutated anyway. 
         */
        std::vector<std::string> exclude_;
    };
} // lazart::model