/**
 * @file attack_model_parser.hpp
 * @author Etienne Boespflug (etienne.boespflug@gmail.com)
 * @brief This file defines the main parser of the YAML attack model input file of Wolverine.
 * 
 * @version 4.0
 */
#pragma once

#include <string>
#include <memory>
#include <set>
#include <utility>

#include "model/attack_model.hpp"
#include "model/fault_models/fault_models.hpp"
#include "model/mutation.hpp"
#include "mutation/tasks/countermeasures/countermeasures.hpp"
#include "util/util.hpp"
#include "yaml-cpp/node/type.h"
#include "yaml-cpp/yaml.h"

namespace llvm {
class Instruction;
class Module;
} // namespace llvm

namespace lazart {
namespace cli {
struct args;
} // namespace cli

namespace model {
/**
 * @brief The attack_model_parser is the root parser of the Lazart analysis parsing process. 
 * 
 * @todo Create struct location containing information about fault model (line / file / etc) or fault space for error display.
 */
class attack_model_parser {
    /** @brief The mutation context. */
    Mutation& mutation_;

  public:
    /**
     * @brief Construct a new attack model parser object for the specified mutation.
     * 
     * @param mut context of the mutation.
     */
    attack_model_parser(Mutation &mut);

    /**
     * @brief Parse an attack model from the program arguments.
     * 
     * @param args the argument of the program, containing the attack
     *  model file location.
     * @return true if the parsing ended successfully
     * @return false if an error occurred during parsing.
     */
    bool parse(const lazart::cli::args& args);

  private:
    /// @brief the list of parsed fault model nodes and the corresponding fault model pointer (from attack model).
    std::vector<std::pair<YAML::Node, fault_model*>> parsed_nodes_{};
    /**
     * @brief Returns the fault model corresponding to the specified YAML node or `nullptr`.
     * 
     * @param node the fault model node.
     * @return fault_model* the corresponding fault model in the attack model.
     */
    fault_model* get_model(YAML::Node& node);
    /**
     * @brief Add a fault model node and its corresponding fault model pointer (from attack model).
     * 
     * @param node the YAML node to be added.
     * @param model the pointer to the fault model. 
     */
    void add_model(YAML::Node& node, fault_model* model);

    /**
     * @brief Actual parsing from the root node of the YAML attack model file.
     * 
     * @param root_node the root node of the attack model.
     * @param args the CLI arguments.
     */
    void parse_(const YAML::Node& root_node, const lazart::cli::args& args);

    /**
     * @brief Parses the mutation section of the file.
     * 
     * @param root_node root YAML node of the section.
     * @param args the arguments of the program.
     */
    void parse_main_section(const YAML::Node& root_node, const lazart::cli::args& args);
    
    /* Parse Tasks section. */

    /**
     * @brief Parse the "tasks" section of the YAML attack model.
     * 
     * @param root_node root node of the "tasks" section.
     */
    void parse_tasks(const YAML::Node& root_node);

    /* Countermeasures */
    /**
     * @brief Parses the countermeasures section of the YAML attack model.
     * 
     * @param cm_section root node of the countermeasure section.
     */
    void parse_countermeasures_section(const YAML::Node& cm_section);

    /**
     * @brief Parses a countermeasure task node and returns the parsed object.
     * 
     * @param cm_node the root node of the countermeasure.
     * @param n the index of the countermeasure node to be parsed, used for error display.
     * @return std::unique_ptr<tasks::countermeasure> the parsed countermeasure task.
     */
    std::unique_ptr<tasks::countermeasure> parse_countermeasure_task(const YAML::Node& cm_node, int n);

    /**
     * @brief Parses a test-multiplication (TM) countermeasure task node.
     * 
     * @param cm_node the root node of the countermeasure.
     * @param cm_str the name of the countermeasure.
     * @param on the list of functions on which the countermeasure should be applied.
     * @param ccps the list of countermeasure application points to be transformed, mutating all point if empty.
     * @param trigger_fct the name of Lazart's detector function to be called. 
     * @param n the index of the countermeasure node to be parsed, used for error display.
     * @return std::unique_ptr<tasks::countermeasure> the parsed countermeasure task.
     */
    std::unique_ptr<tasks::countermeasure> parse_test_multiplication(const YAML::Node& cm_node, const std::string& cm_str, const std::vector<std::string>& on, const std::vector<std::string>& ccps, const std::string& trigger_fct, int n);
    
    /**
     * @brief Parses a load-multiplication (LM) countermeasure task node.
     * 
     * @param cm_node the root node of the countermeasure.
     * @param cm_str the name of the countermeasure.
     * @param on the list of functions on which the countermeasure should be applied.
     * @param ccps the list of countermeasure application points to be transformed, mutating all point if empty.
     * @param trigger_fct the name of Lazart's detector function to be called. 
     * @param n the index of the countermeasure node to be parsed, used for error display.
     * @return std::unique_ptr<tasks::countermeasure> the parsed countermeasure task.
     */
    std::unique_ptr<tasks::countermeasure> parse_load_multiplication(const YAML::Node& cm_node, const std::string& cm_str, const std::vector<std::string>& on, const std::vector<std::string>& ccps, const std::string& trigger_fct, int n);
    
    /**
     * @brief Parses a SecSwift Control Flow (SSCF) countermeasure task node.
     * 
     * @param cm_node the root node of the countermeasure.
     * @param cm_str the name of the countermeasure.
     * @param on the list of functions on which the countermeasure should be applied.
     * @param ccps the list of countermeasure application points to be transformed, mutating all point if empty.
     * @param trigger_fct the name of Lazart's detector function to be called. 
     * @param n the index of the countermeasure node to be parsed, used for error display.
     * @return std::unique_ptr<tasks::countermeasure> the parsed countermeasure task.
     */
    std::unique_ptr<tasks::countermeasure> parse_sec_swift(const YAML::Node& cm_node, const std::string& cm_str, const std::vector<std::string>& on, const std::vector<std::string>& ccps, const std::string& trigger_fct, int n);

    /**
     * @brief Parses the fault space section of the file.
     * 
     * @param root_node root YAML node of the section.
     */
    void parse_fault_space_section(const YAML::Node& root_node);

    /**
     * @brief Parses the fault models section of the file.
     * 
     * @param root_node root YAML node of the section.
     */
    void parse_fault_models_section(const YAML::Node& root_node);

    /**
     * @brief Parses a list of fault models in the field `model` of `base_node`, from a fault-space section (named `fs_name`).
     * 
     * @param fs_name the name of the fault space section. 
     * @param fs_type_name the name of the type of fault space section (function of basic-block).
     * @param base_node the node of the fault space section.
     * @return std::vector<fault_model*> the list of parsed fault models.
     */
    std::vector<fault_model*> parse_models_list(const std::string& fs_name, const std::string& fs_type_name, const YAML::Node& base_node);

    /**
     * @brief Parse the fault space for a function section.
     * 
     * @param function_name the name of the function.
     * @param function_node the YAML node of the function.
     */
    void parse_function(const std::string& function_name, const YAML::Node& function_node);

    /**
     * @brief Parses the fault space for a basic block section.
     * 
     * @param bb_name the name of the basic block.
     * @param bb_node the YAML node of the basic block.
     */
    void parse_bb(const std::string& bb_name, const YAML::Node& bb_node);

    /**
     * @brief Parses a fault model YAML object.
     * 
     * @param fm_node root YAML node of the fault model.
     * @param fct_name fault space index (display purpose only).
     * @param n fault model index (display purpose only).
     * @return std::unique_ptr<fault_model> the parsed fault model.
     */
    std::unique_ptr<fault_model> parse_fault_model(const YAML::Node& fm_node, const std::string& fct_name, int n);

    /**
     * @brief Parses a Test Inversion (TI) fault model YAML node.
     * 
     * @param fm_node root YAML node of the fault model.
     * @param name name of the fault model.
     * @param fm_str string describing the fault model, used for error display.
     * @return std::unique_ptr<fault_model> the parsed fault model.
     */
    std::unique_ptr<fault_model> parse_ti_model(const YAML::Node& fm_node, const std::string& name, const std::string& fm_str);

    /**
     * @brief Parses a Data Load (DL) fault model YAML node.
     * 
     * @param fm_node root YAML node of the fault model.
     * @param name name of the fault model.
     * @param fm_str string describing the fault model, used for error display.
     * @return std::unique_ptr<fault_model> the parsed fault model.
     */
    std::unique_ptr<fault_model> parse_data_model(const YAML::Node& fm_node, const std::string& name, const std::string& fm_str);

    /**
     * @brief Parses a Test Fallthrough (TF) fault model YAML node.
     * 
     * @param fm_node root YAML node of the fault model.
     * @param name name of the fault model.
     * @param fm_str string describing the fault model, used for error display.
     * @return std::unique_ptr<fault_model> the parsed fault model.
     * @note Not supported for mutation.
     */
    std::unique_ptr<fault_model> parse_tf_model(const YAML::Node& fm_node, const std::string& name, const std::string& fm_str);
    
    /**
     * @brief Parses a Switch Call (SC) fault model YAML node.
     * 
     * @param fm_node root YAML node of the fault model.
     * @param name name of the fault model.
     * @param fm_str string describing the fault model, used for error display.
     * @return std::unique_ptr<fault_model> the parsed fault model.
     * @note Not supported for mutation.
     */
    std::unique_ptr<fault_model> parse_sc_model(const YAML::Node& fm_node, const std::string& name, const std::string& fm_str);


    /* Error management */

    /**
     * @brief Check that the type of a YAML node's field is the expected one and print error/warning otherwise.
     * 
     * @param field_name the name of the field inside the node. 
     * @param type_name the name of the expected type (for error display).
     * @param node the node to be check.
     * @param expected the expected YAML node type.
     * @return true if the type is correct.
     * @return false if the type is incorrect.
     */
    bool check_type(const std::string& field_name, const std::string& type_name, const YAML::Node& node, const YAML::NodeType::value expected);

    /**
     * @brief Check that the type of a YAML node's field is a string and print error/warning otherwise.
     * 
     * @param field_name the name of the field inside the node. 
     * @param node the node to be check.
     * @return true if the field is a string.
     * @return false if the field is not a string.
     */
    bool check_string(const std::string& field_name, const YAML::Node& node)
    {
      return check_type(field_name, "string", node, YAML::NodeType::Scalar);
    }

    /**
     * @brief Check that the type of a YAML node's field is a sequence (list) and print error/warning otherwise.
     * 
     * @param field_name the name of the field inside the node. 
     * @param node the node to be check.
     * @return true if the field is a list.
     * @return false if the field is not a list.
     */
    bool check_sequence(const std::string& field_name, const YAML::Node& node)
    {
      return check_type(field_name, "list", node, YAML::NodeType::Sequence);
    }


    /**
     * @brief Check that the type of a YAML node's field is a map and print error/warning otherwise.
     * 
     * @param field_name the name of the field inside the node. 
     * @param node the node to be check.
     * @return true if the field is a map.
     * @return false if the field is not a map.
     */
    bool check_map(const std::string& field_name, const YAML::Node& node)
    {
      return check_type(field_name, "object", node, YAML::NodeType::Map);
    }

    /**
     * @brief Verify the field inside a YAML node and print error/warning if unknown field are encountered.
     * 
     * @param expected the list of expected field names.
     * @param node the node to be checked.
     * @param msg the message to be displayed on error.
     * @return true if the node is correct.
     * @return false if at least one unknown field has been found.
     */
    bool expect_fields(const std::vector<std::string>& expected, const YAML::Node& node, const std::string& msg);

    /**
     * @brief Returns the string representing the position of a node inside the YAML attack model file (used for error/warning displays).
     * 
     * @param mark the YAML mark containing the location information.
     * @return std::string the string representation of the location.
     */
    std::string mark_string(YAML::Mark mark);
};

} // namespace models
} // namespace lazart
