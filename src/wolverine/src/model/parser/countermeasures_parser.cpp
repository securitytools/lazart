#include "model/parser/attack_model_parser.hpp"

#include <boost/algorithm/string/predicate.hpp>

#include "mutation/tasks/countermeasures/test_multiplication.hpp"
#include "mutation/tasks/countermeasures/load_multiplication.hpp"
#include "mutation/tasks/countermeasures/sec_swift.hpp"

// Expects fault space to have been parsed
void lazart::model::attack_model_parser::parse_countermeasures_section(const YAML::Node& root_node) 
{ 
    if(!root_node) // Optional
        return;
    if(!check_sequence("countermeasures", root_node))
        return;
   

    int i = 0;
    for(const auto& cm_node: root_node)
    {
        auto cm = parse_countermeasure_task(cm_node, i);
        if(cm != nullptr)
            mutation_.am.countermeasures.push_back(std::move(cm));
        i++;
    }
}


std::unique_ptr<lazart::tasks::countermeasure> lazart::model::attack_model_parser::parse_countermeasure_task(const YAML::Node& cm_node, int n) 
{
    const auto cm_str = "countermeasure object " + std::to_string(n) + " at " + mark_string(cm_node.Mark());
    if(!cm_node.IsMap()) {
        Log().critical("invalid " + cm_str + ".");
        return nullptr;
    }
         
    auto type_attr = cm_node["type"];
    if(!type_attr)
    {
        Log().critical("cm type is required for " + cm_str + ".");
        return nullptr;
    }
    if(!check_string("type", type_attr))
    {
        Log().critical("invalid 'type' field, string required, for " + cm_str + ".");
        return nullptr;
    }

    auto type_str = type_attr.Scalar();

    auto on_list_node = cm_node["on"];
    std::vector<std::string> on_list;
    if(!on_list_node) {
        Log().critical("expected function list for field 'on' at " + cm_str + ".");
        return nullptr;
    }

    int i = 0;
    for(auto item: on_list_node) {
        if(check_string("item " + std::to_string(i), item))
            on_list.push_back(item.Scalar());
        ++i;
    }
    

    auto ccp_list_node = cm_node["ccps"];
    std::vector<std::string> ccp_list;
    if(ccp_list_node)
    {
        if(!check_sequence("'ccps' field", ccp_list_node)) {
            Log().critical("expected list for field 'ccps' at " + cm_str + ".");
            return nullptr;
        }            

        int i = 0;
        for(auto item: ccp_list_node)
        {
            if(check_string("item " + std::to_string(i), item))
                ccp_list.push_back(item.Scalar());
            ++i;
        }
    }

    auto trigger_fct_node = cm_node["trigger_function"];
    std::string trigger_fct = tasks::trigger_fct_default;
    if(trigger_fct_node) 
    {
        if(!trigger_fct_node.IsScalar()) {
            Log().warning("invalid value for field 'trigger_function', expected string at " + cm_str + ".");
            return nullptr;
        }

        trigger_fct = trigger_fct_node.Scalar();
        // TODO check in module ? 
    }

    if(type_str == "test-multiplication" || type_str == "tm")
        return parse_test_multiplication(cm_node, cm_str, on_list, ccp_list, trigger_fct, n);
    if(type_str == "load-multiplication" || type_str == "lm")
        return parse_load_multiplication(cm_node, cm_str, on_list, ccp_list, trigger_fct, n);
    if(type_str == "sec_swift" || type_str == "sec")
        return parse_sec_swift(cm_node, cm_str, on_list, ccp_list, trigger_fct, n);
    Log().warning("unknown countermeasure type: " + type_str);

    return nullptr;
}

std::unique_ptr<lazart::tasks::countermeasure> lazart::model::attack_model_parser::parse_test_multiplication(const YAML::Node& cm_node, const std::string& cm_str, const std::vector<std::string>& on, const std::vector<std::string>& ccps, const std::string& trigger_fct, int n)
{

    auto depth_node = cm_node["depth"];
    int depth = 1;
    if(depth_node) {
        if(!depth_node.IsScalar()) {
            Log().warning("invalid value for field 'depth', expected integer at " + cm_str + ".");
            return nullptr;
        }
        try {
            int v = depth_node.as<int>();
            depth = v;
        }
        catch (const YAML::BadConversion& e) {
            Log().warning("invalid value for field 'depth', expected integer at " + cm_str + ".");
            return nullptr;
         } 
    }

    std::unique_ptr<tasks::test_multiplication> ret(new tasks::test_multiplication{mutation_, depth, on, ccps, trigger_fct});

    expect_fields({"type", "on", "trigger_function", "ccps", /*"cm_block",*/ "depth"}, cm_node, cm_str);

    return ret;
}

std::unique_ptr<lazart::tasks::countermeasure> lazart::model::attack_model_parser::parse_load_multiplication(const YAML::Node& cm_node, const std::string& cm_str, const std::vector<std::string>& on, const std::vector<std::string>& ccps, const std::string& trigger_fct, int n)
{
    auto depth_node = cm_node["depth"];
    int depth = 1;
    if(depth_node) {
        if(!depth_node.IsScalar()) {
            Log().warning("invalid value for field 'depth', expected integer at " + cm_str + ".");
            return nullptr;
        }
        try {
            int v = depth_node.as<int>();
            depth = v;
        }
        catch (const YAML::BadConversion& e) {
            Log().warning("invalid value for field 'depth', expected integer at " + cm_str + ".");
            return nullptr;
         } 
    }

    std::unique_ptr<tasks::load_multiplication> ret(new tasks::load_multiplication{mutation_, depth, on, ccps, trigger_fct});

    expect_fields({"type", "on", "trigger_function", "ccps", /*"cm_block",*/ "depth"}, cm_node, cm_str);

    return ret;
}

std::unique_ptr<lazart::tasks::countermeasure> lazart::model::attack_model_parser::parse_sec_swift(const YAML::Node& cm_node, const std::string& cm_str, const std::vector<std::string>& on, const std::vector<std::string>& ccps, const std::string& trigger_fct, int n)
{
    auto mut_functions = cm_node["MUT_SPECIFICS_FUNC"];
    auto mut_uncond = cm_node["MUT_UNCOND_BR"];
    auto prepare_without_if = cm_node["PREPARE_WITH_IF"];
    int mut = 1;int mut_unc = 1;int prepare = 1;
    if(mut_functions){
        if(!mut_functions.IsScalar()) {
            Log().warning("invalid value for field 'mut_specific_functions', expected integer at " + cm_str + ".");
            return nullptr;
        }
        try {
            int v = mut_functions.as<int>();
            mut = v;
        }
        catch (const YAML::BadConversion& e) {
            Log().warning("invalid value for field 'mut_specific_functions', expected integer at " + cm_str + ".");
            return nullptr;
        } 
    }
    if(mut_uncond){
        if(!mut_uncond.IsScalar()) {
            Log().warning("invalid value for field 'MUT_UNCOND', expected integer at " + cm_str + ".");
            return nullptr;
        }
        try {
            int v = mut_uncond.as<int>();
            mut_unc = v;
        }
        catch (const YAML::BadConversion& e) {
            Log().warning("invalid value for field 'MUT_UNCOND', expected integer at " + cm_str + ".");
            return nullptr;
        } 
    }
    if(prepare_without_if){
        if(!prepare_without_if.IsScalar()) {
            Log().warning("invalid value for field 'prepare_without_if', expected integer at " + cm_str + ".");
            return nullptr;
        }
        try {
            int v = prepare_without_if.as<int>();
            prepare = v;
        }
        catch (const YAML::BadConversion& e) {
            Log().warning("invalid value for field 'prepare_without_if', expected integer at " + cm_str + ".");
            return nullptr;
        } 
    }
    std::unique_ptr<tasks::sec_swift> ret(new tasks::sec_swift{mutation_, mut, mut_unc, prepare, on, ccps, trigger_fct});

    // expect_fields({"type", "on", "trigger_function", "ccps", /*"cm_block",*/}, cm_node, cm_str);

    return ret;
}