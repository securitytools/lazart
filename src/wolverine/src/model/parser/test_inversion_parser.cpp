#include "model/parser/attack_model_parser.hpp"

#include "model/fault_models/test_inversion.hpp"

std::unique_ptr<lazart::model::fault_model> lazart::model::attack_model_parser::parse_ti_model(const YAML::Node& fm_node, const std::string& name, const std::string& fm_str)
{
    auto protected_detectors_node = fm_node["protected_detectors"];
    std::vector<std::string> protected_detectors{};
    if(protected_detectors_node) 
    {
        if(!check_sequence("protected_detectors", protected_detectors_node))
            return nullptr;

        for(auto trigger_fct_node: protected_detectors_node)
        {
            if(!trigger_fct_node.IsScalar()) {
                Log().warning("invalid value for item in 'protected_detectors', expected string at " + fm_str + ".");
                continue;
            }

            protected_detectors.push_back(trigger_fct_node.Scalar());   
        }
        
    } else {
        protected_detectors = {tasks::trigger_fct_default};
    }

    auto inl = false;
    auto opt_inl_node = fm_node["inline"];
    if(opt_inl_node)
    {
        if(opt_inl_node.IsScalar())
        {
            inl = opt_inl_node.as<bool>();
        }
        else {
            Log().warning("expected boolean for optional field 'inline'.");
        }
    }

    return std::unique_ptr<lazart::model::fault_model>(new test_inversion{name, std::move(protected_detectors), inl});
}