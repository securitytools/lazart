#include "model/parser/attack_model_parser.hpp"

#include "yaml-cpp/yaml.h"

#include "core/wolverine.hpp"

#include "core/cli/argparse.hpp"
#include "model/mutation.hpp"
#include "model/fault_models/fault_models.hpp"
//#include "lazart/models/parsers/parsing_error.hpp"
//#include "utils/utils.hpp"

#include "llvm/utils.hpp"

#include <string>
#include <set>
#include <vector>
#include <algorithm>

// Util
std::string lazart::model::attack_model_parser::mark_string(YAML::Mark mark)
{
    if(!mark.is_null())
        return "[" + std::to_string(mark.line + 1) + ":" + std::to_string(mark.column) + "]";
    return "[?:?]";
}

lazart::model::attack_model_parser::attack_model_parser(Mutation& a) :
 mutation_( a )
{
}

bool lazart::model::attack_model_parser::check_type(const std::string& field_name, const std::string& type_name, const YAML::Node& node, const YAML::NodeType::value expected)
{
    if(node.Type() == expected)
        return true;
    Log().warning("invalid type for field '" + field_name + "' at " + mark_string(node.Mark()) + ", expected " + type_name + ".");
    return false;
}

bool lazart::model::attack_model_parser::parse(const lazart::cli::args& args) 
{

    Log().debug("parsing mutation file " + args.attack_model_path);

    try {
        YAML::Node root_node = YAML::LoadFile(args.attack_model_path);
        parse_(root_node, args);
    } catch (YAML::Exception& e) {
        Log().critical("mutation file parsing ended prematurely: " + std::string(e.what()) + ".\n" + Log().counter.str() + " generated.");
        return false;
    }

    Log().info("mutation file parsing ended. " + Log().counter.str() + " generated.");

    return Log().counter.error_count == 0;
}

void lazart::model::attack_model_parser::parse_(const YAML::Node& root_node, const lazart::cli::args& args) 
{
    // Root
    if (!root_node.IsMap()) {
        Log().critical("invalid root object, expected map.");
        return;
    }

    parse_main_section(root_node, args);

    parse_fault_space_section(root_node);

    parse_fault_models_section(root_node);
}

void lazart::model::attack_model_parser::parse_main_section(const YAML::Node& root_node, const lazart::cli::args& args) 
{
    {
        auto version = root_node["version"];
        if(version)
        {
            if(check_string("version", version))
            {
                auto s = version.Scalar(); 
                if(s != lazart::version_str)
                    Log().warning("mutation file uses a different version (" + s + ") than wolverine (" + lazart::version_str + ").");
            }
        }
    }

    auto tasks = root_node["tasks"];
    if(tasks)
    {
        parse_tasks(tasks);
    }
}

void lazart::model::attack_model_parser::parse_tasks(const YAML::Node& task_node) 
{    
    if(check_map("tasks", task_node))
    {
        for(auto item: task_node)
        { 
            if(!item.first.IsScalar()) {
                Log().critical("expected task identifier at " + mark_string(item.Mark()) + ".");
                continue;
            }

            // Create task.
            auto task_name = item.first.Scalar();

            if(task_name == "countermeasures") {
                parse_countermeasures_section(item.second);
                continue;
            }

            // TODO: checks
            std::vector<std::string> fcts;
            if(item.second.IsScalar())
                fcts.push_back(item.second.Scalar());
            else if(item.second.IsSequence())
            {
                for(const auto& ts: item.second)
                {
                    if(ts.IsScalar())
                        fcts.push_back(ts.Scalar());
                    else 
                        Log().critical("invalid type for task scope item at " + mark_string(ts.Mark()) + "expected string.");
                }
            }
            else
                Log().critical("invalid type for task scope item at " + mark_string(item.second.Mark()) + ": expected string or string array.");


            if(task_name == "add_trace")
                mutation_.am.add_trace_scope = fcts;
            else if(task_name == "rename_bb")
                mutation_.am.rename_bb_scope = fcts;
            else
                Log().critical("unknown task type " + task_name + " at " + mark_string(item.first.Mark()) + ".");
       } 
    }
}

// Expects fault space to have been parsed
void lazart::model::attack_model_parser::parse_fault_models_section(const YAML::Node& root_node) 
{ 
    auto fm_section = root_node["fault-models"];
    if(!fm_section) // Optional
        return;
    if(!check_sequence("fault_models", fm_section))
        return;

    int i = 0;
    for(auto model: fm_section)
    {
        auto model_ptr = get_model(model);
        if(!model_ptr)
        {
            auto m = parse_fault_model(model, "fault model section", 0);
            if(m != nullptr)
            {
                mutation_.am.models_.push_back(std::move(m));
                auto model_ptr = mutation_.am.models_.back().get();

                if(model_ptr->get_name() == "")
                {
                    Log().warning("unamed model at " + mark_string(model.Mark()) + " is not used in any function.");
                }


                add_model(model, model_ptr);    
            }
            else
                Log().critical("cannot parse model at " + mark_string(model.Mark()));  
            i++;
        }
    }
}

// Fault space section

void lazart::model::attack_model_parser::parse_fault_space_section(const YAML::Node& root_node) 
{
    // "fault-space"
    auto fault_space_section = root_node["fault-space"];
    if(!fault_space_section)
        return Log().critical("missing required 'fault-space' section.");     
    if(!check_map("fault-space", fault_space_section))
        return;

    // functions
    auto functions = fault_space_section["functions"];
    if(functions) {
        if(check_map("functions", functions)) {
            for (const auto& fault_space : functions) {
                parse_function(fault_space.first.as<std::string>(), fault_space.second);
            }
        }
    }

    // basic-blocks
    auto bbs = fault_space_section["basic-blocks"];
    if(bbs) {
        if(check_map("basic-blocks", bbs)) {
            for (const auto& bb : bbs) {
                parse_bb(bb.first.as<std::string>(), bb.second);
            }
        }        
    }    
}


std::vector<lazart::model::fault_model*> lazart::model::attack_model_parser::parse_models_list(const std::string& fs_name, const std::string& fs_type_name, const YAML::Node& base_node)
{

    auto models = base_node["models"];
    if(!models) {
        Log().critical("missing required 'models' list for " + fs_type_name + " '" + fs_name + "' at " + mark_string(base_node.Mark()) + ".");    
        return {};
    } 
    if(!check_sequence("models", models))
        return {};
    if (models.size() < 1) {
        Log().warning("no model specified at " + mark_string(models.Mark()) + ".");
        return {};
    }

    std::vector<fault_model*> models_ptrs;

    // List
    auto i = 0;
    for(auto model: models)
    {
        // Check if already exists ...
        if(auto model_ptr = get_model(model))
        {
            if(std::find(models_ptrs.begin(), models_ptrs.end(), model_ptr) != models_ptrs.end())
                Log().warning("duplicate model at " + mark_string(model.Mark()) + " in " + fs_type_name + " " + fs_name);
            else
                models_ptrs.push_back(model_ptr);
        }
        else
        {
            {
                auto m = parse_fault_model(model, fs_name, i);
                if(m != nullptr)
                {
                    mutation_.am.models_.push_back(std::move(m));
                    auto model_ptr = mutation_.am.models_.back().get();

                    add_model(model, model_ptr);          
                    models_ptrs.push_back(model_ptr);    
                }
                else
                    Log().critical("cannot parse model at " + mark_string(model.Mark()));  
            }
        }
        i++;
    }

    return models_ptrs;
}

void lazart::model::attack_model_parser::parse_bb(const std::string& bb_name, const YAML::Node& bb_node)
{
    // TODO: 'callees'
    // Note: canno retrieve bb here, 'rename_bb' pass has to be run before.

    auto models_ptrs = parse_models_list(bb_name, "basic block", bb_node);
    if(models_ptrs.size() == 0) {
        Log().warning("empty list of model for basic block " + bb_name);
        return;
    }

    // Add models to bb.
    mutation_.am.fault_space_bb_[bb_name] = models_ptrs;
}

void lazart::model::attack_model_parser::parse_function(const std::string& function_name, const YAML::Node& function_node)
{
    // TODO: 'callees',  


    // Retrieve function.
    llvm::Function* fct = nullptr;
    if(function_name != "__all__")
    {
        fct = mutation_.module->getFunction(function_name);
        
        if(fct == nullptr)
            return Log().critical("function " + function_name + " doesn't exists.");
    }

    auto models_ptrs = parse_models_list(function_name, "function", function_node);
    if(models_ptrs.size() == 0) {
        Log().warning("empty list of model for function " + function_name);
        return;
    }

    // Add models to fct.
    std::pair<llvm::Function*, std::vector<fault_model*>> fs_pair = std::pair<llvm::Function*, std::vector<fault_model*>>(fct, models_ptrs);

    if(function_name == "__all__")
        mutation_.am.all_functions_ = std::move(fs_pair.second);
    else
        mutation_.am.fault_space_[fs_pair.first] = fs_pair.second;
}

lazart::model::fault_model* lazart::model::attack_model_parser::get_model(YAML::Node& node)
{
    for(auto& item: parsed_nodes_)
    {
        if(item.first == node) {
            return item.second;
        }
    }
    return nullptr;
}

void lazart::model::attack_model_parser::add_model(YAML::Node& node, fault_model* model)
{
    if(!get_model(node))
    {
        parsed_nodes_.push_back(std::make_pair(node, model));
    }
    else
        Log().warning("cannot add");
}


bool lazart::model::attack_model_parser::expect_fields(const std::vector<std::string>& expected, const YAML::Node& node, const std::string& msg)
{
    if(!node.IsMap()) {
        Log().warning("expected map for " + msg);
        return false;
    }

    std::vector<std::string> unexpected;
    for(auto& item: node) {
        assert(item.first.IsScalar() && "lib error ?");
        
        if(item.first.IsScalar() && (std::find(std::begin(expected), std::end(expected), item.first.Scalar()) == std::end(expected))) {
            unexpected.push_back(item.first.Scalar());
        }
    }

    if(unexpected.size() > 0) {
        Log().warning("unexpected field(s) ['" +  util::join(unexpected, "', '") + "'] for " + msg);
        return false;
    }
    return true;
}

std::unique_ptr<lazart::model::fault_model> lazart::model::attack_model_parser::parse_fault_model(const YAML::Node& fm_node, const std::string& fct_name, int n) 
{
    const auto fm_str = "fault-model object " + std::to_string(n) + " in " + fct_name + " at " + mark_string(fm_node.Mark());
    if(!fm_node.IsMap()) {
        Log().critical("invalid " + fm_str + ".");
        return nullptr;
    }
     
    // Name.
    std::string name = "";
    {
        auto name_attr = fm_node["name"];
        if(name_attr)
            if(check_string("name", name_attr))
            {                
                name = name_attr.Scalar();
            } 
    }
    
    auto type_attr = fm_node["type"];
    if(!type_attr)
    {
        Log().critical("model type is required for " + fm_str + ".");
        return nullptr;
    }
    if(!check_string("type", type_attr))
    {
        Log().critical("invalid 'type' field, string required, for " + fm_str + ".");
        return nullptr;
    }

    auto type_str = type_attr.Scalar(); 

    if(type_str == "test-inversion" || type_str == "ti")
        return parse_ti_model(fm_node, name, fm_str);
    if(type_str == "data-load" || type_str == "dl" || type_str == "data")
        return parse_data_model(fm_node, name, fm_str);
    if(type_str == "test-falltrought" || type_str == "tf")
        return parse_tf_model(fm_node, name, fm_str);
    if(type_str == "switch-call" || type_str == "sc")
        return parse_sc_model(fm_node, name, fm_str);
    Log().warning("unknown model type: " + type_str);

    return nullptr;
}