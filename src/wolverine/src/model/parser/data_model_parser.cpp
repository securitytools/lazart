#include "model/parser/attack_model_parser.hpp"

#include <boost/algorithm/string/predicate.hpp>

#include "model/fault_models/data_load.hpp"

std::unique_ptr<lazart::model::fault_model> lazart::model::attack_model_parser::parse_data_model(const YAML::Node& fm_node, const std::string& name, const std::string& fm_str)
{
    auto parse_data_value = [&](const YAML::Node& dv_node, const std::string& msg) -> std::pair<bool, data_load_model_value> {
        if(!dv_node.IsScalar())
        {
            Log().warning("expected scalar " + msg + ".");
            return {false, data_load_model_value{}};
        }
        
        try {
            long v = dv_node.as<int>();
            return {true, data_load_model_value(v)};
        }
        catch (const YAML::BadConversion& e) { } // Ignore...
        try {
            double f = dv_node.as<double>();
            return {true, data_load_model_value(f)};
        }
        catch (const YAML::BadConversion& e) { } // Ignore...
        try {
            std::string value = dv_node.as<std::string>();


            if(value == "__sym__")
                return {true, data_load_model_value{}};
            else if (boost::starts_with(value, "__sym__:"))
            {
                // TODO search predicate here ?
                return {true, data_load_model_value(value.substr(8), true)};
            } 
            else
                return {true, data_load_model_value{value}};
        }
        catch (const YAML::BadConversion& e) 
        { 
            Log().warning("invalid data value, expected fixed value, 'symbolic' or predicate name " + msg + " in " + fm_str + ".");
            return {false, data_load_model_value{}};
        }
        
    };

    util::optional<data_load_model_value> all_value;

    auto all_node = fm_node["all"];
    bool all_defined = false;
    if(all_node)
    {
        all_defined = true;
        auto all_p = parse_data_value(all_node, "for 'all' field value in " + fm_str);
        if(all_p.first)
            all_value.set(all_p.second);
    }

    std::map<std::string, data_load_model_value> vars;
    bool vars_defined = false;
    auto vars_node = fm_node["vars"];
    if(vars_node)
    {
        vars_defined = true;
        if(!check_map("vars", vars_node))
            return nullptr;
        if (vars_node.size() < 1)
            Log().warning("empty 'vars' list for " + fm_str + ".");


        auto i = 0;
        for(auto var: vars_node)
        {

            all_defined = true;
            auto all_p = parse_data_value(var.second, "for '" + var.first.Scalar() + "' " + std::to_string(i) + "th variable's value in " + fm_str);
            if(all_p.first)
                vars.insert(std::pair<std::string, data_load_model_value>(var.first.Scalar(), all_p.second));
        }
    }

    std::vector<std::string> excludes;
    bool excludes_defined = false;
    auto excludes_node = fm_node["excludes"];
    if(excludes_node)
    {
        excludes_defined = true;
        if(!check_sequence("excludes", excludes_node))
            return nullptr;

        int i = 0;
        for(auto exclude: excludes_node)
        {
            i++;
            if(!exclude.IsScalar())
            {
                Log().warning("expected string for item " + std::to_string(i) + " in 'excludes' list for " + fm_str + ".");
                continue;
            }
            excludes.push_back(exclude.Scalar());
        }
    }


    if(excludes_defined && !all_defined)
    {
        Log().critical("'excludes' field defined without 'all' field for " + fm_str + "");
        return nullptr;
    }
    
    // Options
    auto use_sym_fct = false;
    auto opt_use_sym_fct_node = fm_node["use-sym-fct"];
    if(opt_use_sym_fct_node)
    {
        if(opt_use_sym_fct_node.IsScalar())
        {
            use_sym_fct = opt_use_sym_fct_node.as<bool>();
        }
        else {
            Log().warning("expected boolean for optional field 'use-sym-fct'.");
        }
    }

    auto use_sym = false;
    auto opt_use_sym_node = fm_node["use-sym"];
    if(opt_use_sym_node)
    {
        if(opt_use_sym_node.IsScalar())
        {
            use_sym = opt_use_sym_node.as<bool>();
        }
        else {
            Log().warning("expected boolean for optional field 'use-sym'.");
        }
    }

    auto inl = false;
    auto opt_inl_node = fm_node["inline"];
    if(opt_inl_node)
    {
        if(opt_inl_node.IsScalar())
        {
            inl = opt_inl_node.as<bool>();
        }
        else {
            Log().warning("expected boolean for optional field 'inline'.");
        }
    }

    return std::unique_ptr<lazart::model::fault_model>(new data_load_model{all_value, vars, excludes, name, inl, use_sym_fct, use_sym});
}