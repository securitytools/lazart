#include "model/parser/attack_model_parser.hpp"
#include "model/fault_models/test_fallthrough.hpp"

#include "yaml-cpp/yaml.h"


std::unique_ptr<lazart::model::fault_model> lazart::model::attack_model_parser::parse_tf_model(const YAML::Node& fm_node, const std::string& name, const std::string& fm_str)
{
    auto mode_node = fm_node["mode"];
    auto mode = test_fallthrough::Mode::ThenElse;
    bool all_defined = false;
    if(mode_node)
    {
        if(!check_string("mode", fm_node))
            return nullptr;     
        auto mode_str = mode_node.as<std::string>();
        if(mode_str == "then-else" || mode_str == "te")
            mode = test_fallthrough::Mode::ThenElse;
        else if(mode_str == "else-then" || mode_str == "et")
            mode = test_fallthrough::Mode::ThenElse;
            // TODO both ?
        else {
            Log().critical("invalid mode '" + mode_str + "' for model '" + fm_str + "' at " + fm_str + ", expected 'then-else' or 'else-then'."); 
            return nullptr;
        }
    }

    return std::unique_ptr<lazart::model::fault_model>(new test_fallthrough{name, mode});
}