#include "model/attack_model.hpp"

#include <algorithm>
#include <set>

#include "model/fault_models/fault_models.hpp"
#include "util/util.hpp"

#include <llvm/IR/Function.h>
#include <llvm/IR/Instructions.h>
#include "llvm/Support/raw_ostream.h"


bool lazart::model::attack_model::has_function(const std::string& name) const
{
    auto f = get_function(name);

    return f != nullptr;
}

llvm::Function* lazart::model::attack_model::get_function(const std::string& name) const
{
    for(auto& item: fault_space_)
    {
        if(item.first->getName().str() == name)
            return item.first;
    }
    return nullptr;
}

bool lazart::model::attack_model::add_function(llvm::Function* function)
{
    if(fault_space_.count(function))
        return false;
    fault_space_[function] = {};
    return true;
}

lazart::model::fault_model* lazart::model::attack_model::add_fault_model(std::unique_ptr<fault_model>&& model)
{
    // Should check ? 
    models_.push_back(std::move(model));
    return models_.back().get();
}

bool lazart::model::attack_model::bind_model(fault_model* model, llvm::Function* function)
{
    if(!(fault_space_.count(function)))
        return false;
    auto& models = fault_space_[function];
    if(std::find_if(std::begin(models_), std::end(models_), [=](std::unique_ptr<fault_model>& p) {
        return p.get() == model;
    }) == std::end(models_)) {
        Log().warning("model not registered");
    }
    models.push_back(model);
    return true;
}

bool lazart::model::attack_model::bind_model(fault_model* model, llvm::BasicBlock* bb)
{
    auto bb_name = bb->getName().str();
    if(!(fault_space_bb_.count(bb_name)))
        return false;
    auto& models = fault_space_bb_[bb_name];
    if(std::find_if(std::begin(models_), std::end(models_), [=](std::unique_ptr<fault_model>& p) {
        return p.get() == model;
    }) == std::end(models_)) {
        Log().warning("model not registered");
    }
    models.push_back(model);
    return true;
}

bool lazart::model::attack_model::has_task(llvm::Function* fun, const std::vector<std::string>& vec) const 
{
    auto fname = fun->getName().str();

    if(std::find(vec.begin(), vec.end(), fname) != vec.end())
        return true;

    if(std::find(vec.begin(), vec.end(), "__all__") != vec.end())
        return true;
    
    if(std::find(vec.begin(), vec.end(), "__mut__") != vec.end())
    {
        if(fault_space_.count(fun))
            return true;
    }

    return false;
}

bool lazart::model::attack_model::has_task(llvm::BasicBlock& bb, TaskType type) const
{
    const std::vector<std::string>* vec = nullptr;
    switch(type)
    {
        case TaskType::RenameBB: vec = &rename_bb_scope;
            break;
        case TaskType::AddTrace: vec = &add_trace_scope;
            break;
        default:
            Log().critical("unknown task type.");
            return false;
    }

    auto fname = bb.getParent()->getName().str();

    return has_task(bb.getParent(), *vec);
}

lazart::model::fault_model* lazart::model::attack_model::model_by_name(const std::string& name)
{
    if(name == "")
        return nullptr;

    // Ensure no '\0'
    auto name_cpy = name;
    name_cpy.erase(std::remove(name_cpy.begin(), name_cpy.end(), '\0'), name_cpy.end());

    for(const auto& model: models_)
        if(model->get_name() == name_cpy)
            return model.get();
    return nullptr;
}

std::vector<lazart::model::fault_model*> lazart::model::attack_model::mutating_models(const std::vector<fault_model*>& models, llvm::Instruction& inst, Mutation& mutation) const
{
    std::vector<fault_model*> mutating{};

    for (auto model: models)
    {
        if(model->should_mutate(inst, mutation))
            mutating.push_back(model);
    }

    return mutating;
}

std::vector<lazart::model::fault_model*> lazart::model::attack_model::mutating_models(llvm::Instruction& inst, Mutation& mutation) const
{
    auto bb = inst.getParent();
    auto bb_name = bb->getName().str();
    auto fct = bb->getParent();

    std::vector<lazart::model::fault_model*> mutating{};

    // Basic block definition, highest priority.
    if(bb_name != "" && fault_space_bb_.count(bb_name))
    {
        auto bb_mut = mutating_models(fault_space_bb_.at(bb_name), inst, mutation);
        mutating.insert(mutating.begin(), bb_mut.begin(), bb_mut.end()); 
    }

    // Function definition.
    if(fault_space_.count(fct))
    {
        auto fct_mut = mutating_models(fault_space_.at(fct), inst, mutation);
        mutating.insert(mutating.begin(), fct_mut.begin(), fct_mut.end()); 
    }

    // All function, lowest priority.
    auto all_mut = mutating_models(all_functions_, inst, mutation);
    mutating.insert(mutating.begin(), all_mut.begin(), all_mut.end()); 

    return mutating;
}

bool lazart::model::attack_model::should_mutate(llvm::Instruction& inst, Mutation& mutation) const
{
    auto mutating = mutating_models(inst, mutation);

    if(!mutating.empty())
        return true;
    return false;
}


std::pair<llvm::Instruction*, lazart::mutation::fault> lazart::model::attack_model::mutate(int ip_id, llvm::Instruction& inst, Mutation& mutation)
{
    auto mutating = mutating_models(inst, mutation);

    if(mutating.empty())
        return std::make_pair<llvm::Instruction*, lazart::mutation::fault>(&inst, {});

    if(mutating.size() > 1)
    {
        std::string inst_str;
        llvm::raw_string_ostream(inst_str) << &inst;
        Log().warning("multiple models (" + std::to_string(mutating.size()) + ") are available for instruction" + inst_str + "', first will be chosen.");
    }

    return mutating[0]->mutate(ip_id, &inst, mutation);
}

std::vector<lazart::model::fault_model*> lazart::model::attack_model::unused_models() const
{
    std::set<fault_model*> used{}; // List of model used
    for(auto& fct: fault_space_)
        for(auto model: fct.second)
            used.insert(model);
    for(auto& bb: fault_space_bb_)
        for(auto model: bb.second)
            used.insert(model);
    for(auto model: all_functions_)
        used.insert(model);

    std::vector<fault_model*> unused{};

    for(auto& model: models_)
        if(!used.count(model.get()))
            unused.push_back(model.get());

    return unused;
}

std::string lazart::model::attack_model::str() const
{
    std::string ret = "";

    // Tasks
    ret += " - tasks:\n";
    if(rename_bb_scope.size() > 0)
        ret += "   - rename_bb: [" + util::join(rename_bb_scope) + "]\n";
    if(add_trace_scope.size() > 0)
        ret += "   - add_trace: [" + util::join(add_trace_scope) + "]\n";

    if(countermeasures.size() != 0) {
        ret += "  - countermeasures:\n";
        for(const auto& cm: countermeasures) {
            ret += "    - " + cm->str() + "\n";
        } 
    }

    // Fault space
    auto add_model = [&ret](const std::string& indent, fault_model* model) {
        ret += indent + " - model " + model->get_name() + ": " + model->str() + "\n";
    };
    ret += " - functions: " + std::to_string(fault_space_.size()) + "\n";
    if(all_functions_.size() > 0)
    {
        ret += "   - __all__:\n";
        for(auto model: all_functions_)
        {
            if(model == nullptr) Log().critical("null model");
            add_model("    ", model);
        } 
    }
    for(auto& fct: fault_space_)
    {
        ret += "   - " + fct.first->getName().str() + ":\n";
        for(auto model: fct.second)
        {
            if(model == nullptr) Log().critical("null model");
            add_model("    ", model);
        }
    }
    for(auto& bb: fault_space_bb_)
    {
        ret += "  - " + bb.first + ":\n";
        for(auto model: bb.second)
        {
            if(model == nullptr) Log().critical("null model");
            add_model("    ", model);
        }
    }
    
    // Models
    ret += " - models: " + std::to_string(models_.size()) + "\n";
    for(auto model: unused_models())
        add_model("    ", model);

    return ret;
}
