#pragma once 
/**
 * @file attack_model.hpp
 * @author Etienne Boespflug (etienne.boespflug@gmail.com)
 * @brief This file defines the attack model corresponding to the specification of where and when faults can be injected for a mutation.
 * @version 4.0
 */

#include <map>
#include <memory>
#include <vector>

#include "model/fault_models/fault_models.hpp"
#include "mutation/fault_space/fault_space.hpp"
#include "mutation/tasks/tasks.hpp"
#include "mutation/tasks/countermeasures/countermeasures.hpp"

#include "llvm/llvm_fwd.hpp"

namespace lazart
{
namespace model {
    class attack_model_parser; // for friend access.

    /**
     * @brief The `attack_model` class represent the association to fault models to code structures (basic blocks or functions).
     * 
     * @since 4.0
     */
    class attack_model
    {
        public:
            attack_model() {}
            attack_model(const attack_model&) = delete;
            attack_model& operator=(const attack_model&) = delete;
            attack_model& operator=(attack_model&&) = default;
            attack_model(attack_model&&) = default;
            ~attack_model() = default;

            friend attack_model_parser;

            /**
             * @brief Determines if the instruction `inst` should be mutated by at least one fault model.
             * 
             * @param inst the instruction to be check.
             * @param mutation the context of the mutation. 
             * @return true if at least one model can should mutate this instruction.
             * @return false if not model can mutate the instruction.
             */
            bool should_mutate(llvm::Instruction& inst, Mutation& mutation) const;
            
            /**
             * @brief Returns the list of mutating models for the specified instruction.
             * 
             * @param inst the instruction to be check.
             * @param mutation the context of the mutation. 
             * @return std::vector<fault_model*> the list of mutating models.
             */
            std::vector<fault_model*> mutating_models(llvm::Instruction& inst, Mutation& mutation) const;

            /**
             * @brief Returns the list of mutating models in `models` for the specified instruction.
             * 
             * @param models the list of model to use to search mutating models.
             * @param inst the instruction to be check.
             * @param mutation the context of the mutation. 
             * @return the list of mutating models.
             */
            std::vector<fault_model*> mutating_models(const std::vector<fault_model*>& models, llvm::Instruction& inst, Mutation& mutation) const;
            
            /**
             * @brief Mutates the specified instruction with with the first mutating model.
             * 
             * Should not be called when needing instrumentation's state filter (use `mutating_models`).
             * 
             * @param ip_id the identifier of the injection point.
             * @param inst the instruction to be check.
             * @param mutation the context of the mutation. 
             * @return std::pair<llvm::Instruction*, mutation::fault> the pair of the next instruction after mutation and the generated fault (if pair.first is not null).
             */
            std::pair<llvm::Instruction*, mutation::fault> mutate(int ip_id, llvm::Instruction& inst, Mutation& mutation);

            /**
             * @brief Returns the string representation of the attack model (used for console display).
             * 
             * @return std::string the string representation of the attack model. 
             */
            std::string str() const;

            /**
             * @brief Returns `true` if the specified tasks type `tt` is applicable on the basic block `bb`, `false` otherwise.
             * 
             * @param bb the basic block on which the task should be applied.
             * @param tt the task type.
             * @return true if the task type should be applied.
             * @return false if the task type should not be applied.
             */
            bool has_task(llvm::BasicBlock& bb, TaskType tt) const;
            
            /**
             * @brief  Returns `true` if the specified tasks type `tt` is applicable on the function `bb` depending on the application space `on`, `false` otherwise.
             * 
             * @param fun the function on which the task should be applied.
             * @param vec the vector of string corresponding to applicable function or codes (`__mut__`, `__all__`).
             * @return true if the task type should be applied.
             * @return false if the task type should not be applied.
             */
            bool has_task(llvm::Function* fun, const std::vector<std::string>& vec) const; 

            /**
             * @brief Returns the fault model associated to the specified `name`.
             * 
             * @param name the name of the fault model.
             * @return fault_model* the pointer to the fault model.
             */
            fault_model* model_by_name(const std::string& name);

            /**
             * @brief Returns `true` if the specified function exists in the attack model, `false` otherwise. 
             * 
             * Note that it only search inside the attack model and not in preprocessing tasks.
             * 
             * @param name the name of the function.
             * @return true if the function is in the attack model.
             * @return false if the function is not in the attack model.
             */
            bool has_function(const std::string& name) const;
            
            /**
             * @brief Get the function with the specified name, `nullptr` otherwise.
             * 
             * @param name the name of the function.
             * @return llvm::Function* a pointer to the function with the name, `nullptr` otherwise.
             */
            llvm::Function* get_function(const std::string& name) const;

            /**
             * @brief Add the specified function to the attack model, associated with an empty list of fault models.
             * 
             * @param function the function to be added.
             * @return true if the function has been successfully added.
             * @return false if the function couldn't be added.
             */
            bool add_function(llvm::Function* function);

            /**
             * @brief Add the specified fault `model` smart pointer and returns a pointer to the model.
             * 
             * @param model the fault model to be added. 
             * @return fault_model* the pointer to the fault model.
             */
            fault_model* add_fault_model(std::unique_ptr<fault_model>&& model);

            /**
             * @brief Associates a `model` to a `function` in the attack model.
             * 
             * @param model the model to be applied on `function`.
             * @param function the function on which `model` should be applied.
             * @return true if the model and the function has been correctly associated.
             * @return false if the association failed.
             */
            bool bind_model(fault_model* model, llvm::Function* function);

            /**
             * @brief Associates a `model` to a basic block `bb` in the attack model.
             * 
             * @param model the model to be applied on `bb`.
             * @param bb the basic block on which `model` should be applied.
             * @return true if the model and the basic block has been correctly associated.
             * @return false if the association failed.
             */
            bool bind_model(fault_model* model, llvm::BasicBlock* bb);

            /**
             * @brief The list of countermeasures to apply. Should never be more than size 1 in current version (countermeasures chaining unsupported).
             * @todo Chained application of countermeasures.
             */
            std::vector<std::unique_ptr<tasks::countermeasure>> countermeasures = {};

        private:
            /// @brief List of fault models.
            std::vector<std::unique_ptr<fault_model>> models_;
            /**
             * @brief List of functions. 
             */
            std::vector<fault_model*> all_functions_;

            /**
             * @brief Association of fault models to functions.
             */
            std::map<llvm::Function*, std::vector<fault_model*>> fault_space_; 

            /**
             * @brief Association of fault models to basic block names.             * 
             * @note only names are store because 'rename_bb' pass has to be run before the basic block name can be found. Verification is done during mutation loop.
             */
            std::map<std::string, std::vector<fault_model*>> fault_space_bb_; 

            /**
             * @brief Returns the list of pointers on all unused fault model in the attack model.
             * 
             * @return std::vector<lazart::model::fault_model*> the list of unused fault models.
             */
            std::vector<lazart::model::fault_model*> unused_models() const;

            // Tasks
            /// @brief The List of codes string for application space of 'add_trace' task. 
            std::vector<std::string> add_trace_scope = {};
            /// @brief The list of codes string for application space of 'rename_bb' task.
            std::vector<std::string> rename_bb_scope = {};
    };
}
}