/**
 * @file mutation.hpp
 * @author Etienne Boespflug (etienne.boespflug@gmail.com)
 * @brief Definition of the Mutation class, handling the LLVM module, the attack model and everything related to the mutation main loop.
 * @version 4.0
 */
#pragma once

#include "util/log.hpp"
#include "model/attack_model.hpp"

#include "mutation/fault_space/fault_space.hpp"
#include "mutation/tasks/countermeasures/countermeasures.hpp"

#include <llvm/IR/Module.h>

namespace lazart {

/**
 * @brief The `model` namespace contains definition relatives to the model of a mutation on lazart, including fault models, attack model and parsing of the YAML input file.
 */
namespace model {

/**
 * @brief The Mutation structure holds LLVM's module and context, the attack model and parameters for a mutation pass in Lazart. 
 *
 * It includes the fault space and the mutation pass parameters, and the counters for errors and warning during mutation.
 *  
 * @since 3.0
 */
class Mutation {

public:

    /**
     * @brief Construct a new Mutation object from the the specified LLVM's context and module.
     * 
     * @param c the LLVM context of the application.
     * @param m the LLVM module of the mutation.
     * @param fm the fault limit for the mutation.
     */
    Mutation(llvm::LLVMContext& c, llvm::Module* m, int fm) :
        context{ c },
        module{ m },
        fault_limit{ fm}
    { }

    /// @brief The name of the mutation (currently unused).
    std::string name;
    /// @brief The attack model of the mutation.
    attack_model am;
    /// @brief The context of the mutation.
    llvm::LLVMContext& context;
    /// @brief The module on which the mutation is done.
    llvm::Module* module;

    /// @brief The fault limit for the mutation (global fault limit for all fault models). Default to 2.
    int fault_limit = 2;

    /// @brief The information about all IPs in the program (including those already inside the input module).
    mutation::fault_space fault_space{};
    /// @brief The information about all detectors in the program (including those already inside the input module).
    tasks::countermeasures_space ccp_space{};

};
} // namespace model
} // namespace lazart
