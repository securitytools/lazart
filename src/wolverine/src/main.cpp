/**
 * @file main.cpp
 * @author Etienne Boespflug (etienne.boespflug@gmail.com)
 * @brief Main source file for Wolverine.
 * @version 4.0
 */


#include <iostream>
#include <memory>
#include <regex>
#include <fcntl.h>
#include <system_error>
#include <unistd.h>
#include <stdexcept>

#include <boost/program_options.hpp>
#include <boost/algorithm/string/predicate.hpp>

// Wolverine
#include "core/cli/argparse.hpp"
#include "core/wolverine.hpp"
#include "model/parser/attack_model_parser.hpp"
#include "export/export.hpp"
#include "util/util.hpp"
#include "llvm/utils.hpp"
#include "mutation/mutation_pass.hpp"
#include "mutation/tasks/tasks.hpp"

// LLVM
#include <llvm/IR/Constants.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Module.h>

#include <llvm/Bitcode/BitcodeReader.h>
#include <llvm/Bitcode/BitcodeWriter.h>
#include <llvm/Support/MemoryBuffer.h>

#include <llvm/IR/IRPrintingPasses.h>
#include <llvm/Support/raw_ostream.h>

#include <llvm/Bitcode/BitcodeReader.h>
#include <llvm/Bitcode/BitcodeWriter.h>
#include <llvm/Support/MemoryBuffer.h>

#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Verifier.h>
#include <llvm/IRReader/IRReader.h>
#include <llvm/Support/SourceMgr.h>
#include <llvm/Support/raw_ostream.h>

#include "llvm/utils.hpp"


/**
 * @brief Main function.
 * 
 * @param argc argument count.
 * @param argv argument vector.
 * @return int 
 */
int main(int argc, char* argv[]) {
    using namespace lazart;

    // Parsing program arguments.
    cli::args args;
    {
        try {
            args = cli::get_args(argc, argv);
        } catch (std::exception& e) {
            Log().critical("invalid argument " + std::string(e.what()));
        } catch (...) {
            Log().critical("invalid argument");
        }
        //args.dump();

        if (args.help)
            return exit_code::Success;

        if (args.abort)
            exit(exit_code::CLIAbort);
    }

    // Open module.
    Log().debug("reading bytecode.");
    llvm::LLVMContext ctx;
    llvm::SMDiagnostic sm_diag; // TODO verify ?
    auto main_module = llvm::parseIRFile(args.module_path, sm_diag, ctx);

    if (!main_module) {
        Log().critical("cannot open file '" + args.module_path + ". Exiting.");
        exit(exit_code::CannotOpenSourceBytecode);
    }

    auto mutation = model::Mutation(main_module->getContext(), main_module.get(), args.fault_limit);

    // Parsing attack model.
    {
        auto parser = model::attack_model_parser(mutation);

        if (!parser.parse(args)) {
            Log().info("attack-model:\n " + mutation.am.str());
            return exit_code::ParsingError;
        }
        Log().debug("attack-model:\n" + mutation.am.str());
        Log().counter.reset();
    }

    // Preprocess
    {
        lazart::tasks::preprocess(mutation);

        auto [valid, err] = xllvm::verify_module(mutation.module);
        if (!valid)
            Log().warning("invalid 'protected' module.");
        if (!err)
            Log().warning("debug informations are corrupted for 'protected' module.");

        if (!results::save_module(lazart::paths::out_countermeasures, mutation))
            Log().warning("cannot save 'protected' module");
            
        Log().counter.reset();
    }

    // Run mutation
    {
        Log().debug("mutation...");
        mutation::mutation_pass mutation_pass{&mutation};

        for (auto& function : mutation.module->functions()) { // pass 
            mutation_pass.run(function);
        }

        Log().info("mutation ended. " + Log().counter.str() + " generated.");
        Log().counter.reset();
    }

    if (!args.no_inlining) {
        tasks::inline_mutations(mutation);
    }

    // Module clean.    
    if (!args.no_clean_unused_function) {
        Log().full("cleaning module");
        lazart::tasks::remove_unused_functions(mutation);
        Log().info("module clean.");
    }

    // Verify.
    {
        auto [valid, err] = xllvm::verify_module(mutation.module);
        if (!valid) {
            Log().critical("broken module, abort.");
            return exit_code::BrokenModule;
        }
        if (!err)
            Log().warning("debug information are corrupted, cannot save.");
        Log().full("module is valid.");
    }

    // Save module.
    if (!results::save_module_unsafe(args.mutated_module_path, mutation)) {
        Log().critical("cannot save module");
        return lazart::exit_code::SaveModuleFailed;
    }
    Log().info("module saved.");

    // Exports
    {
        if (!results::export_fault_space(lazart::paths::default_ip_yaml, mutation.fault_space))
            Log().warning("cannot export fault space.");
        if (!results::export_cm_space(lazart::paths::default_det_yaml, mutation.ccp_space))
            Log().warning("cannot export detectors list.");
    }

    auto [valid, err] = xllvm::verify_module(mutation.module);
    if (!valid) {
        Log().critical("broken module, abort.");
        return exit_code::BrokenModule;
    }
    if (!err)
        Log().warning("debug information are corrupted.");
    Log().full("module is valid.");
    return exit_code::Success;
}
