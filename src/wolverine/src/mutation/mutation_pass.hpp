/**
 * @file mutation_pass.hpp
 * @author Etienne Boespflug (etienne.boespflug@gmail.com)
 * @brief This files defines the mutation pass class which corresponds to the main mutation loop of Wolverine, doing all attack model related mutation.
 * @version 4.0
 */

#pragma once

#include <llvm/IR/Function.h>
#include <llvm/Pass.h>

#include "model/mutation.hpp"
#include "mutation/instr/user_defined_ip.hpp"

namespace lazart {
    /**
     * @brief The `mutation` namespace contains all definitions related to main mutation loop, excepting definition in `lazart::model` relative to the parsing and storing of the mutation parameters.
     * 
     */
    namespace mutation {
        class instrumentation_state; // fwd.

        /**
         * @brief The `mutation_state` structure holds statistic about mutation.
         */
        struct mutation_state
        {
            /// @brief The current basic block index of the loop.
            int basic_block_count = 0;
            /// @brief The current injection points count.
            int ip_count = 0;
        };

        /**
         * @brief The `mutation_pass` holds the main mutation loop algorithm using instrumentations information and attack model to apply fault model mutation on instruction when required. 
         */
        class mutation_pass {
        public:
            /**
             * @brief Construct a new `mutation_pass` object with the specified mutation parameters.
             * 
             * @param mut the parameters of the mutation.
             */
            mutation_pass(model::Mutation* mut) : mutation_{ mut }
            {}

            /**
             * @brief Run the mutation on the specified `function`.
             * 
             * @param function the function on which the mutation will be applied.
             * @return true if the function has been modified during the mutation.
             * @return false if no modification where made on the function.
             */
            virtual bool run(llvm::Function &function);

        private:
            /**
             * @brief The current mutation state. 
             */
            mutation_state state_{};
            
            /**
             * @brief The scanner for user defined IPs. 
             */
            user_defined_ip_scanner scanner_;

            /**
             * @brief The mutation parameters. 
             */
            model::Mutation* mutation_ = nullptr;
        };
    } // mutation
} // lazart
