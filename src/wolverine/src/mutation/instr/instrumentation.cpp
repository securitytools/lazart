
#include "mutation/instr/instrumentation.hpp"
#include "model/mutation.hpp"

#include "util/util.hpp"
#include "llvm/utils.hpp"

#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Module.h>

#include <boost/algorithm/string/predicate.hpp>
#include "llvm/Support/raw_ostream.h" // errs()

namespace fct_names = lazart::mutation::instrumentation_functions;
constexpr auto field = lazart::fields::mutation;

void lazart::mutation::instrumentation_state::basic_block_reset()
{
    bb_disabled_ = false;
    for(auto& rem : to_remove_)
        rem->eraseFromParent();
    to_remove_.clear();
}

bool lazart::mutation::instrumentation_state::is_known(const std::string& fct_name, bool only_wolverine_fct)
{
    auto w = fct_names::is_wolverine_fct(fct_name);
    if(only_wolverine_fct)
        return w;

    auto l = fct_names::is_lazart_fct(fct_name);

    return w || l || boost::starts_with(fct_name, names::mutation_fct_prefix);  
}

lazart::mutation::model_state lazart::mutation::instrumentation_state::get_model_state(model::fault_model* model) const
{
    if(model_states_.contains(model))
        return model_states_.at(model);
    return default_state_;
}


std::vector<lazart::model::fault_model*>& lazart::mutation::instrumentation_state::filter(llvm::Instruction& inst, std::vector<model::fault_model*>& mutating, model::Mutation& mutation) const
{
    // Tracing
    auto str_model = [](model::fault_model* model){
        auto name = model->get_name();
        if(name == "")
            name = std::to_string(reinterpret_cast<std::uintptr_t>(model));
        return name;
    };
    auto str_vec = [&str_model](std::vector<model::fault_model*>& mutating) {
        std::string ret = "[";

        for(auto model: mutating) {
            ret += str_model(model) + " ";
        }
        return ret + "]";
    };

    Log().magenta().debug  ("       mutating: " + str_vec(mutating), lazart::fields::instr);
    Log().magenta().debug("        > instr-state: " + str(), lazart::fields::instr);
    
    // Remove disabled
    mutating.erase(
    std::remove_if(mutating.begin(), mutating.end(),
        [this](const auto& m) { return get_model_state(m) == model_state::Disabled; }),
    mutating.end());

    // Push enabled
    for(const auto& [model, state]: model_states_) {
        if(get_model_state(model) == model_state::Enabled) { // Search for enabled.
            if(std::find(mutating.begin(), mutating.end(), model) == mutating.end()) { // Not already in list.
                if(model->should_mutate(inst, mutation)) { // Correct instruction type.
                    mutating.insert(mutating.begin(), model); // Insert (higher priority) + deque ? 
                }
            }
        }
    }

    Log().magenta().debug("       mutating-filtered: " + str_vec(mutating), lazart::fields::instr);

    return mutating;
}

bool lazart::mutation::instrumentation_state::scan_instruction(llvm::Instruction* instr, model::Mutation& mut)
{
    if (llvm::isa<llvm::CallInst>(instr)) {
        auto call = llvm::cast<llvm::CallInst>(instr);
        auto fct_name = xllvm::get_called_fct_name(call);
        
        auto unsupported_log = [&](){Log().critical("Unsupported internal '" + fct_name + "' at [], not recognizing Lazart's instrumentation function (inst_state::scan_instr).");};

        /**
         * @brief Reset all model of specified state to None and reset default_state if equal to specified state. 
         */
        auto reset_state = [this](model_state to_reset) {
            if(default_state_ == to_reset)
                default_state_ = model_state::None;
            std::for_each(std::begin(model_states_), std::end(model_states_), [to_reset](auto& pair){
                auto& ms = pair.second;
                if(ms == to_reset)
                    ms = model_state::None;
            });
        };

        /**
         * @brief Set all state to the specified state and set default state.
         * 
         */
        auto set_state = [this](model_state state) {
            default_state_ = state; 
            std::for_each(std::begin(model_states_), std::end(model_states_), [state](auto& pair){
                pair.second = state;
            });
        };

        if(fct_names::is_wolverine_fct(fct_name)) {
            to_remove_.push_back(instr);

            if(fct_names::is_global(fct_name)) { // enable_all, disable_all, reset, reset_disabled, reset_enabled
                if(fct_name == fct_names::disable_all) {
                    set_state(model_state::Disabled);      
                } else if (fct_name == fct_names::enable_all) {
                    set_state(model_state::Enabled);                   
                } else if (fct_name == fct_names::reset_disabled) {
                    reset_state(model_state::Disabled);
                } else if (fct_name == fct_names::reset_enabled) {
                    reset_state(model_state::Enabled);
                } else if (fct_name == fct_names::reset) {
                    reset_state(model_state::Enabled);
                    reset_state(model_state::Disabled);
                    default_state_ = model_state::None;
                }
                else {
                    unsupported_log();
                }
            } 
            else if (fct_names::is_named(fct_name)) { // disable_model_named, enable_model_named, reset_model_named}
                std::string model_name = xllvm::get_string_literal_arg(cast<llvm::CallInst>(instr), 0);
                auto model = mut.am.model_by_name(model_name);

                if(model) {
                    if(fct_name == fct_names::disable_model_named) {
                        model_states_[model] = model_state::Disabled;
                    } else if (fct_name == fct_names::enable_model_named) {
                        model_states_[model] = model_state::Enabled;
                    } else if (fct_name == fct_names::reset_model_named) {
                        model_states_[model] = model_state::None;
                    }
                    else {
                        unsupported_log();
                    }
                } else {
                    Log().warning("cannot retrieve the model named '" + model_name + "'.");
                }

            }  else {
                unsupported_log();
            }
            
            Log().yellow().verbose("      found directive: " + fct_name + " at " + xllvm::get_location(instr) + ".", field);
            Log().yellow().verbose("        instr-state: " + str(), lazart::fields::instr);
            return true;
        }
    }
    return false;
}

void lazart::mutation::instrumentation_state::scan_basic_block(llvm::BasicBlock& basic_block)
{
    basic_block_reset();
    std::vector<llvm::Instruction*> to_remove{};
    for (llvm::BasicBlock::iterator inst = basic_block.begin(), end = basic_block.end(); inst != end; inst++)         
        if (llvm::CallInst* call = llvm::dyn_cast<llvm::CallInst>(&*inst)) {
            llvm::Function* called = call->getCalledFunction();

            if(called)
            {
                auto fct_name = std::string(fct_names::disable_bb);
                if(called->getName().equals(fct_name))
                {
                    to_remove.push_back(&*inst);
                    Log().yellow().verbose("    found directive: " + fct_name + ".", field);
                    if(bb_disabled_)
                    {
                        Log().warning("Multiple call to Lazart primitive '" + fct_name + "' in basic block " + basic_block.getName().str() + ".");
                    }
                    bb_disabled_ = true;
                }
            }
        }
        
    for(auto& rem : to_remove) {
        rem->eraseFromParent();
    }
}

// Called at start of fct.
void lazart::mutation::instrumentation_state::scan_function(llvm::Function& function)
{
    std::vector<llvm::Instruction*> to_remove{};
    model_states_.clear();

    for (llvm::inst_iterator inst = llvm::inst_begin(function), end = llvm::inst_end(function); inst != end; ++inst)
    {
        if (llvm::CallInst* call = llvm::dyn_cast<llvm::CallInst>(&*inst)) {
            llvm::Function* called = call->getCalledFunction();

            if(called)
            {
                auto fct_name = called->getName().str();
                if(util::starts_with(fct_name, fct_names::prefix)) {
                    if(called->getName().equals(std::string(fct_names::disable_function)))
                    {
                        to_remove.push_back(&*inst);
                        Log().yellow().verbose("    found directive: " + fct_name + ".", field);
                        if(function_disabled_)
                        {
                            Log().warning("Multiple call to Lazart primitive '" + std::string(fct_names::disable_function) + "' in function " + fct_name + ".");
                        }
                        function_disabled_ = true;
                    }
                    else if(!is_known(fct_name))
                    {
                        Log().warning("Found function '" + fct_name + "' at " + xllvm::get_location(call) + " not recognizing Lazart's instrumentation function (instr_state:scan_fn).");
                    }
                }
            }
        }
    }
    
    for(auto& rem : to_remove)
        rem->eraseFromParent();
}
        
std::string lazart::mutation::instrumentation_state::str() const
{
    auto state_str = [](const model_state& state) -> std::string {
        if(state == model_state::Enabled)
            return "enabled";
        if(state == model_state::Disabled)
            return "disabled";
        return "none";
    };

    std::string ret = "default " + state_str(default_state_) + ", ";
    ret += "named (" + std::to_string(model_states_.size()) + "): {";
    for(const auto& [k,v]: model_states_)  {
        ret += "(" + k->get_name() + ": " + state_str(v) + ")";
    }
    return ret + "}";
}