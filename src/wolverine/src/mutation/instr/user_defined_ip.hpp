/**
 * @file user_defined_ip.hpp
 * @author Etienne Boespflug (etienne.boespflug@gmail.com)
 * @brief This file defines the default scanner for User Defined IP allowing to retrieve IP already in the source LLVM module.
 * @version 4.0
 */
#pragma once

#include "model/fault_models/fault_models.hpp"
#include "model/mutation.hpp"

#include "llvm/llvm_fwd.hpp"
#include "mutation/fault_space/fault_space.hpp"

#include <vector>

namespace lazart::mutation {
    struct mutation_state; // fwd

    /**
     * @brief The `user_defined_ip_scanner` class corresponds to the default scanner to check if an instruction corresponds to an user-defined IP. 
     */
    class user_defined_ip_scanner {
        public:
            user_defined_ip_scanner() {}

            /// @brief IP declaration for mutation macros.
            static constexpr const char* declare_uip = "_LZ__DECLARE_UIP";

            // Model names

            /// @brief String code for jump model.
            static constexpr const char* model_ti = "Jump";
            /// @brief String code for switch-call model.
            static constexpr const char* model_sc = "SwitchCall";
            /// @brief String code for user defined model.
            static constexpr const char* model_user = "User";
            
            /**
             * @brief Scan the `instruction` and detect user defined mutation function calls Returns `true` if the instruction as been consumed (if a mutation function is recognized).
             * 
             * @param mut the mutation parameters. 
             * @param ms the current state of the mutation. 
             * @param instr the instruction to be scanned. 
             * @return true if the instruction as been consumed.
             * @return false if nothing has been detected.
             */
            bool scan_instruction(model::Mutation& mut, mutation_state& ms, llvm::Instruction* instr);

            /**
             * @brief Erase all instructions in `to_remove` and clear the list. 
             */
            void erase_instructions();

        private:
            /**
             * @brief List of instruction that has been consumed and have to be removed after basic-block exploration. 
             */
            std::vector<llvm::Instruction*> to_remove_;
    };
}