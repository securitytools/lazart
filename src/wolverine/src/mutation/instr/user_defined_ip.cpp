#include "user_defined_ip.hpp"

#include <boost/algorithm/string/predicate.hpp>

#include "core/wolverine.hpp"
#include "model/fault_models/test_inversion.hpp"
#include "model/fault_models/data_load.hpp"
#include "mutation/mutation_pass.hpp"

#include "llvm/utils.hpp"
#include "llvm/IR/Instruction.h"


void lazart::mutation::user_defined_ip_scanner::erase_instructions()
{
    for(auto& rm : to_remove_)
        rm->eraseFromParent();
    to_remove_.clear();
}

bool lazart::mutation::user_defined_ip_scanner::scan_instruction(model::Mutation& mut, mutation_state& ms, llvm::Instruction* instr)
{
    if (llvm::isa<llvm::CallInst>(instr)) {
        auto fname = xllvm::get_called_fct_name(llvm::cast<llvm::CallInst>(instr));

        auto bb = instr->getParent();
        auto fct = bb->getParent();

        if (boost::starts_with(fname, names::mutation_fct_prefix))
        {
            if(boost::starts_with(fname, model::test_inversion::ti_mut_fct)) {
                auto ip_id = xllvm::get_string_literal_arg(cast<llvm::CallInst>(instr), 2, true);
                Log().light_blue().info("      found IP " + ip_id + " (ti) at " + md::get_location(instr).str() + ".");
                std::string edge = (fname == model::test_inversion::ti_mut_fct) ? "both" : 
                (fname == model::test_inversion::ti_false_mut_fct ? "false" : "true");


                mut.fault_space.faults.push_back(lazart::mutation::fault{
                    .user_generated = true,
                    .ip_id = ip_id,
                    .ip_num = (ms.ip_count++),
                    .function = fct,
                    .type = model::fault_type::TestInversion,
                    .args = "null",
                    .bb_name = bb->getName().str(),
                    .loc = md::get_location(instr),
                    .data_emitter = [=](YAML::Emitter& out) { 
                        if(edge == "both")
                            return;
                        
                        out << YAML::Key << "data" << YAML::BeginMap;
                        out << YAML::Key << "edge" << YAML::Value << edge; 
                        out << YAML::EndMap; 
                    }

                });
                return true;
            }
            else if (boost::starts_with(fname, lazart::names::mutation_fct_prefix + "dl")) {
                auto ip_id = xllvm::get_string_literal_arg(cast<llvm::CallInst>(instr), 2, true);
                Log().light_blue().info("      found IP " + ip_id + " (dl) at " + md::get_location(instr).str() + ".");
                // Check type
                std::string type = "";
                if(fname.find("i8") != std::string::npos)
                    type = "i8";
                else if(fname.find("i16") != std::string::npos)
                    type = "i16";
                else if(fname.find("i32") != std::string::npos)
                    type = "i32";
                else if(fname.find("i64") != std::string::npos)
                    type = "i64";
                else if(fname.find("i128") != std::string::npos)
                    type = "i128";
                else 
                    Log().warning("unknown data type : " + fname + ".");

                mut.fault_space.faults.push_back(lazart::mutation::fault{
                    .user_generated = true,
                    .ip_id = std::string(ip_id),
                    .ip_num = (ms.ip_count++),
                    .function = fct,
                    .args = "null",
                    .type =  model::fault_type::DataLoad,
                    .bb_name = bb->getName().str(),
                    .loc = md::get_location(instr),
                    .data_emitter = [=](YAML::Emitter& out) { 
                        out << YAML::Key << "data" << YAML::BeginMap;

                        out << YAML::Key << "value" << YAML::Value << "unknown";
                        out << YAML::Key << "var" << YAML::Value << "unknown";
                        out << YAML::Key << "impl" << YAML::Value << fname;
                        out << YAML::Key << "type" << YAML::Value << type;
                        out << YAML::EndMap; 
                    }
                });
                return true;
            }
        } else if(boost::starts_with(fname, declare_uip)) {
            auto ip_id = xllvm::get_string_literal_arg(cast<llvm::CallInst>(instr), 0, true);
            auto type = xllvm::get_string_literal_arg(cast<llvm::CallInst>(instr), 1, true);   
            auto data = xllvm::get_string_literal_arg(cast<llvm::CallInst>(instr), 2, true);        

            Log().light_blue().info("      found IP " + ip_id + "(type:'" + type + "';data:'" + data + "') at " + md::get_location(instr).str() + ".");
            
            to_remove_.push_back(instr);

            mut.fault_space.faults.push_back(lazart::mutation::fault{
                .user_generated = true,
                .ip_id = ip_id,
                .ip_num = (ms.ip_count++),
                .function = fct,
                .type = model::to_string(type),
                .args = "null",
                .bb_name = bb->getName().str(),
                .loc = md::get_location(instr),
                .data_emitter = [=](YAML::Emitter& out) { 
                        out << YAML::Key << "data" << YAML::BeginMap;
                        out << YAML::Key << "custom" << YAML::Value << data;
                        out << YAML::EndMap; 
                }
            });
            return true;
        }
        
    }
    return false;
}