/**
 * @file instrumentation.hpp
 * @author Etienne Boespflug (etienne.boespflug@gmail.com)
 * @brief This file contains definitions related to user instrumentation of the code, such as mutation directive or user defined IPs.
 * @version 4.0
 */
#pragma once

#include <array>
#include <vector>
#include <string>

#include "llvm/llvm_fwd.hpp"

#include "model/fault_models/fault_models.hpp"

namespace lazart {
namespace mutation {
    /**
     * @brief The namespace `instrumentation_functions` contains constants for instrumentation prefix and names.
     */
    namespace instrumentation_functions {
        /// @brief Prefix for all Lazart. TODO redundant with wolverine ? 
        constexpr const char* prefix = "_LZ__";

        /// @brief Directive to disable all mutations in the function.
        constexpr const char* disable_function = "_LZ__DISABLE_FUNCTION";
        /// @brief Directive to disable all mutations in the basic block.
        constexpr const char* disable_bb = "_LZ__DISABLE_BB";

        /// @brief Directive to reset all enabling/disabling directives.
        constexpr const char* reset = "_LZ__RESET";
        /// @brief Directive to disable all models.
        constexpr const char* disable_all = "_LZ__DISABLE_ALL";
        /// @brief Directive to enable all models.
        constexpr const char* enable_all = "_LZ__ENABLE_ALL";        
        /// @brief Directive to reset all enabling directives.
        constexpr const char* reset_enabled = "_LZ__RESET_ENABLED";
        /// @brief Directive to reset all disabling directives.
        constexpr const char* reset_disabled = "_LZ__RESET_DISABLED";

        /// @brief Directive to disable a named model.
        constexpr const char* disable_model_named = "_LZ__DISABLE_MODEL";
        /// @brief Directive to enable a named model.
        constexpr const char* enable_model_named = "_LZ__ENABLE_MODEL";
        /// @brief Directive to reset all disabling directives to a named model.
        constexpr const char* reset_model_named = "_LZ__RESET_MODEL";
        /// @brief Directive to rename a basic block.
        constexpr const char* rename_bb = "_LZ__RENAME_BB";

        /**
         * @brief List of lazart's instrumentation functions.
         */
        constexpr auto instrumentation_functions = std::array{disable_function, disable_bb, 
                                                     disable_all, enable_all,
                                                     disable_model_named, enable_model_named,
                                                     rename_bb,
                                                     reset, reset_enabled, reset_disabled, reset_model_named};

        /**
         * @brief List of Lazart's directives for named fault models. 
         */
        constexpr auto named_directives = std::array{disable_model_named, enable_model_named, reset_model_named};
        /**
         * @brief List of Lazart's global directives.
         */
        constexpr auto global_directives = std::array{enable_all, disable_all, reset, reset_disabled, reset_enabled};

        /**
         * @brief List of lazart's functions (excluding instrumentation).
         */
        constexpr auto lazart_functions = std::array{"_LZ__DECLARE_UIP", "_LZ__triggered", "_LZ__trigger_detector", "_LZ__trigger_detector_stop"};

        /**
         * @brief Predicate determining if a function name corresponds to Wolverine's functions.
         * 
         * @param fct_name the name of the function.
         * @return true if the function is Wolverine function.
         * @return false if the function is not Wolverine function.
         */
        inline bool is_wolverine_fct(const std::string& fct_name) { return std::find(std::begin(instrumentation_functions), std::end(instrumentation_functions), fct_name) != std::end(instrumentation_functions);}

        /**
         * @brief Predicate determining if a function name corresponds to Lazart's functions.
         * 
         * @param fct_name the name of the function.
         * @return true if the function is Lazart function.
         * @return false if the function is not Lazart function.
         */
        inline bool is_lazart_fct(const std::string& fct_name) { return std::find(std::begin(lazart_functions), std::end(lazart_functions), fct_name) != std::end(lazart_functions);}

        /**
         * @brief Predicate determining if a function name corresponds to Lazart's named instrumentation directives.
         * 
         * @param fct_name the name of the function.
         * @return true if the function is a named instrumentation directive.
         * @return false if the function is not a named instrumentation directive.
         */
        inline bool is_named(const std::string& fct_name) { return std::find(std::begin(named_directives), std::end(named_directives), fct_name) != std::end(named_directives);}

        /**
         * @brief Predicate determining if a function name corresponds to Lazart's global instrumentation directives.
         * 
         * @param fct_name the name of the function.
         * @return true if the function is a global instrumentation directive.
         * @return false if the function is not a global instrumentation directive.
         */
        inline bool is_global(const std::string& fct_name) { return std::find(std::begin(global_directives), std::end(global_directives), fct_name) != std::end(global_directives);}
    }
    /**
     * @brief The model_state enum represents the three possible instrumentation state for a fault model or a set of fault models.
     */
    enum class model_state {
        Disabled = -1, // Forced disabled.
        None = 0, // Default state (depends on attack model).
        Enabled = 1 // Forced enabled.
    };

    /**
     * @brief The `instrumentation_state` holds current state of instrumentation directive and provides functions to update the state with instruction during mutation pass. 
     */
    class instrumentation_state {
    public:

        /**
         * @brief Reset the instrumentation state for a basic block.
         * 
         * Called on each new basic block to reset state.
         */
        void basic_block_reset();

        /**
         * @brief Scan the specified `function` for instrumentation directive, and delete the corresponding instructions if any.
         * 
         * @param function the function to be scanned.
         */
        void scan_function(llvm::Function& function);

        /**
         * @brief Scan the specified `bb` for instrumentation directive, and delete the corresponding instructions if any.
         * 
         * @param bb the basic block to be scanned.
         */
        void scan_basic_block(llvm::BasicBlock& bb);

        /**
         * @brief Scan the instruction to detect Lazart's instrumentation directives.
         * 
         * @param instr the instruction to be scanned.
         * @param mutation the mutation state.
         * @return true if the instruction has been consumed.
         * @return false if the instruction is not Lazart's instrumentation directives.
         */
        bool scan_instruction(llvm::Instruction* instr, model::Mutation& mutation);

        /**
         * @brief Filter a list of fault model `mutating` for the specified instruction `inst`. Higher priority models are placed at the front of the list and models are enabled / disabled depending on current instrumentation state.
         * 
         * @param inst the instruction on which the fault models should be applied.
         * @param mutating the list of fault model mutating the instruction in the attack model.
         * @param mutation the mutation state.
         * @return std::vector<model::fault_model*>& the filter list of fault models. 
         */
        std::vector<model::fault_model*>& filter(llvm::Instruction& inst, std::vector<model::fault_model*>& mutating, model::Mutation& mutation) const;

        /**
         * @brief Returns `true` if the mutation is enabled in the current function, `false` otherwise. 
         */
        bool is_function_active() const { return !function_disabled_; }

        /**
         * @brief Returns `true` if mutation is enabled in the current basic block, `false` otherwise. 
         */
        bool is_bb_active() const { return !bb_disabled_; }

        /**
         * @brief Returns the state of the specified model.
         * 
         * @param fm the fault model.
         * @return the state of the model.
         */
        model_state get_model_state(model::fault_model* fm) const;

        /** 
         * @return std::string the string representation of the instrumentation state. 
         */
        std::string str() const;
    
    private:
        /// @brief Predicate determining if the current function is disabled by instrumentation state.
        bool function_disabled_ = false;
        /// @brief Predicate determining if the current basic block is disabled by instrumentation state.
        bool bb_disabled_ = false;
        /// @brief State of fault model (None if not in map).
        std::map<model::fault_model*, model_state> model_states_;
        /// @brief Default state of models not present in the `model_states_` map.
        model_state default_state_ = model_state::None;

        /// @brief Instruction to be removed after basic block mutation.
        std::vector<llvm::Instruction*> to_remove_; 

        /**
         * @brief Return a boolean determining if the function named `fct_name` is a Wolverine or Lazart function. 
         * 
         * @param fct_name the name of the function.
         * @param only_wolverine_fct if `true`, only Wolverine specific function will return `true`.
         * @return true if the function belongs to Lazart. 
         * @return false if the function is not related to Lazart.
         */
        static bool is_known(const std::string& fct_name, bool only_wolverine_fct = false);
    };
} // mutation
} // lazart
