/**
 * @file metadata.hpp
 * @author Etienne Boespflug (etienne.boespflug@gmail.com)
 * @brief This file contains definitions related to manipulation of LLVM's metadata in Wolverine.
 * 
 * It contains commons Metadata structures and utility function to tag LLVM value with metadata or extract metadata.
 * 
 * @version 4.0
 */
#pragma once 

#include <string>
#include "llvm/llvm_fwd.hpp"

namespace lazart {
    /**
     * @brief The `md` namespaces contains definitions relative to manipulation of LLVM's metadata and debug information in Wolverine.
     * 
     */
    namespace md {
        /**
         * @brief The source_loc struct holds information about source code location.
         * 
         */
        struct source_loc 
        {
            /// @brief The line number of the location.
            unsigned line = -1;
            /// @brief The column number of the location.
            unsigned col = -1;
            /// @brief The name of the source code of the location.
            std::string file;

            /**
             * @brief Returns the string representation of the source location. Use the format '[col, line, file]' with '?' if the data is not available.
             * 
             * @return std::string the string representation of the source location.
             */
            std::string str() const {
                std::string col_str = (col < 0) ? "?" : std::to_string(col);     
                std::string line_str = (line < 0) ? "?" : std::to_string(line);
                std::string file_str = (file == "") ? "?" : file;
                
                return "[" + line_str + "," + col_str + "]";
            }
        };

        /// @brief TODO ? NOT IMPLEMENTED
        llvm::MDNode* get_debug_info();
        /// @brief TODO ? NOT IMPLEMENTED
        llvm::MDNode* get_debug();

        /**
         * @brief The `md::names` namespace defines constants string identifiers for Wolverine's metadata.
         */
        namespace names {
            /// @brief The source name of a variable. Mostly used for data load fault model.
            constexpr auto var_name = "lz.w.var_name";
            /// @brief Set the instruction of an IP as associated with a countermeasure point. Store the ID of the detector.
            constexpr auto cm_ip = "lz.w.cm_ip"; 
        }

        /**
         * @brief Add variable source `name` as metadata to the specific instruction.
         * 
         * @param inst the instruction to be tagged.
         * @param name the name of the variable.
         */
        void add_lz_var_name(llvm::Instruction* inst, const std::string& name);
        /**
         * @brief Get the variable source name metadata of the instruction, or an empty string.
         * 
         * @param inst the instruction from which the metadata is extracted.
         * @return std::string the name of the variable, or empty string.
         */
        std::string get_lz_var_name(const llvm::Instruction* inst);

        /**
         * @brief Set the countermeasure identifier `ccp_id` metadata to the specified instruction.
         * 
         * @param inst the instruction to be tagged.
         * @param ccp_id the id of the associated countermeasure point.
         */
        void set_lz_cm_ip(llvm::Instruction* inst, const std::string& ccp_id);
        /**
         * @brief Get the countermeasure identifier metadata of the instruction, or an empty string.
         * 
         * @param inst the instruction from which the metadata is extracted.
         * @return std::string the countermeasure identifier, or empty string. 
         */
        std::string get_lz_cm_ip(llvm::Instruction* inst);
        /**
         * @brief Return `true` if the instruction `inst` has a countermeasure point (detector/CCP) associated, `false` otherwise.
         * 
         * @param inst the instruction from which the metadata is extracted.
         */
        bool is_lz_cm_ip(llvm::Instruction* inst);

        /**
         * @brief Get the source location information from debug metadata of the instruction `i`.
         * 
         * @param i the instruction from which the metadata is extracted.
         * @return source_loc the source location information.
         */
        source_loc get_location(const llvm::Instruction* i);
        
        /**
         * @brief Get the LLVM MDNode corresponding to the specified LLVM value. The metadata are searched in all instruction of the `function`.
         * 
         * @param value the value from which the metadata are extracted.
         * @param function the function in which the metadata instruction are scanned.
         * @return llvm::MDNode* 
         * @note https://stackoverflow.com/questions/21410675/getting-the-original-variable-name-for-an-llvm-value
         */
        llvm::MDNode* get_var_mdnode(const llvm::Value* value, const llvm::Function* function);
        /**
         * @brief Retrieve the name of the variable in the source. 
         * 
         * Searches into enclosing function debug information (see #get_var_mdnode) if available or return the name of the variable in the LLVM bytecode.
         * 
         * @param value the value from which the metadata are extracted.
         * @return llvm::StringRef the retrieved name of the variable.
         * @note https://stackoverflow.com/questions/21410675/getting-the-original-variable-name-for-an-llvm-value
         */
        llvm::StringRef get_source_name(const llvm::Value* value);    
        /**
         * @brief Retrieve the name of the variable loaded in the `inst` load instruction.
         * 
         * Use Lazart's name metadata if the name cannot be retrieved from LLVM's metadata.
         * 
         * @param inst the instruction from which the source name will be retrieved.
         * @return * llvm::StringRef the retrieved name of the variable.
         */
        llvm::StringRef get_var_name(const llvm::LoadInst* inst);      
        
    }
}