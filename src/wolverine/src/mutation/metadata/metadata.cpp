#include "metadata.hpp"

#include <cstdlib>
#include <iostream>
#include <sstream>

#include "util/util.hpp"

#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Module.h>
#include <llvm/Transforms/Utils/BasicBlockUtils.h>
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/DebugInfoMetadata.h"
#include "llvm/IR/Metadata.h"
#include <llvm/IR/Verifier.h>
#include <llvm/Support/raw_ostream.h>
#include "llvm/IR/DebugInfoMetadata.h"
#include "llvm/IR/Metadata.h"
#include "llvm/utils.hpp"

 
lazart::md::source_loc lazart::md::get_location(const llvm::Instruction* inst)
{
    auto de = inst->getDebugLoc();
    if(!de)
        return {};

    auto* scope = cast<llvm::DIScope>(de->getScope());
    std::string fname = scope->getFilename();
    return {.line = de.getLine(), .col = de.getCol(), .file = fname};
}

// https://stackoverflow.com/questions/21410675/getting-the-original-variable-name-for-an-llvm-value
llvm::MDNode* lazart::md::get_var_mdnode(const llvm::Value* value, const llvm::Function* function) 
{
    for (llvm::const_inst_iterator it = llvm::inst_begin(function), end = llvm::inst_end(function); it != end; ++it) 
    {
        const llvm::Instruction* inst = &*it;
        if (const llvm::DbgDeclareInst* dd_inst = dyn_cast<llvm::DbgDeclareInst>(inst)) 
        {
            if (dd_inst->getAddress() == value) 
                return dd_inst->getVariable();
        } 
        else if (const llvm::DbgValueInst* dv_inst = dyn_cast<llvm::DbgValueInst>(inst)) 
        {
            if (dv_inst->getValue() == value) 
                return dv_inst->getVariable();
        }
    }
    return nullptr;
}



// https://stackoverflow.com/questions/21410675/getting-the-original-variable-name-for-an-llvm-value
llvm::StringRef lazart::md::get_source_name(const llvm::Value* value) 
{
    const llvm::Function* function = xllvm::get_enclosing_function(value);
    if (!function) 
        return value->getName();

    const llvm::MDNode* node = get_var_mdnode(value, function);
    if (node) {
        return dyn_cast<llvm::DIVariable>(node)->getName();
    } 

    return ""; 
}

llvm::StringRef lazart::md::get_var_name(const llvm::LoadInst* l_inst) 
{
    auto var = l_inst->getPointerOperand();
    auto var_name = md::get_source_name(var).str();

    auto lz_var = get_lz_var_name(l_inst);
    
    if(lz_var != "" && var_name != "")
        Log().warning("lazart var name and source name are not available for the instruction " + get_location(l_inst).str() + ".");
        
    if(var_name != "")
        return var_name;

    if(lz_var != "")
        return lz_var;

    

    return l_inst->getName();
}

// VAR NAME


void lazart::md::add_lz_var_name(llvm::Instruction* inst, const std::string& name)
{
    auto& ctx = xllvm::get_context(inst);
    auto node = llvm::MDNode::get(ctx, llvm::MDString::get(ctx, name));
    inst->setMetadata(names::var_name, node);
}

std::string lazart::md::get_lz_var_name(const llvm::Instruction* inst)
{
    auto node = inst->getMetadata(names::var_name);
    if(node == nullptr)
        return "";
    return llvm::cast<llvm::MDString>(node->getOperand(0))->getString();
}

// CM IP 

std::string lazart::md::get_lz_cm_ip(llvm::Instruction* inst)
{
    auto node = inst->getMetadata(names::cm_ip);
    if(node == nullptr)
        return "";
    return llvm::cast<llvm::MDString>(node->getOperand(0))->getString();
}

void lazart::md::set_lz_cm_ip(llvm::Instruction* inst, const std::string& ccp_id)
{
    auto& ctx = xllvm::get_context(inst);
    auto node = llvm::MDNode::get(ctx, llvm::MDString::get(ctx, ccp_id));
    inst->setMetadata(names::cm_ip, node);
}



bool lazart::md::is_lz_cm_ip(llvm::Instruction* inst)
{
    return get_lz_cm_ip(inst) != "";
}