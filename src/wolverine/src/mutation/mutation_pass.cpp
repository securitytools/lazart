#include "mutation_pass.hpp"
#include "core/wolverine.hpp"
#include "util/util.hpp"
#include "core/wolverine.hpp"
#include "llvm/utils.hpp"

#include "model/mutation.hpp"
#include "mutation/instr/instrumentation.hpp"
#include "mutation/instr/user_defined_ip.hpp"
#include "mutation/tasks/tasks.hpp"

#include <boost/algorithm/string/predicate.hpp>

constexpr auto field = lazart::fields::mutation;

bool lazart::mutation::mutation_pass::run(llvm::Function& function)
{
    bool modif = false;
    auto fname = function.getName().str();

    if(boost::starts_with(fname, names::lazart_prefix))
    {
        Log().full("  ignoring Lazart's internal " + fname, field);
        return false;
    }
    if(boost::starts_with(fname, names::llvm_prefix))
    {
        Log().full("  ignoring LLVM's internal " + fname, field);
        return false;
    }
    
    Log().verbose("  running on fct " + fname, field);
    instrumentation_state state;
    state.scan_function(function);

    for (auto bb = function.begin(), end = function.end(); bb != end; bb++)
    {
        start:
        auto& basic_block = *bb; 
        state.scan_basic_block(basic_block);
        bool modif_in_bb = false;
        
        Log().verbose("    running on bb " + basic_block.getName().str(), field);

        for (auto inst = basic_block.begin(), i_end = basic_block.end(); inst != i_end; inst++) 
        {
            Log().debug("      instr " + xllvm::get_location(&*inst) + ": " + xllvm::to_string(*inst), field);
            if(scanner_.scan_instruction(*mutation_, state_, &*inst)) {
                // Mutation function, end.
            } 
            else if(state.scan_instruction(&*inst, *mutation_)) {
                // Instrumentation directive, end.
            }
            else if(state.is_bb_active() && state.is_function_active()) // Still parsing directive even if function is disabled. 
            {
                auto mutating = mutation_->am.mutating_models(*inst, *mutation_);
                state.filter(*inst, mutating, *mutation_);

                if(mutating.size() == 0) 
                    continue;

                auto mutating_model = mutating[0];
                auto mret = mutating_model->mutate(state_.ip_count, &*inst, *mutation_);
                
                if(mret.first != nullptr)
                {
                    mutation_->fault_space.faults.push_back(mret.second);
                    modif_in_bb = true;   
                    state_.ip_count++;
                    if(&basic_block != mret.first->getParent()) {
                        // Retrieve basic block (inline mutations only).
                        while(&*bb != mret.first->getParent()) {
                            bb++;
                            if(bb == function.end()) {
                                Log().critical("gone to the end of the function basic block (inline bug?).");
                                continue;
                            }
                        }
                        Log().debug("   - rematching " + basic_block.getName().str() + " => " + bb->getName().str() + ".", field);
                        
                        modif = modif_in_bb || modif;
                        goto start;
                    }
                }
            } 
        }
        modif = modif_in_bb || modif;
    }
    scanner_.erase_instructions(); // Remove lasting declarations.
    
    return modif;
}