/**
 * @file test_multiplication.hpp
 * @author Etienne Boespflug (etienne.boespflug@gmail.com)
 * @brief This files defines the Test Multiplication countermeasure (TM).
 * @version 4.0
 */
#pragma once


#include "llvm/llvm_fwd.hpp"
#include "countermeasures.hpp"

#include <llvm/IR/DebugLoc.h>

namespace lazart {

    namespace tasks {

        /**
         * @brief The `test_multiplication` class corresponds to a Load Multiplication parameters, holding specific parameters.
         * 
         * Uses 'tm_point_id:depth' syntax for the `ccps` description field.
         */
        class test_multiplication : public weighted_countermeasure {
            public:            
                /**
                 * @brief Construct a new `test_multiplication` object for the specified mutation with parameters.
                 * 
                 * @param mutation the context of the mutation. 
                 * @param global_depth the application depth (weight) used if no information is indicated by the user.
                 * @param on the list of functions on which the mutation should be applied, can include code string such as "__sym__" and "__mut__".
                 * @param ccps the list of countermeasure application points to be modified (if empty, all possible cm points are transformed).
                 * @param trigger_fct the name of detector triggering function to be used.
                 */
                test_multiplication(model::Mutation& mutation, int global_depth = 1, std::vector<std::string> on = {}, std::vector<std::string> ccps = {}, const std::string& trigger_fct = trigger_fct_default) : weighted_countermeasure(mutation, global_depth, on, ccps, trigger_fct) {}


                virtual bool apply() override;

                virtual std::string str() const override;

                /**
                 * @brief  The `cm_point` struct holds information about the TM application point.
                 * 
                 * It uses an unique identifier integer, that will be transformed into two application point for the true and the false branch.
                 */
                struct cm_point {
                    /// @brief The branch instruction to be protected.
                    llvm::BasicBlock* source;
                    /// @brief The False branch.
                    llvm::Value* targetF;
                    /// @brief The True branch.
                    llvm::Value* targetT;
                    /// @brief The condition of the branch instruction to be protected.
                    llvm::Value* cond;
                    /// @brief The function englobing the application point.
                    llvm::Function* function;
                    /// @brief The source code location debug metadata.
                    md::source_loc loc;
                    /// @brief The identifier of the application point.
                    uint32_t id;
                    /// @brief The basic block after the point of insertion.
                    llvm::BasicBlock* insert_before = nullptr;
                    /// @brief Debug info associated to the load instruction.
                    llvm::MDNode* dbg;
                };

            protected:
                /**
                 * @brief Apply the test multiplication on a specific application point's branch.
                 * 
                 * @param data the application point. 
                 * @param target the target of the branch.
                 * @param id_base the base identifier of the application point.
                 * @param depth the depth of application of LM.
                 * @param true_branch determines if the protected branch is a true branch or a false branch.
                 * @return true if the program has been modified.
                 * @return false if the program hasn't been modified.
                 */
                bool apply_branch(lazart::tasks::test_multiplication::cm_point data, llvm::BasicBlock*& target, const std::string& id_base, int depth, bool true_branch);

                /**
                 * @brief Apply the test multiplication scheme on the specified application point.
                 * 
                 * @param data the application point. 
                 * @return true if the program has been modified.
                 * @return false if the program hasn't been modified.
                 */
                bool apply(cm_point data);
        };
    }
}