
#include "mutation/tasks/countermeasures/sec_swift.hpp"
#include "countermeasures.hpp"
#include "llvm/utils.hpp"
#include "model/mutation.hpp"

#include "llvm/Support/raw_ostream.h"
#include "model/fault_models/test_inversion.hpp"
#include "model/fault_models/fault_models.hpp"
#include "mutation/fault_space/fault_space.hpp"

#include "mutation/metadata/metadata.hpp"

using namespace std;
using namespace llvm;

std::string lazart::tasks::sec_swift::str() const
{
      return "SECSWIFT";
}

void lazart::tasks::sec_swift::setIds(llvm::Module *module){
      for (auto &function : module->functions())
      {
            if(should_apply(function)){
                  std::vector<std::string> vectorBBs;
                  auto i = 0;
                  for (llvm::BasicBlock& bb : function){
                        vectorBBs.push_back(bb.getName().str());
                        Log().debug("BB : "+bb.getName().str() + " ID : "+ to_string(i++));
                  }
                  blocks.insert(std::pair<std::string, std::vector<std::string>>(function.getName().str(),vectorBBs));
            }
      }
}     
std::vector<std::string> lazart::tasks::sec_swift::get_list_bb(llvm::Function& f){
      for(auto item : blocks)
      {
            if(item.first==f.getName().str()){
                  return item.second;
            }
      }
      return {};
}

int lazart::tasks::sec_swift::get_id(Function& f,BasicBlock& bb){
      std::vector<std::string> vectorBBs = lazart::tasks::sec_swift::get_list_bb(f);
      string bb_name = bb.getName().str();
      for(unsigned int i = 0; i < vectorBBs.size(); i++)
      {
            if(vectorBBs[i]==bb_name)
                  return i;
      }
      return -1;
}
int lazart::tasks::sec_swift::get_id_name(Function& f,std::string bb_name){
      std::vector<std::string> vectorBBs = lazart::tasks::sec_swift::get_list_bb(f);
      for(unsigned int i = 0; i < vectorBBs.size(); i++)
      {
            if(vectorBBs[i]==bb_name)
                  return i;
      }
      return -1;
}

bool lazart::tasks::sec_swift::is_last_bb(BasicBlock& bb){
      return !dyn_cast<BranchInst>(bb.getTerminator());
}
bool lazart::tasks::sec_swift::is_first_bb(BasicBlock& bb){
      std::vector<std::string> vect = lazart::tasks::sec_swift::get_list_bb(*bb.getParent());
      return vect[0]==bb.getName().str();
}

bool lazart::tasks::sec_swift::is_last_conditional(BasicBlock &bb){
      return dyn_cast<BranchInst>(bb.getTerminator())->isConditional();
}


bool lazart::tasks::sec_swift::runOnBB(BasicBlock& bb){
      int id_block = lazart::tasks::sec_swift::get_id(*bb.getParent(),bb);
      auto int32Type = Type::getInt32Ty(mut.module->getContext());
      auto int32PtrType = Type::getInt32PtrTy(mut.module->getContext());
      auto voidType = Type::getVoidTy(mut.module->getContext());
      IRBuilder<> Builder(bb.getFirstNonPHI());
      if(lazart::tasks::sec_swift::is_first_bb(bb)){
            auto gsr_alloca = Builder.CreateAlloca(Type::getInt32Ty(mut.module->getContext()));
            auto rts_alloca = Builder.CreateAlloca(Type::getInt32Ty(mut.module->getContext()));
            GSR = Builder.CreateStore(Builder.getInt32(id_block), gsr_alloca);
            RTS = Builder.CreateStore(Builder.getInt32(0), rts_alloca);
      } else {
            Type *args_verify[] = {int32Type, int32PtrType, int32PtrType};
            auto funType_verify = FunctionType::get(voidType, args_verify, false);
            auto verify_func = mut.module->getOrInsertFunction("SECSWIFT_VERIFY", funType_verify);
            Value *args1[] = {Builder.getInt32(id_block), GSR->getPointerOperand(), RTS->getPointerOperand()};
            Builder.CreateCall(verify_func, args1);

            // cm_space
            static auto todo_id = 0;
            std::string id = std::to_string(todo_id++);
            lazart::tasks::countermeasure_point cp = {
                  .id = id , 
                  .type = "sec-swift",
                  .function = bb.getParent(),
                  .bb_name = bb.getName().str(),
                  .loc = md::get_location(bb.getFirstNonPHI()),
                  .data_emitter = [=](YAML::Emitter& out) { 
                  out << YAML::Key << "data" << YAML::BeginMap;
                  out << YAML::Key << "cond" << YAML::Value << (true ? "true" : "false");
                  
                  out << YAML::Key << "cond_prepare" << YAML::Value << (prepare_with_if ? "true" : "false"); 
                  out << YAML::EndMap; 
                  }
            };

            mut.ccp_space.ccps.push_back(cp);
            Log().full("      created ccp " + id + ".");
      }
      for (Instruction &inst : bb){
            if(isa<BranchInst>(inst)){
                  BranchInst* branchInst = dyn_cast<BranchInst>(&inst);
                  auto location = md::get_location(branchInst);

                  IRBuilder<> Builder(branchInst);
                  if(branchInst->isUnconditional()){
                        if(mut_uncond){
                              Type *args_uncond_prepare[] = {int32Type, int32Type, int32PtrType};
                              auto funType_uncond_p =FunctionType::get(voidType, args_uncond_prepare, false);
                              auto uncond_prepare = mut.module->getOrInsertFunction("SECSWIFT_UNCOND_PREPARE", funType_uncond_p);
                              Value* v = branchInst->getOperand(0);
                              std::string bb_name1 = v->getName().str();
                              int idBB1 = get_id_name(*bb.getParent(),bb_name1);
                              Value *args3[] = {Builder.getInt32(id_block),Builder.getInt32(idBB1), RTS->getPointerOperand()};
                              Builder.CreateCall(uncond_prepare, args3);
                        }

                  } else if(branchInst->isConditional()){
                        Type *argsType2[] = {int32Type, int32Type, int32Type, int32Type,int32PtrType};
                        auto funType2 =FunctionType::get(voidType, argsType2, false);
                        auto cond_prepare = mut.module->getOrInsertFunction("SECSWIFT_COND_PREPARE", funType2);
                        if(!prepare_with_if){
                              cond_prepare = mut.module->getOrInsertFunction("SECSWIFT_COND_PREPARE_WITHOUT_IF", funType2);
                        }
                        Value* condition = Builder.CreateZExt(branchInst->getOperand(0),Type::getInt32Ty(mut.module->getContext()));
                        Value* v1 = branchInst->getOperand(2);
                        Value* v2 = branchInst->getOperand(1);
                        // id of block 1 :
                        std::string bb_name1 = v1->getName().str();
                        int idBB1 = get_id_name(*bb.getParent(),bb_name1);
                        // id of block 2 : 
                        std::string bb_name2 = v2->getName().str();
                        int idBB2 = get_id_name(*bb.getParent(),bb_name2);
                        Value *args2[] = {condition, Builder.getInt32(idBB1),Builder.getInt32(idBB2), Builder.getInt32(id_block),RTS->getPointerOperand()};
                        Builder.CreateCall(cond_prepare, args2);
                  }
            }
      }
      //errs() << bb;
      return true;
}
void lazart::tasks::sec_swift::create_special_function(){
      Log().debug("################### Start : creation of special functions ###################### ");
      auto int32Type = Type::getInt32Ty(mut.module->getContext());
      auto int32PtrType = Type::getInt32PtrTy(mut.module->getContext());
      auto voidType = Type::getVoidTy(mut.module->getContext());

      // Construct verify function :
      Type *args_verify[] = {int32Type, int32PtrType, int32PtrType};// #TODO : _LZ_trigger
      auto funType_verify = FunctionType::get(voidType, args_verify, false);
      auto verify_func = mut.module->getOrInsertFunction("SECSWIFT_VERIFY", funType_verify);
      SECSWIFT_VERIFY(cast<Function>(verify_func.getCallee()));

      // Construct prepare_uncond function :
      Type *args_uncond_prepare[] = {int32Type, int32Type, int32PtrType};
      auto funType_uncond_p =FunctionType::get(voidType, args_uncond_prepare, false);
      auto uncond_prepare = mut.module->getOrInsertFunction("SECSWIFT_UNCOND_PREPARE", funType_uncond_p);
      SECSWIFT_UNCOND_PREPARE(cast<Function>(uncond_prepare.getCallee()));
      
      // Construct prepare_cond function :
      Type *argsType2[] = {int32Type, int32Type, int32Type, int32Type,int32PtrType};
      auto funType2 =FunctionType::get(voidType, argsType2, false);
      auto cond_prepare = mut.module->getOrInsertFunction("SECSWIFT_COND_PREPARE", funType2);
      SECSWIFT_COND_PREPARE(cast<Function>(cond_prepare.getCallee()));

      // Construct prepare_cond_without_if function :
      auto cond_prepare_with_if = mut.module->getOrInsertFunction("SECSWIFT_COND_PREPARE_WITHOUT_IF", funType2);
      SECSWIFT_COND_PREPARE_WITHOUT_IF(cast<Function>(cond_prepare_with_if.getCallee()));
      Log().debug("################### End : creation of special functions ###################### ");
}
// Apply method :
bool lazart::tasks::sec_swift::apply()
{
      Log().debug("##################### BEGIN : SecSwift Contermeasure ###################");
      setIds(mut.module);
      create_special_function();
      for (auto curFref = mut.module->getFunctionList().begin(), 
            endFref = mut.module->getFunctionList().end(); 
            curFref != endFref; ++curFref) {  

            Function& function = *curFref;

            if(!should_apply(function))
                  continue;

            for (BasicBlock& bb : function)
            {
                  lazart::tasks::sec_swift::runOnBB(bb);
            }
      }
      if(mut_func){
            auto int32Type = Type::getInt32Ty(mut.module->getContext());
            auto int32PtrType = Type::getInt32PtrTy(mut.module->getContext());
            auto voidType = Type::getVoidTy(mut.module->getContext());
            Type *args_verify[] = {int32Type, int32PtrType, int32PtrType};// #TODO : _LZ_trigger
            auto funType_verify = FunctionType::get(voidType, args_verify, false);
            auto verify_func = mut.module->getOrInsertFunction("SECSWIFT_VERIFY", funType_verify);
            Type *argsType2[] = {int32Type, int32Type, int32Type, int32Type,int32PtrType};
            auto funType2 =FunctionType::get(voidType, argsType2, false);
            auto cond_prepare = mut.module->getOrInsertFunction("SECSWIFT_COND_PREPARE", funType2);

            auto cond_prepare_if = mut.module->getOrInsertFunction("SECSWIFT_COND_PREPARE_WITHOUT_IF", funType2);

            auto name = "ti_sf";
            auto fn = trigger_fct->getName().str();
            auto tiptr = std::unique_ptr<lazart::model::fault_model>(new model::test_inversion{name, {fn}, false});
            
            // mut.am.models_.push_back(std::move(tiptr));
            auto model = mut.am.add_fault_model(std::move(tiptr));
            Function* fct1 = cast<Function>(cond_prepare.getCallee());
            Function* fct2 = cast<Function>(verify_func.getCallee());
            Function* fct3 = cast<Function>(cond_prepare_if.getCallee());
            mut.am.add_function(fct1);
            mut.am.bind_model(model,fct1);
            mut.am.add_function(fct2);
            mut.am.bind_model(model,fct2);
            mut.am.add_function(fct3);
            mut.am.bind_model(model,fct3);

            // Log().info(mut.am.str()); // to print the attack model
      }
      // mut.am.fault_space_.insert(std::pair<llvm::Function*, std::vector<lazart::model::fault_model*>>(cast<Function>(cond_prepare.getCallee()), {ti}));
      // mut.am.fault_space_.insert(std::pair<llvm::Function*, std::vector<lazart::model::fault_model*>>(cast<Function>(verify_func.getCallee()), {ti}));
      Log().debug("##################### END : SecSwift Contermeasure ###################");
      return true;

}


void lazart::tasks::sec_swift::SECSWIFT_VERIFY(Function *F) {
      BasicBlock *BB2 = BasicBlock::Create(F->getContext(), "BB2", F);
      BasicBlock *BB1 = BasicBlock::Create(F->getContext(), "BB1", F, BB2);
      BasicBlock *BB0 = BasicBlock::Create(F->getContext(), "Verify_GSR_Secswift", F, BB1);

      Function::arg_iterator it = F->arg_begin();
      Value *ID_current_bb = &*it;
      ID_current_bb->setName("ID_current_bb");it++; // ID
      Value *GSR_arg = &*it;
      GSR_arg->setName("GSR_arg");it++; // GSR
      
      Value *RTS_arg = &*it;
      RTS_arg->setName("RTS_arg"); // RTS 
      
      IRBuilder<> Builder0(BB0);
      LoadInst *_GSR_ = Builder0.CreateLoad(GSR_arg);
      md::add_lz_var_name(_GSR_, "_LZ__ss_GSR");
      LoadInst *_RTS_ = Builder0.CreateLoad(RTS_arg);
      md::add_lz_var_name(_RTS_, "_LZ__ss_RTS");
      Value *_xor_ = Builder0.CreateXor(_GSR_, _RTS_);
      Builder0.CreateStore(_xor_, GSR_arg);
      Value *cmp = Builder0.CreateICmpNE(ID_current_bb, _xor_);
      Builder0.CreateCondBr(cmp, BB1, BB2);

      IRBuilder<> Builder1(BB1);
      std::string msg = "[CM]"; // #TODO ? ?
      std::string name = "CM";  // #TODO ? ?
      llvm::Value* str = Builder1.CreateGlobalStringPtr(msg, name);
      std::vector<llvm::Value*> args;
      args.push_back(str);
      Builder1.CreateCall(trigger_fct,args);
      Builder1.CreateBr(BB2);

      IRBuilder<> Builder2(BB2);
      Builder2.CreateRetVoid();
}
void lazart::tasks::sec_swift::SECSWIFT_COND_PREPARE_WITHOUT_IF(Function *F) {
      // Formule : RTS = ID_current ^ (condition * ID_true + (1-condition) * ID_false)

      Value *One = ConstantInt::get(Type::getInt32Ty(F->getContext()), 1);

      BasicBlock *RETURN = BasicBlock::Create(F->getContext(), "RETURN", F);
      BasicBlock *BB0 = BasicBlock::Create(F->getContext(), "UPDATE_RTS", F, RETURN);

      Function::arg_iterator it = F->arg_begin();
      Value *condition = &*it;
      condition->setName("condition");it++; // Condition
      Value *ID_true_bb = &*it;
      ID_true_bb->setName("ID_true_bb");it++; // ID_true
      Value *ID_false_bb = &*it;
      ID_false_bb->setName("ID_false_bb");it++;// ID_false
      Value *ID_current_bb = &*it;
      ID_current_bb->setName("ID_current_bb");it++; // GSR 
      Value *RTS_arg = &*it; RTS_arg->setName("RTS_arg");

      IRBuilder<> Builder0(BB0);
      // 1-condition
      Value *Sub = Builder0.CreateSub(One, condition);
      // condition * ID_true
      Value *Mul_left = Builder0.CreateMul(condition, ID_true_bb);
      // (1-condition) * ID_false
      Value *Mul_right = Builder0.CreateMul(Sub, ID_false_bb);
      // (condition * ID_true + (1-condition) * ID_false)
      Value *res = Builder0.CreateAdd(Mul_left,Mul_right);
      // ID_current ^ (condition * ID_true + (1-condition) * ID_false)
      Value *_xor_ = Builder0.CreateXor(ID_current_bb,res);
      Builder0.CreateStore(_xor_, RTS_arg);
      Builder0.CreateBr(RETURN);

      IRBuilder<> Builder2(RETURN);
      Builder2.CreateRetVoid();

}
void lazart::tasks::sec_swift::SECSWIFT_COND_PREPARE(Function *F) {
      BasicBlock *return_block = BasicBlock::Create(F->getContext(), "return_block", F);
      BasicBlock *BB2 = BasicBlock::Create(F->getContext(), "BB2", F, return_block);
      BasicBlock *BB1 = BasicBlock::Create(F->getContext(), "BB1", F, BB2);
      BasicBlock *BB0 = BasicBlock::Create(F->getContext(), "Prepare_RTS_if_Secswift", F, BB1);

      Function::arg_iterator it = F->arg_begin();
      Value *condition = &*it;
      condition->setName("condition");it++; // Condition
      Value *ID_true_bb = &*it;
      ID_true_bb->setName("ID_true_bb");it++; // ID_true
      Value *ID_false_bb = &*it;
      ID_false_bb->setName("ID_false_bb");it++;// ID_false
      Value *ID_current_bb = &*it;
      ID_current_bb->setName("ID_current_bb");it++; // GSR 
      Value *RTS_arg = &*it; RTS_arg->setName("RTS_arg");

      IRBuilder<> Builder0(BB0);
      Value *cmp = Builder0.CreateICmpEQ(condition, Builder0.getInt32(1));
      Builder0.CreateCondBr(cmp, BB1, BB2);

      IRBuilder<> Builder1(BB1);
      Value *_xor_ = Builder1.CreateXor(ID_true_bb, ID_current_bb);
      Builder1.CreateStore(_xor_, RTS_arg);
      Builder1.CreateBr(return_block);

      IRBuilder<> Builder2(BB2);
      Value *_xor__ = Builder2.CreateXor(ID_false_bb, ID_current_bb);
      Builder2.CreateStore(_xor__, RTS_arg);
      Builder2.CreateBr(return_block);

      IRBuilder<> Builder3(return_block);
      Builder3.CreateRetVoid();
}

void lazart::tasks::sec_swift::SECSWIFT_UNCOND_PREPARE(Function *F) {
      BasicBlock *BB = BasicBlock::Create(F->getContext(), "BB", F);
      Function::arg_iterator it = F->arg_begin();
      Value *ID_current_bb = &*it;
      ID_current_bb->setName("ID_current_block");it++;
      Value *IDnext_arg = &*it;
      IDnext_arg->setName("ID_next_block");it++;
      Value *RTS_arg = &*it;
      RTS_arg->setName("RTS_arg");

      IRBuilder<> builder(BB);
      Value *_xor_ = builder.CreateXor(ID_current_bb, IDnext_arg);
      builder.CreateStore(_xor_, RTS_arg);
      builder.CreateRetVoid();
}
