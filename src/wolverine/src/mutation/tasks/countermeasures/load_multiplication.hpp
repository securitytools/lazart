/**
 * @file load_multiplication.hpp
 * @author Etienne Boespflug (etienne.boespflug@gmail.com)
 * @brief This files defines the Load Multiplication countermeasure (LM).
 * @version 4.0
 * @note Currently not updated for Lazart 4. Do not work.
 */
#pragma once

#include "countermeasures.hpp"

#include "llvm/llvm_fwd.hpp"

namespace lazart {

    namespace tasks {

        /**
         * @brief The `load_multiplication` class corresponds to a Load Multiplication parameters, holding specific parameters.
         * 
         * Uses 'lm_point_id:depth' syntax for the `ccps` description field.
         */
        class load_multiplication : public weighted_countermeasure {
            public:              
                /**
                 * @brief Construct a new `weighted_countermeasure` object for the specified mutation with parameters.
                 * 
                 * @param mutation the context of the mutation. 
                 * @param global_depth the application depth (weight) used if no information is indicated by the user.
                 * @param on the list of functions on which the mutation should be applied, can include code string such as "__sym__" and "__mut__".
                 * @param ccps the list of countermeasure application points to be modified (if empty, all possible cm points are transformed).
                 * @param trigger_fct the name of detector triggering function to be used.
                 */
                load_multiplication(model::Mutation& mutation, int global_depth = 1, std::vector<std::string> on = {}, std::vector<std::string> ccps = {}, const std::string& trigger_fct = trigger_fct_default) : weighted_countermeasure(mutation, global_depth, on, ccps, trigger_fct) {}

                virtual bool apply() override;

                virtual std::string str() const override;

                /**
                 * @brief The `cm_point` struct holds information about a countermeasure application point for LM countermeasure.
                 * 
                 * It uses an unique identifier string for the cm point. Store debug information (currently removed by wolverine). 
                 */
                struct cm_point {
                    /// @brief The `load` instruction to be protected.
                    llvm::LoadInst* load;
                    /// @brief The source code location debug metadata.
                    md::source_loc loc;
                    /// @brief The string identifier of the application point.
                    std::string id;
                    /// @brief Debug info associated to the load instruction.
                    llvm::MDNode* dbg;
                };

            protected:
                /**
                 * @brief Apply the load multiplication scheme with the specified `depth` on the specified countermeasure application point (load instruction).
                 * 
                 * @param cp the countermeasure application point.
                 * @param id_base the base identifier for the ccp.
                 * @param depth the depth of application of LM.
                 * @return true if the program has been modified.
                 * @return false if the program hasn't been modified.
                 */
                bool apply_load(const cm_point& cp, const std::string& id_base, int depth);
        };
    }
}