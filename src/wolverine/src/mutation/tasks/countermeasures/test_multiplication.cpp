#include "mutation/tasks/countermeasures/test_multiplication.hpp"

#include "model/mutation.hpp"
#include "mutation/metadata/metadata.hpp"
#include "util/util.hpp"

#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Instruction.h"
#include "llvm/Support/raw_ostream.h"
#include <llvm/IR/DebugLoc.h>
#include <llvm/IR/DebugInfoMetadata.h>

std::string lazart::tasks::test_multiplication::str() const
{
    std::string ret = "TM(" + std::to_string(depth) + ", [" + util::join(on, ",") + "]";

    if(ccps.size() != 0) {
        ret += ", ccps=[" + util::join(on, ",") + "]";
    }  
    if(trigger_fct_str != tasks::trigger_fct_default) {
        ret += ", cm_fct=" + trigger_fct_str;
    }   
    return ret += ")";
}

bool lazart::tasks::test_multiplication::apply_branch(lazart::tasks::test_multiplication::cm_point data, llvm::BasicBlock*& target, const std::string& id_base, int depth, bool true_branch)
{
    auto& context = mut.module->getContext();
    auto insert_before = data.insert_before;
    auto source_bb_str = data.source->getName().str();

    for(int i = depth; i > 0; --i)
    {
        auto id = id_base + (i > 1 ? std::to_string(i) : "");
        auto cmBB = llvm::BasicBlock::Create(context, source_bb_str + ".tm.cm" + id_base + std::to_string(i), data.function, insert_before);
        // Todo set info ++ 

        // CM Block
        llvm::IRBuilder<> cmBuilder((cmBB));
        llvm::Value* cm_str = cmBuilder.CreateGlobalStringPtr(id, lazart::names::wolverine_prefix + std::to_string(data.id) + ".tm." + id_base + id);

        cmBuilder.CreateCall(trigger_fct, {cm_str})->setMetadata("dbg", data.dbg);
        cmBuilder.CreateBr(target)->setMetadata("dbg", data.dbg);

        // Test redundancy
        auto tmBB = llvm::BasicBlock::Create(context, source_bb_str + ".tm." + id_base, data.function, cmBB);
        llvm::IRBuilder<> target_builder((tmBB));
        if(true_branch) {
            auto red_br = target_builder.CreateCondBr(data.cond, target, cmBB);
            red_br->setMetadata("dbg", data.dbg);
            md::set_lz_cm_ip(red_br, id);
        }
        else {
            auto red_br = target_builder.CreateCondBr(data.cond, cmBB, target);
            red_br->setMetadata("dbg", data.dbg);
            md::set_lz_cm_ip(red_br, id);
        }

        target = tmBB;        
        insert_before = tmBB;

        // cm_space
        lazart::tasks::countermeasure_point cp = {
            .id = id,
            .type = "test-multiplication",
            .function = data.source->getParent(),
            .bb_name = data.source->getName().str(),
            .loc = data.loc,
            .data_emitter = [=](YAML::Emitter& out) { 
                out << YAML::Key << "data" << YAML::BeginMap;
                out << YAML::Key << "edge" << YAML::Value << (true_branch ? "true" : "false"); 
                out << YAML::Key << "depth" << YAML::Value << depth;
                out << YAML::EndMap; 
            }
        };

        mut.ccp_space.ccps.push_back(cp);
        Log().full("      created ccp " + id + ".");
    }

    return true;
}

bool lazart::tasks::test_multiplication::apply(cm_point data) {

    Log().info("    creating detectors " + std::to_string(data.id) + " in " + data.source->getName().str());
    auto source_bb_str = data.source->getName().str();

    auto true_id = std::to_string(data.id) + "T";
    auto false_id = std::to_string(data.id) + "F";

    
    auto targetT = cast<llvm::BasicBlock>(data.targetT);
    auto targetF = cast<llvm::BasicBlock>(data.targetF);



    apply_branch(data, targetT, true_id, depth_of(true_id), true);
    apply_branch(data, targetF, false_id, depth_of(false_id), false);
    
    data.source->getTerminator()->eraseFromParent();

    llvm::IRBuilder<> precBuilder(data.source);
    precBuilder.CreateCondBr(data.cond, targetT, targetF)->setMetadata("dbg", data.dbg);

    return true;
}

bool lazart::tasks::test_multiplication::apply()
{
    Log().info("tm: appling countermeasure: " + str() + ".");

    std::vector<cm_point> points{};
    bool modif{ false }; 
    uint32_t id{};

    for (auto curFref = mut.module->getFunctionList().begin(), 
              endFref = mut.module->getFunctionList().end(); 
              curFref != endFref; ++curFref) {  

        llvm::Function& function = *curFref;

        if(!should_apply(function))
            continue;

        bool last_used{ false }; // retrieve data.insert_before.
        for (llvm::BasicBlock& bb : function)
        {
            if(last_used)
            {
                if(points.size() != 0)
                    points.rbegin()->insert_before = &bb;
                last_used = false;
            }
            auto last = dyn_cast<llvm::BranchInst>(bb.getTerminator());
            if (last && last->isConditional()) {
                cm_point cp = {
                    .cond = last->getOperand(0),
                    .targetF = last->getOperand(1),
                    .targetT = last->getOperand(2),
                    .source = &bb,
                    .id = id++,
                    .function = &function,
                    .loc = md::get_location(last),
                    .dbg = last->getMetadata("dbg")
                };

                points.push_back(cp);
                last_used = true;
            }
        }
    }

    Log().full("  " + std::to_string(points.size()) + " points found.");
    for(auto& cp: points) {
        modif |= apply(cp);
    }
    
    return modif;
}
