/**
 * @file countermeasures.hpp
 * @author Etienne Boespflug (etienne.boespflug@gmail.com)
 * @brief This files defines the class `countermeasures` base class of all countermeasures application passes.
 * @version 4.0
 */

#pragma once

#include "llvm/llvm_fwd.hpp"
#include "mutation/metadata/metadata.hpp"

#include <string>
#include <vector>
#include <functional>

#include "yaml-cpp/yaml.h" // emitter

namespace lazart {
    // FWD
    namespace model {
        class Mutation;
    }

    namespace tasks {
        /// @brief Name of the default detector function in Lazart.
        constexpr auto trigger_fct_default = "_LZ__trigger_detector";
        /// @brief Name of the default non-blocking detector function in Lazart.
        constexpr auto trigger_fct_default_nb = "_LZ__trigger_detector";

        /**
         * @brief The `countermeasure_point` class holds information about a countermeasure application point in the program. 
         * 
         * Its unique identifier `id` is used to retrieve information about specific CM point in Lazart Python and can be used by the user to specify precise countermeasure application space through the `on` field in YAML attack model.
         */
        struct countermeasure_point {
            /// @brief String identifier for the countermeasure_point.
            std::string id;
            /// @brief Boolean determining if the CP is user-generated (instrumentation) or created by Wolverine.
            bool user_generated = false;
            /// @brief Countermeasure name.
            std::string type;
            /// @brief LLVM function of the CP.
            llvm::Function* function;
            /// @brief Name of the enclosing basic block.
            std::string bb_name;
            /// @brief Source location of the CP (matched to close instruction).
            md::source_loc loc;
            /// @brief Emitter for model-specific information.
            std::function<void(YAML::Emitter&)> data_emitter = [](YAML::Emitter& out) { out << YAML::Key << "data" << YAML::Value << YAML::Null;};
        };

        /**
         * @brief Returns true if the target block is a detector according to a specified detector list.
         * 
         * @param target the basic block to be checked.
         * @param protected_detectors_fct the list of name of detector functions.
         * @return true if the basic block is a detector.
         * @return false if the basic block is not a detector.
         */
        bool is_ccp(llvm::BasicBlock* target, const std::vector<std::string>& protected_detectors_fct);

        /**
         * @brief The countermeasure space corresponds to the set of the countermeasure application points added by Wolverine or from the original module.
         * 
         * The countermeasures space is updated during the countermeasure application and some IP generated during mutation could be linked with their corresponding cm_ip. 
         */
        struct countermeasures_space {
            std::vector<countermeasure_point> ccps{};
        };

        /**
         * @brief The `countermeasure` class is the base virtual class for all countermeasure type.
         */
        class countermeasure {
            public:
                /**
                 * @brief Construct a new `countermeasure` object for the specified mutation with parameters.
                 * 
                 * @param mutation the context of the mutation. 
                 * @param on_ the list of functions on which the mutation should be applied, can include code string such as "__sym__" and "__mut__".
                 * @param ccps_ the list of countermeasure application points to be modified (if empty, all possible cm points are transformed).
                 * @param trigger_fct_ the name of detector triggering function to be used.
                 */
                countermeasure(model::Mutation& mutation, const std::vector<std::string>& on_ = {}, std::vector<std::string> ccps_ = {}, const std::string& trigger_fct_ = trigger_fct_default);
                virtual ~countermeasure() noexcept = default;

                /**
                 * @brief Abstract function applying the countermeasures of the LLVM module.
                 * 
                 * This function is virtual and should be overriden by children classes.
                 * 
                 * @return true if the module was modified by the countermeasure.
                 * @return false if no modification was made.
                 */
                virtual bool apply() { return false; }

                /**
                 * @brief Return the string representation of the countermeasure, including parameters.
                 * 
                 * This function is virtual and should be overriden by children classes.
                 * 
                 * @return std::string the string representation of the countermeasure.
                 */
                virtual std::string str() const = 0;

            protected:
                /**
                 * @brief Return true if the countermeasure should be applied on the specified function, false otherwise. Use the member field `on` to check the application space and verify that no LLVM or Lazart internal are mutated. 
                 * 
                 * @param fun the function on which the countermeasure should apply.
                 * @return true if the countermeasures should be applied on this function. 
                 * @return false if the countermeasure is not applicable on the function.
                 */
                bool should_apply(llvm::Function& fun) const;

                /// @brief The mutation context.
                model::Mutation& mut;
                /// @brief The list of countermeasures application points to modify. If empty, all points are transformed.
                std::vector<std::string> ccps;
                /// @brief Name of the trigger function used (blocking or non-blocking detector usually).
                std::string trigger_fct_str;
                /// @brief Trigger function used by the countermeasure.
                llvm::Function* trigger_fct;
                /// @brief The list of function names on which the countermeasure will be applied. String code such as "__mut__" and "__all__" are usable.
                std::vector<std::string> on;                
        };

        /**
         * @brief The `weighted_countermeasure` class corresponds to a countermeasures that can use different protection level for each application point. Each IP protection is thus weighted by an integer. 
         */
        class weighted_countermeasure : public countermeasure {   
            public:         
                /**
                 * @brief Construct a new `weighted_countermeasure` object for the specified mutation with parameters.
                 * 
                 * @param mutation the context of the mutation. 
                 * @param global_depth the application depth (weight) used if not information is precised.
                 * @param on the list of functions on which the mutation should be applied, can include code string such as "__sym__" and "__mut__".
                 * @param ccps the list of countermeasure application points to be modified (if empty, all possible cm points are transformed).
                 * @param trigger_fct the name of detector triggering function to be used.
                 */
                weighted_countermeasure(model::Mutation& mutation, int global_depth = 1, std::vector<std::string> on = {}, std::vector<std::string> ccps = {}, const std::string& trigger_fct = trigger_fct_default) : countermeasure(mutation, on, ccps, trigger_fct),
                    depth{ global_depth } {}

                /**
                 * @brief Returns the depth (weight) of a specified countermeasures application point. If an application point 'id' is protected with depth 2, two application points are generated: 'id.1' and 'id.2'. 
                 * 
                 * @param ccp_id the countermeasure application point ID. 
                 * @return int the depth of the application point.
                 */
                int depth_of(const std::string& ccp_id) const;
                
                virtual std::string str() const = 0;

            protected:
                /// @brief Global depth used if no specific depth is specified for an application point.
                int depth;
        };
    }
}