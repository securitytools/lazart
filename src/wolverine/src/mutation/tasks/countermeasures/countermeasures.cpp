#include "countermeasures.hpp"

#include "util/util.hpp"
#include "core/wolverine.hpp"
#include "model/mutation.hpp"

#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Instruction.h"

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/predicate.hpp>

bool lazart::tasks::is_ccp(llvm::BasicBlock* target, const std::vector<std::string>& protected_detectors_fct)
{
    for(auto& inst : *target) {
        if (llvm::isa<llvm::CallInst>(inst)) {
            llvm::CallInst * callInst = llvm::dyn_cast<llvm::CallInst>(&inst);
            std::string fname;
            llvm::Function *cf;
            if((cf = callInst->getCalledFunction()))
            {
                fname = cf->getName().str();
            } 
            else if((cf = llvm::dyn_cast<llvm::Function>(callInst->getCalledValue()->stripPointerCasts())))
            {
                fname = cf->getName().str();
            }
            else {
                return false;
            }
            if(std::find(std::begin(protected_detectors_fct), std::end(protected_detectors_fct), fname) != std::end(protected_detectors_fct)) {
                return true;
            }
        }
    }
    return false;
}

lazart::tasks::countermeasure::countermeasure(model::Mutation& mutation, const std::vector<std::string>& on_, std::vector<std::string> ccps_, const std::string& trigger_fct_) : 
                    mut{ mutation },
                    ccps{ ccps_ },
                    trigger_fct_str{ trigger_fct_ },
                    on{ on_ }
{
    auto& context = mut.module->getContext();

    // Get cm function
    llvm::Type* args[] = {llvm::Type::getInt8PtrTy(context)};
    auto trigger_fct_ty = llvm::FunctionType::get(llvm::Type::getVoidTy(context), args, false); // More generic ? 
    auto cm_callee = mut.module->getOrInsertFunction(trigger_fct_str, trigger_fct_ty);
    trigger_fct = dyn_cast<llvm::Function>(cm_callee.getCallee());
}

bool lazart::tasks::countermeasure::should_apply(llvm::Function& function) const
{
    auto fname = function.getName().str();

    // Ignore internals.
    if(boost::starts_with(fname, names::lazart_prefix))
        return false;

    // Not in spec.
    if(!mut.am.has_task(&function, on))
        return false;

    return true;
}

int lazart::tasks::weighted_countermeasure::depth_of(const std::string& ccp_id) const
{
    for(const auto& ccp_str: ccps)
    {
        auto [ccp_id_str, ccp_depth_str] = util::split(ccp_str);
        if(ccp_id_str == ccp_id)
        {
            int ccp_depth = -1;
            try {
                ccp_depth = boost::lexical_cast<int>(ccp_depth_str);
            } catch(const boost::bad_lexical_cast& e) {
                Log().warning("invalid value for ccp " + ccp_id_str + " :" + e.what());
            }
            return ccp_depth;
        }
    }

    //Log().warning("unknown ccp '" + ccp_id + "'.");
    return depth;
}