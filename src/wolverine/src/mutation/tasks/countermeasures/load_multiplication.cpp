// TODO, not working for now.
#include "mutation/tasks/countermeasures/load_multiplication.hpp"

#include "model/mutation.hpp"

#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Instruction.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Support/raw_ostream.h"

#include <string>
#include "llvm/utils.hpp"

#include "util/util.hpp"

std::string lazart::tasks::load_multiplication::str() const
{
    std::string ret = "LM(" + std::to_string(depth) + ", [" + util::join(on, ",") + "]";

    if(ccps.size() != 0) {
        ret += ", ccps=[" + util::join(on, ",") + "]";
    }  
    if(trigger_fct_str != tasks::trigger_fct_default) {
        ret += ", cm_fct=" + trigger_fct_str;
    }   
    return ret += ")";
}


std::string basename(const std::string& str, const std::string& delim) {
    auto pos = str.find(delim);
    if(pos == std::string::npos)
        return str;
    return str.substr(0, pos);
}


bool lazart::tasks::load_multiplication::apply_load(const cm_point& cp, const std::string& id_base, int depth)
{
    auto& context = mut.module->getContext();
    auto* insert_point = cp.load->getNextNode();
    auto bb = cp.load->getParent();
    auto bb_bname = basename(bb->getName().str(), ".lm");
    
    // TODO ! 
    auto join_cm = false;
    auto full = false; 

    Log().full("insert point: " + xllvm::to_string(*insert_point));

    auto id = id_base + (depth > 1 ? std::to_string(depth) : "");

    if(insert_point == nullptr) {
        Log().warning("invalid insert point.");
        return false;
    }
    if(depth < 1) {
        Log().warning("invalid depth value: " + std::to_string(depth) + ".");
    }


    int local_depth = 1;
    //auto bb_tail = bb->SplitBlockAndInsertIfThenElse(icmp, insert_point, bb_cm, bb_cm);
    //auto bb_tail = llvm::SplitBlock(bb, insert_point);


    auto bb_cm = llvm::BasicBlock::Create(context, bb_bname + ".lm." + id + ".cm", bb->getParent(), nullptr);

    llvm::IRBuilder<> cmBuilder((bb_cm));
    llvm::Value* cm_str = cmBuilder.CreateGlobalStringPtr(id, lazart::names::wolverine_prefix + cp.id + ".lm." + id);

    cmBuilder.CreateCall(trigger_fct, {cm_str})->setMetadata("dbg", cp.dbg);

    /* TODO: 
 
    // Head    
    llvm::IRBuilder<> bbBuilder(bb);
    bbBuilder.SetInsertPoint(insert_point);
    std::vector<llvm::Instruction*> clones;
    for(auto i = 0; i < depth; i++)  {
        auto* cloned = cp.load->clone();
        cloned->setName("cloned_" + id);
        clones.push_back(cloned);
    }

    //cloned->setMetadata("dbg", cp.dbg);
    //cloned->insertBefore(insert_point);    
    
    auto effective_depth = depth;
    if(!full)
        effective_depth = 1;

    auto from = bb;
    std::vector<llvm::Instruction*> icmp_list;
    std::vector<llvm::BasicBlock*> tails;

    for(auto i = 0; i < depth; ++i) {

        auto icmp = bbBuilder.CreateICmpNE(cp.load, clones[i]);
        icmp_list.push_back(icmp);
        // TAIL
        auto bb_tail_term = llvm::SplitBlockAndInsertIfThen(icmp, insert_point, false, 
                                        nullptr, nullptr, nullptr, bb_cm);
        auto tail = insert_point->getParent();
        from = bb_tail_term;
        bb_tail_term->setName(bb_bname + ".lm." + id + "." + std::to_string(i));
        bb_cm->moveBefore(bb_tail_term);
    }*/
    //auto icmp = bbBuilder.CreateICmpNE(cp.load, cloned);
    // TAIL
    //auto bb_tail_term = llvm::SplitBlockAndInsertIfThen(icmp, insert_point, false,  nullptr, nullptr, nullptr, bb_cm);
    //if(bb_tail_term == nullptr)  {}
    /*auto bb_tail = insert_point->getParent();
        bb_tail->setName(bb_bname + ".lm." + id + ".1");
    bb_cm->moveBefore(bb_tail);*/

    //cmBuilder.CreateBr(bb_tail)->setMetadata("dbg", cp.dbg);
    
    Log().full("FUNCTION: " + xllvm::to_string(*(bb->getParent())));


    /*
    // cm_space
    lazart::tasks::countermeasure_point cp = {
        .id = id,
        .type = "load-multiplication",
        .function = data.source->getParent(),
        .bb_name = data.source->getName().str(),
        .loc = data.loc,
        .data_emitter = [=](YAML::Emitter& out) { 
            out << YAML::Key << "data" << YAML::BeginMap;
            out << YAML::Key << "join_cm" << YAML::Value << false; 
            out << YAML::Key << "full" << YAML::Value << false; // multiple tm ?  
            out << YAML::Key << "depth" << YAML::Value << depth;
            out << YAML::EndMap; 
        }
    };

    mut.ccp_space.ccps.push_back(cp);
    Log().full("      created ccp " + id + ".");*/
    
    return true;
    /*

    //
    // CHANGES 

    // Head
    
    llvm::IRBuilder<> bbBuilder(bb);
    bbBuilder->setInsertPoint(insert_point);
    //auto* cloned = cp.load->clone();
    //bb->getInstList().push_back(cloned);
    if(bb == nullptr)
        Log().warning("null...");
    auto icmp = bbBuilder.CreateICmpEQ(cp.load, cp.load/*cloned*/ //);

    // Split & Tail
    //auto bb_tail = bb->SplitBlockAndInsertIfThenElse(icmp, insert_point, bb_cm, bb_cm);
    //bb_tail->setName("lm." + id + "." + "tail");

    /*
    auto br_old = bb->getTerminator();

    if(br_old == nullptr)
    {
        Log().warning("Null br_old");
    }
    if(!dyn_cast<llvm::BranchInst>(br_old))
    {
        Log().warning("Expected a branch instruction for basic block " + bb->getName().str());
    }

    llvm::BranchInst *br_replacement = llvm::BranchInst::Create(bb_tail, bb_cm, icmp);

    br_replacement->insertAfter(br_old);
    br_old->replaceAllUsesWith(br_replacement);
    br_old->eraseFromParent();

    // CM Block

    // CM bb
    auto bb_cm = llvm::BasicBlock::Create(context, "lm." + id + "." + "cm", bb->getParent(), bb_tail);

        
    llvm::IRBuilder<> cmBuilder((bb_cm));
    llvm::Value* cm_str = cmBuilder.CreateGlobalStringPtr(id, lazart::names::wolverine_prefix + cp.id + ".lm." + id);

    std::vector<llvm::Value*> args;
    args.push_back(cm_str);

    cmBuilder.CreateCall(trigger_fct, args)->setMetadata("dbg", cp.dbg);
    cmBuilder.CreateBr(bb_tail)->setMetadata("dbg", cp.dbg);

    
   
    //br_replacement->getFunction()->dump();
 
    //auto br = bbBuilder.CreateCondBr(icmp, bb_tail, bb_cm);

    Log().full("FUNCTION: " + xllvm::to_string(*(bb->getParent())));

    // insert load 2 
    // Split bb 
    // Clone load 
    // Create CM BB
    // Branch origin => (cm, splitted)
    return true; // TODO*/
}

bool lazart::tasks::load_multiplication::apply()
{
    Log().info("lm: appling countermeasure: " + str() + ".");

    std::vector<cm_point> points{};
    bool modif{ false }; 
    uint32_t id_counter{};

    for (auto curFref = mut.module->getFunctionList().begin(), 
              endFref = mut.module->getFunctionList().end(); 
              curFref != endFref; ++curFref) {  

        llvm::Function& function = *curFref;

        if(!should_apply(function))
            continue;


        for (llvm::BasicBlock& bb: function) 
        {
            for (auto inst = bb.begin(), end = bb.end(); inst != end; inst++) {
                auto bInst = dyn_cast<llvm::LoadInst>(inst);
                if(bInst) // && something ?
                {
                    cm_point cp = {
                        .id = std::to_string(id_counter++),
                        .loc = md::get_location(bInst),
                        .load = bInst,
                        .dbg = bInst->getMetadata("dbg") 
                    };

                    points.push_back(cp);
                }
            }
        }
    }
    
    Log().full("lm: " + std::to_string(points.size()) + " application points found.");
    
    for(auto& cp: points) {
        auto id_base = cp.id;
        modif |= apply_load(cp, id_base, depth_of(id_base));
    }
    Log().full("lm applied.");
    
    return modif;
}
