/**
 * @file test_multiplication.hpp
 * @author Abderrahmane Bouguern
 * @brief This files defines the ST Microelectronics's SecSwift Control FLOW (SSCF).
 * @version 4.0
 */

#pragma once

#include "countermeasures.hpp"
#include <llvm/IR/Function.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Constants.h>

#include <llvm/IR/IRBuilder.h>
#include <cstdlib>

#include "llvm/Support/raw_ostream.h"

#include <set>

#include <iostream>
#include <typeinfo>
#include <utility>
#include <vector>
#include <string>
#include <set>
#include <limits>
#include <fstream> 
#include <bits/stdc++.h>
#include <sstream>
#include <iterator>


#include <map>
#include <string>
#include "mutation/instr/instrumentation.hpp"
namespace lazart {
      namespace tasks {
            class sec_swift : public countermeasure {
                  public:
                        sec_swift(model::Mutation& mutation,int mut_functions = 1, int mut_uncond_br = 1, int prepare_if = 1, std::vector<std::string> on = {}, std::vector<std::string> ccps = {}, const std::string& trigger_fct = trigger_fct_default): countermeasure(mutation, on, ccps, trigger_fct){
                               mut_func = mut_functions;
                               mut_uncond = mut_uncond_br;
                               prepare_with_if = prepare_if;
                         }
                        virtual bool apply() override;
                        virtual std::string str() const override;
                  // private:
                  //       Function* SECSWIFT_VERIFY_F;
                  //       Function* SECSWIFT_COND_PREPARE_F;
                  protected:
                        /** Structures */
                        std::map<std::string, std::vector<std::string>> blocks = {};
                        llvm::StoreInst *GSR;
                        llvm::StoreInst *RTS;
                        void setIds(llvm::Module *module);
                        std::vector<std::string> get_list_bb(llvm::Function& f);
                        int get_id(llvm::Function& f,llvm::BasicBlock& bb);
                        int get_id_name(llvm::Function& f,std::string bb_name);
                        // void affiche_vector(std::vector<std::string> vect);
                        // void affiche_map(std::map<std::string, std::vector<std::string>> blocks);
                        // void affiche_all_blocksIds(llvm::Module *module,std::map<std::string, std::vector<std::string>> blocks);
                        /** Is last bb **/
                        bool is_last_bb(llvm::BasicBlock& bb);
                        bool is_first_bb(llvm::BasicBlock& bb);
                        bool is_last_conditional(llvm::BasicBlock &bb);
                        bool runOnBB(llvm::BasicBlock& bb);
                        void create_special_function();
                        void SECSWIFT_VERIFY(llvm::Function *F);
                        void SECSWIFT_COND_PREPARE_WITHOUT_IF(llvm::Function *F);
                        void SECSWIFT_COND_PREPARE(llvm::Function *F);
                        void SECSWIFT_UNCOND_PREPARE(llvm::Function *F);
                        int mut_func;
                        int mut_uncond;
                        int prepare_with_if;
            };
      }
}