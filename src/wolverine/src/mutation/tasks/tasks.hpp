/**
 * @file tasks.hpp
 * @author Etienne Boespflug (etienne.boespflug@gmail.com)
 * @brief This file contains definitions related to preprocessing and postprocessing tasks in Wolverine.
 * @version 4.0
 * 
 * Include most transformations that are not part of the main mutation loop.
 */
#pragma once

#include <llvm/IR/Function.h>
#include <llvm/Pass.h>

#include "llvm/llvm_fwd.hpp"

namespace lazart {
    /**
     * @brief This enum enumerates the preprocessing tasks of Wolverine, expected countermeasures application ones.
     * 
     * @todo Other solution, see lazart::tasks.
     */
    enum class TaskType
    {
        /// @brief Task adding traces to control flow for trace replay.
        AddTrace,
        /// @brief Task renaming the unamed basic blocks with numbered identifiers.
        RenameBB
    };

    // fwd
    namespace model {
        class Mutation;
    }
    
    /**
     * @brief The `lazart::tasks` namespace contains definition of function for preprocessing tasks ('rename bb' and 'add trace'), countermeasures applications, and several other transformation not related to the main mutation loop.
     * 
     * @todo update to a list of preprocess & post-process tasks with dict parameters in YAML.
     */
    namespace tasks {
        /**
         * @brief Apply the basic block renaming task for the specified `mutation` on the specified basic block.
         * 
         * The current basic block counter `bb_id` is modified if the basic block is renamed. Only unamed basic blocks are renamed.
         * 
         * @param mutation the context of the mutation.
         * @param basic_block the basic block to be renamed.
         * @param bb_id the current renamed basic blocks counter.
         * @return true if the basic block was renamed.
         * @return false if the basic block wasn't renamed.
         */
        bool rename_basic_block(model::Mutation& mutation, llvm::BasicBlock& basic_block, int& bb_id);

        /**
         * @brief Apply the trace addition task for the specified `mutation` on the specified basic block.
         * 
         * @param mutation the context of the mutation.
         * @param basic_block the basic block to be renamed.
         * @return true if the basic block was modified by the pass.
         * @return false if no modification has been done.
         */
        bool add_trace(model::Mutation& mutation, llvm::BasicBlock& basic_block);

        /**
         * @brief Remove unused functions from the LLVM module.
         * 
         * @param mutation the context of the mutation.
         */
        void remove_unused_functions(lazart::model::Mutation& mutation);

        /**
         * @brief Apply all preprocessing tasks on the specified `mutation`.
         * 
         * @param mutation the context of the mutation.
         * @return true if the module has been modified by the preprocessing tasks.
         * @return false if no modification has been done.
         */
        bool preprocess(lazart::model::Mutation& mutation);

        /**
         * @brief Apply all preprocessing tasks on the specified basic block.
         * 
         * @param mutation the context of the mutation.
         * @param basic_block the basic block to be preprocessed.
         * @param bb_id the current basic block identifier for the current 
         * @return true if the basic block was modified.
         * @return false if no modification has been done.
         * 
         * @note `bb_id` should be a tasks class member.
         */
        bool preprocess_bb(lazart::model::Mutation& mutation, llvm::BasicBlock& basic_block, int& bb_id);
        
        /**
         * @brief Apply inlining of mutation functions calls inside the module.
         * 
         * @param mutation the context of the mutation.
         */
        void inline_mutations(lazart::model::Mutation& mutation);
    }
}