#include "tasks.hpp"

#include "model/mutation.hpp"
#include "core/wolverine.hpp"
#include "llvm/utils.hpp"
#include "export/export.hpp"
#include "util/util.hpp"
#include "mutation/instr/instrumentation.hpp"

#include <iostream>
#include <sstream>

#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Module.h>
#include <llvm/Transforms/Utils/BasicBlockUtils.h>
#include "llvm/Support/raw_ostream.h"

#include <boost/algorithm/string/predicate.hpp>



#include <llvm/IR/DebugInfoMetadata.h>
#include <llvm/IR/DebugLoc.h>
#include <llvm/IR/Metadata.h>
#include <llvm/Transforms/Utils/Local.h> // replaceAllDbgUses
#include <llvm/Transforms/Utils/Cloning.h>

constexpr auto field = lazart::fields::preprocess;

bool lazart::tasks::rename_basic_block(lazart::model::Mutation&, llvm::BasicBlock& basic_block, int& bb_id)
{
    // Handle _LZ__RENAME_BB
    for (llvm::Instruction &inst : basic_block)
    {   
        if (llvm::isa<llvm::CallInst>(inst))
        {
            llvm::CallInst *callInst = llvm::dyn_cast<llvm::CallInst>(&inst);
            if (llvm::Function *calledFunction = callInst->getCalledFunction())
            {
                if(calledFunction->getName().str() == lazart::mutation::instrumentation_functions::rename_bb) {
                    std::string name = lazart::xllvm::get_string_literal_arg(callInst);
                    basic_block.setName(name.c_str());
                    inst.eraseFromParent();
                    Log().yellow().debug("             renamed " + basic_block.getName().str() + " to " + name.c_str() + ".", field);
                    return true;
                }
            }
        }
    }
    if (!basic_block.hasName()) 
    {
        basic_block.setName("bb" + std::to_string(bb_id++));
        Log().yellow().debug("             renamed " + basic_block.getName().str() + " to " + std::to_string(bb_id)  + ".", field);
        return true;
    }
    return false;
}

void lazart::tasks::remove_unused_functions(lazart::model::Mutation& mutation){        
    auto get_fct = [&](const std::string& name) -> llvm::Function* {
        for (auto curFref = mutation.module->getFunctionList().begin(), 
            endFref = mutation.module->getFunctionList().end(); 
            curFref != endFref; ++curFref) {
                if(curFref->getName().str() == name)
                    return &(*curFref);
        }
        return nullptr;
    };

    auto fun = get_fct("main");
    if(!fun) {
        Log().critical("cannot find function main.");
        return;
    }

    // Only KLEE / Lazart primitives & check for predicate pointers
        
    auto set = lazart::xllvm::callees(fun);

    std::set<llvm::Function*> to_remove{};
    for(auto& function : mutation.module->functions()) {
        auto fname = function.getName().str();
        if(fname != "main" && !boost::starts_with(fname, names::llvm_prefix)){
            if(boost::starts_with(fname, names::lazart_prefix)) {
                if(!set.count(&function)) {
                    to_remove.insert(&function);
                }
            }
        }
    }

    // Verify possible Lazart's standard predicates being called and passed by function pointer. Fault space is tested here.
    for(auto& ip: mutation.fault_space.faults) {
        if(ip.type == lazart::model::fault_type::DataLoad) {
            
        }
    }

    for(auto function: to_remove) {
        function->eraseFromParent();
    }
}

// TODO: add full trace methods
bool lazart::tasks::add_trace(lazart::model::Mutation& mutation, llvm::BasicBlock& bb)
{
    // Creating trace
    std::string msg = "[TRACE] " + bb.getName().str() + "\n"; // String content
    std::string name = names::wolverine_prefix + "trace_str_" + bb.getParent()->getName().str() + "_" + bb.getName().str(); // String name

    auto& context = mutation.context;
    llvm::IRBuilder<> builder(mutation.module->getContext());

    if (bb.empty())
        builder.SetInsertPoint(&bb);
    else
        builder.SetInsertPoint(bb.getFirstNonPHI());

    std::vector<llvm::Type*> printf_args({llvm::Type::getInt8PtrTy(context)});
    auto callee = mutation.module->getOrInsertFunction("printf", llvm::FunctionType::get(llvm::Type::getInt8Ty(context), printf_args, true));
    auto fun = dyn_cast<llvm::Constant>(callee.getCallee());

    llvm::Value* str = builder.CreateGlobalStringPtr(msg, name);

    builder.CreateCall(fun, {str}, names::wolverine_prefix + "trace_call");

    return true;
}

bool lazart::tasks::preprocess(lazart::model::Mutation& mutation)
{   
    Log().info("preprocessing starting.");
    bool modif = false;
    int bb_count{};   

    for (auto curFref = mutation.module->getFunctionList().begin(), 
              endFref = mutation.module->getFunctionList().end(); 
              curFref != endFref; ++curFref) {  

        llvm::Function& function = *curFref;
        auto fname = function.getName().str();


        if(boost::starts_with(fname, names::lazart_prefix))
        {
            continue;
        }

        Log().yellow().info("   preprocessing function " + function.getName().str() + ".", field);
        for (llvm::BasicBlock& bb : function)
        {
            modif = preprocess_bb(mutation, bb, bb_count) || modif;
        }

    }
    
    // Check preprocessed module.
    auto [valid, err] = xllvm::verify_module(mutation.module);
    if(!valid)
        Log().warning("invalid 'preprocessed' module.");
    if(!err)
        Log().warning("debug information are corrupted for 'preprocessed' module.");

    if(!results::save_module(lazart::paths::out_preprocessing, mutation))
        Log().warning("cannot save 'preprocessed' module");

    // Cms
    for(auto& cm: mutation.am.countermeasures) {
        modif |= cm->apply();
    }
    
    Log().info("preprocessing ended. " + Log().counter.str() + " generated.");

    return modif;
}


bool lazart::tasks::preprocess_bb(lazart::model::Mutation& mutation, llvm::BasicBlock& basic_block, int& bb_id)
{
    bool modif = false;

    // Preprocessing tasks.
    bool apply_rename_bb = mutation.am.has_task(basic_block, TaskType::RenameBB);
    
    if(apply_rename_bb) {
        Log().yellow().info("       - rename_bb on " + basic_block.getName().str() + ": " + (apply_rename_bb ? "applied" : "n/a."), field);
        modif = tasks::rename_basic_block(mutation, basic_block, bb_id) || modif;
    }
    bool apply_add_trace = mutation.am.has_task(basic_block, TaskType::AddTrace);
    
    if(apply_add_trace) {
        Log().yellow().info("       - add_trace on " + basic_block.getName().str() + ": " + (apply_add_trace ? "applied" : "n/a."), field);
        modif = tasks::add_trace(mutation, basic_block) || modif;
    }

    return modif;
}

void lazart::tasks::inline_mutations(lazart::model::Mutation& mutation) 
{
    constexpr auto field = lazart::fields::postprocess;
    
    std::vector<llvm::CallInst*> list;

    for (auto curFref = mutation.module->getFunctionList().begin(), 
              endFref = mutation.module->getFunctionList().end(); 
              curFref != endFref; ++curFref) {  

        llvm::Function& function = *curFref;
        auto fname = function.getName().str();

        if(boost::starts_with(fname, lazart::names::lazart_prefix))
            continue;

        if(boost::starts_with(fname, lazart::names::llvm_prefix))
            continue;

        // TODO check __mut__ only

        for (llvm::BasicBlock& basic_block : function)
        {
            for (auto inst = basic_block.begin(), end = basic_block.end(); inst != end; inst++) 
            {                
                if (llvm::CallInst* call = llvm::dyn_cast<llvm::CallInst>(&*inst))
                {
                    auto called_fct_name = lazart::xllvm::get_called_fct_name(call);
                    if(boost::starts_with(called_fct_name, lazart::names::mutation_fct_prefix))
                    {                  
                        list.push_back(call);

                    }
                }
            }
        }
    }

    Log().debug("inlining " + std::to_string(list.size()) + " calls.");
    for(auto call: list) {
        llvm::InlineFunctionInfo ifi;
        llvm::InlineFunction(call, ifi);
    }

}
