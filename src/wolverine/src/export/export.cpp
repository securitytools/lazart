#include "export.hpp"

#include "mutation/fault_space/fault_space.hpp"


#include "model/fault_models/fault_models.hpp"

#include "mutation/tasks/countermeasures/countermeasures.hpp"

#include <fstream>

#include "yaml-cpp/yaml.h"

#include "util/util.hpp"

#include "llvm/IR/Function.h"

#include <fcntl.h>

#include "model/mutation.hpp"   

#include <llvm/Support/MemoryBuffer.h>
#include <llvm/Bitcode/BitcodeReader.h>
#include <llvm/Bitcode/BitcodeWriter.h>
#include <llvm/Support/MemoryBuffer.h>

#include "util/util.hpp"

bool lazart::results::save_module(const std::string& filename, const model::Mutation& mut)
{
    // TODO: ugly & check valid
    int out_fd = open(filename.c_str(), O_WRONLY | O_TRUNC | O_CREAT, 0644);
    llvm::raw_ostream* out = new llvm::raw_fd_ostream(out_fd, true);
    if(mut.module == nullptr)
        Log().warning("null module");
    if(out == nullptr) {
        Log().warning("cannot open file descriptor.");
        return false;
    }
    llvm::WriteBitcodeToFile(*mut.module, *out);
    out->flush();
    delete out;
    return true;
}


bool lazart::results::save_module_unsafe(const std::string& filename, const model::Mutation& mut)
{
    // TODO check valid
    std::error_code EC;
    llvm::raw_fd_ostream OS(filename, EC, llvm::sys::fs::F_None);
    llvm::WriteBitcodeToFile(*mut.module, OS);
    OS.flush();
    return true;
}


bool lazart::results::export_fault_space(const std::string& filename, const mutation::fault_space& fs)
{
    // Main
    YAML::Emitter out;
    out.SetIndent(4);
    //out.SetMapStyle(YAML::Flow);


    out << YAML::BeginSeq;
    for(const auto& fault: fs.faults)
    {
        out << YAML::BeginMap;
        out << YAML::Key << "id" << YAML::Value << fault.ip_id;
        out << YAML::Key << "num" << YAML::Value << fault.ip_num;
        out << YAML::Key << "type" << YAML::Value << model::fault_type_str(fault.type);
        out << YAML::Key << "user_generated" << YAML::Value << fault.user_generated;
        if(fault.det_id == "") 
            out << YAML::Key << "detector_id" << YAML::Value << YAML::Null;
        else
            out << YAML::Key << "detector_id" << YAML::Value << fault.det_id;
        fault.data_emitter(out);
        //out << YAML::Key << "data" << YAML::Value << fault.args;

        //out << YAML::Key << "test" << YAML::Value << 

        out << YAML::Key << "loc" << YAML::BeginMap;
        out << YAML::Key << "function" << YAML::Value << fault.function->getName().str();
        out << YAML::Key << "basic_block" << YAML::Value << fault.bb_name;
        out << YAML::Key << "file" << YAML::Value << fault.loc.file;
        out << YAML::Key << "line" << YAML::Value << fault.loc.line;
        out << YAML::Key << "column" << YAML::Value << fault.loc.col;
        out << YAML::EndMap; 

        out << YAML::EndMap;        
    }
    out << YAML::EndSeq;

    if(!out.good())
    {
        Log().critical("An error occurred when trying to create the IP list.");
        return false;
    }

    std::ofstream f(filename);
    f << out.c_str();

    return true;
}

bool lazart::results::export_cm_space(const std::string& filename, const tasks::countermeasures_space& cms)
{
    // Main
    YAML::Emitter out;
    out.SetIndent(4);


    out << YAML::BeginSeq;
    for(const auto& ccp: cms.ccps)
    {
        out << YAML::BeginMap;
        out << YAML::Key << "id" << YAML::Value << ccp.id;
        out << YAML::Key << "type" << YAML::Value << ccp.type;
        out << YAML::Key << "user_generated" << YAML::Value << ccp.user_generated;
        ccp.data_emitter(out);

        out << YAML::Key << "loc" << YAML::BeginMap;
        out << YAML::Key << "function" << YAML::Value << ccp.function->getName().str();
        out << YAML::Key << "basic_block" << YAML::Value << ccp.bb_name;
        out << YAML::Key << "file" << YAML::Value << ccp.loc.file;
        out << YAML::Key << "line" << YAML::Value << ccp.loc.line;
        out << YAML::Key << "column" << YAML::Value << ccp.loc.col;
        out << YAML::EndMap; 

        out << YAML::EndMap;        
    }
    out << YAML::EndSeq;

    if(!out.good())
    {
        Log().critical("An error occurred when trying to create the CCP list.");
        return false;
    }

    std::ofstream f(filename);
    f << out.c_str();

    return true;
}
