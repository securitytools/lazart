#include "util/log.hpp"

using namespace lazart::util::term_modifiers::controls;
using namespace lazart::util::term_modifiers::colors;

lazart::util::Log::Log(VerbosityLevel level, std::ostream& os) : _os{os},
                                                                 _level{level},
                                                                 _log_file{ nullptr }
{}


lazart::util::Log::~Log()
{
    close_log_file();
}

void lazart::util::Log::open_log_file(const std::string& path)
{
    close_log_file();
    _log_file.open(path, std::ofstream::out | std::ofstream::app);
}

void lazart::util::Log::close_log_file()
{
    if(_log_file.is_open())
        _log_file.close();
}

void lazart::util::Log::enable_field(const std::string& field, bool enabled)
{
    if(enabled)
        _enabled_fields.insert(field);
    else {
        _enabled_fields.erase(field);
    }
}

void lazart::util::Log::fraw(const std::string& msg) 
{
    if(_log_file.is_open())
        _log_file << msg << "\n";
}

void lazart::util::Log::raw(const std::string& msg, VerbosityLevel minimumLevel) {
    if (_level >= minimumLevel)
        _os << msg << '\n';
}

void lazart::util::Log::critical(const std::string& msg, const std::string& field) {
    fraw("[E] " + msg);
    counter.error_count++;
    if (_level >= VerbosityLevel::Error && (field == "" || _enabled_fields.count(field)))
        _os << term_modifiers::colors::red << "error: " <<  term_modifiers::reset << msg << '\n';
}

void lazart::util::Log::warning(const std::string& msg, const std::string& field) {
    if(w_error) {
        critical(msg, field);
    } else {
        fraw("[W] " + msg);
        counter.warning_count++;
        if (_level >= VerbosityLevel::Warning && (field == "" || _enabled_fields.count(field)))
            _os << term_modifiers::brown << "warning: " <<  term_modifiers::reset << msg << '\n';        
    }
}

void lazart::util::Log::info(const std::string& msg, const std::string& field) {
    fraw("[I] " + msg);
    if (_level >= VerbosityLevel::Info && (field == "" || _enabled_fields.count(field)))
        _os << msg << '\n';
    _os << term_modifiers::reset;
}

void lazart::util::Log::verbose(const std::string& msg, const std::string& field) {
    fraw("[V] " + msg);
    if (_level >= VerbosityLevel::Verbose && (field == "" || _enabled_fields.count(field)))
        _os << msg << '\n';
    _os << term_modifiers::reset;
}

void lazart::util::Log::debug(const std::string& msg, const std::string& field) {
    fraw("[D] " + msg);
    if (_level >= VerbosityLevel::Debug && (field == "" || _enabled_fields.count(field)))
        _os << msg << '\n';
    _os << term_modifiers::reset;
}

void lazart::util::Log::full(const std::string& msg, const std::string& field) {
    fraw("[A] " + msg);
    if (_level >= VerbosityLevel::Full && (field == "" || _enabled_fields.count(field)))
        _os << msg << '\n';
    _os << term_modifiers::reset;
}