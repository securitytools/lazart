/**
 * @file log.hpp
 * @author Etienne Boespflug (etienne.boespflug@gmail.com)
 * @brief Definition of several utility functions for Lazart.
 * @version 3.0
 * 
 * This file contains definitions of Lazart's utility functions.
 */
#pragma once

#ifndef LAZART_WOLVERINE_UTIL_LOG_HPP
#define LAZART_WOLVERINE_UTIL_LOG_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <set>
#include <optional>

#include "error_counter.hpp"

namespace lazart {
namespace util {  
    
/**
 * @brief The namespace term_modifiers contains definitions for console color and style modifiers and corresponding stream operator functions.
 */
namespace term_modifiers {
/**
 * @brief The CLSCode enum enumerates codes for console manipulation instructions.
 * 
 * **UNIX only**.
 */
enum class CLSCode {
    RESET = 0,
    BOLD = 1,
    DARK = 2,
    ITALIC = 3,
    UNDERLINE = 4,
    BLINK = 5,
    INVERSE = 7,
    BOLD_OFF = 24,
    DARK_OFF = 22,
    ITALIC_OFF = 23,
    UNDERLINE_OFF = 21,
    BLINK_OFF = 25,
    INVERSE_OFF = 27,

    FG_BLACK = 30,
    FG_RED = 31,
    FG_GREEN = 32,
    FG_YELLOW = 33,
    FG_BLUE = 34,
    FG_MAGENTA = 35,
    FG_CYAN = 36,
    FG_WHITE = 37,
    FG_DEFAULT = 39,

    BG_BLACK = 40,
    BG_RED = 41,
    BG_GREEN = 42,
    BG_YELLOW = 43,
    BG_BLUE = 44,
    BG_MAGENTA = 45,
    BG_CYAN = 46,
    BG_WHITE = 47,
    BG_DEFAULT = 49
};

/**
 * @brief A CLSModifier encapsulate a CLSCode value and overload stream input operator (<<) to add the corresponding UNIX console code to the stream depending on the modifier code.
 * 
 */
class CLSModifier {
    CLSCode _cc;

  public:
    CLSModifier(CLSCode cc) : _cc(cc) {}

    friend std::ostream& operator<<(std::ostream& os, const CLSModifier& mod) {
        return os << "\033[" << static_cast<int>(mod._cc) << "m";
    }
}; 

/**
 * @brief The controls inline namespace contains stream operator overloads for control CLSCode. 
 */
inline namespace controls {

/**
 * @brief Resets all modifiers.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& reset(std::ostream& os) { return os << CLSModifier(CLSCode::RESET); }

/**
 * @brief Set the future characters to bold.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& bold(std::ostream& os) { return os << CLSModifier(CLSCode::BOLD); }

/**
 * @brief Set the future characters as dark, allowing to obtains different colors.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& dark(std::ostream& os) { return os << CLSModifier(CLSCode::DARK); }

/**
 * @brief Set the future characters as italic.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& italic(std::ostream& os) { return os << CLSModifier(CLSCode::ITALIC); }

/**
 * @brief Set the future characters as underlined.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& underline(std::ostream& os) { return os << CLSModifier(CLSCode::UNDERLINE); }

/**
 * @brief Set the future characters as blinking.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& blink(std::ostream& os) { return os << CLSModifier(CLSCode::BLINK); }
/**
 * @brief Enable the inverse modifier.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& inverse(std::ostream& os) { return os << CLSModifier(CLSCode::INVERSE); }
/**
 * @brief Disable the bold modifier.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& bold_reset(std::ostream& os) { return os << CLSModifier(CLSCode::BOLD_OFF); }
/**
 * @brief Disable the dark modifier.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */

inline std::ostream& dark_reset(std::ostream& os) { return os << CLSModifier(CLSCode::DARK_OFF); }
/**
 * @brief Disable the italic modifier.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& italic_reset(std::ostream& os) { return os << CLSModifier(CLSCode::ITALIC_OFF); }
/**
 * @brief Disable the underline modifier.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& underline_reset(std::ostream& os) { return os << CLSModifier(CLSCode::UNDERLINE_OFF); }
/**
 * @brief Disable the blink modifier.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& blink_reset(std::ostream& os) { return os << CLSModifier(CLSCode::BLINK_OFF); }
/**
 * @brief Disable the inverse modifier.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& inverse_reset(std::ostream& os) { return os << CLSModifier(CLSCode::INVERSE_OFF); }
} // namespace controls


/**
 * @brief The colors inline namespace contains stream operator overloads to control console color (character and background). 
 */
inline namespace colors {
/**
 * @brief Set character color to black.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& black(std::ostream& os) { return os << CLSModifier(CLSCode::FG_BLACK) << bold_reset; }

/**
 * @brief Set character color to dark gray.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& dark_gray(std::ostream& os) { return os << CLSModifier(CLSCode::FG_BLACK) << bold; }

/**
 * @brief Set character color to red.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& red(std::ostream& os) { return os << CLSModifier(CLSCode::FG_RED) << bold_reset; }

/**
 * @brief Set character color to rose.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& rose(std::ostream& os) { return os << CLSModifier(CLSCode::FG_RED) << bold; }

/**
 * @brief Set character color to green.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& green(std::ostream& os) { return os << CLSModifier(CLSCode::FG_GREEN) << bold_reset; }

/**
 * @brief Set character color to light green.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& light_green(std::ostream& os) { return os << CLSModifier(CLSCode::FG_GREEN) << bold; }

/**
 * @brief Set character color to brown.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& brown(std::ostream& os) { return os << CLSModifier(CLSCode::FG_YELLOW) << bold_reset; }

/**
 * @brief Set character color to yellow.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& yellow(std::ostream& os) { return os << CLSModifier(CLSCode::FG_YELLOW) << bold; }

/**
 * @brief Set character color to blue.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& blue(std::ostream& os) { return os << CLSModifier(CLSCode::FG_BLUE) << bold_reset; }

/**
 * @brief Set character color to light blue.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& light_blue(std::ostream& os) { return os << CLSModifier(CLSCode::FG_BLUE) << bold; }

/**
 * @brief Set character color to magenta.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& magenta(std::ostream& os) { return os << CLSModifier(CLSCode::FG_MAGENTA) << bold_reset; }

/**
 * @brief Set character color to purple.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& purple(std::ostream& os) { return os << CLSModifier(CLSCode::FG_MAGENTA) << bold; }

/**
 * @brief Set character color to cyan.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& cyan(std::ostream& os) { return os << CLSModifier(CLSCode::FG_CYAN) << bold_reset; }

/**
 * @brief Set character color to light cyan.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& light_cyan(std::ostream& os) { return os << CLSModifier(CLSCode::FG_CYAN) << bold; }

/**
 * @brief Set character color to white.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& white(std::ostream& os) { return os << CLSModifier(CLSCode::FG_WHITE) << bold_reset; }

/**
 * @brief Set character color to light gray.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& light_gray(std::ostream& os) { return os << CLSModifier(CLSCode::FG_WHITE) << bold; }

/**
 * @brief Reset character color.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& fg_reset(std::ostream& os) { return os << CLSModifier(CLSCode::FG_DEFAULT) << bold_reset; }

inline std::ostream& bblack(std::ostream& os) { return os << CLSModifier(CLSCode::BG_BLACK); }

/**
 * @brief Set character color to red.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& bred(std::ostream& os) { return os << CLSModifier(CLSCode::BG_RED); }

/**
 * @brief Set character color to green.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& bgreen(std::ostream& os) { return os << CLSModifier(CLSCode::BG_GREEN); }

/**
 * @brief Set character color to yellow.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& byellow(std::ostream& os) { return os << CLSModifier(CLSCode::BG_YELLOW); }

/**
 * @brief Set character color to blue.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& bblue(std::ostream& os) { return os << CLSModifier(CLSCode::BG_BLUE); }

/**
 * @brief Set character color to magenta.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& bmagenta(std::ostream& os) { return os << CLSModifier(CLSCode::BG_MAGENTA); }

/**
 * @brief Set character color to cyan.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& bcyan(std::ostream& os) { return os << CLSModifier(CLSCode::BG_CYAN); }

/**
 * @brief Set character color to white.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& bwhite(std::ostream& os) { return os << CLSModifier(CLSCode::BG_WHITE); }


/**
 * @brief Reset background color.
 * 
 * @param os output stream.
 * @return std::ostream& the modified output stream.
 */
inline std::ostream& bg_reset(std::ostream& os) { return os << CLSModifier(CLSCode::BG_DEFAULT); }
} // namespace colors
} // namespace term_modifiers 

/**
 * @brief This enum enumerate the different verbosity level used in Wolverine, allowing to specify the level of output of the tool.
 * 
 */
enum class VerbosityLevel {
    /// @brief Lower verbosity level, no output at all.
    None,
    /// @brief Only errors are displayed.
    Error,
    /// @brief Only errors and warnings are displayed.
    Warning,
    /// @brief Main information are displayed.
    Info,
    /// @brief More information displayed.
    Verbose,
    /// @brief Debug verbosity level showing several additional data.
    Debug,
    /// @brief All available logging is enabled.
    Full
};

/**
 * @brief This class corresponds to a logger handling verbosity level to describe the verbosity of Wolverine. The Log class is used as a singleton, see the function Log().
 */
class Log {
    /// @brief stdout stream.
    std::ostream& _os;
    /// @brief The current verbosity level of the Logger.
    VerbosityLevel _level;
    /// @brief output file stream.
    std::ofstream _log_file{ nullptr };
    /// @brief set of fields activated. Disabled field produces file log but not terminal output. 
    std::set<std::string> _enabled_fields;
    /// @brief 

  public:

    /// @brief counter for warning and error, updated by the Logger.
    error_counter counter;
    /// @brief A boolean determining if warning should be considered as errors.
    bool w_error = false;

    /**
     * @brief Construct a new Log object with optional verbosity level and output stream.
     * 
     * @param level the verbosity level to be used.
     * @param os the output stream to be used (by default std::cout).
     */
    explicit Log(VerbosityLevel level = VerbosityLevel::Info, std::ostream& os = std::cout);
    
    /**
     * @brief Destroy the Log object, closing the output file if needed. 
     */
    ~Log();

    /**
     * @brief Set the current verbosity level.
     * 
     * @param vl 
     */
    void set_level(VerbosityLevel vl) { _level = vl; }

    /**
     * @brief Return the current verbosity level.
     * 
     * @return VerbosityLevel 
     */
    VerbosityLevel level() const { return _level; }

    /**
     * @brief Open the Log file, closing previous one if exists.
     * 
     * @param path the path to the output log file to be written.
     */
    void open_log_file(const std::string& path);

    /**
     * @brief Close the output log file if it exists. 
     */
    void close_log_file();

    /**
     * @brief Set a log field to enabled. Disabled field produce output only in log file.
     * 
     * @param field field name.
     * @param enabled determines if the field is enabled.
     */
    void enable_field(const std::string& field, bool enabled = true);

    /**
     * @brief Log a critical level message.
     * 
     * @param msg the message to be logged.
     * @param field the field of the message, always enabled if not specified.
     */
    void critical(const std::string& msg, const std::string& field = "");

    /**
     * @brief Log a warning level message.
     * 
     * @param msg the message to be logged.
     * @param field the field of the message, always enabled if not specified.
     */
    void warning(const std::string& msg, const std::string& field = "");

    /**
     * @brief Log an information level message.
     * 
     * @param msg the message to be logged.
     * @param field the field of the message, always enabled if not specified.
     */
    void info(const std::string& msg, const std::string& field = "");

    /**
     * @brief Log a verbose level message.
     * 
     * @param msg the message to be logged.
     * @param field the field of the message, always enabled if not specified.
     */
    void verbose(const std::string& msg, const std::string& field = "");

    /**
     * @brief Log a debug level message.
     * 
     * @param msg the message to be logged.
     * @param field the field of the message, always enabled if not specified.
     */
    void debug(const std::string& msg, const std::string& field = "");

    /**
     * @brief Log a full level message.
     * 
     * @param msg the message to be logged.
     * @param field the field of the message, always enabled if not specified.
     */
    void full(const std::string& msg, const std::string& field = "");
    
    /**
     * @brief Log the message if the current verbosity level is at least equals to the specified level.
     * 
     * @param msg the message to be logged.
     * @param level the minimal required verbosity level.
     */
    void raw(const std::string& msg, VerbosityLevel level = VerbosityLevel::Info);
    
    /**
     * @brief Log the message in the output log file if opened.
     * 
     * @param msg 
     */
    void fraw(const std::string& msg);

    /**
     * @brief Returns the internal output stream.
     * 
     * @return the output stream.
     */
    std::ostream& os() { return _os; }

    // Modifiers

    /** TODO: documentation for Log's modifiers 
    */
    Log& bold() { _os << util::term_modifiers::bold; return *this;}
    Log& dark() { _os << util::term_modifiers::dark; return *this;}
    Log& italic() { _os << util::term_modifiers::italic; return *this;}
    Log& underline() { _os << util::term_modifiers::underline; return *this;}
    Log& blink() { _os << util::term_modifiers::blink; return *this;}
    Log& inverse() { _os << util::term_modifiers::inverse; return *this;}

    Log& black() { _os << util::term_modifiers::black; return *this;}
    Log& dark_gray() { _os << util::term_modifiers::dark_gray; return *this;}
    Log& red() { _os << util::term_modifiers::red; return *this;}
    Log& green() { _os << util::term_modifiers::green; return *this;}
    Log& light_green() { _os << util::term_modifiers::light_green; return *this;}
    Log& yellow() { _os << util::term_modifiers::yellow; return *this; }
    Log& blue() { _os << util::term_modifiers::blue; return *this;}
    Log& light_blue() { _os << util::term_modifiers::light_blue; return *this;}
    Log& magenta() { _os << util::term_modifiers::magenta; return *this; }
    Log& purple() { _os << util::term_modifiers::purple; return *this; }
    Log& cyan() { _os << util::term_modifiers::cyan; return *this;}
    Log& light_cyan() { _os << util::term_modifiers::light_cyan; return *this;}
    Log& white() { _os << util::term_modifiers::white; return *this;}
    Log& light_gray() { _os << util::term_modifiers::light_gray; return *this;}
    Log& fg_reset() { _os << util::term_modifiers::fg_reset; return *this;}
    Log& bblack() { _os << util::term_modifiers::cyan; return *this;}
    Log& bgreen() { _os << util::term_modifiers::bgreen; return *this;}
    Log& byellow() { _os << util::term_modifiers::byellow; return *this;}
    Log& bmagenta() { _os << util::term_modifiers::bmagenta; return *this;}
    Log& bcyan() { _os << util::term_modifiers::bcyan; return *this;}
    Log& reset() { _os << util::term_modifiers::reset; return *this;}
};
}
} // namespace lazart::util

#endif // LAZART_WOLVERINE_UTIL_LOG_HPP
