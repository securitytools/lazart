/**
 * @file util.hpp
 * @author Etienne Boespflug (etienne.boespflug@gmail.com)
 * @brief Contains utility definitions and functions.
 * @version 4.0
 */
#pragma once

#include <llvm/IR/Function.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Constants.h>

#include <llvm/IR/IRBuilder.h>

#include "llvm/Support/raw_ostream.h" // errs()

#include <set>

#include <iostream>
#include <typeinfo>
#include <utility>
#include <vector>
#include <string>
#include <set>
#include "util/log.hpp"

/**
 * @brief Returns the singleton logger of Wolverine.
 * 
 * @return lazart::util::Log& the logger of wolverine.
 */
lazart::util::Log& Log();

namespace lazart {
    /**
     * @brief The util namespace contains utility definitions.
     */
namespace util {
    /**
     * @brief Return the value of a pair (value, boolean) if the second element is true, and the default item otherwise.
     * 
     * @tparam T The type of the element (p.first).
     * @param p the pair (value, boolean).
     * @param def the default value.
     * @return T p.first if p.second is true, def otherwise.
     */
    template<typename T>
    T value_or(std::pair<T, bool> p, T def)
    {
        if(p.second)
            return p.first;
        return def;
    }

    /**
     * @brief Basic implementation of std::optional (C++ version is too old in current docker).
     * 
     * @tparam T the type of the optional object.
     */
    template<typename T>
    class optional
    {
        /// @brief The value stored.
        T value;
        /// @brief A boolean determining if the optional is active or not.
        bool active = false;

    public:
        /**
         * @brief Construct a new optional<T> object with the specified value. The object is inactive by default.
         * 
         * @param v the initial value.
         */
        optional<T>(T v) : value{v} {}

        /**
         * @brief Construct a new optional<T> object with default constructed value and inactive.
         */
        optional<T>() : value{} {}

        /**
         * @brief Set the value of the optional and set it active.
         * 
         * @param v the value to be set.
         */
        void set(T v) 
        {
            value = v;
            active = true;
        }

        /**
         * @brief Returns the value in the optional, ignoring the active status.
         * 
         * @return T the value of the optional object.
         */
        T get() const { return value; } // should throw ?

        /**
         * @brief Reset the optional to be inactive (no destructor called).
         */
        void reset() { active = false; }

        /**
         * @brief Returns true if the optional is active, false otherwise.
         * 
         * @return true if the optional is active.
         * @return false if the object is inactive.
         */
        bool has_value() const { return active; }

        /**
         * @brief Returns the value of the optional if active, the default value otherwise.
         * 
         * @param def the default value if the optional is inactive.
         * @return T the value or default depending on active status.
         */
        T value_or(T def) const
        {
            if(active)
                return value;
            return def;
        }
    };

    /**
     * @brief Split a string with a delimiter, splitting only the first occurrence. An empty second string is returned if nothing was split.
     * 
     * @param str the string to be split.
     * @param delim the delimiter for the split.
     * @return std::pair<std::string, std::string> the two string after the split.
     */
    std::pair<std::string, std::string> split(const std::string& str, char delim = ':');

    /**
     * @brief Join all string in the vector vec with the specified separator.
     * 
     * @param vec the list of string to be joined.
     * @param sep the separator for the join.
     * @return std::string the string formed by the join of all string in vec with the separator sep.
     */
    std::string join(const std::vector<std::string>& vec, const std::string& sep = ", ");

    /**
     * @brief Return true if the string str starts with the specified prefix, false otherwise.
     * 
     * @param str the string to be searched in.
     * @param prefix the prefix to be searched in str.
     * @return true if prefix is a prefix of str.
     * @return false if prefix is not a prefix of str.
     */
    bool starts_with(const std::string& str, const std::string& prefix);

    /**
     * @brief Find the specified LLVM function pointer in a set of function pointers.
     * 
     * TODO: move that in llvm.h.
     * 
     * @param f the function to be searched.
     * @param set the set of function.
     * @return true if f is in set.
     * @return false if f is not in set.
     */
    bool find_f(llvm::Function* f, std::set<llvm::Function*>& set);
}
} // namespace lazart