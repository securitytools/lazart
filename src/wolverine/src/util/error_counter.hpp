/**
 * @file error_counter.hpp
 * @author Etienne Boespflug (etienne.boespflug@gmail.com)
 * @brief This file defines the error_counter structure that is used to count errors and warnings during program execution.
 *  
 * @version 4.0
 */
#pragma once

#include <string>


/**
 * @brief The error_counter class holds a warning and an error counters. 
 */
struct error_counter {
    /** @brief Number of warnings encountered during operations. */
    unsigned warning_count = 0u;
    /** @brief Number of errors encountered during operations. */
    unsigned error_count = 0u;

    /**
     * @brief Resets the error and warnings counts. 
     */
    void reset() {
        warning_count = 0u;
        error_count = 0u;
    }

    /**
     * @brief Increments the error count.
     */
    void error() { error_count++; }

    /**
     * @brief Increments the warning count.
     */
    void warning() { warning_count++; }

    [[nodiscard]] std::string str() const {
        return std::to_string(warning_count) + " warning(s) and " + std::to_string(error_count) + " error(s)";
    }

    /**
     * @brief Returns `false` if the total count of errors and warnings is null, `true` otherwise.
     * 
     * @return bool a predicate determining if there is at least one error or warning.
     */
    [[nodiscard]] bool has_ew() const { return !(error_count == 0u && warning_count == 0u); }
};

