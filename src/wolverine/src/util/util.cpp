#include "util.hpp"


lazart::util::Log& Log() {
    static lazart::util::Log l(lazart::util::VerbosityLevel::Debug, std::cout);
    return l;
}


std::pair<std::string, std::string> lazart::util::split(const std::string& str, char delim)
{
    auto idx = str.find_first_of(delim);

    if(idx == std::string::npos) 
        return {str, ""};
    return {str.substr(0, idx), str.substr(idx + 1)};
}

std::string lazart::util::join(const std::vector<std::string>& vec, const std::string& sep)
{
    if(vec.size() == 0)
        return "";
    if(vec.size() == 1)
        return vec[0];
    std::string ret = "[";
    for(std::size_t i = 0; i < vec.size() - 1; ++i)
    {
        ret += vec[i] + sep;
    }
    
    std::string tmp = vec.back();

    return ret + tmp;
}


bool lazart::util::starts_with(const std::string& str, const std::string& prefix)
{
    return !str.compare(0, prefix.size(), prefix);
}

bool lazart::util::find_f(llvm::Function* f, std::set<llvm::Function*>& set) {
    for(auto it = set.begin(); it != set.end();) {
        if(*it == f){
            return true;
        }
    }
    return false;
}