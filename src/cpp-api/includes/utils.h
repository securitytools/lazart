#pragma once

/**
 * @file utils.h
 * @author etienne boespflug (etienne.boespflug@gmail.com)
 * @brief Utility macros for Lazart analysis.
 * @version 4.0
 * 
 * Contains utility macros for Lazart analysis.
 */ 

/**
 * @def _LZ__MAX_DEPTH(max_depth, task)
 * 
 * @brief Helper which execute `task` expression when the counter is reached. Can be used with `klee_assume(false);` to stop exploration after a depth limit.
 * 
 * @param max_depth the depth limit at which the expression `task` is executed.
 * @param task expression to be executed when max depth is reached.
 */
#define _LZ__MAX_DEPTH(max_depth, task) { \
    static int _counter = 0; \
    _LZ__DISABLE_BB(); \
    if(_counter++ >= max_depth) { \
        task; \
    } \
}

/**
 * @def _LZ__MAX_DEPTH_E(max_depth, task, msg)
 * 
 * @brief Helper which execute `task` expression when the counter is reached (triggering an user event with `msg` data), which print the counter state at Lazart's BasicBlock event. 
 * 
 * Can be used with `klee_assume(false);` to stop exploration after a depth limit.
 * 
 * @param max_depth the depth limit at which the expression `task` is executed.
 * @param task expression to be executed when max depth is reached.
 * @param msg user event data when counter is reached.
 */
#define _LZ__MAX_DEPTH_E(max_depth, task, msg) { \
    static int _counter = 0; \
    _LZ__DISABLE_BB(); \
    printf("[TRACE] c=%d\n", _counter); \
    if(_counter++ >= max_depth) { \
        _LZ__DISABLE_BB(); \
        LAZART_EVENT(msg); \
        task; \
    } \
}