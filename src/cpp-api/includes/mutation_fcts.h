#pragma once

/**
 * @file mutation_fcts.h
 * @author etienne boespflug (etienne.boespflug@gmail.com)
 * @brief Mutation functions for faults models.
 * @version 4.0
 * 
 * Contains the interface of Lazart's mutation function for user implemented faults models.
 */ 

/**
 * @brief Test inversion mutation function allowing a fault to invert the conditional branchment.
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value of br depending on the fault.
 * 
 * @param br the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param current_bb the name of the basic block containing the branch.
 * @param target_a the name of the target basic block for the true branch.
 * @param target_b the name of the target basic block for the false branch.
 * @return int the faulted value in case of fault, the original value otherwise.
 */
int _LZ__mut_ti(int br, int fault_limit, const char* ip_id_str, const char* current_bb, const char* target_a, const char* target_b);

/**
 * @brief Test inversion mutation function allowing a fault to only invert the true branch 
 * of a conditional branching.
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value of br depending on the fault.
 * 
 * @param br the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param current_bb the name of the basic block containing the branch.
 * @param target_a the name of the target basic block for the true branch.
 * @param target_b the name of the target basic block for the false branch.
 * @return int the faulted value in case of fault, the original value otherwise.
 */
int _LZ__mut_ti_true(int br, int fault_limit, const char* ip_id_str, const char* current_bb, const char* target_a, const char* target_b);

/**
 * @brief Test inversion mutation function allowing a fault to only invert the false branch 
 * of a conditional branching.
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value of br depending on the fault.
 * 
 * @param br the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param current_bb the name of the basic block containing the branch.
 * @param target_a the name of the target basic block for the true branch.
 * @param target_b the name of the target basic block for the false branch.
 * @return int the faulted value in case of fault, the original value otherwise.
 */
int _LZ__mut_ti_false(int br, int fault_limit, const char* ip_id_str, const char* current_bb, const char* target_a, const char* target_b);

/* Data Load: Pre-declarations for documentation */

// i8

/**
 * @brief DL mutation function allowing to fault an `i8` value, symbolic (unconstrained). 
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * Faulted value is symbolic and unconstrained.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @return _LZ__t_int8_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int8_t _LZ__mut_dl_i8_sym(_LZ__t_int8_t original, int fault_limit, const char *ip_id_str);

/**
 * @brief DL mutation function allowing to fault an `i8` value, symbolic and constrained by predicate (such as `P(original, injected) is true). 
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * Faulted value is symbolic and unconstrained.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param P the predicate to constraint the injected value.
 * @return _LZ__t_int8_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int8_t _LZ__mut_dl_i8_sym_pred(_LZ__t_int8_t original, int fault_limit, const char *ip_id_str, _LZ__t_int8_t (*P)(_LZ__t_int8_t, _LZ__t_int8_t));

/**
 * @brief DL mutation function allowing to fault an `i8` value, symbolic and constrained with function transformation (such as `F(original, injected)` is returned in case of fault).
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param F the function called to modify the injected value in case of fault.
 * @return _LZ__t_int8_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int8_t _LZ__mut_dl_i8_sym_fct(_LZ__t_int8_t original, int fault_limit, const char *ip_id_str, _LZ__t_int8_t(*F)(_LZ__t_int8_t, _LZ__t_int8_t));

/**
 * @brief DL mutation function allowing to fault an `i8` value, injecting `F(original)`.
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param F the function called to modify the original value in case of fault.
 * @return _LZ__t_int8_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int8_t _LZ__mut_dl_i8_fct(_LZ__t_int8_t original, int fault_limit, const char *ip_id_str, _LZ__t_int8_t(*F)(_LZ__t_int8_t));

/**
 * @brief DL mutation function allowing to fault an `i8` variable with fixed value.
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param value the value to be injected.
 * @return _LZ__t_int8_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int8_t _LZ__mut_dl_i8_fix(_LZ__t_int8_t original, int fault_limit, const char *ip_id_str, _LZ__t_int8_t value);

// i16

/**
 * @brief DL mutation function allowing to fault an `i16` value, symbolic (unconstrained). 
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * Faulted value is symbolic and unconstrained.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @return _LZ__t_int16_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int16_t _LZ__mut_dl_i16_sym(_LZ__t_int16_t original, int fault_limit, const char *ip_id_str);

/**
 * @brief DL mutation function allowing to fault an `i16` value, symbolic and constrained by predicate (such as `P(original, injected)` is true). 
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * Faulted value is symbolic and unconstrained.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param P the predicate to constraint the injected value.
 * @return _LZ__t_int16_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int16_t _LZ__mut_dl_i16_sym_pred(_LZ__t_int16_t original, int fault_limit, const char *ip_id_str, _LZ__t_int16_t (*P)(_LZ__t_int16_t, _LZ__t_int16_t));

/**
 * @brief DL mutation function allowing to fault an `i16` value, symbolic and constrained with function transformation (such as `F(original, injected)` is returned in case of fault).
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param F the function called to modify the injected value in case of fault.
 * @return _LZ__t_int16_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int16_t _LZ__mut_dl_i16_sym_fct(_LZ__t_int16_t original, int fault_limit, const char *ip_id_str, _LZ__t_int16_t(*F)(_LZ__t_int16_t, _LZ__t_int16_t));

/**
 * @brief DL mutation function allowing to fault an `i16` value, injecting `F(original)`.
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param F the function called to modify the original value in case of fault.
 * @return _LZ__t_int16_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int16_t _LZ__mut_dl_i16_fct(_LZ__t_int16_t original, int fault_limit, const char *ip_id_str, _LZ__t_int16_t(*F)(_LZ__t_int16_t));

/**
 * @brief DL mutation function allowing to fault an `i16` variable with fixed value.
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param value the value to be injected.
 * @return _LZ__t_int16_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int16_t _LZ__mut_dl_i16_fix(_LZ__t_int16_t original, int fault_limit, const char *ip_id_str, _LZ__t_int16_t value);

// i32

/**
 * @brief DL mutation function allowing to fault an `i32` value, symbolic (unconstrained). 
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * Faulted value is symbolic and unconstrained.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @return _LZ__t_int32_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int32_t _LZ__mut_dl_i32_sym(_LZ__t_int32_t original, int fault_limit, const char *ip_id_str);

/**
 * @brief DL mutation function allowing to fault an `i32` value, symbolic and constrained by predicate (such as `P(original, injected)` is true). 
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * Faulted value is symbolic and unconstrained.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param P the predicate to constraint the injected value.
 * @return _LZ__t_int32_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int32_t _LZ__mut_dl_i32_sym_pred(_LZ__t_int32_t original, int fault_limit, const char *ip_id_str, _LZ__t_int32_t (*P)(_LZ__t_int32_t, _LZ__t_int32_t));

/**
 * @brief DL mutation function allowing to fault an `i32` value, symbolic and constrained with function transformation (such as `F(original, injected)` is returned in case of fault).
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param F the function called to modify the injected value in case of fault.
 * @return _LZ__t_int32_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int32_t _LZ__mut_dl_i32_sym_fct(_LZ__t_int32_t original, int fault_limit, const char *ip_id_str, _LZ__t_int32_t(*F)(_LZ__t_int32_t, _LZ__t_int32_t));

/**
 * @brief DL mutation function allowing to fault an `i32` value, injecting `F(original)`.
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param F the function called to modify the original value in case of fault.
 * @return _LZ__t_int32_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int32_t _LZ__mut_dl_i32_fct(_LZ__t_int32_t original, int fault_limit, const char *ip_id_str, _LZ__t_int32_t(*F)(_LZ__t_int32_t));

/**
 * @brief DL mutation function allowing to fault an `i32` variable with fixed value.
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param value the value to be injected.
 * @return _LZ__t_int32_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int32_t _LZ__mut_dl_i32_fix(_LZ__t_int32_t original, int fault_limit, const char *ip_id_str, _LZ__t_int32_t value);

// i64

/**
 * @brief DL mutation function allowing to fault an `i64` value, symbolic (unconstrained). 
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * Faulted value is symbolic and unconstrained.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @return _LZ__t_int64_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int64_t _LZ__mut_dl_i64_sym(_LZ__t_int64_t original, int fault_limit, const char *ip_id_str);

/**
 * @brief DL mutation function allowing to fault an `i64` value, symbolic and constrained by predicate (such as `P(original, injected)` is true). 
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * Faulted value is symbolic and unconstrained.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param P the predicate to constraint the injected value.
 * @return _LZ__t_int64_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int64_t _LZ__mut_dl_i64_sym_pred(_LZ__t_int64_t original, int fault_limit, const char *ip_id_str, _LZ__t_int64_t (*P)(_LZ__t_int64_t, _LZ__t_int64_t));

/**
 * @brief DL mutation function allowing to fault an `i64` value, symbolic and constrained with function transformation (such as `F(original, injected)` is returned in case of fault).
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param F the function called to modify the injected value in case of fault.
 * @return _LZ__t_int64_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int64_t _LZ__mut_dl_i64_sym_fct(_LZ__t_int64_t original, int fault_limit, const char *ip_id_str, _LZ__t_int64_t(*F)(_LZ__t_int64_t, _LZ__t_int64_t));

/**
 * @brief DL mutation function allowing to fault an `i64` value, injecting `F(original)`.
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param F the function called to modify the original value in case of fault.
 * @return _LZ__t_int64_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int64_t _LZ__mut_dl_i64_fct(_LZ__t_int64_t original, int fault_limit, const char *ip_id_str, _LZ__t_int64_t(*F)(_LZ__t_int64_t));

/**
 * @brief DL mutation function allowing to fault an `i64` variable with fixed value.
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param value the value to be injected.
 * @return _LZ__t_int64_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int64_t _LZ__mut_dl_i64_fix(_LZ__t_int64_t original, int fault_limit, const char *ip_id_str, _LZ__t_int64_t value);

// i128

/**
 * @brief DL mutation function allowing to fault an `i128` value, symbolic (unconstrained). 
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * Faulted value is symbolic and unconstrained.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @return _LZ__t_int128_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int128_t _LZ__mut_dl_i128_sym(_LZ__t_int128_t original, int fault_limit, const char *ip_id_str);

/**
 * @brief DL mutation function allowing to fault an `i128` value, symbolic and constrained by predicate (such as `P(original, injected)` is true). 
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * Faulted value is symbolic and unconstrained.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param P the predicate to constraint the injected value.
 * @return _LZ__t_int128_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int128_t _LZ__mut_dl_i128_sym_pred(_LZ__t_int128_t original, int fault_limit, const char *ip_id_str, _LZ__t_int128_t (*P)(_LZ__t_int128_t, _LZ__t_int128_t));

/**
 * @brief DL mutation function allowing to fault an `i128` value, symbolic and constrained with function transformation (such as `F(original, injected)` is returned in case of fault).
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param F the function called to modify the injected value in case of fault.
 * @return _LZ__t_int128_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int128_t _LZ__mut_dl_i128_sym_fct(_LZ__t_int128_t original, int fault_limit, const char *ip_id_str, _LZ__t_int128_t(*F)(_LZ__t_int128_t, _LZ__t_int128_t));

/**
 * @brief DL mutation function allowing to fault an `i128` value, injecting `F(original)`.
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param F the function called to modify the original value in case of fault.
 * @return _LZ__t_int128_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int128_t _LZ__mut_dl_i128_fct(_LZ__t_int128_t original, int fault_limit, const char *ip_id_str, _LZ__t_int128_t(*F)(_LZ__t_int128_t));

/**
 * @brief DL mutation function allowing to fault an `i128` variable with fixed value.
 * 
 * Includes the symbolic boolean `inject` check and doesn't inject if the specified 
 * `fault_limit` is reached. Returns the faulted or original value depending on the fault.
 * 
 * Other versions of this function exist for other integer types (`i8`, `i16`, `i32`, 
 * `i64`, `i128`).
 * 
 * @param original the nominal conditional value.
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param value the value to be injected.
 * @return _LZ__t_int128_t the faulted value in case of fault, the original value otherwise.
 */
_LZ__t_int128_t _LZ__mut_dl_i128_fix(_LZ__t_int128_t original, int fault_limit, const char *ip_id_str, _LZ__t_int128_t value);

/** @brief Internal, not documented. */
#define _LZ__STRCAT_(a, b) a ## b
/** Internal, not documented. */
#define _LZ__STRCAT(a, b) _LZ__STRCAT_(a, b)

/** Internal, not documented. */
#define _LZ__STR(x) #x
/** Internal, not documented. */
#define _LZ__TOSTR(x) _LZ__STR(x)

/** Internal, not documented. */
#define _LZ__TMPL_mut_data_sym(type, tname) type _LZ__STRCAT(_LZ__mut_dl_, _LZ__STRCAT(tname, _sym)) \
    (type original, int fault_limit, const char* ip_id_str)
/** Internal, not documented. */
#define _LZ__TMPL_mut_data_sym_pred(type, tname) type _LZ__STRCAT(_LZ__mut_dl_, _LZ__STRCAT(tname, _sym_pred)) \
    (type original, int fault_limit, const char* ip_id_str, type (*P)(type, type))
/** Internal, not documented. */
#define _LZ__TMPL_mut_data_sym_fct(type, tname) type _LZ__STRCAT(_LZ__mut_dl_, _LZ__STRCAT(tname, _sym_fct)) \
    (type original, int fault_limit, const char* ip_id_str, type (*F)(type, type))
/** Internal, not documented. */
#define _LZ__TMPL_mut_data_fct(type, tname) type _LZ__STRCAT(_LZ__mut_dl_, _LZ__STRCAT(tname, _fct)) \
    (type original, int fault_limit, const char* ip_id_str, type (*F)(type))
/** Internal, not documented. */
#define _LZ__TMPL_mut_data_fix(type, tname) type _LZ__STRCAT(_LZ__mut_dl_, _LZ__STRCAT(tname, _fix)) \
    (type original, int fault_limit, const char* ip_id_str, type value)

/** Internal, not documented. */
#define _LZ__TMPL_mut_data_all(type, tname) _LZ__TMPL_mut_data_sym(type, tname); \
    _LZ__TMPL_mut_data_sym_pred(type, tname); \
    _LZ__TMPL_mut_data_sym_fct(type, tname); \
    _LZ__TMPL_mut_data_fct(type, tname); \
    _LZ__TMPL_mut_data_fix(type, tname)

_LZ__TMPL_mut_data_all(_LZ__t_int8_t, i8);
_LZ__TMPL_mut_data_all(_LZ__t_int16_t, i16);
_LZ__TMPL_mut_data_all(_LZ__t_int32_t, i32);
_LZ__TMPL_mut_data_all(_LZ__t_int64_t, i64);
_LZ__TMPL_mut_data_all(_LZ__t_int128_t, i128);