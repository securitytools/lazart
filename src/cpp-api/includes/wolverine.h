#pragma once

/**
 * @file wolverine.h
 * @author etienne boespflug (etienne.boespflug@gmail.com)
 * @brief Instrumentation function for Wolverine.
 * @version 4.0
 * 
 * Contains placeholder functions (never defined) used to specify instrumentation directives.
 * Those functions are processed during the mutation step (by **Wolverine**).
 */


/*****************
 * INSTRUMENTATION
 *****************/

/**
 * @brief Specifies the name of the enclosing basic block. This function can be placed anywhere in the basic block. Additional calls inside the same basic block will be ignored.
 * 
 * Instrumentation functions don't have definition and are removed by the mutation process 
 * in Wolverine. 
 * This function is removed during the preprocessing 'rename_bb' pass.
 * 
 * @param name the name of the basic block.
 */
void _LZ__RENAME_BB(const char* name);

/**
 * @brief Disables all fault injection in current function. This function can be placed 
 * anywhere in the function.
 * 
 * Instrumentation functions don't have definition and are removed by the mutation process 
 * in Wolverine.
 * This function is removed during the mutation pass.
 */
void _LZ__DISABLE_FUNCTION();

/**
 * @brief Disables all fault injection in current basic block. This function can be placed 
 * anywhere in the basic block.
 * 
 * Instrumentation functions don't have definition and are removed by the mutation process 
 * in Wolverine.
 * This function is removed during the mutation pass.
 */
void _LZ__DISABLE_BB();

/**
 * @brief Mark all model as disabled, disabling fault injection mutation until another 
 * instrumentation function change the models activation states or until the end
 * of the function.
 * Relies on the order of the basic block in IR.
 * 
 * Instrumentation functions don't have definition and are removed by the mutation process 
 * in Wolverine.
 * This function is removed during the mutation pass.
 */
void _LZ__DISABLE_ALL();

/**
 * @brief Mark all model actives on the current function as enabled until another 
 * instrumentation function change the models activation states or until the end
 * of the function.
 * Relies on the order of the basic block in IR.
 * 
 * Instrumentation functions don't have definition and are removed by the mutation process 
 * in Wolverine.
 * This function is removed during the mutation pass.
 */
void _LZ__ENABLE_ALL();

/**
 * @brief Disable a specific named model until another instrumentation function 
 * change the models activation states or until the end of the function.
 * Relies on the order of the basic block in IR.
 * Named model are specified in the attack model.
 * 
 * Instrumentation functions don't have definition and are removed by the mutation process 
 * in Wolverine.
 * This function is removed during the mutation pass.
 * 
 * @param name the name of the model to be disabled.
 */
void _LZ__DISABLE_MODEL(const char* name);

/**
 * @brief Enable a specific named model until another instrumentation function 
 * change the models activation states or until the end of the function.
 * Relies on the order of the basic block in IR.
 * Named model are specified in the attack model.
 * 
 * Instrumentation functions don't have definition and are removed by the mutation process 
 * in Wolverine.
 * This function is removed during the mutation pass.
 * 
 * @param name the name of the model to be enabled.
 */
void _LZ__ENABLE_MODEL(const char* name);

/**
 * @brief Resets the activation states for all models.
 * 
 * Instrumentation functions don't have definition and are removed by the mutation process 
 * in Wolverine.
 * This function is removed during the mutation pass.
 */
void _LZ__RESET();

/**
 * @brief Resets the activation state of all currently disabled models.
 * Relies on the order of the basic block in IR.
 * 
 * Instrumentation functions don't have definition and are removed by the mutation process 
 * in Wolverine.
 * This function is removed during the mutation pass.
 */
void _LZ__RESET_DISABLED();

/**
 * @brief Resets the activation state of all currently enabled models.
 * Relies on the order of the basic block in IR.
 * 
 * Instrumentation functions don't have definition and are removed by the mutation process 
 * in Wolverine.
 * This function is removed during the mutation pass.
 */
void _LZ__RESET_ENABLED();

//  _LZ__DECLARE_UIP(ip_id_str, "SwitchCall", "call1:" STR(call1) ";call2:" STR(call2)); 
void _LZ__DECLARE_UIP(const char* ip_name, const char* model, const char* data_str);

/**
 * @brief Resets the activation state of the specified named model.
 * Relies on the order of the basic block in IR.
 * Named model are specified in the attack model.
 * 
 * Instrumentation functions don't have definition and are removed by the mutation process 
 * in Wolverine.
 * This function is removed during the mutation pass.
 * 
 * @param name the name of the model to be disabled.
 */
void _LZ__RESET_MODEL(const char* name);

// Other functions.

/**
 * @brief Used to defined the location of a user-defined-IP with mutation macro. 
 * 
 * Used by JMP and SC mutation macros to declare the IP to wolverine (no mutation function can be found). 
 * Model specific static data can be set inside the string `data`.
 * 
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param model the string code of the model (see `lazart::model::fault_type`).
 * @param data model specific static data (can be empty).
 */
void _LZ__DECLARE_UIP(const char* ip_id_str, const char* model, const char* data);