#pragma once 

/**
 * @file mutation_macros.h
 * @author etienne boespflug (etienne.boespflug@gmail.com)
 * @brief Mutation functions for faults models.
 * @version 4.0
 * 
 * Contains the mutation macros for Jump and SwitchCall fault models..
 */ 

#include <stdio.h>

/**
 * @brief Preprocessor stringification.
 */
#define STR(a) #a


#ifdef _LZ__NO_REPLAY_CHECK
/**
 * @brief Helper macro for printf which use klee_is_replay depending on _LZ__NO_REPLAY_CHECK macro.
 */
#   define _LZ__TMP_print(...) printf(__VA_ARGS__)
#else
#   define _LZ__TMP_print(...)  if(klee_is_replay()) { printf(__VA_ARGS__); }
#endif

/**
 * @def _LZ__SWITCH_CALL(fault_limit, ip_id_str, call1, call2)
 * 
 * @brief Mutation macro for switch call model allowing the fault to change the call expression to `call2`.
 * 
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param call1 nominal call expression.
 * @param call2 faulted call expression.
 */
#define _LZ__SWITCH_CALL(fault_limit, ip_id_str, call1, call2) {\
        _LZ__DISABLE_BB(); \
        int inject; \
        _LZ__DECLARE_UIP(ip_id_str, "SwitchCall", "call1:" STR(call1) ";call2:" STR(call2)); \
        klee_make_symbolic(&inject, sizeof(inject), (ip_id_str)); \
        if(inject & (_LZ__fault_count < fault_limit)) { \
            _LZ__DISABLE_BB(); \
            _LZ__fault_count++; \
            call2; \
            _LZ__TMP_print("\n[FAULT] [SC] [%s] [~] by [~]\n", (ip_id_str)); \
        } else { \
            _LZ__DISABLE_BB(); \
            call1; \
        } \
        _LZ__DISABLE_BB(); \
}

/**
 * @def _LZ__SWITCH_CALL_2(fault_limit, ip_id_str, call1, call2, call3)
 * 
 * @brief Mutation macro for switch call model allowing the fault to change the call expression to `call2` or `call3`.
 * 
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param call1 nominal call expression.
 * @param call2 first faulted call expression.
 * @param call3 second faulted call expression.
 */
#define _LZ__SWITCH_CALL_2(fault_limit, ip_id_str, call1, call2, call3) {\
        _LZ__DISABLE_BB(); \
        int inject; \
        _LZ__DECLARE_UIP(ip_id_str, "SwitchCall", "call1:" STR(call1) ";call2:" STR(call2) ";call3:" STR(call3)); \
        klee_make_symbolic(&inject, sizeof(inject), (ip_id_str)); \
        if((inject == 1) & (_LZ__fault_count < fault_limit)) { \
            _LZ__DISABLE_BB(); \
            _LZ__fault_count++; \
            call2; \
            _LZ__TMP_print("\n[FAULT] [SC] [%s1] [~] by [~]\n", (ip_id_str)); \
        } else if((inject == 2) & (_LZ__fault_count < fault_limit)) { \
            _LZ__DISABLE_BB(); \
            _LZ__fault_count++; \
            call3; \
            _LZ__TMP_print("\n[FAULT] [SC] [%s2] [~] by [~]\n", (ip_id_str)); \
        } else { \
            _LZ__DISABLE_BB(); \
            call1; \
        } \
        _LZ__DISABLE_BB(); \
} 

/**
 * @def _LZ__SWITCH_CALL_3(fault_limit, ip_id_str, call1, call2, call3, call4)
 * 
 * @brief Mutation macro for switch call model allowing the fault to change the call expression to `call2`, `call3` or `call4`.
 * 
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param call1 nominal call expression.
 * @param call2 first faulted call expression.
 * @param call3 second faulted call expression.
 * @param call4 third faulted call expression.
 */
#define _LZ__SWITCH_CALL_3(fault_limit, ip_id_str, call1, call2, call3, call4) {\
        _LZ__DISABLE_BB(); \
        int inject; \
        _LZ__DECLARE_UIP(ip_id_str, "SwitchCall", "call1:" STR(call1) ";call2:" STR(call2) ";call3:" STR(call3) ";call4:" STR(call4)); \
        klee_make_symbolic(&inject, sizeof(inject), (ip_id_str)); \
        if((inject == 1) & (_LZ__fault_count < fault_limit)) { \
            _LZ__DISABLE_BB(); \
            _LZ__fault_count++; \
            call2; \
            _LZ__TMP_print("\n[FAULT] [SC] [%s1] [~] by [~]\n", (ip_id_str)); \
        } else if((inject == 2) & (_LZ__fault_count < fault_limit)) { \
            _LZ__DISABLE_BB(); \
            _LZ__fault_count++; \
            call3; \
            _LZ__TMP_print("\n[FAULT] [SC] [%s2] [~] by [~]\n", (ip_id_str)); \
        } else if((inject == 3) & (_LZ__fault_count < fault_limit)) { \
            _LZ__DISABLE_BB(); \
            _LZ__fault_count++; \
            call4; \
            _LZ__TMP_print("\n[FAULT] [SC] [%s3] [~] by [~]\n", (ip_id_str)); \
        } else { \
            _LZ__DISABLE_BB(); \
            call1; \
        } \
        _LZ__DISABLE_BB(); \
}

/* JUMP Model */

/**
 * @def _LZ__JUMP(fault_limit, ip_id_str, target)
 * 
 * @brief Mutation macro for jump model allowing the fault to jump to the label `target`.
 * 
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point (should be unique).
 * @param target destination label for the jump.
 */
#define _LZ__JUMP(fault_limit, ip_id_str, target) { \
        _LZ__DISABLE_BB(); \
        if(_LZ__fault_count < fault_limit) { \
            _LZ__DISABLE_BB(); \
            int inject; \
            klee_make_symbolic(&inject, sizeof(inject), (ip_id_str)); \
            if(inject) { \
                _LZ__DISABLE_BB(); \
                _LZ__DECLARE_UIP(ip_id_str, "Jump", "target:" STR(target)); \
                _LZ__fault_count++; \
                _LZ__TMP_print("\n[FAULT] [JMP] [%s] jump to %s\n", (ip_id_str), STR(target)); \
                goto target; \
            } else {\
            _LZ__DISABLE_BB(); \
            } \
            _LZ__DISABLE_BB(); \
        } \
        _LZ__DISABLE_BB(); \
    }

/**
 * @def _LZ__JUMP_2(fault_limit, ip_id_str, target1, ip_id_str2, target2)
 * 
 * @brief Mutation macro for jump model allowing the fault to jump to the label `target1` or `target2`.
 * 
 * Two IPs are generated (one for `target1`'s jump and one for `target2`).
 * 
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point targeting `target1` (should be unique).
 * @param target1 destination label for the first possible jump.
 * @param ip_id_str2 the ID of the Injection Point targeting `target2` (should be unique).
 * @param target2 destination label for the second possible jump.
 */
#define _LZ__JUMP_2(fault_limit, ip_id_str, target1, ip_id_str2, target2) { \
        _LZ__DISABLE_BB(); \
        if(_LZ__fault_count < fault_limit) { \
            _LZ__DISABLE_BB(); \
            int inject; \
            klee_make_symbolic(&inject, sizeof(inject), (ip_id_str)); \
            if(inject == 1) { \
                _LZ__DISABLE_BB(); \
                _LZ__DECLARE_UIP(ip_id_str, "Jump", "target:" STR(target)); \
                _LZ__fault_count++; \
                _LZ__TMP_print("\n[FAULT] [JMP] [%s] jump to %s\n", (ip_id_str), STR(target1)); \
                goto target1; \
            } \
            else if(inject == 2) { \
                _LZ__DISABLE_BB(); \
                _LZ__DECLARE_UIP(ip_id_str2, "Jump", "target:" STR(target2)); \
                _LZ__fault_count++; \
                _LZ__TMP_print("\n[FAULT] [JMP] [%s] jump to %s\n", (ip_id_str2), STR(target2)); \
                goto target2; \
            }  else {\
            _LZ__DISABLE_BB(); \
            } \
            _LZ__DISABLE_BB(); \
        } \
        _LZ__DISABLE_BB(); \
    }

/**
 * @def _LZ__JUMP_3(fault_limit, ip_id_str, target1, ip_id_str2, target2, ip_id_str3, target3)
 * 
 * @brief Mutation macro for jump model allowing the fault to jump to the label `target1`, `target2` or `target3`.
 * 
 * Three IPs are generated (one for `target1`'s jump, one for `target2` and one for `target3`).
 * 
 * @param fault_limit the limit of fault (doesn't inject if reached).
 * @param ip_id_str the ID of the Injection Point targeting `target1` (should be unique).
 * @param target1 destination label for the first possible jump.
 * @param ip_id_str2 the ID of the Injection Point targeting `target2` (should be unique).
 * @param target2 destination label for the second possible jump.
 * @param ip_id_str3 the ID of the Injection Point targeting `target3` (should be unique).
 * @param target3 destination label for the third possible jump.
 */
#define _LZ__JUMP_3(fault_limit, ip_id_str, target1, ip_id_str2, target2, ip_id_str3, target3) { \
        _LZ__DISABLE_BB(); \
        if(_LZ__fault_count < fault_limit) { \
            _LZ__DISABLE_BB(); \
            int inject; \
            klee_make_symbolic(&inject, sizeof(inject), (ip_id_str)); \
            if(inject == 1) { \
                _LZ__DISABLE_BB(); \
                _LZ__fault_count++; \
                _LZ__DECLARE_UIP(ip_id_str, "Jump", "target:" STR(target)); \
                _LZ__TMP_print("\n[FAULT] [JMP] [%s] jump to %s\n", (ip_id_str), STR(target1)); \
                goto target1; \
            } \
            else if(inject == 2) { \
                _LZ__DISABLE_BB(); \
                _LZ__fault_count++; \
                _LZ__DECLARE_UIP(ip_id_str2, "Jump", "target:" STR(target2)); \
                _LZ__TMP_print("\n[FAULT] [JMP] [%s] jump to %s\n", (ip_id_str2), STR(target2)); \
                goto target2; \
            } \
            else if(inject == 3) { \
                _LZ__DISABLE_BB(); \
                _LZ__fault_count++; \
                _LZ__DECLARE_UIP(ip_id_str3, "Jump", "target:" STR(target3)); \
                _LZ__TMP_print("\n[FAULT] [JMP] [%s] jump to %s\n", (ip_id_str3), STR(target3)); \
                goto target3; \
            }  else {\
            _LZ__DISABLE_BB(); \
            } \
            _LZ__DISABLE_BB(); \
        } \
        _LZ__DISABLE_BB(); \
    }