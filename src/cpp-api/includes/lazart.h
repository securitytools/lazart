/**
 * @file lazart.h
 * @author etienne boespflug (etienne.boespflug@gmail.com)
 * @brief The main file of Lazart's C/C++ API.
 * @version 3.1
 * 
 * Contains the interface of Lazart's C/C++ API. This header is automatically included
 * at compile time for all analysis made with Lazart.
 * 
 * If the macro LAZART is not defined, this file has no effect. 
 */
#pragma once

#ifndef H_LAZART_API
#define H_LAZART_API

#ifdef LAZART

// Klee's includes (such as klee_assume, klee_assert...).
#include <klee/klee.h>

#ifdef _LZ__NO_STD
typedef int _LZ__t_bool;
enum { _LZ__t_false, _LZ__t_true };

typedef signed char _LZ__t_int8_t;
typedef signed short int _LZ__t_int16_t;
typedef signed int _LZ__t_int32_t;
typedef signed long int _LZ__t_int64_t;
typedef __int128_t _LZ__t_int128_t;

#else
#include <stdbool.h>
#include <stdint.h>

/** Boolean type in Lazart.*/
typedef bool _LZ__t_bool;
/** 8 bits integer type in Lazart.*/
typedef int8_t _LZ__t_int8_t;
/** 16 bits integer type in Lazart.*/
typedef int16_t _LZ__t_int16_t;
/** 32 bits integer type in Lazart.*/
typedef int32_t _LZ__t_int32_t;
/** 64 bits integer type in Lazart.*/
typedef int64_t _LZ__t_int64_t;
/** 128 bits integer type in Lazart.*/
typedef __int128_t _LZ__t_int128_t;

enum { 
    /**
     * @brief True value for _LZ__t_bool.
     */
    _LZ__t_false = false,

    /**
     * @brief False value for _LZ__t_bool.
     */
    _LZ__t_true = true
};
#endif
 
/**
 * @brief Global fault counter for analysis, use it for custom mutation function.
 */
extern int _LZ__fault_count;

/*******************
 * Detectors
 *******************/

/**
 * Integral value representing the detection boolean. True if a detector has been triggered,
 *  false otherwise.
 * This variable should not be faulted.  
 * 
 * @see _LZ__triggered(void)
 * @see _LZ__trigger_detector(int)
 */
extern _LZ__t_bool _LZ__detector_alarm_;

/**
 * @return true if a detector has been triggered during this (symbolic) execution, false
 *  otherwise.
 */
_LZ__t_bool _LZ__triggered(void);

/**
 * Signals that the detector identified by the ID `_det_id` has been triggered.
 * 
 * Makes a stdout output to trace the detector trigger for Klee's replay. Set the variable
 * `_LZ__detector_alarm_` to true.
 * 
 * @param _det_id the ID of the triggered detector.
 * 
 * @see _LZ__detector_alarm_
 */
void _LZ__trigger_detector(const char* _det_id);

/**
 * Signals that the detector identified by the ID `_det_id` has been triggered.
 * This version stop the execution with a call to `klee_assume(false)` (or `exit(0)` if 
 * `_LZ__CM_USE_EXIT` is defined). 
 * 
 * With `klee_assume` version, the trace will be considered as not satisfying the oracle.
 * 
 * Makes a stdout output to trace the detector trigger for Klee's replay. Set the variable
 * `_LZ__detector_alarm_` to true. 
 * 
 * @param _det_id the ID of the triggered detector.
 * 
 * @see _LZ__detector_alarm_
 */
void _LZ__trigger_detector_stop(const char* _det_id);

#include "mutation_fcts.h"
#include "mutation_macros.h"

/*******************
 * MAIN
 ******************/

/**
 * @def _LZ__CM(id)
 * 
 * Macro representing the detection of an attack by a detector identified by `id`.
 * 
 * Uses `_LZ__trigger_detector` if `_LZ__ATTACKS` (attack analysis define) or 
 * `_LZ__FORCE_NS_DETECTORS` is defined, uses `_LZ__trigger_detector_stop`.
 * 
 * @param id the ID of the triggered detector.
 */

#ifdef _LZ__ATTACKS
#ifdef _LZ__FORCE_NS_DETECTORS
#define _LZ__CM(id) _LZ__trigger_detector((id))
#else
#define _LZ__CM(id) _LZ__trigger_detector_stop((id))
#endif

#else
#define _LZ__CM(id) _LZ__trigger_detector((id))
#endif

/**
 * @def _LZ__ORACLE(oracle)
 * 
 * Macro defining an oracle (attack objective) of an analysis.
 * All paths that does not match the predicate `oracle` will not be explored.
 * 
 * @param oracle the predicate for the paths to be considered in analysis.
 */
#define _LZ__ORACLE(oracle) klee_assume((oracle))


/**
 * @def _LZ__EVENT(f_,...)
 * 
 * Macro defining an user event, which will be saved into traces events when encountered during
 * exploration.
 * 
 * 
 * @param f_ format string containing additional data of the event (see `printf`).
 * @param ... variadic arguments for the format string `f_`.
 */
#define _LZ__EVENT(f_, ...) if(klee_is_replay()) { printf(("\n[USER_EVENT] " f_ "\n"), ##__VA_ARGS__); }

#include "wolverine.h"
#include "utils.h"

#else
#define _LZ__ORACLE(oracle)
#define _LZ__CM(id)
#define _LZ__EVENT(f_, ...)

// todo: needed for other instrumentation function ?
#define _LZ__RENAME_BB(string)

#endif // LAZART
#else

// Nothing.

#endif // H_LAZART_API
