/**
 * @file lazart.c
 * @author etienne boespflug (etienne.boespflug@gmail.com)
 * @brief Contains implementation of Lazart's API.
 * @version 4.0
 * @since 3.0
 * 
 * Contains the implementation of Lazart's C/C++ API. This source is automatically 
 * compiled and linked at compile time for all analysis made with Lazart.
 */

#include "lazart.h"
#include <stdio.h>

#ifdef _LZ__CM_USE_EXIT
#include <stdlib.h>
#endif


_LZ__t_bool _LZ__detector_alarm_ = _LZ__t_false;

_LZ__t_bool _LZ__triggered(void)
{
    return _LZ__detector_alarm_;
}

void _LZ__trigger_detector(const char* _ccp_id)
{
    _LZ__detector_alarm_ = _LZ__t_true;
    // See wiki for print syntax.
    printf("\n[CM] triggered %s \n", _ccp_id);
}

void _LZ__trigger_detector_stop(const char* _ccp_id)
{
    _LZ__detector_alarm_ = _LZ__t_true;
    // See wiki for print syntax.
#ifdef _LZ__DET_REPLAY_CHECK
    if(klee_is_replay()) {
#endif
        printf("\n[CM] triggered %s \n", _ccp_id);
        printf("\n[CM] exit\n");
#ifdef _LZ__DET_REPLAY_CHECK
    }
#endif

#ifdef _LZ__CM_USE_EXIT
    exit(0);
#else
    klee_assume(_LZ__t_false); // Cut program.
#endif
}

/**
 * Mutation functions 
 */

/**
 * @brief The fault count ghost variable allowing to verify that a fault limit is not reached.
 * 
 */
int _LZ__fault_count = 0;


// TEST INVERSION 

int _LZ__mut_ti(int br, int fault_limit, const char* ip_id_str, const char* current_bb, const char* target_a, const char* target_b)
{
#ifndef _LZ__FCOUNT_ASSUME
    if(_LZ__fault_count >= fault_limit) 
        return br;
#endif

    int inject;
    klee_make_symbolic(&inject, sizeof(inject), ip_id_str);
    
    if(inject)
    {
#ifdef _LZ__FCOUNT_ASSUME
        klee_assume(_LZ__fault_count < fault_limit);
#endif
        _LZ__fault_count++;

#ifndef _LZ__NO_REPLAY_CHECK
        if(klee_is_replay()) {
#endif
#ifdef _LZ__TI_DISTINCT
            printf("\n[FAULT] [TI] [%s%s] to force %s to %s\n", ip_id_str, (br) ? "F":"T", current_bb, (br) ? target_b : target_a);
#else
            printf("\n[FAULT] [TI] [%s] to force %s to %s\n", ip_id_str, current_bb, (br) ? target_b : target_a);
#endif
#ifndef _LZ__NO_REPLAY_CHECK
        }
#endif        
        return !br;
    }
    return br;
}

int _LZ__mut_ti_true(int br, int fault_limit, const char* ip_id_str, const char* current_bb, const char* target_a, const char* target_b)
{
    if(!br)
        return !br;

#ifndef _LZ__FCOUNT_ASSUME
    if(_LZ__fault_count >= fault_limit) 
        return br;
#endif

    int inject;
    klee_make_symbolic(&inject, sizeof(inject), ip_id_str);
    
    if(inject)
    {
#ifdef _LZ__FCOUNT_ASSUME
        klee_assume(_LZ__fault_count < fault_limit);
#endif
        _LZ__fault_count++;
#ifndef _LZ__NO_REPLAY_CHECK
        if(klee_is_replay()) {
#endif
#ifdef _LZ__TI_DISTINCT
            printf("\n[FAULT] [TI] [%sT] to force %s to %s\n", ip_id_str, current_bb, (br) ? target_a : target_b);
#else
            printf("\n[FAULT] [TI] [%s] to force %s to %s\n", ip_id_str, current_bb, (br) ? target_b : target_a);
#endif

#ifndef _LZ__NO_REPLAY_CHECK
        }
#endif
        return !br;
    }
    return br;
}

int _LZ__mut_ti_false(int br, int fault_limit, const char* ip_id_str, const char* current_bb, const char* target_a, const char* target_b)
{
    if(br)
        return br;
#ifndef _LZ__FCOUNT_ASSUME
    if(_LZ__fault_count >= fault_limit) 
        return br;
#endif

    int inject;
    klee_make_symbolic(&inject, sizeof(inject), ip_id_str);
    
    if(inject)
    {
#ifdef _LZ__FCOUNT_ASSUME
        klee_assume(_LZ__fault_count < fault_limit);
#endif
        _LZ__fault_count++;

#ifndef _LZ__NO_REPLAY_CHECK
        if(klee_is_replay()) {
#endif
#ifdef _LZ__TI_DISTINCT
            printf("\n[FAULT] [TI] [%sF] to force %s to %s\n", ip_id_str, current_bb, (br) ? target_b : target_a);
#else
            printf("\n[FAULT] [TI] [%s] to force %s to %s\n", ip_id_str, current_bb, (br) ? target_b : target_a);
#endif
#ifndef _LZ__NO_REPLAY_CHECK
        }
#endif
        return !br;
    }
    return br;
}

// DATA LOAD  

#ifdef _LZ__NO_REPLAY_CHECK
#   ifndef _LZ__NO_MUT_VALUE
#   define _LZ__TMPLi_mut_data_printval(tname, tklee, fmt) printf("\n[FAULT] [DL] [%s] [" fmt "]\n", ip_id_str, _LZ__STRCAT(klee_get_value_, tklee)(value))
#   define _LZ__TMPLi_mut_data_printval_cast(tname, fmt, cast) printf("\n[FAULT] [DL] [%s] [" fmt "]\n", ip_id_str, value)
#   else
#   define _LZ__TMPLi_mut_data_printval(tname, tklee, fmt) printf("\n[FAULT] [DL] [%s] [~]\n", ip_id_str);
#   define _LZ__TMPLi_mut_data_printval_cast(tname, fmt, cast) printf("\n[FAULT] [DL] [%s] [~]\n", ip_id_str)
#   endif
#else
#   ifndef _LZ__NO_MUT_VALUE
#   define _LZ__TMPLi_mut_data_printval(tname, tklee, fmt) if(klee_is_replay()) { printf("\n[FAULT] [DL] [%s] [" fmt "]\n", ip_id_str, _LZ__STRCAT(klee_get_value_, tklee)(value)); }
#   define _LZ__TMPLi_mut_data_printval_cast(tname, fmt, cast) if(klee_is_replay()) { printf("\n[FAULT] [DL] [%s] [" fmt "]\n", ip_id_str, value); }
#   else
#   define _LZ__TMPLi_mut_data_printval(tname, tklee, fmt) if(klee_is_replay()) { printf("\n[FAULT] [DL] [%s] [~]\n", ip_id_str); }
#   define _LZ__TMPLi_mut_data_printval_cast(tname, fmt, cast) if(klee_is_replay()) { printf("\n[FAULT] [DL] [%s] [~]\n", ip_id_str); }
#   endif
#endif

#define _LZ__TMPLi_mut_data_sym(type, tname, tklee, tfmt) type _LZ__STRCAT(_LZ__mut_dl_, _LZ__STRCAT(tname, _sym)) \
    (type original, int fault_limit, const char* ip_id_str) { \
    if(_LZ__fault_count >= fault_limit) \
        return original;\
    int inject;\
    type value; \
    klee_make_symbolic(&inject, sizeof(inject), ip_id_str); \
    \
    if(inject) { \
        _LZ__fault_count++; \
        \
        klee_make_symbolic(&value, sizeof(value), "value"); \
        _LZ__TMPLi_mut_data_printval(tname, tklee, tfmt); \
        return value; \
    } \
    return original; \
}

#define _LZ__TMPLi_mut_data_sym_pred(type, tname, tklee, tfmt) type _LZ__STRCAT(_LZ__mut_dl_, _LZ__STRCAT(tname, _sym_pred)) \
    (type original, int fault_limit, const char* ip_id_str, type (*P)(type, type)) { \
    if(_LZ__fault_count >= fault_limit) \
        return original;\
    int inject;\
    type value; \
    klee_make_symbolic(&inject, sizeof(inject), ip_id_str); \
    \
    if(inject) { \
        _LZ__fault_count++; \
        \
        klee_make_symbolic(&value, sizeof(value), "value"); \
        if(P != NULL) \
            klee_assume(P(original, value)); \
        _LZ__TMPLi_mut_data_printval(tname, tklee, tfmt); \
        return value; \
    } \
    return original; \
}


#define _LZ__TMPLi_mut_data_sym_fct(type, tname, tklee, tfmt) type _LZ__STRCAT(_LZ__mut_dl_, _LZ__STRCAT(tname, _sym_fct)) \
    (type original, int fault_limit, const char* ip_id_str, type (*F)(type, type)) { \
    if(_LZ__fault_count >= fault_limit) \
        return original;\
    int inject;\
    type value; \
    klee_make_symbolic(&inject, sizeof(inject), ip_id_str); \
    \
    if(inject) { \
        _LZ__fault_count++; \
        \
        klee_make_symbolic(&value, sizeof(value), "value"); \
        if(F != NULL) \
            value = F(original, value); \
        _LZ__TMPLi_mut_data_printval(tname, tklee, tfmt); \
        return value; \
    } \
    return original; \
}

#define _LZ__TMPLi_mut_data_fct(type, tname, tfmt, tcast) type _LZ__STRCAT(_LZ__mut_dl_, _LZ__STRCAT(tname, _fct)) \
    (type original, int fault_limit, const char* ip_id_str, type (*F)(type)) { \
    if(_LZ__fault_count >= fault_limit) \
        return original;\
    int inject;\
    klee_make_symbolic(&inject, sizeof(inject), ip_id_str); \
    \
    if(inject) { \
        _LZ__fault_count++; \
        \
        type value = F(original); \
        _LZ__TMPLi_mut_data_printval_cast(tname, tfmt, tcast); \
        return value; \
    } \
    return original; \
}

#define _LZ__TMPLi_mut_data_fix(type, tname, tfmt, tcast) type _LZ__STRCAT(_LZ__mut_dl_, _LZ__STRCAT(tname, _fix)) \
    (type original, int fault_limit, const char* ip_id_str, type value) { \
    if(_LZ__fault_count >= fault_limit) \
        return original;\
    int inject;\
    klee_make_symbolic(&inject, sizeof(inject), ip_id_str); \
    \
    if(inject) { \
        _LZ__fault_count++; \
        \
        printf("\n[FAULT] [DL] [%s] [" tfmt "]\n", ip_id_str, value); \
        return value; \
    } \
    return original; \
}



#define _LZ__TMPLi_mut_data_all(type, tname, tklee, tfmt, tcast) _LZ__TMPLi_mut_data_sym(type, tname, tklee, tfmt); \
    _LZ__TMPLi_mut_data_sym_pred(type, tname, tklee, tfmt); \
    _LZ__TMPLi_mut_data_sym_fct(type, tname, tklee, tfmt); \
    _LZ__TMPLi_mut_data_fct(type, tname, tfmt, tcast); \
    _LZ__TMPLi_mut_data_fix(type, tname, tfmt, tcast);


_LZ__TMPLi_mut_data_all(_LZ__t_int8_t, i8, i32, "%d", _LZ__t_int32_t);
_LZ__TMPLi_mut_data_all(_LZ__t_int16_t, i16, i32, "%d", _LZ__t_int32_t);
_LZ__TMPLi_mut_data_all(_LZ__t_int32_t, i32, i32, "%d", _LZ__t_int32_t);
_LZ__TMPLi_mut_data_all(_LZ__t_int64_t, i64, i64, "%ld", _LZ__t_int64_t);

/* UNDEFS */
#undef _LZ__TMPLi_mut_data_printval
#undef _LZ__TMPLi_mut_data_printval_cast
#undef _LZ__TMPLi_mut_data_fix
#undef _LZ__TMPLi_mut_data_sym
#undef _LZ__TMPLi_mut_data_sym_pred
#undef _LZ__TMPLi_mut_data_sym_fct
#undef _LZ__TMPLi_mut_data_fct
#undef _LZ__TMPLi_mut_data_all

