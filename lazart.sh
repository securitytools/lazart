#!/bin/bash

echo "Lazart 4.0"
echo "For more information, please consult the documentation: https://gricad-gitlab.univ-grenoble-alpes.fr/securitytools/lazart/-/wikis/home."
[ ! "$(docker ps -a | grep -E '^lazart4\.0')" ] && make box
docker exec -it lazart4.0 /bin/bash
