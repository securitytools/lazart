FROM ubuntu:20.04

LABEL maintainer="Etienne Boespflug - <etienne.boespflug@gmail.com>"
LABEL description="Lazart's base image containing Klee, solvers and compilers."

USER root

# Setup requirements
RUN apt-get update
RUN apt-get install -y graphviz
RUN apt-get install -y vim
RUN apt-get install -y ocaml-native-compilers
RUN apt-get install -y wget
RUN apt-get install -y --no-install-recommends apt-utils
RUN apt-get install -y gdb
RUN apt-get install -y doxygen
RUN apt-get install -y python3 python3-pip
RUN ln -s /usr/bin/python3 /bin/python
RUN pip3 install lit tabulate wllvm
RUN apt-get install -y libglpk-dev libgmp3-dev
RUN pip3 install glpk


# LLVM Environment
RUN apt-get install -y build-essential curl libcap-dev git cmake libncurses5-dev python2-minimal unzip libtcmalloc-minimal4 libgoogle-perftools-dev
#RUN apt-get install -y gcc-multilib g++-multilib
RUN apt-get install -y clang-9 llvm-9 llvm-9-dev llvm-9-tools  
ENV LLVM_CONFIG_BINARY=/usr/bin/llvm-config-9
ENV CLANG_BIN_PATH=/usr/bin/clang-9
ENV CLANGXX_BIN_PATH=/usr/bin/clang++-9
# LLVM aliases
RUN ln -s /usr/bin/llvm-config-9 /bin/llvm-config
RUN ln -s /usr/bin/clang-9 /bin/clang
RUN ln -s /usr/bin/clang++-9 /bin/clang++
RUN ln -s /usr/bin/llvm-dis-9 /bin/llvm-dis
RUN ln -s /usr/bin/llvm-link-9 /bin/llvm-link
RUN ln -s /usr/bin/opt-9 /bin/opt

# Install Rust.
RUN curl https://sh.rustup.rs -sSf | bash -s -- -y
ENV PATH="/root/.cargo/bin:${PATH}"

# Z3s
RUN git clone  --depth 1 --branch z3-4.12.3 https://github.com/Z3Prover/z3.git
RUN cd z3 && CXX=clang++ CC=clang python scripts/mk_make.py
RUN cd z3 && cd build && make && make install

# STP
RUN apt-get -y update
RUN apt-get install -y cmake bison flex libboost-all-dev python perl
RUN git clone https://github.com/stp/minisat && cd minisat && mkdir build && cd build && cmake .. && cmake --build . && cmake --install . && command -v ldconfig && ldconfig
RUN git clone https://github.com/stp/stp --branch smtcomp2020 && cd stp && git submodule init && git submodule update && mkdir build && cd build && cmake .. && cmake --build . && cmake --install .

# µlibc
RUN cd home && git clone --branch klee_uclibc_v1.4 https://github.com/klee/klee-uclibc.git && cd klee-uclibc && ./configure --make-llvm-lib && make -j2 && cd ..
RUN git clone https://github.com/klee/klee-uclibc.git && cd klee-uclibc && ./configure --make-llvm-lib && make -j2 && cd ..
ENV ULIBC_PATH=/home/klee-uclibc

# (Optional) Build LibC++:
# (Optional) Get Google test sources:

RUN apt-get install -y libsqlite3-0 libsqlite3-dev

# Install Klee (Should use local version ?): cd build && cmake -DENABLE_SOLVER_STP=ON
RUN cd home && git clone https://github.com/klee/klee.git && cd klee && git checkout c966cc6 && cd .. && mkdir build && cd build && cmake \
    -DENABLE_SOLVER_STP=ON \
    -ENABLE_SOLVER_Z3=ON  \
    -DENABLE_UNIT_TESTS=OFF \
    -DENABLE_SYSTEM_TESTS=OFF \
    -DENABLE_KLEE_UCLIBC=ON \
    -DKLEE_UCLIBC_PATH="$ULIBC_PATH" \
    -DLLVM_CONFIG_BINARY="$LLVM_CONFIG_BINARY" \
      -DLLVMCC="$CLANG_BIN_PATH" \
      -DLLVMCXX="$CLANGXX_BIN_PATH" ../klee && make
ENV KLEE_BIN_PATH=/home/build/bin/klee
ENV KLEE_INCLUDE_PATH=/home/klee/include
ENV PATH=$PATH:/home/build/bin
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/build/lib/
ENV LIBRARY_PATH=$LIBRARY_PATH:/home/klee/include

# Runs forever.
CMD tail -f /dev/null
